"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Express = require("express");
const logger = require("morgan");
const bodyParser = require("body-parser");
const session = require("express-session");
const cookieParser = require("cookie-parser");
const AlbumRouter = require("./routes/AlbumRouter");
//Routes
const staticRoot = "../pinkpig2/www/";
// Sample repository of registrations (for demo purposes just in memory
// Creates and configures an ExpressJS web server.
function App() {
    // ref to Express instance
    let application = Express();
    let router = Express.Router();
    middleware();
    routes();
    // Configure Express middleware.
    function middleware() {
        application.use(logger('dev'));
        application.use(cookieParser());
        application.use("/", Express.static(staticRoot));
        application.use(bodyParser.json({ limit: "90mb" }));
        application.use(bodyParser.urlencoded({ extended: false, limit: "90mb" }));
        application.use(session({ secret: "yca secret password", cookie: { maxAge: 600000 } }));
    }
    // Configure API endpoints.
    function routes() {
        router.get("/*", (req, res, next) => {
            res.sendfile("index.html", { root: staticRoot });
        });
        //---->
        application.use('/', router);
        application.use("/api/v0.1/album/", AlbumRouter.router);
    }
    return application;
}
exports.default = App();
