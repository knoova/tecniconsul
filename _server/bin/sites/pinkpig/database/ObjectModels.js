"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const imageSchema = new mongoose.Schema({
    url: {
        type: String,
        required: false
    },
    urls: {
        type: Object,
        required: false
    },
    name: {
        type: String,
        required: true
    },
    mime: {
        type: String,
        required: true
    },
    ext: {
        type: String,
        required: true
    },
    modelIn: {
        type: String,
        required: false
    },
    mua: {
        type: String,
        required: false
    },
    portfolioOf: {
        type: String,
        required: false
    },
    createdAt: {
        type: Date,
        required: false
    },
    modifiedAt: {
        type: Date,
        required: false
    }
}).pre('save', (next) => {
    let now = new Date();
    if (!this.createdAt)
        this.createdAt = now;
    else
        this.modifiedAt = now;
    next();
});
const categoriaSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    slug: {
        type: String,
        required: true
    },
    cover: {
        type: String,
        required: false
    },
    createdAt: {
        type: String,
        required: false
    },
    modifiedAt: {
        type: String,
        required: false
    }
}).pre('save', (next) => {
    let now = new Date().toISOString();
    if (!this.createdAt)
        this.createdAt = now;
    else
        this.modifiedAt = now;
    next();
});
exports.modelli = {
    image: mongoose.model("ppp_images", imageSchema),
    categoria: mongoose.model("ppp_categories", categoriaSchema)
};
