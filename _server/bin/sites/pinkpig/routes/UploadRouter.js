"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const AWS = require("aws-sdk");
const crypto = require("crypto");
const mongoose = require("mongoose");
const conf = require("../../conf");
const ObjectModels_1 = require("../database/ObjectModels");
const resize = require("../applications/ResizeImage");
// const baseUrl = "https://pinkpig.s3.amazonaws.com/"
const options = {
    accessKeyId: conf.pinkpig.aws.s3.accessKeyId,
    secretAccessKey: conf.pinkpig.aws.s3.secretAccessKey
};
const params = {
    Bucket: conf.pinkpig.aws.s3.Bucket
};
function actualUpload(args) {
    return __awaiter(this, void 0, void 0, function* () {
        const putParams = yield promiseUpload({
            Body: args.mFile,
            Key: args.hash + "." + (args.ext ? args.ext : getExt(args.name)),
            ContentType: args.mime ? args.mime : mimeFromExt(getExt(args.name))
        });
        const mImage = {
            url: conf.pinkpig.aws.s3.baseUrl + putParams.key,
            name: args.name,
            ext: args.ext ? args.ext : getExt(args.name),
            description: args.description,
            mime: args.mime ? args.mime : mimeFromExt(getExt(args.name))
        };
        return mImage;
    });
}
function setName(args) {
    return args.hash + `_${args.size}.${args.ext}`;
}
function actualMultiUpload(args) {
    return __awaiter(this, void 0, void 0, function* () {
        const putParamsXS = yield promiseUpload({
            Body: args.mFiles.xsmall,
            Key: setName({ size: "xsmall", hash: args.hash, ext: args.ext }),
            ContentType: args.mime ? args.mime : mimeFromExt(getExt(args.name))
        });
        const putParamsS = yield promiseUpload({
            Body: args.mFiles.small,
            Key: setName({ size: "small", hash: args.hash, ext: args.ext }),
            ContentType: args.mime ? args.mime : mimeFromExt(getExt(args.name))
        });
        const putParamsM = yield promiseUpload({
            Body: args.mFiles.medium,
            Key: setName({ size: "medium", hash: args.hash, ext: args.ext }),
            ContentType: args.mime ? args.mime : mimeFromExt(getExt(args.name))
        });
        const putParamsL = yield promiseUpload({
            Body: args.mFiles.large,
            Key: setName({ size: "large", hash: args.hash, ext: args.ext }),
            ContentType: args.mime ? args.mime : mimeFromExt(getExt(args.name))
        });
        const mImage = {
            urls: {
                xsmall: conf.pinkpig.aws.s3.baseUrl + putParamsXS.key,
                small: conf.pinkpig.aws.s3.baseUrl + putParamsS.key,
                medium: conf.pinkpig.aws.s3.baseUrl + putParamsM.key,
                large: conf.pinkpig.aws.s3.baseUrl + putParamsL.key
            },
            name: args.name,
            ext: args.ext ? args.ext : getExt(args.name),
            description: args.description,
            mime: args.mime ? args.mime : mimeFromExt(getExt(args.name))
        };
        return mImage;
    });
}
exports.actualMultiUpload = actualMultiUpload;
function promiseUpload(args) {
    return __awaiter(this, void 0, void 0, function* () {
        const putParams = {
            Body: args.Body,
            Bucket: 'pinkpig',
            Key: args.Key ? args.Key : crypto.randomBytes(24).toString("hex"),
            ContentType: args.ContentType,
            ACL: 'public-read'
        };
        const s3 = new AWS.S3(options);
        const mP = new Promise((resolve, reject) => {
            s3.putObject(putParams, function (err, data) {
                if (err)
                    reject(err);
                else
                    resolve({ data, key: args.Key });
            });
        });
        return yield mP;
    });
}
function getExt(filename) {
    const a = filename.split(".");
    return a[a.length - 1];
}
function mimeFromExt(ext) {
    switch (ext) {
        case "jpg":
            return "image/jpg";
        case "png":
            return "image/png";
        case "jpeg":
            return "image/jpg";
        case "gif":
            return "image/gif";
        case "pdf":
            return "application/pdf";
        default:
            return "";
    }
}
function UploadRouter() {
    const router = express_1.Router();
    init();
    function saveOne(req, res, next) {
        const hash = crypto.randomBytes(24).toString("hex");
        const s3 = new AWS.S3(options);
        s3.getBucketLocation(params, function (err, data) {
            return __awaiter(this, void 0, void 0, function* () {
                if (err)
                    throw new Error("Error: " + err + err.stack);
                const fileToUpload = new Buffer(req.body.base64Image.replace(/^data:\w+\/\w+;base64,/, ""), 'base64');
                let pro = new Promise((resolve, reject) => { });
                if (getExt(req.body.name) == "jpg" || getExt(req.body.name) == "jpeg") {
                    const im = yield resize.resizeMultiform(fileToUpload);
                    pro = actualMultiUpload({ mFiles: im, hash, ext: "jpg", mime: mimeFromExt("jpg"), name: req.body.name, description: req.body.description });
                }
                else {
                    pro = actualUpload({ mFile: fileToUpload, hash, name: req.body.name, description: req.body.description });
                }
                pro.then(mImage => {
                    mongoose.connect(conf.pinkpig.db_url + conf.pinkpig.db_name).then(connection => {
                        ObjectModels_1.modelli.image.create(mImage).then(response => {
                            res.status(200).send(response);
                        }).catch(error => {
                            res.status(500).send(error);
                        });
                    });
                });
            });
        });
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        router.post('/put/', saveOne);
        router.get("/:id");
    }
    return {
        init,
        router
    };
}
exports.UploadRouter = UploadRouter;
const UploadRoutes = UploadRouter();
UploadRoutes.init();
exports.router = UploadRoutes.router;
