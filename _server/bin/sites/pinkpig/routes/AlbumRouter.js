"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const conf = require("../../conf");
const mongoose = require("mongoose");
const ObjectModels_1 = require("../database/ObjectModels");
function AlbumRouter() {
    const router = express_1.Router();
    init();
    function setCategory(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const connection = yield mongoose.connect(conf.pinkpig.db_url + conf.pinkpig.db_name);
                const mAlbum = req.body;
                const a = yield ObjectModels_1.modelli.categoria.findOne({ slug: mAlbum.slug });
                if (!a) {
                    const r = yield ObjectModels_1.modelli.categoria.create(mAlbum);
                    res.status(200).send({ ok: true, message: "Categoria creata" });
                }
                else {
                    a.cover = mAlbum.cover;
                    a.title = mAlbum.title;
                    a.description = mAlbum.description;
                    const saved = yield a.save();
                    res.status(200).send({ ok: true, message: "Categoria aggiornata" });
                }
            }
            catch (e) {
                console.error(e);
                res.status(500).send({ ok: false, message: "Server error" });
            }
        });
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        //----- SLIDERS
        router.post('/categories/set', setCategory);
    }
    return {
        init,
        router
    };
}
// Create the ModelRouter, and export its configured Express.Router
const FaqRoutes = AlbumRouter();
FaqRoutes.init();
exports.router = FaqRoutes.router;
