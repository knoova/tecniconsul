"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const mailer = require("./mail");
function LoginsRouter() {
    const router = express_1.Router();
    init();
    function init() {
        function sendMail(req, res, next) {
            mailer.sendMail({ indirizzo: "francesco.galleano@gmail.com", oggetto: req.body.obj, testo: req.body.body });
            res.status(200).send({ resp: "mail sent" });
        }
        router.post('/sendcustomdata', sendMail);
    }
    return {
        init,
        router
    };
}
// Create the ModelRouter, and export its configured Express.Router
const LoginRoutes = LoginsRouter();
LoginRoutes.init();
exports.router = LoginRoutes.router;
