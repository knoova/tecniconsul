"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const allDb = require("mongodb");
const tecniconsul_Models_1 = require("../database/tecniconsul_Models");
function FAQsRouter() {
    const router = express_1.Router();
    init();
    const repo = tecniconsul_Models_1.modelli.faq;
    const cssRepo = tecniconsul_Models_1.modelli.css;
    function getFAQs(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        let query = {};
        if (req.body.whichFAQ)
            query = { _id: new allDb.ObjectId(req.body.whichFAQ) };
        if (req.body.part)
            query = { part: req.body.part };
        if (req.body.category)
            query = { category: req.body.category };
        repo.find(query).then(response => {
            res.status(200).send(response);
            console.log(response);
        }).catch(err => {
            res.status(500).send(err);
        });
        // })
    }
    function setFAQ(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const mFAQ = req.body;
        repo.create(mFAQ, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        //   })
    }
    function getCss(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        let query = {};
        cssRepo.find(query).then(response => {
            res.status(200).send(response);
            console.log(response);
        });
        //  })
    }
    function changeCss(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const myobj = {
            css: req.body.css
        };
        console.log("Css body", req.body);
        if (req.body._id) {
            const myId = new allDb.ObjectId(req.body._id);
            cssRepo.update(myId, myobj, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            });
        }
        else {
            cssRepo.create(myobj, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            });
        }
        //  })
    }
    function changeFAQ(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const myobj = Object.assign({}, req.body);
        const myId = new allDb.ObjectId(req.body._id);
        repo.update(myId, myobj, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        //  })
    }
    function deleteFAQ(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        repo.deleteById(req.body._id, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        //  })
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        router.post('/setcss', changeCss);
        router.post('/getcss', getCss);
        //----- SLIDERS
        router.post('/faqs', getFAQs);
        router.post('/setfaq', setFAQ);
        router.post('/changefaq', changeFAQ);
        router.post('/deletefaq', deleteFAQ);
    }
    return {
        init,
        router
    };
}
// Create the ModelRouter, and export its configured Express.Router
const FaqRoutes = FAQsRouter();
FaqRoutes.init();
exports.router = FaqRoutes.router;
