"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const Html = require("../HtmlBuilder");
const conf = require("../../conf");
const md5 = require("md5");
const Db = require("mongodb");
const tecniconsul_Models_1 = require("../database/tecniconsul_Models");
function LoginsRouter() {
    const router = express_1.Router();
    init();
    function getPage(page) {
        return Html.Header({ title: conf.tecniconsul.shortName }) +
            Html.Body({ functionName: page }) +
            Html.Footer({ name: conf.tecniconsul.name, address: conf.tecniconsul.address, codicefiscale: conf.tecniconsul.codicefiscale, piva: conf.tecniconsul.piva });
    }
    function init() {
        const repoRegistration = tecniconsul_Models_1.modelli.user;
        function setUtente(req, res, next) {
            // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const registration = {
                email: req.body.email,
                nome: req.body.nome,
                cognome: req.body.cognome,
                privato: req.body.privato,
                azienda: req.body.azienda,
                denominazione: req.body.denominazione,
                cf: req.body.cf,
                piva: req.body.piva,
                provincia: req.body.provincia,
                indirizzo: req.body.indirizzo,
                telefono: req.body.telefono,
                password: req.body.password,
                acconsente: true,
                codcliente: req.body.codcliente
            };
            repoRegistration.findOne({ email: req.body.email }, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    if (response)
                        res.status(500).send({ type: 500, error: "Record già esistente" });
                    else
                        repoRegistration.create(registration, (error, resp) => {
                            if (error) {
                                res.status(500).send(error);
                            }
                            if (!resp)
                                res.status(404).send({ type: 404, error: "User non creato", response });
                            else {
                                returnUser(resp._id).then(user => {
                                    req.session.user = user;
                                    res.cookie("user", JSON.stringify(user), { expires: new Date(Date.now() + 30 * 24 * 60 * 60 * 1000) });
                                    res.status(200).send(user);
                                });
                            }
                        });
                }
            });
            // })
        }
        function getUtente(req, res, next) {
            // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const t = req.body.type;
            let cond = {};
            if (req.body)
                cond = Object.assign({}, req.body);
            repoRegistration.find(cond, (error, resp) => {
                if (error) {
                    res.status(500).send(error);
                }
                if (!resp)
                    res.status(404).send({ type: 404, error: "User non trovato", resp });
            }).then(users => {
                res.status(200).send(users);
            });
            //  })
        }
        function deleteUtente(req, res, next) {
            //mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            repoRegistration.deleteById(req.body._id, (error) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send({ deleted: true });
                    console.log({ deleted: true });
                }
            });
            //})
        }
        function changeUtente(req, res, next) {
            //mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const myobj = req.body;
            console.log(req.body);
            repoRegistration.update(new Db.ObjectId(req.body._id), myobj, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            });
            //})
        }
        function logout(req, res, next) {
            // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            req.session.destroy(err => {
                console.log(err);
                if (err)
                    res.status(500).send({ type: 500, error: "Impossibile eliminare la sessione" });
                else {
                    res.clearCookie("user");
                    res.status(200).send(true);
                }
            });
            // })
        }
        function login(req, res, next) {
            console.log("Tentativo di connessione", req.body);
            // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            if (req.body.user == "creautenteadmin") {
                const mUser = {
                    email: "francesco.galleano@gmail.com",
                    nome: "Francesco",
                    cognome: "Galleano",
                    privato: false,
                    azienda: true,
                    denominazione: "PinkPig Studio",
                    cf: "GLLFNC89C28Z611W",
                    piva: "02871460347",
                    provincia: "Parma",
                    indirizzo: "Via Bolzoni 2",
                    telefono: "+393317891785",
                    password: md5("Tecniconsul2018@"),
                    codcliente: "admin",
                    isAdmin: true
                };
                repoRegistration.create(mUser, (error, response) => {
                    if (error) {
                        res.status(500).send(error);
                    }
                    else {
                        req.session.user = response;
                        res.cookie("user", JSON.stringify(response), { expires: new Date(Date.now() + 30 * 24 * 60 * 60 * 1000) });
                        res.status(200).send(response);
                    }
                });
            }
            else {
                const codcliente = req.body.user;
                repoRegistration.findOne({ codcliente: codcliente }, (error, response) => {
                    if (error) {
                        res.status(500).send(error);
                    }
                    else {
                        console.log(codcliente, response);
                        if (!response)
                            res.status(404).send({ type: 404, error: "Codice cliente non esistente", response });
                        else if (response.password == req.body.password) {
                            returnUser(response._id).then(user => {
                                //mailer.sendMail({ indirizzo: user.email, oggetto: "Accesso al sito", testo: "Login effettuato" })
                                req.session.user = user;
                                res.cookie("user", JSON.stringify(user), { expires: new Date(Date.now() + 30 * 24 * 60 * 60 * 1000) });
                                res.status(200).send(user);
                            });
                        }
                        else
                            res.status(500).send({ type: 500, error: "Password non valida" });
                    }
                });
            }
            //})
        }
        function returnUser(_id) {
            return repoRegistration.findOne({ _id }, (error, resp) => {
                if (error) {
                    return undefined;
                }
                if (!resp)
                    return undefined;
                else {
                }
            }).then((resp) => {
                return {
                    _id: resp._id,
                    email: resp.email,
                    nome: resp.nome,
                    cognome: resp.cognome,
                    privato: resp.privato,
                    azienda: resp.azienda,
                    denominazione: resp.denominazione,
                    cf: resp.cf,
                    piva: resp.piva,
                    acconsente: resp.acconsente,
                    provincia: resp.provincia,
                    indirizzo: resp.indirizzo,
                    telefono: resp.telefono,
                    password: resp.password,
                    codcliente: resp.codcliente,
                    isAdmin: resp.isAdmin
                };
            });
        }
        router.post('/setutente', setUtente);
        router.post('/changeutente', changeUtente);
        router.post('/getutente', getUtente);
        router.post('/deleteutente', deleteUtente);
        router.post('/login', login);
        router.post('/logout', logout);
    }
    return {
        init,
        router
    };
}
// Create the ModelRouter, and export its configured Express.Router
const LoginRoutes = LoginsRouter();
LoginRoutes.init();
exports.router = LoginRoutes.router;
