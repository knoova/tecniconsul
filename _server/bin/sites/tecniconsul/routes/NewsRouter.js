"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const allDb = require("mongodb");
const tecniconsul_Models_1 = require("../database/tecniconsul_Models");
function NewsRouter() {
    const router = express_1.Router();
    init();
    const repo = tecniconsul_Models_1.modelli.news;
    const cssRepo = tecniconsul_Models_1.modelli.css;
    function getNews(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        let query = {};
        if (req.body.whichNews)
            query = { _id: new allDb.ObjectId(req.body.whichNews) };
        repo.find(query).then(response => {
            res.status(200).send(response);
            console.log(response);
        });
        //  })
    }
    function setNews(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const mNews = req.body;
        repo.create(mNews, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        // })
    }
    function getCss(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        let query = {};
        cssRepo.find(query).then(response => {
            res.status(200).send(response);
            console.log(response);
        });
        //  })
    }
    function changeCss(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const myobj = {
            css: req.body.css
        };
        console.log("Css body", req.body);
        if (req.body._id) {
            const myId = new allDb.ObjectId(req.body._id);
            cssRepo.update(myId, myobj, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            });
        }
        else {
            cssRepo.create(myobj, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            });
        }
        // })
    }
    function changeNews(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const myobj = {
            title: req.body.title,
            subtitle: req.body.subtitle,
            body: req.body.body,
            date: req.body.date,
            banner: req.body.banner
        };
        const myId = new allDb.ObjectId(req.body._id);
        repo.update(myId, myobj, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        // })
    }
    function deleteNews(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        console.log(req.body);
        repo.deleteById(req.body.id, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        //  })
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        //----- SLIDERS
        router.post('/news', getNews);
        router.post('/setnews', setNews);
        router.post('/changenews', changeNews);
        router.post('/deletenews', deleteNews);
    }
    return {
        init,
        router
    };
}
// Create the ModelRouter, and export its configured Express.Router
const FaqRoutes = NewsRouter();
FaqRoutes.init();
exports.router = FaqRoutes.router;
