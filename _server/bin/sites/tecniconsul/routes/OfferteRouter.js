"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const conf = require("../../conf");
const mongoose = require("mongoose");
const allDb = require("mongodb");
const tecniconsul_Models_1 = require("../database/tecniconsul_Models");
function NewsRouter() {
    const router = express_1.Router();
    init();
    const repo = tecniconsul_Models_1.modelli.news;
    const offerteRepo = tecniconsul_Models_1.modelli.offerte;
    function getNews(req, res, next) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            let query = {};
            if (req.body.whichNews)
                query = { _id: new allDb.ObjectId(req.body.whichNews) };
            repo.find(query).then(response => {
                res.status(200).send(response);
                console.log(response);
            });
        });
    }
    function setNews(req, res, next) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const mNews = req.body;
            repo.create(mNews, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            });
        });
    }
    function getOfferta(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const connectio = yield mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name);
            const myobj = req.body;
            console.log(myobj);
            const offerta = yield offerteRepo.findOne({ casaAziendaPlacet: myobj.casaAziendaPlacet, luceGas: myobj.luceGas });
            res.status(200).send(offerta);
        });
    }
    function changeOfferte(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const connectio = yield mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name);
            const myobj = req.body;
            console.log(myobj);
            const offerta = yield offerteRepo.findOne({ casaAziendaPlacet: myobj.casaAziendaPlacet, luceGas: myobj.luceGas });
            console.log(offerta);
            if (offerta) {
                offerta.o1 = myobj.o1;
                offerta.o2 = myobj.o2;
                offerta.o3 = myobj.o3;
                offerta.o4 = myobj.o4;
                offerta.save();
                res.status(200).send({ ok: true, message: "updated" });
            }
            else {
                offerteRepo.create(myobj, (error, response) => {
                    if (error) {
                        res.status(500).send(error);
                    }
                    else {
                        res.status(200).send({ ok: true, message: "created" });
                        console.log(response);
                    }
                });
            }
        });
    }
    function changeNews(req, res, next) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const myobj = {
                title: req.body.title,
                subtitle: req.body.subtitle,
                body: req.body.body,
                date: req.body.date,
                banner: req.body.banner
            };
            const myId = new allDb.ObjectId(req.body._id);
            repo.update(myId, myobj, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            });
        });
    }
    function deleteNews(req, res, next) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            console.log(req.body);
            repo.deleteById(req.body.id, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            });
        });
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        //----- SLIDERS
        router.post('/news', getNews);
        router.post('/setnews', setNews);
        router.post('/changenews', changeNews);
        router.post('/deletenews', deleteNews);
        router.post("/setofferta", changeOfferte);
        router.post("/getofferta", getOfferta);
    }
    return {
        init,
        router
    };
}
// Create the ModelRouter, and export its configured Express.Router
const FaqRoutes = NewsRouter();
FaqRoutes.init();
exports.router = FaqRoutes.router;
