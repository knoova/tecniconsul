"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const Db = require("mongodb");
const tecniconsul_Models_1 = require("../database/tecniconsul_Models");
function RegioniRouter() {
    const router = express_1.Router();
    init();
    const repoU = tecniconsul_Models_1.modelli.ufficio;
    const repoP = tecniconsul_Models_1.modelli.provincia;
    const repoR = tecniconsul_Models_1.modelli.regione;
    const repoComuni = tecniconsul_Models_1.modelli.comune;
    const repoCabine = tecniconsul_Models_1.modelli.cabina;
    function getRegioni(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        let query = {};
        repoR.find(query).then(response => {
            res.status(200).send(response);
            console.log(response);
        });
        // })
    }
    function setRegione(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const mRegione = Object.assign({}, req.body);
        repoR.create(mRegione, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        //  })
    }
    function changeRegione(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const mRegione = Object.assign({}, req.body);
        const myId = new Db.ObjectId(req.body._id);
        repoR.update(myId, mRegione, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        // })
    }
    function deleteRegione(req, res, next) {
        // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        repoR.deleteById(req.body._id, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        //  })
    }
    function deleteProvincia(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        repoP.deleteById(req.body._id, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        //  })
    }
    function setProvincia(req, res, next) {
        // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const mProvincia = Object.assign({}, req.body);
        repoP.create(mProvincia, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        // })
    }
    function setUfficio(req, res, next) {
        // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const mUfficio = Object.assign({}, req.body);
        repoU.create(mUfficio, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        //  })
    }
    function getUfficio(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        let query = {};
        repoU.find(query).then(response => {
            res.status(200).send(response);
            console.log(response);
        });
        //  })
    }
    function changeUfficio(req, res, next) {
        // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const mUfficio = Object.assign({}, req.body);
        const myId = new Db.ObjectId(req.body._id);
        repoU.update(myId, mUfficio, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        // })
    }
    function deleteUfficio(req, res, next) {
        // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        repoU.deleteById(req.body._id, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        //  })
    }
    function deleteComune(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        repoComuni.deleteById(req.body._id, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        //  })
    }
    function deleteCabina(req, res, next) {
        // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        repoCabine.deleteById(req.body._id, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        // })
    }
    function getProvinceOfRegione(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const repoP = tecniconsul_Models_1.modelli.provincia;
        const regione = req.params.idRegione;
        let obj = {};
        if (regione)
            obj = { regione };
        repoP.find(obj).then(response => {
            res.status(200).send(response);
            console.log(response);
        });
        //  })
    }
    function getProvinciaById(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const repoP = tecniconsul_Models_1.modelli.provincia;
        const _id = req.params.id;
        let obj = { _id };
        repoP.findOne(obj).then(response => {
            res.status(200).send(response);
            console.log(response);
        });
        // })
    }
    function getProvince(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const repoP = tecniconsul_Models_1.modelli.provincia;
        repoP.find({}).then(response => {
            res.status(200).send(response);
            console.log(response);
        });
        //  })
    }
    function setComune(req, res, next) {
        //   mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const mComune = Object.assign({}, req.body);
        repoComuni.create(mComune, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        // })
    }
    function getComuneById(req, res, next) {
        // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const _id = req.params.id;
        let obj = { _id };
        repoComuni.findOne(obj).then(response => {
            res.status(200).send(response);
            console.log(response);
        });
        //  })
    }
    function getComuni(req, res, next) {
        // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        console.log(req.body);
        repoComuni.find(Object.assign({}, req.body)).then(response => {
            res.status(200).send(response);
            console.log(response);
        });
        //  })
    }
    function setCabina(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const mCabina = Object.assign({}, req.body);
        repoCabine.create(mCabina, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        //  })
    }
    function getCabinaById(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const _id = req.params.id;
        let obj = { _id };
        repoCabine.findOne(obj).then(response => {
            res.status(200).send(response);
            console.log(response);
        });
        //  })
    }
    function getCabine(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        console.log(req.body);
        repoCabine.find(Object.assign({}, req.body)).then(response => {
            res.status(200).send(response);
            console.log(response);
        });
        //  })
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        router.post('/regioni', getRegioni);
        router.post('/setregione', setRegione);
        router.post('/changeregione', changeRegione);
        router.post('/deleteregione', deleteRegione);
        router.post('/province/:idRegione', getProvinceOfRegione);
        router.post('/provincia/:id', getProvinciaById);
        router.post('/province', getProvince);
        router.post('/setprovincia', setProvincia);
        router.post('/deleteprovincia', deleteProvincia);
        router.post('/setufficio', setUfficio);
        router.post('/getufficio', getUfficio);
        router.post('/changeufficio', changeUfficio);
        router.post('/deleteufficio', deleteUfficio);
        router.post('/deletecomune', deleteComune);
        router.post('/deletecabina', deleteCabina);
        router.post("/setcomune", setComune);
        router.post("/comune/:id", getComuneById);
        router.post("/comuni", getComuni);
        router.post("/setcabina", setCabina);
        router.post("/cabina/:id", getCabinaById);
        router.post("/cabine", getCabine);
    }
    return {
        init,
        router
    };
}
// Create the ModelRouter, and export its configured Express.Router
const RegioniRoutes = RegioniRouter();
RegioniRoutes.init();
exports.router = RegioniRoutes.router;
