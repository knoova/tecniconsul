"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const allDb = require("mongodb");
const tecniconsul_Models_1 = require("../database/tecniconsul_Models");
function UtilsRouter() {
    const router = express_1.Router();
    init();
    const repo = tecniconsul_Models_1.modelli.util;
    function getUtils(req, res, next) {
        // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        let query = {};
        if (req.body.whichUtils)
            query = { _id: new allDb.ObjectId(req.body.whichUtils) };
        repo.find(query).then(response => {
            res.status(200).send(response);
            console.log(response);
        });
        //  })
    }
    function setUtils(req, res, next) {
        // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        console.log("here i am");
        const mUtils = req.body;
        console.log(mUtils);
        repo.create(mUtils, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        //  })
    }
    function changeUtils(req, res, next) {
        // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        const myobj = {
            title: req.body.title,
            slug: req.body.slug,
            body: req.body.body
        };
        const myId = new allDb.ObjectId(req.body._id);
        repo.update(myId, myobj, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        //  })
    }
    function deleteUtils(req, res, next) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        repo.deleteById(req.body._id, (error, response) => {
            if (error) {
                res.status(500).send(error);
            }
            else {
                res.status(200).send(response);
                console.log(response);
            }
        });
        //  })
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        //----- SLIDERS
        router.post('/utils', getUtils);
        router.post('/setutils', setUtils);
        router.post('/changeutils', changeUtils);
        router.post('/deleteutils', deleteUtils);
    }
    return {
        init,
        router
    };
}
// Create the ModelRouter, and export its configured Express.Router
const FaqRoutes = UtilsRouter();
FaqRoutes.init();
exports.router = FaqRoutes.router;
