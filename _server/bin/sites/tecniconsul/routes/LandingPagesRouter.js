"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const conf = require("../../conf");
const mongoose = require("mongoose");
const allDb = require("mongodb");
const tecniconsul_Models_1 = require("../database/tecniconsul_Models");
function LandingRouter() {
    const router = express_1.Router();
    init();
    function getLanding(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = yield mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name);
            const query = { _id: new allDb.ObjectId(req.params.slug) };
            const response = yield tecniconsul_Models_1.modelli.landingPage.findOne(query);
            res.status(200).send(response);
            console.log(response);
        });
    }
    function getAllLanding(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = yield mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name);
            const response = yield tecniconsul_Models_1.modelli.landingPage.find();
            res.status(200).send(response);
            console.log(response);
        });
    }
    function setLanding(req, res, next) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            console.log("here i am");
            const mLanding = req.body;
            console.log(mLanding);
            tecniconsul_Models_1.modelli.landingPage.create(mLanding, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            });
        });
    }
    function changeLanding(req, res, next) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const myobj = {
                slug: req.body.slug,
                body: req.body.body
            };
            const myId = new allDb.ObjectId(req.body._id);
            tecniconsul_Models_1.modelli.landingPage.update(myId, myobj, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            });
        });
    }
    function deleteLanding(req, res, next) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            tecniconsul_Models_1.modelli.landingPage.deleteById(req.body._id, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            });
        });
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        //----- SLIDERS
        router.get('/landing/:slug', getLanding);
        router.post('/landing', getAllLanding);
        router.post('/setlanding', setLanding);
        router.post('/changelanding', changeLanding);
        router.post('/deletelanding', deleteLanding);
    }
    return {
        init,
        router
    };
}
// Create the ModelRouter, and export its configured Express.Router
const FaqRoutes = LandingRouter();
FaqRoutes.init();
exports.router = FaqRoutes.router;
