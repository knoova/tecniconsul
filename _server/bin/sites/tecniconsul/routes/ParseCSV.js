"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const csvparser = require("csv-parse");
const allDb = require("mongodb");
const tecniconsul_Models_1 = require("../database/tecniconsul_Models");
function parseCSVCabine(args) {
    const output = [];
    csvparser(args.content, {
        trim: true,
        skip_empty_lines: true,
        delimiter: ";"
    })
        // Use the readable stream api
        .on('readable', function () {
        let record;
        while (record = this.read()) {
            output.push(record);
        }
    })
        // When we are done, test that the parsed output matched what expected
        .on('end', function () {
        console.log("csv output", output);
        // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        console.log();
        const cabinaDB = tecniconsul_Models_1.modelli.cabina;
        let obj = {};
        if (args.type == "tabella1")
            obj = { tabella1: output };
        if (args.type == "tabella2")
            obj = { tabella2: output };
        if (args.type == "tabella3")
            obj = { tabella3: output };
        if (args.type == "tabella4")
            obj = { tabella4: output };
        cabinaDB.update(new allDb.ObjectId(args.cabinaID), obj, (error, response) => {
            if (error) {
                console.error("problema", error);
            }
            else {
                console.log(response);
            }
        });
    });
    //   })
}
exports.parseCSVCabine = parseCSVCabine;
function parseCSVUtenti(args) {
    const output = [];
    csvparser(args.content, {
        trim: true,
        skip_empty_lines: true,
        delimiter: ";"
    })
        // Use the readable stream api
        .on('readable', function () {
        let record;
        while (record = this.read()) {
            output.push(record);
        }
    })
        // When we are done, test that the parsed output matched what expected
        .on('end', function () {
        console.log("csv output", output);
        //mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        console.log();
        const utenteDB = tecniconsul_Models_1.modelli.user;
        let obj = output;
        for (let user of obj)
            utenteDB.create(user, (error, response) => {
                if (error) {
                    console.error("problema", error);
                }
                else {
                    console.log(response);
                }
            });
    });
    // })
}
exports.parseCSVUtenti = parseCSVUtenti;
