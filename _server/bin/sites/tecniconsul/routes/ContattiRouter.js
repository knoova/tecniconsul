"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const Db = require("mongodb");
const mongoose = require("mongoose");
const conf = require("../../conf");
const tecniconsul_Models_1 = require("../database/tecniconsul_Models");
function ContattoRouter() {
    const router = express_1.Router();
    init();
    const repo = tecniconsul_Models_1.modelli.contatto;
    function getContatti(req, res, next) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            let query = {};
            repo.find(query).then(response => {
                res.status(200).send(response);
                console.log(response);
            });
        });
    }
    function setContatto(req, res, next) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const mContatto = Object.assign({}, req.body);
            repo.create(mContatto, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            });
        });
    }
    function changeContatto(req, res, next) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const mRegione = Object.assign({}, req.body);
            const myId = new Db.ObjectId(req.body._id);
            repo.update(myId, mRegione, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            });
        });
    }
    function deleteContatto(req, res, next) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            repo.deleteById(req.body._id, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            });
        });
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        router.post('/setcontatto', setContatto);
        router.post('/getcontatto', getContatti);
        router.post('/changecontatto', changeContatto);
        router.post('/deletecontatto', deleteContatto);
    }
    return {
        init,
        router
    };
}
// Create the ModelRouter, and export its configured Express.Router
const RegioniRoutes = ContattoRouter();
RegioniRoutes.init();
exports.router = RegioniRoutes.router;
