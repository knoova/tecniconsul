"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const database = require("./database");
const ProvinciaSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    regione: {
        type: String,
        required: true
    }
});
const UtenteimportatoSchema = new mongoose.Schema({});
const ComuneSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    regione: {
        type: String,
        required: true
    },
    provincia: {
        type: String,
        required: true
    }
});
const CabinaSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    regione: {
        type: String,
        required: true
    },
    provincia: {
        type: String,
        required: true
    },
    comune: {
        type: String,
        required: true
    },
    tabella1: {
        type: Object,
        required: false
    },
    tabella2: {
        type: Object,
        required: false
    },
    tabella3: {
        type: Object,
        required: false
    },
    tabella4: {
        type: Object,
        required: false
    }
});
const ContattoSchema = new mongoose.Schema({
    azienda: {
        type: Boolean,
        required: false
    },
    privato: {
        type: String,
        required: false
    },
    nome: {
        type: String,
        required: true
    },
    cognome: {
        type: String,
        required: false
    },
    age: {
        type: String,
        required: false
    },
    professione: {
        type: String,
        required: false
    },
    orario: {
        type: String,
        required: false
    },
    comune: {
        type: String,
        required: false
    },
    telefono: {
        type: String,
        required: false
    },
    commenti: {
        type: String,
        required: false
    },
    acconsento: {
        type: Boolean,
        required: false
    },
    tipo: {
        type: String,
        required: false
    },
    documento: {
        type: String,
        required: false
    }
});
const FAQSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    subtitle: {
        type: String,
        required: true
    },
    logo: {
        type: String,
        required: false
    },
    part: {
        type: String,
        required: false
    },
    category: {
        type: String,
        required: false
    },
    createdAt: {
        type: Date,
        required: false
    },
    modifiedAt: {
        type: Date,
        required: false
    }
});
const imageSchema = new mongoose.Schema({
    url: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        required: false
    },
    modifiedAt: {
        type: Date,
        required: false
    }
});
const NewsSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    subtitle: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    date: {
        type: String,
        required: true
    },
    banner: {
        type: String,
        required: false
    },
    createdAt: {
        type: Date,
        required: false
    },
    modifiedAt: {
        type: Date,
        required: false
    }
});
const RegioneSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    }
});
const UfficioSchema = new mongoose.Schema({
    city: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: false
    },
    phone: {
        type: String,
        required: false
    },
    hours: {
        type: String,
        required: false
    }
});
const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    privato: {
        type: Boolean,
        required: false
    },
    azienda: {
        type: Boolean,
        required: false
    },
    nome: {
        type: String,
        required: false
    },
    cognome: {
        type: String,
        required: false
    },
    denominazione: {
        type: String,
        required: false
    },
    cf: {
        type: String,
        required: true
    },
    piva: {
        type: String,
        required: false
    },
    provincia: {
        type: String,
        required: true
    },
    indirizzo: {
        type: String,
        required: true
    },
    telefono: {
        type: String,
        required: false
    },
    password: {
        type: String,
        required: true
    },
    acconsente: {
        type: Boolean,
        required: false
    },
    codcliente: {
        type: String,
        required: true
    },
    isAdmin: {
        type: Boolean,
        required: false
    }
});
exports.UtilsSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    slug: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        required: false
    },
    modifiedAt: {
        type: Date,
        required: false
    }
});
exports.LandingPagesSchema = new mongoose.Schema({
    slug: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        required: false
    },
    modifiedAt: {
        type: Date,
        required: false
    }
});
exports.CssSchema = new mongoose.Schema({
    css: {
        type: String,
        required: false
    }
});
const OffertaSchema = new mongoose.Schema({
    casaAziendaPlacet: { required: true, type: String },
    luceGas: { required: true, type: String },
    o1: { required: true, type: Object },
    o2: { required: true, type: Object },
    o3: { required: true, type: Object },
    o4: { required: true, type: Object }
});
exports.modelli = {
    provincia: database.RepositoryBase(mongoose.model("Provincia", ProvinciaSchema)),
    comune: database.RepositoryBase(mongoose.model("Comune", ComuneSchema)),
    cabina: database.RepositoryBase(mongoose.model("Cabina", CabinaSchema)),
    contatto: database.RepositoryBase(mongoose.model("Contatto", ContattoSchema)),
    faq: database.RepositoryBase(mongoose.model("FAQ", FAQSchema)),
    image: database.RepositoryBase(mongoose.model("image", imageSchema)),
    news: database.RepositoryBase(mongoose.model("News", NewsSchema)),
    ufficio: database.RepositoryBase(mongoose.model("Ufficio", UfficioSchema)),
    regione: database.RepositoryBase(mongoose.model("Regione", RegioneSchema)),
    user: database.RepositoryBase(mongoose.model("Users", UserSchema)),
    util: database.RepositoryBase(mongoose.model("Utils", exports.UtilsSchema)),
    landingPage: database.RepositoryBase(mongoose.model("LandingPages", exports.LandingPagesSchema)),
    css: database.RepositoryBase(mongoose.model("Css", exports.CssSchema)),
    offerte: database.RepositoryBase(mongoose.model("Offerte", OffertaSchema))
};
