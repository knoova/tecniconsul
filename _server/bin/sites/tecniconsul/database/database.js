"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
function RepositoryBase(schemaModel) {
    function create(item, callback) {
        schemaModel.create(item, callback);
    }
    function retrieve(callback) {
        schemaModel.find({}, callback);
    }
    function update(_id, item, callback) {
        schemaModel.update({ _id: _id }, item, callback);
    }
    function deleteById(_id, callback) {
        schemaModel.remove({ _id: this.toObjectId(_id) }, (err) => callback(err, null));
    }
    function findById(_id, callback) {
        schemaModel.findById(_id, callback);
    }
    function findOne(cond, callback) {
        return schemaModel.findOne(cond, callback);
    }
    function find(cond, fields, options, callback) {
        return schemaModel.find(cond, options, callback);
    }
    function toObjectId(_id) {
        return mongoose.Types.ObjectId.createFromHexString(_id);
    }
    return { create, retrieve, update, deleteById, findById, findOne, find, toObjectId };
}
exports.RepositoryBase = RepositoryBase;
