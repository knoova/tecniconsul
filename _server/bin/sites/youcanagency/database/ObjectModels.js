"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    friendlyUrl: {
        type: String,
        required: false
    },
    nome: {
        type: String,
        required: true
    },
    cognome: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    type: {
        type: Number,
        required: true
    },
    sesso: {
        type: Number,
        required: true
    },
    provincia: {
        type: String,
        required: false
    },
    indirizzo: {
        type: String,
        required: false
    },
    compleanno: {
        type: String,
        required: false
    },
    bio: {
        biografia: {
            type: String,
            required: false
        },
        propic: {
            type: String,
            required: false
        },
        profilepic: {
            type: Object,
            required: false
        }
    },
    telefono: {
        type: String,
        required: false
    },
    misure: {
        altezza: {
            type: Number,
            required: false
        },
        seno: {
            type: Number,
            required: false
        },
        vita: {
            type: Number,
            required: false
        },
        fianchi: {
            type: Number,
            required: false
        },
        scarpe: {
            type: Number,
            required: false
        }
    },
    isAdmin: {
        type: Boolean,
        required: false
    },
    isCouncil: {
        type: Boolean,
        required: false
    },
    privacyAndTerms: {
        type: Boolean,
        required: true
    }
}).pre('save', (next) => {
    let now = new Date();
    if (!this.createdAt)
        this.createdAt = now;
    else
        this.modifiedAt = now;
    next();
});
const imageSchema = new mongoose.Schema({
    url: {
        type: String,
        required: false
    },
    urls: {
        type: Object,
        required: false
    },
    name: {
        type: String,
        required: true
    },
    mime: {
        type: String,
        required: true
    },
    ext: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: false
    },
    modelIn: {
        type: String,
        required: false
    },
    mua: {
        type: String,
        required: false
    },
    portfolioOf: {
        type: String,
        required: false
    },
    createdAt: {
        type: Date,
        required: false
    },
    modifiedAt: {
        type: Date,
        required: false
    }
}).pre('save', (next) => {
    let now = new Date();
    if (!this.createdAt)
        this.createdAt = now;
    else
        this.modifiedAt = now;
    next();
});
const albumSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    slug: {
        type: String,
        required: true
    },
    cover: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        required: false
    },
    modifiedAt: {
        type: Date,
        required: false
    }
}).pre('save', (next) => {
    let now = new Date();
    if (!this.createdAt)
        this.createdAt = now;
    else
        this.modifiedAt = now;
    next();
});
const lavoroSchema = new mongoose.Schema({
    titolo: {
        type: String,
        required: true
    },
    descrizione: {
        type: String,
        required: true
    },
    paga: {
        type: Number,
        required: true
    },
    data: {
        type: String,
        required: true
    },
    mail: {
        type: String,
        required: true
    },
    oggetto: {
        type: String,
        required: false
    }
}).pre('save', (next) => {
    let now = new Date();
    if (!this.createdAt)
        this.createdAt = now;
    else
        this.modifiedAt = now;
    next();
});
const messaggioSchema = new mongoose.Schema({
    data: {
        type: String,
        required: true
    },
    messaggio: {
        type: String,
        required: true
    },
    mittenteID: {
        type: String,
        required: true
    },
    riceventeID: {
        type: String,
        required: true
    },
    conversazioneID: {
        type: String,
        required: true
    }
}).pre('save', (next) => {
    let now = new Date();
    if (!this.createdAt)
        this.createdAt = now;
    else
        this.modifiedAt = now;
    next();
});
const conversazioneSchema = new mongoose.Schema({
    ultimoMessaggio: {
        type: String,
        required: true
    },
    dataUltimoMessaggio: {
        type: String,
        required: true
    },
    userID: {
        type: String,
        required: true
    },
    interlocutoreID: {
        type: String,
        required: true
    }
}).pre('save', (next) => {
    let now = new Date();
    if (!this.createdAt)
        this.createdAt = now;
    else
        this.modifiedAt = now;
    next();
});
exports.modelli = {
    user: mongoose.model("users", UserSchema),
    image: mongoose.model("YCA_Image", imageSchema),
    album: mongoose.model("YCA_Album", albumSchema),
    lavoro: mongoose.model("YCA_Lavoro", lavoroSchema),
    messaggio: mongoose.model("YCA_Messaggi", messaggioSchema),
    conversazioni: mongoose.model("YCA_Conversazioni", conversazioneSchema)
};
