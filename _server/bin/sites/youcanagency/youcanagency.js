"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Express = require("express");
const logger = require("morgan");
const bodyParser = require("body-parser");
const Html = require("./applications/HtmlBuilder");
const conf = require("../conf");
const session = require("express-session");
//import * as cookieParser from 'cookie-parser';
const cookieParser = require("cookie-parser");
const mongoose = require("mongoose");
const compression = require("compression");
const modelli = require("./database/ObjectModels");
const user_1 = require("./applications/user");
//Routes
const UploadRouter = require("./routes/UploadRouter");
const LoginRouter = require("./routes/LoginRouter");
const MessaggioRouter = require("./routes/MessaggiRouter");
const PortfolioRouter = require("./routes/PortfolioRouter");
const AlbumRouter = require("./routes/AlbumRouter");
function getPage(page) {
    return Html.Header({ title: conf.youcanagency.shortName, script: ["dom-to-image.js"] }) +
        Html.Body({ functionName: page }) +
        Html.Footer({ name: conf.youcanagency.name, address: conf.youcanagency.address, codicefiscale: conf.youcanagency.codicefiscale, piva: conf.youcanagency.piva });
}
function getPageArgs(page, args, meta) {
    return Html.Header({
        title: conf.youcanagency.shortName,
        meta
    }) +
        Html.Body({ functionName: page, args }) +
        Html.Footer({ name: conf.youcanagency.name, address: conf.youcanagency.address, codicefiscale: conf.youcanagency.codicefiscale, piva: conf.youcanagency.piva });
}
function shouldCompress(req, res) {
    if (req.headers['x-no-compression']) {
        // don't compress responses with this request header
        return false;
    }
    // fallback to standard filter function
    return compression.filter(req, res);
}
// Creates and configures an ExpressJS web server.
function App() {
    // ref to Express instance
    let application = Express();
    let router = Express.Router();
    middleware();
    routes();
    // Configure Express middleware.
    function middleware() {
        application.use(logger('dev'));
        application.use(cookieParser());
        application.use(compression({ filter: shouldCompress }));
        application.use("/", Express.static('../youcanagency/coding/www/'));
        application.use(bodyParser.json({ limit: "90mb" }));
        application.use(bodyParser.urlencoded({ extended: false, limit: "90mb" }));
        application.use(session({ secret: "yca secret password", cookie: { maxAge: 600000 } }));
    }
    // Configure API endpoints.
    function routes() {
        router.get("/", returnPage({
            pageFunc: "home",
            getDataPromise: () => {
                return mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
                    return modelli.modelli.user.find({
                        $and: [
                            { bio: { $exists: true } },
                            { misure: { $exists: true } },
                            { type: { $exists: true } },
                            { type: { $lt: 3 } }
                        ]
                    }, null, {
                        sort: { nome: 1 }
                    }).then(resp1 => {
                        if (!resp1 || resp1.length == 0)
                            throw { type: 404, message: "Shit" };
                        const resp = resp1.map(user => {
                            return user_1.returnUser(user, false);
                        });
                        const num = Math.floor(Math.random() * (resp.length - 1));
                        return {
                            type: "resp",
                            data: resp,
                            meta: [
                                { property: "og:url", content: "http://www.youcanagency.it/user/" + resp[num]._id },
                                { property: "og:type", content: "article" },
                                { property: "og:locale", content: "it_IT" },
                                { property: "og:title", content: resp[num].nome },
                                { property: "og:description", content: (resp[num].bio && resp[num].bio.biografia) ? resp[num].bio.biografia : "Modelle per shooting, modelsharing, advertising. Contattaci per maggiori info!" },
                                { property: "og:image", content: (resp[num].bio && resp[num].bio.propic) ? resp[num].bio.propic : "http://www.youcanagency.it/img/noimageavailable.png" }
                            ]
                        };
                    });
                });
            }
        }));
        router.get('/account/messaggi', returnPage({
            pageFunc: "account_messaggi",
            needLogged: true,
            getDataPromise: (args) => {
                return mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
                    return modelli.modelli.conversazioni.find({ userID: args.user._id }, null, { sort: { dataUltimoMessaggio: -1 } }).then(conversazioni => {
                        return { type: "resp", data: conversazioni };
                    });
                });
            }
        }));
        router.get('/account/composit', returnPage({ pageFunc: "user_composit" }));
        router.get('/login', returnPage({ pageFunc: "login" }));
        router.get('/error', returnPage({
            pageFunc: "error", getDataPromise: (args) => {
                let ret = {};
                if (args.query.code == 403)
                    ret = { errorCode: args.query.code, message: "Accesso vietato" };
                else if (args.query.code == 404)
                    ret = { errorCode: args.query.code, message: "Pagina non trovata" };
                else if (args.query.code == 500)
                    ret = { errorCode: args.query.code, message: "Server error" };
                else
                    ret = { errorCode: 500, message: "Problema sconosciuto" };
                return Promise.resolve({ type: "resp", data: ret });
            }
        }));
        router.get('/register', returnPage({ pageFunc: "register" }));
        router.get('/thankyou/register', returnPage({ pageFunc: "thankyou_register" }));
        router.get('/privacy', returnPage({ pageFunc: "privacy" }));
        router.get('/user/composit', returnPage({ pageFunc: "user_composit" }));
        router.get("/user/modeloftheday", returnPage({
            pageFunc: "user", getDataPromise: () => {
                return mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
                    return modelli.modelli.user.find({
                        $and: [
                            { bio: { $exists: true } },
                            { misure: { $exists: true } },
                            { type: { $exists: true } },
                            { type: { $lt: 3 } }
                        ]
                    }).then(resp => {
                        if (!resp || resp.length == 0)
                            throw { type: 404, message: "Impossibile trovare l'utente" };
                        else {
                            const num = Math.floor(Math.random() * resp.length);
                            return {
                                type: "resp",
                                data: resp[num], meta: [
                                    { property: "og:url", content: "http://www.youcanagency.it/user/" + resp[num]._id },
                                    { property: "og:type", content: "article" },
                                    { property: "og:locale", content: "it_IT" },
                                    { property: "og:title", content: resp[num].nome },
                                    { property: "og:description", content: (resp[num].bio && resp[num].bio.biografia) ? resp[num].bio.biografia : "Modelle per shooting, modelsharing, advertising. Contattaci per maggiori info!" },
                                    { property: "og:image", content: (resp[num].bio && resp[num].bio.propic) ? resp[num].bio.propic : "http://www.youcanagency.it/img/noimageavailable.png" }
                                ]
                            };
                        }
                    });
                });
            }
        }));
        router.get("/user/:id", returnPage({
            pageFunc: "user", getDataPromise: (args) => {
                return mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
                    const id = args.params.id;
                    let _id = "";
                    try {
                        if (id.length < 24)
                            throw new Error("Not a ObjectID");
                        _id = new mongoose.mongo.ObjectId(id).toHexString();
                        return modelli.modelli.user.findOne({ _id }).then(foundUser => {
                            if (!foundUser) {
                                console.error("Impossibile trovare l'utente");
                                return { type: "error", errorCode: 404, message: "Impossibile trovare l'utente" };
                            }
                            else if (foundUser.friendlyUrl) {
                                console.warn("User redirected to friendlyUrl");
                                return { type: "redirect", redirectTo: "/user/" + foundUser.friendlyUrl };
                            }
                            else {
                                console.log("User found");
                                const u = user_1.returnUser(foundUser, args.user && (args.user.isAdmin || foundUser._id == args.user._id));
                                return {
                                    type: "resp",
                                    data: u, meta: [
                                        { property: "og:url", content: "http://www.youcanagency.it/user/" + u._id },
                                        { property: "og:type", content: "article" },
                                        { property: "og:locale", content: "it_IT" },
                                        { property: "og:title", content: u.nome },
                                        { property: "og:description", content: (u.bio && u.bio.biografia) ? u.bio.biografia : "Modelle per shooting, modelsharing, advertising. Contattaci per maggiori info!" },
                                        { property: "og:image", content: (u.bio && foundUser.bio.propic) ? u.bio.propic : "http://www.youcanagency.it/img/noimageavailable.png" }
                                    ]
                                };
                            }
                        });
                    }
                    catch (e) {
                        return modelli.modelli.user.findOne({ friendlyUrl: id }).then(foundUser => {
                            if (!foundUser) {
                                console.error("Impossibile trovare l'utente (FU)");
                                return Promise.resolve({ type: "error", errorCode: 404, message: "Impossibile trovare l'utente" });
                            }
                            else {
                                console.log("User found (FU)");
                                const u = user_1.returnUser(foundUser, false);
                                return {
                                    type: "resp",
                                    data: u,
                                    meta: [
                                        { property: "og:url", content: "http://www.youcanagency.it/user/" + u._id },
                                        { property: "og:type", content: "article" },
                                        { property: "og:locale", content: "it_IT" },
                                        { property: "og:title", content: u.nome },
                                        { property: "og:description", content: (u.bio && u.bio.biografia) ? u.bio.biografia : "Modelle per shooting, modelsharing, advertising. Contattaci per maggiori info!" },
                                        { property: "og:image", content: (u.bio && foundUser.bio.propic) ? u.bio.propic : "http://www.youcanagency.it/img/noimageavailable.png" }
                                    ]
                                };
                            }
                        });
                    }
                });
            }
        }));
        router.get('/account', returnPage({ pageFunc: "account", needLogged: true }));
        router.get('/account/bio', returnPage({ pageFunc: "account_bio", needLogged: true }));
        router.get('/account/misure', returnPage({ pageFunc: "account_misure", needLogged: true }));
        router.get('/account/personali', returnPage({ pageFunc: "account_personali", needLogged: true }));
        router.get('/account/album', returnPage({ pageFunc: "account_album", needLogged: true }));
        router.get('/account/foto', returnPage({ pageFunc: "account_foto", needLogged: true }));
        router.get('/account/users', returnPage({ pageFunc: "account_users", needLogged: true }));
        router.get('/account/liberatorie', returnPage({ pageFunc: "account_liberatorie", needLogged: true }));
        //================= CATHALL =========================>
        router.get('/*', returnPage({ pageFunc: "home", redirect: "/error?code=404" }));
        function goToError(res, args) {
            res.set('Content-Type', 'text/html');
            res.send(getPageArgs("error", JSON.stringify(args)));
        }
        function returnPage(args) {
            function page(req, res, next) {
                if (req.cookies.user)
                    try {
                        req.session.user = JSON.parse(req.cookies.user);
                    }
                    catch (e) {
                        res.clearCookie("user");
                    }
                if (args.needLogged && !req.session.user) {
                    res.clearCookie("user");
                    res.redirect("/login");
                }
                else if (args.needAdmin && (!req.session.user || !req.session.user.isAdmin)) {
                    res.redirect("/error?code=403");
                }
                else {
                    if (args.redirect) {
                        res.redirect(args.redirect);
                    }
                    else if (args.data) {
                        console.log("in data");
                        if (args.data.type == "error") {
                            goToError(res, args.data);
                        }
                        else if (args.data.type == "redirect") {
                            res.redirect(args.data.redirectTo);
                        }
                        else {
                            res.set('Content-Type', 'text/html');
                            res.send(getPageArgs(args.pageFunc, JSON.stringify(args.data.data), args.data.meta));
                        }
                    }
                    else if (args.getData) {
                        console.log("in getData");
                        const d = args.getData({ req, res, next, body: req.body, query: req.query, params: req.params, user: req.session.user });
                        if (d.type == "error") {
                            goToError(res, d);
                        }
                        else if (d.type == "redirect") {
                            res.redirect(d.redirectTo);
                        }
                        else {
                            res.set('Content-Type', 'text/html');
                            res.send(getPageArgs(args.pageFunc, JSON.stringify(d.data), d.meta));
                        }
                    }
                    else if (args.getDataPromise) {
                        console.log("in getDataPromise");
                        args.getDataPromise({ req, res, next, body: req.body, query: req.query, params: req.params, user: req.session.user })
                            .then(result => {
                            if (result.type == "error") {
                                goToError(res, result);
                            }
                            else if (result.type == "redirect") {
                                res.redirect(result.redirectTo);
                            }
                            else {
                                res.set('Content-Type', 'text/html');
                                res.send(getPageArgs(args.pageFunc, JSON.stringify(result.data), result.meta));
                            }
                        })
                            .catch(e => {
                            console.log("My Error", e);
                            res.redirect("/error?code=" + e.type);
                        });
                    }
                    else {
                        res.set('Content-Type', 'text/html');
                        res.send(getPage(args.pageFunc));
                    }
                }
            }
            return page;
        }
        //---->
        application.use('/', router);
        //------->
        application.use("/api/v1/upload", UploadRouter.router);
        application.use("/api/v1/login", LoginRouter.router);
        application.use("/api/v1/messaggi", MessaggioRouter.router);
        application.use("/api/v1/portfolio", PortfolioRouter.router);
        application.use("/api/v1/album", AlbumRouter.router);
        application.use("/file", UploadRouter.router);
    }
    return application;
}
exports.default = App();
