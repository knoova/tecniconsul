"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function returnUser(user, isReqAdmin) {
    let u = {};
    const showOnHome = !!user.bio.propic && !!user.misure;
    const propic = user.bio && user.bio.propic ? user.bio.propic : "http://www.youcanagency.it/img/noimageavailable.png";
    const profilepic = {
        xsmall: user.bio && user.bio.profilepic && user.bio.profilepic.xsmall ? user.bio.profilepic.xsmall : (user.bio.propic ? user.bio.propic : "http://www.youcanagency.it/img/noimageavailable.png"),
        small: user.bio && user.bio.profilepic && user.bio.profilepic.small ? user.bio.profilepic.small : (user.bio.propic ? user.bio.propic : "http://www.youcanagency.it/img/noimageavailable.png"),
        medium: user.bio && user.bio.profilepic && user.bio.profilepic.medium ? user.bio.profilepic.medium : (user.bio.propic ? user.bio.propic : "http://www.youcanagency.it/img/noimageavailable.png"),
        large: user.bio && user.bio.profilepic && user.bio.profilepic.large ? user.bio.profilepic.large : (user.bio.propic ? user.bio.propic : "http://www.youcanagency.it/img/noimageavailable.png")
    };
    if (isReqAdmin)
        u = {
            _id: user._id,
            email: user.email,
            nome: user.nome,
            cognome: user.cognome,
            bio: { biografia: user.bio.biografia, propic, profilepic },
            showOnHome,
            provincia: user.provincia,
            friendlyUrl: user.friendlyUrl,
            indirizzo: user.indirizzo,
            compleanno: user.compleanno,
            viewUrl: user.friendlyUrl ? user.friendlyUrl : user._id,
            telefono: user.telefono,
            sesso: user.sesso,
            misure: user.misure,
            type: user.type,
            isAdmin: user.isAdmin,
            isCouncil: user.isCouncil,
            privacyAndTerms: user.privacyAndTerms
        };
    else
        u = {
            _id: user._id,
            friendlyUrl: user.friendlyUrl,
            nome: user.nome,
            showOnHome,
            bio: { biografia: user.bio.biografia, propic, profilepic },
            provincia: user.provincia,
            sesso: user.sesso,
            misure: user.misure,
            type: user.type,
            viewUrl: user.friendlyUrl ? user.friendlyUrl : user._id
        };
    return u;
}
exports.returnUser = returnUser;
