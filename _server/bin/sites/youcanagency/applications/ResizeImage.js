"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const sharp = require("sharp");
function resize(image) {
    console.log("we are in sharp");
    return sharp(image).resize(1280, 1280, {
        fit: "inside",
        withoutEnlargement: true
    }).toFormat('jpeg').toBuffer();
}
exports.resize = resize;
;
function resizeMultiform(image) {
    return __awaiter(this, void 0, void 0, function* () {
        console.log("we are in sharp");
        const xsmall = yield sharp(image).resize(300, 300, {
            fit: "inside",
            withoutEnlargement: true
        }).toFormat('jpeg').toBuffer();
        const small = yield sharp(image).resize(640, 640, {
            fit: "inside",
            withoutEnlargement: true
        }).toFormat('jpeg').toBuffer();
        const medium = yield sharp(image).resize(1280, 1280, {
            fit: "inside",
            withoutEnlargement: true
        }).toFormat('jpeg').toBuffer();
        const large = yield sharp(image).resize(1920, 1920, {
            fit: "inside",
            withoutEnlargement: true
        }).toFormat('jpeg').toBuffer();
        return { xsmall, small, medium, large };
    });
}
exports.resizeMultiform = resizeMultiform;
;
