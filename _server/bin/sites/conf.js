"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.tecniconsul = {
    db_url: 'mongodb://localhost:27017/',
    db_name: 'TecniconsulDB',
    name: "Tecniconsul s.r.l.",
    shortName: "Tecniconsul",
    piva: "piva",
    codicefiscale: "cf",
    phone: "phone",
    mobile: "cell",
    address: "Via Indirizzo 2, 43123 Parma"
};
