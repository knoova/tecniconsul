"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function mailWelcome(nome, cognome) {
    return `<h2><strong>Ciao ${nome} ${cognome}!</strong></h2>
<h3>Grazie per esserti registrato/a al nostro sito.</h3>
<p>Come vedi &egrave; ancora in costruzione, ma completeremo a breve diverse feature, come:</p>
<ul>
<li>la ricezione delle offerte di lavoro direttamente alla tua mail</li>
<li>possibilit&agrave; di creare album fotografici</li>
<li>possibilit&agrave; di crere e stampare un tuo composit</li>
<li>messaggistica interna per essere contattato/a da altri membri interessati al tuo lavoro</li>
<li>possibilit&agrave; di essere pagata in anticipo tramite criptovalute</li>
<li>un calendario per le prenotazioni e per i turni ai nostri studi</li>
<li>e tante altre sorprese</li>
</ul>
<p>A presto!</p>
<p>&nbsp;</p>
<p>Team <em>YouCan Agency</em></p>`;
}
exports.mailWelcome = mailWelcome;
function mailLogin() {
    return "<p>Accesso effettuato al tuo account</p>";
}
exports.mailLogin = mailLogin;
