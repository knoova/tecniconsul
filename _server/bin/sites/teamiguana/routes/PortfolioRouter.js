"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const conf = require("../../conf");
const mongoose = require("mongoose");
const AWS = require("aws-sdk");
const ObjectModels_1 = require("../database/ObjectModels");
const boh = require("../applications/BuildComposit");
function PortfolioRouter() {
    const router = express_1.Router();
    init();
    function getComposit(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const connection = yield mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name);
                const model = req.body.model;
                const utente = yield ObjectModels_1.modelli.user.findById(model);
                const pdf = yield boh.createPdf({
                    userID: utente._id,
                    frontImage: utente.bio.profilepic.large,
                    rearImage: utente.bio.profilepic.large,
                    measurement: utente.misure,
                    name: utente.nome
                });
                res.status(200).send({ ok: true, data: pdf });
            }
            catch (e) {
                res.status(500).send({ ok: false, message: e });
            }
        });
    }
    function setPortfolio(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            //console.log(req.body)
            const picArray = req.body["images[]"];
            const author = req.body.author;
            const model = req.body.model;
            let counter = picArray.length;
            for (const i of picArray) {
                ObjectModels_1.modelli.image.findOne({ url: i }).then(img => {
                    img.author = author;
                    img.portfolioOf = model;
                    img.save().then(s => {
                        counter--;
                        if (counter == 0) {
                            res.status(200).send({ ok: true });
                        }
                    });
                }).catch(e => res.status(500).send({ type: 500, error: "Errore server nel salvare la foto " + i }));
            }
        });
    }
    function getPortfolio(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            const portfolioOf = req.body.owner;
            ObjectModels_1.modelli.image.find({ portfolioOf }).then(img => {
                if (img.length <= 0) {
                    res.status(404).send({ type: 404, error: "Image of portfolio not found" });
                }
                else
                    res.status(200).send(img);
            }).catch(e => res.status(500).send({ type: 500, error: "Errore server nel recuperare il porfolio di " + portfolioOf }));
        });
    }
    function deleteImage(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            //console.log(req.body)
            const image = req.body.image;
            ObjectModels_1.modelli.image.findById(image).then(img => {
                console.log(img);
                const options = {
                    "accessKeyId": "AKIAI3SX3NVMXZE4FUMQ",
                    "secretAccessKey": "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"
                };
                let bucketInstance = new AWS.S3(options);
                const imageName = img.url.split("/");
                console.log(imageName, imageName[imageName.length - 1]);
                var params = {
                    Bucket: 'youcanagency',
                    Key: imageName[imageName.length - 1]
                };
                bucketInstance.deleteObject(params, function (err, data) {
                    if (err) {
                        console.error("errore", err);
                        res.status(500).send({ type: 500, message: "Impossibile eliminare dal bucket" });
                    }
                    else if (data) {
                        img.remove().then(r => {
                            res.status(200).send({ ok: true });
                        }).catch(e => {
                            res.status(500).send({ type: 500, message: "Impossibile eliminare dal database" });
                        });
                    }
                    else {
                        res.status(500).send({ type: 500, message: "Impossibile eliminare dal bucket" });
                    }
                });
            }).catch(e => res.status(500).send({ type: 500, error: "Errore server nel cancellare la foto " }));
        });
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        //----- SLIDERS
        router.post('/put', setPortfolio);
        router.post('/get', getPortfolio);
        router.post('/deleteimage', deleteImage);
        router.post("/getComposit", getComposit);
    }
    return {
        init,
        router
    };
}
// Create the ModelRouter, and export its configured Express.Router
const PortfolioRoutes = PortfolioRouter();
PortfolioRoutes.init();
exports.router = PortfolioRoutes.router;
