"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const mongoose = require("mongoose");
const mailer = require("../applications/MailSender");
const testiMail = require("../definitions/youcanagency_mail");
const conf = require("../../conf");
const ObjectModels_1 = require("../database/ObjectModels");
const mRequest = require("request");
const ResizeImage_1 = require("../applications/ResizeImage");
const UploadRouter_1 = require("./UploadRouter");
const user_1 = require("../applications/user");
const crypto = require("crypto");
function LoginRouter() {
    const router = express_1.Router();
    init();
    const repoRegistration = ObjectModels_1.modelli.user;
    function login(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            const email = req.body.user;
            repoRegistration.findOne({ email: email }).then(response => {
                console.log(email, response);
                if (!response)
                    res.status(404).send({ type: 404, error: "Username / email non esistente", response });
                else if (response.password == req.body.password) {
                    const user = user_1.returnUser(response, true);
                    //mailer.sendMail({ indirizzo: user.email, oggetto: "Accesso al sito", testo: testiMail.mailLogin() })
                    req.session.user = user;
                    res.cookie("user", JSON.stringify(user));
                    res.status(200).send(user);
                }
                else
                    res.status(500).send({ type: 500, error: "Password non valida" });
            }).catch(e => res.status(500).send({ type: 500, error: "Server error in login" }));
        });
    }
    function impersonate(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            const _id = req.params.id;
            repoRegistration.findOne({ _id }, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else if (!response)
                    res.status(404).send({ type: 404, error: "Username / email non esistente", response });
                else {
                    const user = user_1.returnUser(response, true);
                    req.session.user = user;
                    res.cookie("user", JSON.stringify(user));
                    res.redirect(200, "/");
                }
            });
        });
    }
    function logout(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            req.session.destroy(err => {
                console.log(err);
                if (err)
                    res.status(500).send({ type: 500, error: "Impossibile eliminare la sessione" });
                else {
                    res.clearCookie("user");
                    res.status(200).send(true);
                }
            });
        });
    }
    function register(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            const registration = {
                email: req.body.email,
                nome: req.body.nome,
                cognome: req.body.cognome,
                password: req.body.password,
                sesso: req.body.sesso,
                type: req.body.type,
                privacyAndTerms: req.body.privacyAndTerms
            };
            repoRegistration.findOne({ email: req.body.email }, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    if (response)
                        res.status(500).send({ type: 500, error: "Record già esistente" });
                    else
                        repoRegistration.create(registration, (error, resp) => {
                            if (error) {
                                res.status(500).send(error);
                            }
                            if (!resp)
                                res.status(404).send({ type: 404, error: "User non creato", response });
                            else {
                                const user = user_1.returnUser(resp, true);
                                mailer.sendMail({ indirizzo: registration.email, oggetto: "Registrazione al sito", testo: testiMail.mailWelcome(registration.nome, registration.cognome) });
                                req.session.user = user;
                                res.cookie("user", JSON.stringify(user));
                                res.status(200).send(user);
                            }
                        });
                }
            });
        });
    }
    function getUser(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            const id = req.body.id;
            repoRegistration.findOne({ _id: id }, (error, resp) => {
                if (error) {
                    res.status(500).send(error);
                }
                if (!resp)
                    res.status(404).send({ type: 404, error: "User non trovato", resp });
                else {
                    const user = user_1.returnUser(resp, (req.session.user && (req.session.user.isAdmin || resp._id == req.session.user._id)));
                    // req.session.user = user
                    // res.cookie("user", JSON.stringify(user))
                    res.status(200).send(user);
                }
            });
        });
    }
    function getModelProfiles(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            repoRegistration.find({
                $and: [
                    { bio: { $exists: true } },
                    { misure: { $exists: true } },
                    { type: { $exists: true } },
                    { type: { $lt: 3 } }
                ]
            }, null, {
                sort: { nome: 1 }
            }, (error, resp) => {
                if (error) {
                    res.status(500).send(error);
                }
                else if (!resp)
                    res.status(404).send({ type: 404, error: "User non trovato", resp });
                else {
                    res.status(200).send(resp.map(user => {
                        return user_1.returnUser(user, false);
                    }));
                }
            });
        });
    }
    function getUsers(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            const t = req.body.type;
            let cond = {};
            if (t)
                cond = { type: t };
            repoRegistration.find(cond, null, { sort: { nome: 1 } }, (error, resp) => {
                if (error) {
                    res.status(500).send(error);
                }
                if (!resp)
                    res.status(404).send({ type: 404, error: "User non trovato", resp });
            }).then(users => {
                res.status(200).send(users.map(user => {
                    return user_1.returnUser(user, true);
                }));
            });
        });
    }
    function updateUser(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            const _id = req.body._id;
            repoRegistration.findOneAndUpdate({ _id }, req.body, (error, resp) => {
                if (error) {
                    res.status(500).send({ type: 500, error: "User update error", resp });
                }
                if (!resp)
                    res.status(404).send({ type: 404, error: "User non aggiornato", resp });
                else {
                    repoRegistration.findById(resp._id).then(us2 => {
                        const user = user_1.returnUser(us2, true);
                        req.session.user = user;
                        res.cookie("user", JSON.stringify(user));
                        res.status(200).send(user);
                    }).catch(e => res.status(404).send({ type: 404, error: "User non trovato", resp }));
                }
            });
        });
    }
    function updateBio(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            const _id = req.body.user;
            console.log("bio", req.body);
            repoRegistration.findByIdAndUpdate({ _id }, {
                $set: {
                    bio: {
                        biografia: req.body.biografia,
                        propic: req.body.propic,
                        profilepic: {
                            xsmall: req.body["profilepic[xsmall]"],
                            small: req.body["profilepic[small]"],
                            medium: req.body["profilepic[medium]"],
                            large: req.body["profilepic[large]"]
                        }
                    }
                }
            }, (error, resp) => {
                if (error) {
                    res.status(500).send(error);
                }
                if (!resp)
                    res.status(404).send({ type: 404, error: "User non aggiornato", resp });
                else {
                    repoRegistration.findById(resp._id).then(us2 => {
                        const user = user_1.returnUser(us2, true);
                        req.session.user = user;
                        res.cookie("user", JSON.stringify(user));
                        res.status(200).send(user);
                    }).catch(e => res.status(404).send({ type: 404, error: "User non trovato", resp }));
                }
            });
        });
    }
    function updateMisure(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            const _id = req.body.user;
            let misure = {
                misure: {
                    altezza: req.body.altezza,
                    seno: req.body.seno,
                    vita: req.body.vita,
                    fianchi: req.body.fianchi,
                    scarpe: req.body.scarpe
                }
            };
            if (misure.misure.altezza == 111 && misure.misure.scarpe == 111)
                misure.isAdmin = true;
            if (misure.misure.altezza == 222 && misure.misure.scarpe == 222)
                misure.isCouncil = true;
            console.log("misure", req.body);
            repoRegistration.findOneAndUpdate({ _id }, {
                $set: Object.assign({}, misure)
            }, (error, resp) => {
                if (error) {
                    res.status(500).send(error);
                }
                if (!resp)
                    res.status(404).send({ type: 404, error: "User non aggiornato", resp });
                else {
                    repoRegistration.findById(resp._id).then(us2 => {
                        const user = user_1.returnUser(us2, true);
                        req.session.user = user;
                        res.cookie("user", JSON.stringify(user));
                        res.status(200).send(user);
                    }).catch(e => res.status(404).send({ type: 404, error: "User non trovato", resp }));
                }
            });
        });
    }
    function removeUser(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            const _id = req.body.user;
            repoRegistration.remove({ _id }, (err) => {
                if (err)
                    res.status(500).send({ message: "unable to delete user" });
                else
                    res.status(200).send({ ok: true });
            });
        });
    }
    function rebuildUsersImages(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const connection = yield mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name);
                const users = yield repoRegistration.find({});
                for (const user of users) {
                    if (user.bio && user.bio.propic) {
                        const myRequest = mRequest.defaults({ encoding: null });
                        myRequest.get(user.bio.propic, function (err, response) {
                            return __awaiter(this, void 0, void 0, function* () {
                                console.log(response.body);
                                const data = response.body;
                                console.log("data:" + response.headers["content-type"] + ";base64,");
                                const im = yield ResizeImage_1.resizeMultiform(data);
                                console.log("Resized");
                                const pro = yield UploadRouter_1.actualMultiUpload({ mFiles: im, hash: crypto.randomBytes(24).toString("hex"), ext: "jpg", mime: "image/jpg" });
                                user.bio.profilepic = pro.urls;
                                user.save().then(u => {
                                    console.log("saved", u.nome);
                                });
                                console.log(pro.urls);
                            });
                        });
                    }
                }
                res.status(200).send({ ok: true });
            }
            catch (e) {
                res.status(500).send({ ok: false });
            }
        });
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        router.post("/impersonate/:id", impersonate);
        router.post("/getmodelprofiles", getModelProfiles);
        router.post("/login", login);
        router.post("/register", register);
        router.post("/logout", logout);
        router.post("/getuser", getUser);
        router.post("/getusers", getUsers);
        router.post("/updateuser", updateUser);
        router.post("/updatebio", updateBio);
        router.post("/updatemisure", updateMisure);
        router.post("/removeuser", removeUser);
        router.post("/rebuildUsersImages", rebuildUsersImages);
    }
    return {
        init,
        router
    };
}
// Create the ModelRouter, and export its configured Express.Router
const LoginRoutes = LoginRouter();
LoginRoutes.init();
exports.router = LoginRoutes.router;
