"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const conf = require("../../conf");
const mongoose = require("mongoose");
const allDb = require("mongodb");
const ObjectModels_1 = require("../database/ObjectModels");
function LavoroRouter() {
    const router = express_1.Router();
    init();
    const repo = ObjectModels_1.modelli.lavoro;
    function getLavoro(req, res, next) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            let query = {};
            if (req.body.whichLavoro)
                query = { _id: new allDb.ObjectId(req.body.whichLavoro) };
            repo.find(query).then(response => {
                res.status(200).send(response);
                console.log(response);
            });
        });
    }
    function setLavoro(req, res, next) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const mLavoro = req.body;
            // repo.create(mLavoro, (error, response) => {
            //     if (error) {
            //         res.status(500).send(error);
            //     }
            //     else {
            //         res.status(200).send(response);
            //         console.log(response);
            //     }
            // })
        });
    }
    function changeLavoro(req, res, next) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const myobj = Object.assign({}, req.body);
            const myId = new allDb.ObjectId(req.body._id);
            repo.update(myId, myobj, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            });
        });
    }
    function deleteLavoro(req, res, next) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            // repo.deleteById(req.body._id, (error, response) => {
            //     if (error) {
            //         res.status(500).send(error);
            //     }
            //     else {
            //         res.status(200).send(response);
            //         console.log(response);
            //     }
            // })
        });
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        //----- SLIDERS
        router.post('/lavoro', getLavoro);
        router.post('/setlavoro', setLavoro);
        router.post('/changelavoro', changeLavoro);
        router.post('/deletelavoro', deleteLavoro);
    }
    return {
        init,
        router
    };
}
exports.LavoroRouter = LavoroRouter;
// Create the ModelRouter, and export its configured Express.Router
const FaqRoutes = LavoroRouter();
FaqRoutes.init();
exports.default = FaqRoutes.router;
