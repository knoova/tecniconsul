"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const conf = require("../../conf");
const mongoose = require("mongoose");
const ObjectModels_1 = require("../database/ObjectModels");
function MessaggioRouter() {
    const router = express_1.Router();
    init();
    function getUserConversazioni(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            ObjectModels_1.modelli.conversazioni.find({ userID: req.body.userid }, null, { sort: { dataUltimoMessaggio: -1 } }).then(conversazioni => {
                res.status(200).send(conversazioni);
                console.log(conversazioni);
            }).catch(e => {
                res.status(500).send({ type: 500, error: "Errore server nel recuperare conversazione" });
                console.log({ type: 500, error: "Errore server nel recuperare conversazione", e });
            });
        });
    }
    function getMessaggiConversazione(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            ObjectModels_1.modelli.messaggio.find({ conversazioneID: req.body.convid }, null, { sort: { data: 1 } }).then(messaggi => {
                res.status(200).send(messaggi);
                console.log(messaggi);
            }).catch(e => {
                res.status(500).send({ type: 500, error: "Errore server nel recuperare messaggi" });
                console.log({ type: 500, error: "Errore server nel recuperare messaggi", e });
            });
        });
    }
    function setMessaggio(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            const mMessaggio = req.body;
            console.log(mMessaggio);
            ObjectModels_1.modelli.conversazioni.find({
                $or: [
                    { _id: mMessaggio.conversazioneID },
                    { $and: [{ userID: mMessaggio.mittenteID }, { interlocutoreID: mMessaggio.riceventeID }] },
                    { $and: [{ userID: mMessaggio.riceventeID }, { interlocutoreID: mMessaggio.mittenteID }] }
                ]
            }, (err, conversazioni) => {
                if (err) {
                    console.log(err);
                    res.status(500).send({ type: 500, error: "Errore server nel creare messaggio ricerca" });
                }
                else if (conversazioni && conversazioni.length > 0) {
                    console.log(conversazioni);
                    for (const conversazione of conversazioni) {
                        conversazione.ultimoMessaggio = mMessaggio.messaggio.slice(0, 40) + (mMessaggio.messaggio.length > 40 ? "..." : "");
                        conversazione.dataUltimoMessaggio = mMessaggio.data;
                        conversazione.save();
                        ObjectModels_1.modelli.messaggio.create(Object.assign({}, mMessaggio, { conversazioneID: conversazione._id, receivedByServer: true })).then(m => {
                            console.log({ type: 200, error: "Messaggio creato ", m });
                        }).catch(e => {
                            res.status(500).send({ type: 500, error: "Errore server nel creare messaggio loop" });
                            console.log({ type: 500, error: "Errore server nel creare messaggio loop", e });
                        });
                    }
                    res.status(200).send({ ok: true });
                }
                else {
                    ObjectModels_1.modelli.conversazioni.create({
                        ultimoMessaggio: mMessaggio.messaggio.slice(0, 20) + (mMessaggio.messaggio.length > 20 ? "..." : ""),
                        dataUltimoMessaggio: mMessaggio.data,
                        userID: mMessaggio.mittenteID,
                        interlocutoreID: mMessaggio.riceventeID
                    }).then(conv1 => {
                        ObjectModels_1.modelli.conversazioni.create({
                            ultimoMessaggio: mMessaggio.messaggio.slice(0, 20) + (mMessaggio.messaggio.length > 20 ? "..." : ""),
                            dataUltimoMessaggio: mMessaggio.data,
                            userID: mMessaggio.riceventeID,
                            interlocutoreID: mMessaggio.mittenteID
                        }).then(conv2 => {
                            ObjectModels_1.modelli.messaggio.create(Object.assign({}, mMessaggio, { conversazioneID: conv1._id, receivedByServer: true })).then(m1 => {
                                ObjectModels_1.modelli.messaggio.create(Object.assign({}, mMessaggio, { conversazioneID: conv2._id, receivedByServer: true })).then(m2 => {
                                    res.status(200).send(true);
                                }).catch(e => {
                                    res.status(500).send({ type: 500, error: "Errore server nel creare messaggio conv2/2" });
                                    console.log({ type: 500, error: "Errore server nel creare messaggio conv2/2", e });
                                });
                            }).catch(e => {
                                res.status(500).send({ type: 500, error: "Errore server nel creare messaggio conv2/1" });
                                console.log({ type: 500, error: "Errore server nel creare messaggio conv2/1", e });
                            });
                        }).catch(e => {
                            res.status(500).send({ type: 500, error: "Errore server nel creare messaggio conv2" });
                            console.log({ type: 500, error: "Errore server nel creare messaggio conv2", e });
                        });
                    }).catch(e => {
                        res.status(500).send({ type: 500, error: "Errore server nel creare messaggio conv1" });
                        console.log({ type: 500, error: "Errore server nel creare messaggio conv1", e });
                    });
                }
            });
        });
    }
    function deleteMessaggio(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            ObjectModels_1.modelli.messaggio.deleteOne({ _id: req.body.messageID }).then(r => {
                res.status(200).send({ ok: true });
            }).catch(e => {
                res.status(500).send({ type: 500, error: "Errore server nel cancellare il messaggio" });
                console.log({ type: 500, error: "Errore server nel cancellare il messaggio", e });
            });
        });
    }
    function deleteConversazione(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            ObjectModels_1.modelli.messaggio.deleteMany({ conversazioneID: req.body.convID }).then(e => {
                if (e) {
                    res.status(500).send({ type: 500, error: "Errore server nel cancellare la conversazione" });
                    console.log({ type: 500, error: "Errore server nel cancellare la conversazione", e });
                }
                ObjectModels_1.modelli.conversazioni.deleteOne({ _id: req.body.convID }).then(r => {
                    res.status(200).send({ ok: true });
                }).catch(e => {
                    res.status(500).send({ type: 500, error: "Errore server nel cancellare il messaggio" });
                    console.log({ type: 500, error: "Errore server nel cancellare il messaggio", e });
                });
            });
        });
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        //----- SLIDERS
        router.post('/sendmessaggio', setMessaggio);
        router.post('/getuserconversazioni', getUserConversazioni);
        router.post('/getmessaggiconversazione', getMessaggiConversazione);
        router.post('/deletemessaggio', deleteMessaggio);
        router.post('/deleteconversazione', deleteConversazione);
    }
    return {
        init,
        router
    };
}
// Create the ModelRouter, and export its configured Express.Router
const MessaggioRoutes = MessaggioRouter();
MessaggioRoutes.init();
exports.router = MessaggioRoutes.router;
