"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const conf = require("../../conf");
const mongoose = require("mongoose");
const ObjectModels_1 = require("../database/ObjectModels");
function AlbumRouter() {
    const router = express_1.Router();
    init();
    function getAlbum(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = yield mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name);
            if (req.body.whichAlbum) {
                ObjectModels_1.modelli.album.findById(req.body.whichAlbum).then(response => {
                    if (!response) {
                        res.status(404).send({ type: 404, error: "Album not found" });
                    }
                    else
                        res.status(200).send(response);
                }).catch(e => res.status(500).send({ type: 500, error: "Server error" }));
            }
            else if (req.body.author) {
                ObjectModels_1.modelli.album.find({ author: req.body.author }).then(r => {
                    if (!r || r.length == 0) {
                        res.status(404).send({ type: 404, error: "Album not found" });
                    }
                    else
                        res.status(200).send(r);
                }).catch(e => res.status(500).send({ type: 500, error: "Server error" }));
            }
        });
    }
    function setAlbum(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            const mAlbum = req.body;
            ObjectModels_1.modelli.album.find({ slug: mAlbum.slug }).then(a => {
                if (!a || a.length == 0) {
                    ObjectModels_1.modelli.album.create(mAlbum).then(r => {
                        res.status(200).send({ ok: true });
                    }).catch(e => res.status(500).send({ type: 500, error: "Server error" }));
                }
                else {
                    res.status(500).send({ type: 500, message: "Slug già esitente" });
                }
            });
        });
    }
    function changeAlbum(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            ObjectModels_1.modelli.album.update(req.body._id, Object.assign({}, req.body), (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            });
        });
    }
    function deleteAlbum(req, res, next) {
        mongoose.connect(conf.youcanagency.db_url + conf.youcanagency.db_name).then(connection => {
            // repo.deleteById(req.body._id, (error, response) => {
            //     if (error) {
            //         res.status(500).send(error);
            //     }
            //     else {
            //         res.status(200).send(response);
            //         console.log(response);
            //     }
            // })
        });
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        //----- SLIDERS
        router.post('/find', getAlbum);
        router.post('/create', setAlbum);
        router.post('/changealbum', changeAlbum);
        router.post('/deletealbum', deleteAlbum);
    }
    return {
        init,
        router
    };
}
// Create the ModelRouter, and export its configured Express.Router
const FaqRoutes = AlbumRouter();
FaqRoutes.init();
exports.router = FaqRoutes.router;
