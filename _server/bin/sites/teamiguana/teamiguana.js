"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Express = require("express");
const logger = require("morgan");
const bodyParser = require("body-parser");
const Html = require("./applications/HtmlBuilder");
const conf = require("../conf");
const session = require("express-session");
//import * as cookieParser from 'cookie-parser';
const cookieParser = require("cookie-parser");
const compression = require("compression");
function getPage(page) {
    return Html.Header({ title: conf.youcanagency.shortName, script: ["dom-to-image.js"] }) +
        Html.Body({ functionName: page }) +
        Html.Footer({ name: conf.youcanagency.name, address: conf.youcanagency.address, codicefiscale: conf.youcanagency.codicefiscale, piva: conf.youcanagency.piva });
}
function getPageArgs(page, args, meta) {
    return Html.Header({
        title: conf.youcanagency.shortName,
        meta
    }) +
        Html.Body({ functionName: page, args }) +
        Html.Footer({ name: conf.youcanagency.name, address: conf.youcanagency.address, codicefiscale: conf.youcanagency.codicefiscale, piva: conf.youcanagency.piva });
}
function shouldCompress(req, res) {
    if (req.headers['x-no-compression']) {
        // don't compress responses with this request header
        return false;
    }
    // fallback to standard filter function
    return compression.filter(req, res);
}
// Creates and configures an ExpressJS web server.
function App() {
    // ref to Express instance
    let application = Express();
    let router = Express.Router();
    middleware();
    routes();
    // Configure Express middleware.
    function middleware() {
        application.use(logger('dev'));
        application.use(cookieParser());
        application.use(compression({ filter: shouldCompress }));
        application.use("/", Express.static('../youcanagency/coding/www/'));
        application.use(bodyParser.json({ limit: "90mb" }));
        application.use(bodyParser.urlencoded({ extended: false, limit: "90mb" }));
        application.use(session({ secret: "yca secret password", cookie: { maxAge: 600000 } }));
    }
    // Configure API endpoints.
    function routes() {
        router.get("/", returnPage({ pageFunc: "home" }));
        function goToError(res, args) {
            res.set('Content-Type', 'text/html');
            res.send(getPageArgs("error", JSON.stringify(args)));
        }
        function returnPage(args) {
            function page(req, res, next) {
                if (req.cookies.user)
                    try {
                        req.session.user = JSON.parse(req.cookies.user);
                    }
                    catch (e) {
                        res.clearCookie("user");
                    }
                if (args.needLogged && !req.session.user) {
                    res.clearCookie("user");
                    res.redirect("/login");
                }
                else if (args.needAdmin && (!req.session.user || !req.session.user.isAdmin)) {
                    res.redirect("/error?code=403");
                }
                else {
                    if (args.redirect) {
                        res.redirect(args.redirect);
                    }
                    else if (args.data) {
                        console.log("in data");
                        if (args.data.type == "error") {
                            goToError(res, args.data);
                        }
                        else if (args.data.type == "redirect") {
                            res.redirect(args.data.redirectTo);
                        }
                        else {
                            res.set('Content-Type', 'text/html');
                            res.send(getPageArgs(args.pageFunc, JSON.stringify(args.data.data), args.data.meta));
                        }
                    }
                    else if (args.getData) {
                        console.log("in getData");
                        const d = args.getData({ req, res, next, body: req.body, query: req.query, params: req.params, user: req.session.user });
                        if (d.type == "error") {
                            goToError(res, d);
                        }
                        else if (d.type == "redirect") {
                            res.redirect(d.redirectTo);
                        }
                        else {
                            res.set('Content-Type', 'text/html');
                            res.send(getPageArgs(args.pageFunc, JSON.stringify(d.data), d.meta));
                        }
                    }
                    else if (args.getDataPromise) {
                        console.log("in getDataPromise");
                        args.getDataPromise({ req, res, next, body: req.body, query: req.query, params: req.params, user: req.session.user })
                            .then(result => {
                            if (result.type == "error") {
                                goToError(res, result);
                            }
                            else if (result.type == "redirect") {
                                res.redirect(result.redirectTo);
                            }
                            else {
                                res.set('Content-Type', 'text/html');
                                res.send(getPageArgs(args.pageFunc, JSON.stringify(result.data), result.meta));
                            }
                        })
                            .catch(e => {
                            console.log("My Error", e);
                            res.redirect("/error?code=" + e.type);
                        });
                    }
                    else {
                        res.set('Content-Type', 'text/html');
                        res.send(getPage(args.pageFunc));
                    }
                }
            }
            return page;
        }
        //---->
        application.use('/', router);
    }
    return application;
}
exports.default = App();
