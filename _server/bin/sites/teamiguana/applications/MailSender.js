"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodemailer = require("nodemailer");
function sendMail(args) {
    const transporter = nodemailer.createTransport({
        host: 'smtps.aruba.it',
        port: 465,
        secure: true,
        auth: {
            user: args.mittente ? args.mittente.user : "info@youcanagency.it",
            pass: args.mittente ? args.mittente.password : "yca2014@" // generated ethereal password
        }
    });
    var mailOptions = {
        from: args.mittente ? args.mittente.user : 'info@youcanagency.it',
        to: args.indirizzo,
        subject: args.oggetto,
        html: args.testo
    };
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        }
        else {
            console.log('Email sent: ' + info.response);
        }
    });
}
exports.sendMail = sendMail;
