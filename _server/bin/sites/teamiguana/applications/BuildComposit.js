"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const pdfkit = require("pdfkit");
const request = require("request");
const fs = require("fs");
function createPdf(args) {
    return __awaiter(this, void 0, void 0, function* () {
        const p = new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            const doc = new pdfkit({ size: "A5" });
            const saveto = "../youcanagency/coding/www/pdf";
            const ws = fs.createWriteStream(`${saveto}/${args.userID}.pdf`);
            try {
                const chunks = [];
                ws.on('data', (data) => chunks.push(data));
                ws.on('end', () => {
                    const pdf = Buffer.concat(chunks).toString('base64');
                    resolve(pdf);
                });
            }
            catch (error) {
                reject(error);
            }
            doc.pipe(ws);
            doc.registerFont("Ecaterina", "../youcanagency/coding/www/font/EcatarinaLight-Regular.ttf");
            doc.registerFont("EcaterinaBold", "../youcanagency/coding/www/font/EcatarinaBold-Regular.ttf");
            doc.rect(0, 0, doc.page.width, doc.page.height)
                .fillOpacity(1)
                .fill("#000");
            const images = yield getImages(args.frontImage, args.rearImage);
            doc.image(images.front, 10, 10, { width: doc.page.width - 20, height: doc.page.height - 20 });
            doc.rect(10, (doc.page.height - 110), doc.page.width - 20, 100)
                .fillOpacity(0.5)
                .fill("#000");
            doc.fillOpacity(1)
                .font("EcaterinaBold")
                .fontSize(18)
                .fillColor("#FF00FF")
                .text("YouCanAgency", 10, (doc.page.height - 110), { width: doc.page.width - 20, align: 'center' });
            //doc.moveDown(1)
            doc
                .font("Ecaterina")
                .fontSize(16)
                .fillColor("#FF00FF")
                .text(args.name, 10, (doc.page.height - 70), { width: doc.page.width - 20, align: 'center' });
            doc.end();
        }));
        return yield p;
    });
}
exports.createPdf = createPdf;
function getImages(frontImage, rearImage) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            request({
                url: frontImage,
                encoding: null
            }, (err, req, bodyF) => {
                if (err)
                    reject(err);
                request({
                    url: rearImage,
                    encoding: null
                }, (err, req, bodyR) => {
                    if (err)
                        reject(err);
                    resolve({ front: bodyF, rear: bodyR });
                });
            });
        });
    });
}
function createPdfJs(res, args) {
    return __awaiter(this, void 0, void 0, function* () {
    });
}
exports.createPdfJs = createPdfJs;
