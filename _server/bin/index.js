"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const vhttps = require('vhttps');
const vhost = require('vhost');
const tecniconsul_1 = require("./sites/tecniconsul/tecniconsul");
try {
    const port = 10080;
    const securePort = 443;
    const server = express();
    const vhosts = [
        vhost("www.tecniconsulenergia.it", tecniconsul_1.default),
        vhost('test.tecniconsulenergia.it', tecniconsul_1.default),
        vhost('prova.tecniconsulenergia.it', tecniconsul_1.default),
        vhost('localhost', tecniconsul_1.default)
    ];
    for (const vh of vhosts)
        server.use(vh);
    //========================================= HTTPS ======================================>>>>>>>>>>>>>>>>>>>>>>>
    // try {
    //     // const yca_cert1 = {
    //     //     hostname: "youcanagency.it",
    //     //     cert: fs.readFileSync('/etc/letsencrypt/live/youcanagency.it/fullchain.pem'),
    //     //     key: fs.readFileSync('/etc/letsencrypt/live/youcanagency.it/privkey.pem'),
    //     // }
    //     // const yca_cert2 = {
    //     //     hostname: "www.youcanagency.it",
    //     //     cert: fs.readFileSync('/etc/letsencrypt/live/youcanagency.it/fullchain.pem'),
    //     //     key: fs.readFileSync('/etc/letsencrypt/live/youcanagency.it/privkey.pem'),
    //     // }
    //     // const httpsServer = vhttps.createServer(yca_cert1, [yca_cert1, yca_cert2], server)
    //     // httpsServer.listen(securePort)
    // } catch (e) {
    //     console.error(e)
    // }
    server.listen(port);
    console.log('Express started on port ' + port);
}
catch (e) {
    console.log('Express error in' + e);
}
