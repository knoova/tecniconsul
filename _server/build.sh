#!/bin/bash
echo ""
echo ""
echo "Stopping server"
pm2 stop Server
pm2 start ./bin/index.js --name tempServer
echo ""
echo ""
echo "Installing npm packages for server"
sudo chmod -R 777 ./
sudo npm install
echo ""
echo ""
echo "Starting server"
pm2 stop tempServer
pm2 start ./bin/index.js --name Server