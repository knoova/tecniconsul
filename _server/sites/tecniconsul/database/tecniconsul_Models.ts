import * as mongoose from "mongoose"
import * as database from "./database"
import * as def from "../definitions/tecniconsul"

const ProvinciaSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    regione: {
        type: String,
        required: true
    }
})
const UtenteimportatoSchema = new mongoose.Schema({
    
})


const ComuneSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    regione: {
        type: String,
        required: true
    },
    provincia: {
        type: String,
        required: true
    }
})


const CabinaSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    regione: {
        type: String,
        required: true
    },
    provincia: {
        type: String,
        required: true
    },
    comune: {
        type: String,
        required: true
    },
    tabella1: {
        type: Object,
        required: false
    },
    tabella2: {
        type: Object,
        required: false
    },
    tabella3: {
        type: Object,
        required: false
    },
    tabella4: {
        type: Object,
        required: false
    }
})




const ContattoSchema = new mongoose.Schema({
    azienda: {
        type: Boolean,
        required: false
    },
    privato: {
        type: String,
        required: false
    },
    nome: {
        type: String,
        required: true
    },
    cognome: {
        type: String,
        required: false
    },
    age: {
        type: String,
        required: false
    },
    professione: {
        type: String,
        required: false
    },
    orario: {
        type: String,
        required: false
    },
    comune: {
        type: String,
        required: false
    },
    telefono: {
        type: String,
        required: false
    },
    commenti: {
        type: String,
        required: false
    },
    acconsento: {
        type: Boolean,
        required: false
    },
    tipo: {
        type: String,
        required: false
    },
    documento: {
        type: String,
        required: false
    }
})




const FAQSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    subtitle: {
        type: String,
        required: true
    },
    logo: {
        type: String,
        required: false
    },
    part: {
        type: String,
        required: false
    },
    category: {
        type: String,
        required: false
    },
    createdAt: {
        type: Date,
        required: false
    },
    modifiedAt: {
        type: Date,
        required: false
    }
})



const imageSchema = new mongoose.Schema({
    url: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        required: false
    },
    modifiedAt: {
        type: Date,
        required: false
    }
})




const NewsSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    subtitle: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    date: {
        type: String,
        required: true
    },
    banner: {
        type: String,
        required: false
    },
    createdAt: {
        type: Date,
        required: false
    },
    modifiedAt: {
        type: Date,
        required: false
    }
})


const RegioneSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    }
})

const UfficioSchema = new mongoose.Schema({
    city: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: false
    },
    phone: {
        type: String,
        required: false
    },
    hours: {
        type: String,
        required: false
    }
})




const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    privato: {
        type: Boolean,
        required: false
    },
    azienda: {
        type: Boolean,
        required: false
    },
    nome: {
        type: String,
        required: false
    },
    cognome: {
        type: String,
        required: false
    },
    denominazione: {
        type: String,
        required: false
    },
    cf: {
        type: String,
        required: true
    },
    piva: {
        type: String,
        required: false
    },
    provincia: {
        type: String,
        required: true
    },
    indirizzo: {
        type: String,
        required: true
    },
    telefono: {
        type: String,
        required: false
    },
    password: {
        type: String,
        required: true
    },
    acconsente: {
        type: Boolean,
        required: false
    },
    codcliente: {
        type: String,
        required: true
    },
    isAdmin: {
        type: Boolean,
        required: false
    }
})



export const UtilsSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    slug: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        required: false
    },
    modifiedAt: {
        type: Date,
        required: false
    }
})



export const LandingPagesSchema = new mongoose.Schema({
    slug: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        required: false
    },
    modifiedAt: {
        type: Date,
        required: false
    }
})


export const CssSchema = new mongoose.Schema({
    css: {
        type: String,
        required: false
    }
})


const OffertaSchema = new mongoose.Schema({
    casaAziendaPlacet: { required: true, type: String },
    luceGas: { required: true, type: String },
    o1: { required: true, type: Object },
    o2: { required: true, type: Object },
    o3: { required: true, type: Object },
    o4: { required: true, type: Object }
})



export const modelli = {
    provincia: database.RepositoryBase<def.Provincia>(mongoose.model("Provincia", ProvinciaSchema)),
    comune: database.RepositoryBase<def.Comune>(mongoose.model("Comune", ComuneSchema)),
    cabina: database.RepositoryBase<def.Cabina>(mongoose.model("Cabina", CabinaSchema)),
    contatto: database.RepositoryBase<def.Contatto>(mongoose.model("Contatto", ContattoSchema)),
    faq: database.RepositoryBase<def.FAQ>(mongoose.model("FAQ", FAQSchema)),
    image: database.RepositoryBase<def.Image>(mongoose.model("image", imageSchema)),
    news: database.RepositoryBase<def.News>(mongoose.model("News", NewsSchema)),
    ufficio: database.RepositoryBase<def.Ufficio>(mongoose.model("Ufficio", UfficioSchema)),
    regione: database.RepositoryBase<def.Regione>(mongoose.model("Regione", RegioneSchema)),
    user: database.RepositoryBase<def.Utente>(mongoose.model("Users", UserSchema)),
    util: database.RepositoryBase<def.Utils>(mongoose.model("Utils", UtilsSchema)),
    landingPage: database.RepositoryBase<def.LandingPages>(mongoose.model("LandingPages", LandingPagesSchema)),
    css: database.RepositoryBase<def.Css>(mongoose.model("Css", CssSchema)),
    offerte: database.RepositoryBase<def.Offerte>(mongoose.model("Offerte", OffertaSchema))
}