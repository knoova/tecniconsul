import * as mongoose from "mongoose"


export interface Read<T> {
    retrieve: (callback: (error: any, result: any) => void) => void;
    findById: (id: string, callback: (error: any, result: T) => void) => void;
    findOne(cond?: Object, callback?: (err: any, res: T) => void): mongoose.Query<T>;
    find(cond: Object, fields: Object, options: Object, callback?: (err: any, res: T[]) => void): mongoose.Query<T[]>;
}

export interface Write<T> {
    create: (item: T, callback: (error: any, result: any) => void) => void;
    update: (_id: mongoose.Types.ObjectId, item: T, callback: (error: any, result: any) => void) => void;
    delete: (_id: string, callback: (error: any, result: any) => void) => void;
}

export function RepositoryBase<T extends mongoose.Document>(schemaModel: mongoose.Model<T>) {

    function create(item: T, callback: (error: any, result: T) => void) {
        schemaModel.create(item, callback);
    }

    function retrieve(callback: (error: any, result: T) => void) {
        schemaModel.find({}, callback);
    }

    function update(_id: mongoose.Types.ObjectId, item: T, callback: (error: any, result: any) => void) {
        schemaModel.update({ _id: _id }, item, callback);
    }

    function deleteById(_id: string, callback: (error: any, result: any) => void) {
        schemaModel.remove({ _id: this.toObjectId(_id) }, (err) => callback(err, null));
    }

    function findById(_id: string, callback: (error: any, result: T) => void) {
        schemaModel.findById(_id, callback);
    }

    function findOne(cond?: Object, callback?: (err: any, res: T) => void)/*: mongoose.Query<T[]>*/ {
        return schemaModel.findOne(cond, callback)
    }

    function find(cond?: Object, fields?: Object, options?: Object, callback?: (err: any, res: T[]) => void)/*: mongoose.Query<T[]>*/ {
        return schemaModel.find(cond, options, callback)
    }

    function toObjectId(_id: string): mongoose.Types.ObjectId {
        return mongoose.Types.ObjectId.createFromHexString(_id);
    }

    return { create, retrieve, update, deleteById, findById, findOne, find, toObjectId }
}