import { Router, Request, Response, NextFunction } from 'express';

import * as mongoose from "mongoose"

import * as conf from "../../conf"

import * as allDb from "mongodb"

import { modelli } from "../database/tecniconsul_Models"
import * as def from "../definitions/tecniconsul"



function FAQsRouter() {
    const router: Router = Router();
    init();
    
    const repo = modelli.faq

    const cssRepo = modelli.css


    function getFAQs(req: Request, res: Response, next: NextFunction) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            let query = {}
            if (req.body.whichFAQ)
                query = { _id: new allDb.ObjectId(req.body.whichFAQ) }
            if (req.body.part)
                query = { part: req.body.part }
            if (req.body.category)
                query = { category: req.body.category }

            repo.find(query).then(response => {
                res.status(200).send(response);
                console.log(response);
            }).catch(err => {
                res.status(500).send(err);
            })
       // })
    }

    function setFAQ(req: Request, res: Response, next: NextFunction) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {


            const mFAQ = <def.FAQ>req.body

            repo.create(mFAQ, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
     //   })
    }

    function getCss(req: Request, res: Response, next: NextFunction) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            let query = {}


            cssRepo.find(query).then(response => {
                res.status(200).send(response);
                console.log(response);
            })
      //  })
    }


    function changeCss(req: Request, res: Response, next: NextFunction) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            const myobj = <def.Css>{
                css: req.body.css
            }

            console.log("Css body", req.body)

            if (req.body._id) {
                const myId = new allDb.ObjectId(req.body._id)
                cssRepo.update(myId, myobj, (error, response) => {
                    if (error) {
                        res.status(500).send(error);
                    }
                    else {
                        res.status(200).send(response);
                        console.log(response);
                    }
                })
            }
            else {
                cssRepo.create(myobj, (error, response) => {
                    if (error) {
                        res.status(500).send(error);
                    }
                    else {
                        res.status(200).send(response);
                        console.log(response);
                    }
                })
            }

      //  })

    }

    function changeFAQ(req: Request, res: Response, next: NextFunction) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            const myobj = <def.FAQ>{
                ...req.body
            }

            const myId = new allDb.ObjectId(req.body._id)

            repo.update(myId, myobj, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
      //  })


    }

    function deleteFAQ(req: Request, res: Response, next: NextFunction) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            repo.deleteById(req.body._id, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
      //  })

    }



    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        router.post('/setcss', changeCss);
        router.post('/getcss', getCss);
        //----- SLIDERS
        router.post('/faqs', getFAQs);
        router.post('/setfaq', setFAQ);
        router.post('/changefaq', changeFAQ);
        router.post('/deletefaq', deleteFAQ);
    }

    return {
        init,
        router
    }
}

// Create the ModelRouter, and export its configured Express.Router
const FaqRoutes = FAQsRouter();
FaqRoutes.init();

export const router = FaqRoutes.router;