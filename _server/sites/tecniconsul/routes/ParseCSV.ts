import * as csvparser from "csv-parse"

import * as conf from "../../conf"

import * as database from "../database/database"

import * as Db from "mongodb"

import * as mongoose from "mongoose"

import * as allDb from "mongodb"


import { modelli } from "../database/tecniconsul_Models"
import * as def from "../definitions/tecniconsul"

export function parseCSVCabine(args: { cabinaID: string, content: string, type: string }) {
    const output: any[] = []
    csvparser(
        args.content,
        {
            trim: true,
            skip_empty_lines: true,
            delimiter: ";"
        })
        // Use the readable stream api
        .on('readable', function () {
            let record
            while (record = this.read()) {
                output.push(record)
            }
        })
        // When we are done, test that the parsed output matched what expected
        .on('end', function () {
            console.log("csv output", output)
           // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

                console.log()

                const cabinaDB = modelli.cabina

                let obj = {}

                if (args.type == "tabella1")
                    obj = { tabella1: output }
                if (args.type == "tabella2")
                    obj = { tabella2: output }
                if (args.type == "tabella3")
                    obj = { tabella3: output }
                if (args.type == "tabella4")
                    obj = { tabella4: output }


                cabinaDB.update(new allDb.ObjectId(args.cabinaID), <def.Cabina>obj, (error, response) => {
                    if (error) {
                        console.error("problema", error)
                    }
                    else {
                        console.log(response);
                    }
                })
            })
     //   })
}

export function parseCSVUtenti(args: { content: string }) {
    const output: any[] = []
    csvparser(
        args.content,
        {
            trim: true,
            skip_empty_lines: true,
            delimiter: ";"
        })
        // Use the readable stream api
        .on('readable', function () {
            let record
            while (record = this.read()) {
                output.push(record)
            }
        })
        // When we are done, test that the parsed output matched what expected
        .on('end', function () {
            console.log("csv output", output)
            //mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

                console.log()

                const utenteDB = modelli.user

                let obj = <def.Utente[]>output

                for(let user of obj)
                utenteDB.create(user, (error, response) => {
                    if (error) {
                        console.error("problema", error)
                    }
                    else {
                        console.log(response);
                    }
                })
            })
       // })
}