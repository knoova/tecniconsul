import { Router, Request, Response, NextFunction } from 'express';

import * as mailer from "./mail"

function LoginsRouter() {
    const router: Router = Router();
    init();



    function init() {

        function sendMail(req: Request, res: Response, next: NextFunction) {
            mailer.sendMail({ indirizzo: "francesco.galleano@gmail.com", oggetto: req.body.obj, testo: req.body.body })
            res.status(200).send({ resp: "mail sent" });

        }


        router.post('/sendcustomdata', sendMail);

    }

    return {
        init,
        router
    }
}

// Create the ModelRouter, and export its configured Express.Router
const LoginRoutes = LoginsRouter();
LoginRoutes.init();

export const router = LoginRoutes.router;