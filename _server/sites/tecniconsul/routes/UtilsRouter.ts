import { Router, Request, Response, NextFunction } from 'express';


import * as conf from "../../conf"

import * as mongoose from "mongoose"


import * as database from "../database/database"


import * as allDb from "mongodb"

import { modelli } from "../database/tecniconsul_Models"
import * as def from "../definitions/tecniconsul"


function UtilsRouter() {
    const router: Router = Router();
    init();

    const repo = modelli.util


    function getUtils(req: Request, res: Response, next: NextFunction) {
       // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            let query = {}
            if (req.body.whichUtils)
                query = { _id: new allDb.ObjectId(req.body.whichUtils) }

            repo.find(query).then(response => {
                res.status(200).send(response);
                console.log(response);
            })
      //  })
    }

    function setUtils(req: Request, res: Response, next: NextFunction) {
       // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            console.log("here i am")

            const mUtils = <def.Utils>req.body

            console.log(mUtils)

            repo.create(mUtils, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
      //  })
    }


    function changeUtils(req: Request, res: Response, next: NextFunction) {
       // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            const myobj = <def.Utils>{
                title: req.body.title,
                slug: req.body.slug,
                body: req.body.body
            }

            const myId = new allDb.ObjectId(req.body._id)

            repo.update(myId, myobj, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
      //  })


    }

    function deleteUtils(req: Request, res: Response, next: NextFunction) {
      //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            repo.deleteById(req.body._id, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
      //  })

    }



    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        //----- SLIDERS
        router.post('/utils', getUtils);
        router.post('/setutils', setUtils);
        router.post('/changeutils', changeUtils);
        router.post('/deleteutils', deleteUtils);
    }

    return {
        init,
        router
    }
}

// Create the ModelRouter, and export its configured Express.Router
const FaqRoutes = UtilsRouter();
FaqRoutes.init();

export const router = FaqRoutes.router;