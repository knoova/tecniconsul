import { Router, Request, Response, NextFunction } from 'express';

import * as conf from "../../conf"

import * as database from "../database/database"

import * as Db from "mongodb"

import * as mongoose from "mongoose"

import { modelli } from "../database/tecniconsul_Models"
import * as def from "../definitions/tecniconsul"



function RegioniRouter() {
    const router: Router = Router();
    init();

    const repoU = modelli.ufficio
    const repoP = modelli.provincia
    const repoR = modelli.regione
    const repoComuni = modelli.comune
    const repoCabine = modelli.cabina



    function getRegioni(req: Request, res: Response, next: NextFunction) {
      //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            let query = {}
            repoR.find(query).then(response => {
                res.status(200).send(response);
                console.log(response);
            })
       // })
    }

    function setRegione(req: Request, res: Response, next: NextFunction) {
      //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const mRegione = <def.Regione>{
                ...req.body
            }
            repoR.create(mRegione, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
      //  })
    }

    function changeRegione(req: Request, res: Response, next: NextFunction) {
      //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const mRegione = <def.Regione>{
                ...req.body
            }

            const myId = new Db.ObjectId(req.body._id)
            repoR.update(myId, mRegione, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
       // })
    }

    function deleteRegione(req: Request, res: Response, next: NextFunction) {
       // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            repoR.deleteById(req.body._id, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
      //  })
    }


    function deleteProvincia(req: Request, res: Response, next: NextFunction) {
      //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            repoP.deleteById(req.body._id, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
      //  })
    }



    function setProvincia(req: Request, res: Response, next: NextFunction) {
       // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            const mProvincia = <def.Provincia>{
                ...req.body
            }
            repoP.create(mProvincia, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
       // })
    }


    function setUfficio(req: Request, res: Response, next: NextFunction) {
       // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const mUfficio = <def.Ufficio>{
                ...req.body
            }
            repoU.create(mUfficio, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
      //  })
    }


    function getUfficio(req: Request, res: Response, next: NextFunction) {
      //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            let query = {}
            repoU.find(query).then(response => {
                res.status(200).send(response);
                console.log(response);
            })
      //  })
    }

    function changeUfficio(req: Request, res: Response, next: NextFunction) {
       // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const mUfficio = <def.Ufficio>{
                ...req.body
            }

            const myId = new Db.ObjectId(req.body._id)
            repoU.update(myId, mUfficio, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
       // })
    }

    function deleteUfficio(req: Request, res: Response, next: NextFunction) {
       // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            repoU.deleteById(req.body._id, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
      //  })
    }

    function deleteComune(req: Request, res: Response, next: NextFunction) {
      //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            repoComuni.deleteById(req.body._id, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
      //  })
    }

    function deleteCabina(req: Request, res: Response, next: NextFunction) {
       // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            repoCabine.deleteById(req.body._id, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
       // })
    }


    function getProvinceOfRegione(req: Request, res: Response, next: NextFunction) {
      //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const repoP = modelli.provincia
            const regione = req.params.idRegione
            let obj = {}
            if (regione)
                obj = { regione }
            repoP.find(obj).then(response => {
                res.status(200).send(response);
                console.log(response);
            })
      //  })
    }


    function getProvinciaById(req: Request, res: Response, next: NextFunction) {
      //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const repoP = modelli.provincia
            const _id = req.params.id
            let obj = { _id }
            repoP.findOne(obj).then(response => {
                res.status(200).send(response);
                console.log(response);
            })
       // })
    }


    function getProvince(req: Request, res: Response, next: NextFunction) {
      //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            const repoP = modelli.provincia
            repoP.find({}).then(response => {
                res.status(200).send(response);
                console.log(response);
            })
      //  })
    }



    function setComune(req: Request, res: Response, next: NextFunction) {
     //   mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            const mComune = <def.Comune>{
                ...req.body
            }
            repoComuni.create(mComune, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
       // })
    }


    function getComuneById(req: Request, res: Response, next: NextFunction) {
       // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const _id = req.params.id
            let obj = { _id }
            repoComuni.findOne(obj).then(response => {
                res.status(200).send(response);
                console.log(response);
            })
      //  })
    }


    function getComuni(req: Request, res: Response, next: NextFunction) {
       // mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            console.log(req.body)

            repoComuni.find({ ...req.body }).then(response => {
                res.status(200).send(response);
                console.log(response);
            })
      //  })
    }

    

    function setCabina(req: Request, res: Response, next: NextFunction) {
      //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            const mCabina = <def.Cabina>{
                ...req.body
            }
            repoCabine.create(mCabina, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
      //  })
    }


    function getCabinaById(req: Request, res: Response, next: NextFunction) {
      //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const _id = req.params.id
            let obj = { _id }
            repoCabine.findOne(obj).then(response => {
                res.status(200).send(response);
                console.log(response);
            })
      //  })
    }


    function getCabine(req: Request, res: Response, next: NextFunction) {
      //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            console.log(req.body)

            repoCabine.find({ ...req.body }).then(response => {
                res.status(200).send(response);
                console.log(response);
            })
      //  })
    }



    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        router.post('/regioni', getRegioni);
        router.post('/setregione', setRegione);
        router.post('/changeregione', changeRegione);
        router.post('/deleteregione', deleteRegione);
        router.post('/province/:idRegione', getProvinceOfRegione);
        router.post('/provincia/:id', getProvinciaById);
        router.post('/province', getProvince);
        router.post('/setprovincia', setProvincia);
        router.post('/deleteprovincia', deleteProvincia);
        router.post('/setufficio', setUfficio);
        router.post('/getufficio', getUfficio);
        router.post('/changeufficio', changeUfficio);
        router.post('/deleteufficio', deleteUfficio);
        router.post('/deletecomune', deleteComune);
        router.post('/deletecabina', deleteCabina);
        router.post("/setcomune", setComune)
        router.post("/comune/:id", getComuneById)
        router.post("/comuni", getComuni)
        router.post("/setcabina", setCabina)
        router.post("/cabina/:id", getCabinaById)
        router.post("/cabine", getCabine)
    }

    return {
        init,
        router
    }
}

// Create the ModelRouter, and export its configured Express.Router
const RegioniRoutes = RegioniRouter();
RegioniRoutes.init();

export const router = RegioniRoutes.router;