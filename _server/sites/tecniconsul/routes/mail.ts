import * as nodemailer from 'nodemailer'
export function sendMail(args: { mittente?: { user: string, password: string }, indirizzo: string, cc?: string[], oggetto: string, testo: string }) {
    const transporter = nodemailer.createTransport({
        host: 'smtps.aruba.it',
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            user: args.mittente ? args.mittente.user : "info@youcanagency.it", // generated ethereal user
            pass: args.mittente ? args.mittente.password : "yca2014@" // generated ethereal password
        }
    });

    var mailOptions: nodemailer.SendMailOptions = {
        from: args.mittente ? args.mittente.user : 'info@youcanagency.it',
        to: args.indirizzo,
        cc: args.cc,
        subject: args.oggetto,
        html: args.testo
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}