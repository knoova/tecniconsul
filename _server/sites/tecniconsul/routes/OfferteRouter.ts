import { Router, Request, Response, NextFunction } from 'express';

import * as conf from "../../conf"

import * as mongoose from "mongoose"


import * as database from "../database/database"


import * as allDb from "mongodb"


import { modelli } from "../database/tecniconsul_Models"
import * as def from "../definitions/tecniconsul"




function NewsRouter() {
    const router: Router = Router();
    init();

    const repo = modelli.news

    const offerteRepo = modelli.offerte


    function getNews(req: Request, res: Response, next: NextFunction) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            let query = {}
            if (req.body.whichNews)
                query = { _id: new allDb.ObjectId(req.body.whichNews) }

            repo.find(query).then(response => {
                res.status(200).send(response);
                console.log(response);
            })
        })
    }

    function setNews(req: Request, res: Response, next: NextFunction) {

        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            const mNews = <def.News>req.body

            repo.create(mNews, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
        })
    }

    async function getOfferta(req: Request, res: Response, next: NextFunction) {
        const connectio = await mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name)

        const myobj = <def.Offerte>req.body

        console.log(myobj)

        const offerta = await offerteRepo.findOne({ casaAziendaPlacet: myobj.casaAziendaPlacet, luceGas: myobj.luceGas })

        res.status(200).send(offerta)

    }

    interface Offerta { _id?: string, title: string; intro: string; description: string; }

    interface Offerte {
        casaAziendaPlacet: "casa" | "azienda" | "placet"; luceGas: "luce" | "gas";
        o1: Offerta, o2: Offerta, o3: Offerta, o4: Offerta
    }

    async function changeOfferte(req: Request, res: Response, next: NextFunction) {
        const connectio = await mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name)

        const myobj = <def.Offerte>req.body

        console.log(myobj)

        const offerta = await offerteRepo.findOne({ casaAziendaPlacet: myobj.casaAziendaPlacet, luceGas: myobj.luceGas }) as def.Offerte

        console.log(offerta)
        if (offerta) {
            offerta.o1 = myobj.o1
            offerta.o2 = myobj.o2
            offerta.o3 = myobj.o3
            offerta.o4 = myobj.o4
            offerta.save()
            res.status(200).send({ ok: true, message: "updated" });
        }
        else {
            offerteRepo.create(myobj, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send({ ok: true, message: "created" });
                    console.log(response);
                }
            })
        }


    }

    function changeNews(req: Request, res: Response, next: NextFunction) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            const myobj = <def.News>{
                title: req.body.title,
                subtitle: req.body.subtitle,
                body: req.body.body,
                date: req.body.date,
                banner: req.body.banner
            }

            const myId = new allDb.ObjectId(req.body._id)

            repo.update(myId, myobj, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
        })


    }

    function deleteNews(req: Request, res: Response, next: NextFunction) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            console.log(req.body)

            repo.deleteById(req.body.id, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
        })

    }



    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        //----- SLIDERS
        router.post('/news', getNews);
        router.post('/setnews', setNews);
        router.post('/changenews', changeNews);
        router.post('/deletenews', deleteNews);
        router.post("/setofferta", changeOfferte)
        router.post("/getofferta", getOfferta)
    }

    return {
        init,
        router
    }
}

// Create the ModelRouter, and export its configured Express.Router
const FaqRoutes = NewsRouter();
FaqRoutes.init();

export const router = FaqRoutes.router;