import { Router, Request, Response, NextFunction } from 'express';

import * as conf from "../../conf"

import * as mongoose from "mongoose"


import * as database from "../database/database"


import * as allDb from "mongodb"


import { modelli } from "../database/tecniconsul_Models"
import * as def from "../definitions/tecniconsul"




function NewsRouter() {
    const router: Router = Router();
    init();

    const repo = modelli.news

    const cssRepo = modelli.css


    function getNews(req: Request, res: Response, next: NextFunction) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            let query = {}
            if (req.body.whichNews)
                query = { _id: new allDb.ObjectId(req.body.whichNews) }

            repo.find(query).then(response => {
                res.status(200).send(response);
                console.log(response);
            })
      //  })
    }

    function setNews(req: Request, res: Response, next: NextFunction) {

        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            const mNews = <def.News>req.body

            repo.create(mNews, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
       // })
    }

    function getCss(req: Request, res: Response, next: NextFunction) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            let query = {}


            cssRepo.find(query).then(response => {
                res.status(200).send(response);
                console.log(response);
            })
      //  })
    }


    function changeCss(req: Request, res: Response, next: NextFunction) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            const myobj = <def.Css>{
                css: req.body.css
            }

            console.log("Css body", req.body)

            if (req.body._id) {
                const myId = new allDb.ObjectId(req.body._id)
                cssRepo.update(myId, myobj, (error, response) => {
                    if (error) {
                        res.status(500).send(error);
                    }
                    else {
                        res.status(200).send(response);
                        console.log(response);
                    }
                })
            }
            else {
                cssRepo.create(myobj, (error, response) => {
                    if (error) {
                        res.status(500).send(error);
                    }
                    else {
                        res.status(200).send(response);
                        console.log(response);
                    }
                })
            }
       // })


    }

    function changeNews(req: Request, res: Response, next: NextFunction) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            const myobj = <def.News>{
                title: req.body.title,
                subtitle: req.body.subtitle,
                body: req.body.body,
                date: req.body.date,
                banner: req.body.banner
            }

            const myId = new allDb.ObjectId(req.body._id)

            repo.update(myId, myobj, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
       // })


    }

    function deleteNews(req: Request, res: Response, next: NextFunction) {
        //  mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            console.log(req.body)

            repo.deleteById(req.body.id, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
      //  })

    }



    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        //----- SLIDERS
        router.post('/news', getNews);
        router.post('/setnews', setNews);
        router.post('/changenews', changeNews);
        router.post('/deletenews', deleteNews);
    }

    return {
        init,
        router
    }
}

// Create the ModelRouter, and export its configured Express.Router
const FaqRoutes = NewsRouter();
FaqRoutes.init();

export const router = FaqRoutes.router;