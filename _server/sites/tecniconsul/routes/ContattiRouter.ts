import { Router, Request, Response, NextFunction } from 'express';


import * as database from "../database/database"

import * as Db from "mongodb"

import * as mongoose from "mongoose"

import * as conf from "../../conf"

import { modelli } from "../database/tecniconsul_Models"
import * as def from "../definitions/tecniconsul"




function ContattoRouter() {
    const router: Router = Router();
    init();



    const repo = modelli.contatto

    function getContatti(req: Request, res: Response, next: NextFunction) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            let query = {}
            repo.find(query).then(response => {
                res.status(200).send(response);
                console.log(response);
            })
        })
    }

    function setContatto(req: Request, res: Response, next: NextFunction) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const mContatto = <def.Contatto>{
                ...req.body
            }
            repo.create(mContatto, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
        })
    }

    function changeContatto(req: Request, res: Response, next: NextFunction) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            const mRegione = <def.Contatto>{
                ...req.body
            }

            const myId = new Db.ObjectId(req.body._id)
            repo.update(myId, mRegione, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
        })
    }


    function deleteContatto(req: Request, res: Response, next: NextFunction) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
            repo.deleteById(req.body._id, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
        })
    }




    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        router.post('/setcontatto', setContatto);
        router.post('/getcontatto', getContatti);
        router.post('/changecontatto', changeContatto);
        router.post('/deletecontatto', deleteContatto);
    }

    return {
        init,
        router
    }
}

// Create the ModelRouter, and export its configured Express.Router
const RegioniRoutes = ContattoRouter();
RegioniRoutes.init();

export const router = RegioniRoutes.router;