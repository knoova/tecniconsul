import { Router, Request, Response, NextFunction } from 'express';


import * as conf from "../../conf"

import * as mongoose from "mongoose"


import * as database from "../database/database"


import * as allDb from "mongodb"

import { modelli } from "../database/tecniconsul_Models"
import * as def from "../definitions/tecniconsul"


function LandingRouter() {
    const router: Router = Router();
    init();


    async function getLanding(req: Request, res: Response, next: NextFunction) {
        const connection = await mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name)
        const query = { _id: new allDb.ObjectId(req.params.slug) }

        const response = await modelli.landingPage.findOne(query)
        res.status(200).send(response);
        console.log(response);
    }

    async function getAllLanding(req: Request, res: Response, next: NextFunction) {
        const connection = await mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name)

        const response = await modelli.landingPage.find()
        res.status(200).send(response);
        console.log(response);
    }

    function setLanding(req: Request, res: Response, next: NextFunction) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            console.log("here i am")

            const mLanding = <def.LandingPages>req.body

            console.log(mLanding)

            modelli.landingPage.create(mLanding, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
        })
    }


    function changeLanding(req: Request, res: Response, next: NextFunction) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            const myobj = <def.LandingPages>{
                slug: <string>req.body.slug,
                body: <string>req.body.body
            }

            const myId = new allDb.ObjectId(req.body._id)

            modelli.landingPage.update(myId, myobj, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
        })


    }

    function deleteLanding(req: Request, res: Response, next: NextFunction) {
        mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {

            modelli.landingPage.deleteById(req.body._id, (error, response) => {
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(response);
                    console.log(response);
                }
            })
        })

    }



    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        //----- SLIDERS
        router.get('/landing/:slug', getLanding);
        router.post('/landing', getAllLanding);
        router.post('/setlanding', setLanding);
        router.post('/changelanding', changeLanding);
        router.post('/deletelanding', deleteLanding);
    }

    return {
        init,
        router
    }
}

// Create the ModelRouter, and export its configured Express.Router
const FaqRoutes = LandingRouter();
FaqRoutes.init();

export const router = FaqRoutes.router;