import { Router, Request, Response, NextFunction } from 'express';

import * as AWS from "aws-sdk"

import * as crypto from "crypto"

import * as database from "../database/database"
import { Db } from "mongodb"
import * as mongoose from "mongoose"
import * as conf from "../../conf"

import * as parsecsv from "./ParseCSV"

import { modelli } from "../database/tecniconsul_Models"
import * as def from "../definitions/tecniconsul"

import * as fs from "fs"
import * as  request from "request"



function ImageRouter() {
    const router: Router = Router();
    init();




    function saveOne(req: Request, res: Response, next: NextFunction) {

        const hash = crypto.randomBytes(24).toString("hex")

        const image = {
            host: `/uploadedImg/`,
            file: hash + ".jpg"
        }

        const fileToWrite = new Buffer(req.body.base64Image.replace(/^data:image\/\w+;base64,/, ""), 'base64')

        const mImage = <def.Image>{
            url: image.host + image.file,
            name: req.body.name,
            description: req.body.description
        }

        fs.writeFile("../_client/www/uploadedImg/" + image.file, fileToWrite, function (err) {
            if (err) {
                res.status(500).send(err)
            }

            const repo = modelli.image
            //mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
                repo.create(mImage, (error, response) => {
                    console.log("creation result: ")
                    if (error) {
                        res.status(500).send(error);
                    }
                    else {
                        res.status(200).send(response);
                        console.log(response);
                    }
                })
            })
        //})
    }


    interface upCSV { csv: string, provincia: string, name: string, type: string }

    function savecsv(req: Request, res: Response, next: NextFunction) {
        console.log("save csv", req.body)
        parsecsv.parseCSVCabine({
            cabinaID: req.body.cabina,
            type: req.body.type,
            content: req.body.csv
        })
        res.status(200).send(true);
    }


    function saveXLS(req: Request, res: Response, next: NextFunction) {
        console.log("save csv", req.body)

        const content = req.body.xls
        const name = req.body.name

        fs.writeFile("../_client/www/uploadedXls/" + name + ".xls", content, function (err) {
            if (err) {
                res.status(500).send(err)
            }


            res.status(200).send({ ok: true });
            console.log("xls saved");

        })

    }

    function downloadAndSaveImage(req: Request, resp: Response, next: NextFunction) {

        const nameArray = req.body.url.split("/")
        const name = nameArray[nameArray.length - 1]
        function download(uri: string, filename: string) {
            request.head(uri, function (err, res, body) {
                console.log('content-type:', res.headers['content-type']);
                console.log('content-length:', res.headers['content-length']);

                request(uri).pipe(fs.createWriteStream("../_client/www/uploadedImg/" + filename)).on('close', () => {
                    resp.status(200).send({ ok: true, filename })
                });
            });
        };

        download(req.body.url, name);
    }



    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    function init() {
        router.post('/put', saveOne);
        router.post('/putcsv', savecsv);
        router.post('/putxls', savecsv);
        router.post("/migrate", downloadAndSaveImage)
    }

    return {
        init,
        router
    }
}

// Create the ModelRouter, and export its configured Express.Router
const ImageRoutes = ImageRouter();
ImageRoutes.init();

export const router = ImageRoutes.router;
