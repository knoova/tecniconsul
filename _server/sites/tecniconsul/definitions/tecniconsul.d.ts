import * as mongoose from "mongoose"

export interface Ufficio extends mongoose.Document { city: string, address: string, phone: string, hours: string }
export interface Provincia extends mongoose.Document { regione: string, name: string }
export interface Comune extends mongoose.Document { regione: string, provincia: string, name: string }
export interface Cabina extends mongoose.Document { regione: string, provincia: string, comune: string, name: string, tabella1?: any, tabella2?: any, tabella3?: any, tabella4?: any }
export interface Regione extends mongoose.Document { name: string }
export interface Utente extends mongoose.Document {
    privato: boolean,
    azienda: boolean,
    nome?: string,
    cognome?: string,
    denominazione?: string,
    cf: string,
    piva?: string,
    provincia: string,
    indirizzo: string,
    telefono?: string,
    acconsente: boolean,
    email: string,
    password: string,
    codcliente: string,
    isAdmin?: boolean
}
export interface Utils extends mongoose.Document { title: string, slug: string, body: string }
export interface LandingPages extends mongoose.Document { slug: string, body: string }
export interface Contatto extends mongoose.Document { azienda?: boolean, privato?: boolean, nome: string, cognome?: string, age?: string, professione?: string, orario?: string, comune?: string, telefono?: string, commenti?: string, documento?: string, acconsento?: boolean, tipo: string }
export interface FAQ extends mongoose.Document { title: string, description: string, subtitle: string, logo: string, part: string, category: string }
export interface Image extends mongoose.Document { url: string; name: string; description?: string; createdAt?: Date; modifiedAt?: Date; }
export interface News extends mongoose.Document { title: string, subtitle: string, body: string, date: string, banner?: string }
export interface Css extends mongoose.Document { css: string; createdAt?: Date; modifiedAt?: Date; }

export interface Offerta extends mongoose.Document { title: string; intro: string; description: string; }

export interface Offerte extends mongoose.Document {
    casaAziendaPlacet: "casa" | "azienda" | "placet"; luceGas: "luce" | "gas";
    o1: Offerta, o2: Offerta, o3: Offerta, o4: Offerta
}
