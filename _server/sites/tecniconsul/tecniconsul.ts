import * as http from 'http';
import * as path from 'path';
import * as Express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import * as Html from "./HtmlBuilder"
import * as conf from "../conf"
import * as session from "express-session"
import * as cookieParser from 'cookie-parser';

//Routes
import * as ImageRouter from './routes/ImageRouter';
import * as RegioniRouter from './routes/RegioniRouter';
import * as FaqRouter from "./routes/FaqRouter"
import * as UtilsRouter from './routes/UtilsRouter';
import * as NewsRouter from './routes/NewsRouter';
import * as UtenteRouter from './routes/UtenteRouter';
import * as ContattiRouter from './routes/ContattiRouter';
import * as MailRouter from './routes/SendMailRouter';
import * as LandingRouter from "./routes/LandingPagesRouter"
import * as OfferteRouter from "./routes/OfferteRouter"


import * as utils from './routes/UtilsRouter';

import * as mongoose from "mongoose"


import { modelli } from "./database/tecniconsul_Models"
import * as def from "./definitions/tecniconsul"


// Creates and configures an ExpressJS web server.
function App() {

    //database.openDbConnection(tecniconsul.db_url, tecniconsul.db_name)

    function getPage(page: string, args?: string): string {
        return Html.Header({
            title: conf.tecniconsul.shortName
        }) +
            Html.Body({ functionName: page, args }) +
            Html.Footer({ name: conf.tecniconsul.name, address: conf.tecniconsul.address, codicefiscale: conf.tecniconsul.codicefiscale, piva: conf.tecniconsul.piva })

    }


    const repo = modelli.util


    // ref to Express instance
    let application: Express.Application = Express()

    mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name).then(connection => {
        middleware();
        routes();
    })



    // Configure Express middleware.
    function middleware(): void {
        application.use(logger('dev'));
        application.use(cookieParser())
        application.use(Express.static('../_client/www'));
        application.use('/administration2', Express.static('../_angular/dist/angular'));
        application.use('/administration2/*', Express.static('../_angular/dist/angular'));
        application.use(bodyParser.json({ limit: "99mb" }));
        application.use(bodyParser.urlencoded({ extended: false, limit: "90mb" }));
        application.use(session({ secret: "samurta secret password", cookie: { maxAge: 600000 } }))
    }

    // Configure API endpoints.
    function routes(): void {
        let router = Express.Router();

        router.get('/', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("home"))
        });

        router.get('/home', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("home"))
        });

        router.get('/contatti', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("contatti"))
        });

        router.get('/clienti', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("clienti"))
        });

        router.get('/territori', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("territori"))
        });

        router.get('/faq', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("faq"))
        });

        router.get('/faq/:id', (req, res, next) => {
            const args = JSON.stringify({ id: req.params.id })
            res.set('Content-Type', 'text/html');
            res.send(getPage("faq", args))
        });

        router.get('/offerte', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("offerte"))
        });

        router.get('/migrazione', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("migrazione"))
        });

        router.get('/dettagli/placetluce', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("offertaplacetluce"))
        });

        router.get('/dettagli/placetgas', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("offertaplacetgas"))
        });

        router.get('/dettagli/luce', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("offertaluce"))
        });

        router.get('/dettagli/gas', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("offertagas"))
        });

        router.get('/dettagli/luceazienda', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("offertaluceazienda"))
        });

        router.get('/dettagli/gasazienda', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("offertagasazienda"))
        });

        router.get('/offerte/:id', (req, res, next) => {
            const args = JSON.stringify({ offerta: req.params.id })
            res.set('Content-Type', 'text/html');
            res.send(getPage("offerte", args))
        });

        router.get('/news', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("news"))
        });

        router.get('/leggerebollettaluce', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("leggerebollettaluce"))
        });

        router.get('/leggerebollettagas', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("leggerebollettagas"))
        });

        router.get('/news/:id', (req, res, next) => {
            const args = JSON.stringify({ id: req.params.id })
            res.set('Content-Type', 'text/html');
            res.send(getPage("news", args))
        });

        router.get('/utils/:id', (req, res, next) => {
            //const args = JSON.stringify({ id: req.params.id })
            repo.findOne({ slug: req.params.id }).then(r => {
                res.set('Content-Type', 'text/html');
                res.send(getPage("utils", JSON.stringify(r)))
            })
        });

        router.get('/form', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("form"))
        });

        router.get('/login', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("login"))
        });

        router.get('/thankyou', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("thankyou"))
        });

        router.get('/dettagli', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            res.send(getPage("dettagli"))
        });

        // router.get('/dettagli/:id', (req, res, next) => {
        //     res.set('Content-Type', 'text/html');
        //     res.send(getPage("dettagli"))
        // });


        router.get('/landing/:slug', async (req, res, next) => {
            const connection = await mongoose.connect(conf.tecniconsul.db_url + conf.tecniconsul.db_name)
            const query = { slug: req.params.slug }

            const landingPage = await modelli.landingPage.findOne(query)
            if (landingPage && landingPage._id) {
                console.log("landing", (<any>landingPage).body);
                res.status(200).set('Content-Type', 'text/html').send((<any>landingPage).body);
            }
            else res.status(404).set('Content-Type', 'text/html').send(getPage("home"))
        });

        //===================== AMMINISTRAZIONE
        router.get('/amministrazione', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("amministrazione"))
        });
        router.get('/amministrazione/addcomune', (req, res, next) => {
            console.log("test", req.session.user)
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("addComune"))
        });
        router.get('/amministrazione/changecomune', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("changeComune"))
        });


        
        router.get('/amministrazione/addcabina', (req, res, next) => {
            console.log("test", req.session.user)
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("addCabina"))
        });
        router.get('/amministrazione/changecabina', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("changeCabina"))
        });




        router.get('/amministrazione/addlanding', (req, res, next) => {
            console.log("test", req.session.user)
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("addLanding"))
        });
        router.get('/amministrazione/changelanding', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("changeLanding"))
        });
        router.get('/amministrazione/addprovincia', (req, res, next) => {
            console.log("test", req.session.user)
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("addProvincia"))
        });
        router.get('/amministrazione/changeprovincia', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("changeProvincia"))
        });
        router.get('/amministrazione/addfaq', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("addFaq"))
        });
        router.get('/amministrazione/changefaq', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("changeFaq"))
        });
        router.get('/amministrazione/addregione', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("addRegione"))
        });
        router.get('/amministrazione/changeregione', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("changeRegione"))
        });
        router.get('/amministrazione/addufficio', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("addUfficio"))
        });
        router.get('/amministrazione/changeufficio', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("changeUfficio"))
        });
        router.get('/amministrazione/addutils', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("addUtils"))
        });
        router.get('/amministrazione/changeutils', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("changeUtils"))
        });
        router.get('/amministrazione/addnews', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("addNews"))
        });
        router.get('/amministrazione/changenews', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("changeNews"))
        });
        router.get('/amministrazione/addusers', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("addUsers"))
        });
        router.get('/amministrazione/changeusers', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("changeUsers"))
        });
        router.get('/amministrazione/addlistino', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("addListino"))
        });
        router.get('/amministrazione/changelistino', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("changeListino"))
        });
        router.get('/amministrazione/addOfferte/:casaaziendaplacet/:lucegas', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("addOfferte"))
        });
        router.get('/amministrazione/addOfferte', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user && req.session.user.isAdmin) && !(req.cookies.user && req.cookies.user.isAdmin)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("changeOfferte"))
        });



        router.get('/clienti/fatturazioneelettronica', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("fatturazioneElettronica"))
        });


        router.get('/clienti/datipersonali', (req, res, next) => {
            res.set('Content-Type', 'text/html');
            if (!(req.session.user)) {
                res.clearCookie("user")
                res.send(getPage("login"))
            }
            else
                res.send(getPage("datiPersonali"))
        });


        //---->
        application.use('/', router);
        application.use('/home', router)
        application.use('/contatti', router)
        application.use('/clienti', router)
        application.use('/territori', router)
        application.use('/faq', router)
        application.use('/faq/:id', router)
        application.use('/form', router)
        application.use('/dettagli', router)
        application.use('/dettagli/:id', router)
        application.use('/landing/:id', router)
        application.use('/thankyou', router)
        application.use('/offerte', router)
        application.use('/offerte/:id', router)
        application.use('/offerte/dettagli/luce', router)
        application.use('/dettagli/gas', router)


        //==========> API
        application.use('/login', router)
        application.use('/api/v1/images', ImageRouter.router);
        application.use('/api/v1/regioni', RegioniRouter.router);
        application.use('/api/v1/comuni', RegioniRouter.router);
        application.use('/api/v1/cabine', RegioniRouter.router);
        application.use('/api/v1/faq', FaqRouter.router);
        application.use('/api/v1/utils', UtilsRouter.router);
        application.use('/api/v1/news', NewsRouter.router);
        application.use('/api/v1/office', RegioniRouter.router);
        application.use('/api/v1/contatti', ContattiRouter.router);
        application.use('/api/v1/utente', UtenteRouter.router);
        application.use('/api/v1/login', UtenteRouter.router);
        application.use('/api/v1/mail', MailRouter.router);
        application.use('/api/v1/upload', ImageRouter.router);
        application.use('/api/v1/landing', LandingRouter.router);
        application.use('/api/v1/offerte', OfferteRouter.router);

        //------->
        application.use("/amministrazione", router)
        // application.use("/amministrazione/addfaq", router)
        // application.use("/amministrazione/changefaq", router)
        // application.use("/amministrazione/addregione", router)
        // application.use("/amministrazione/changeregione", router)
        // application.use("/amministrazione/addprovincia", router)
        // application.use("/amministrazione/changeprovincia", router)
        // application.use("/amministrazione/addufficio", router)
        // application.use("/amministrazione/changeufficio", router)
        // application.use("/amministrazione/addusers", router)
        // application.use("/amministrazione/changeusers", router)


        //---------->

    }


    return application

}

export default App()
