export function Header(args: { title: string, css?: string[], cssString?: string, script?: string[], meta?: { property: string, content: string }[] }) {

    const localLoadedScript = [
        "jquery-3.2.1.min.js", "index.js"
    ]
    const fp = [
        "<!DOCTYPE html>",
        "<html lang=\"it\">",
        "<head>",
        "<title>" + args.title + "</title>",
        "<link rel=\"stylesheet\" href=\"/css/main.css\" type=\"text/css\">",
        "<link rel=\"icon\" href=\"/img/favicon.png\" type=\"image/png\" />"]

    if (args.css)
        for (const c of args.css)
            fp.push("<link rel=\"stylesheet\" href=\"/js/" + c + "\" type=\"text/css\" />")

    if (args.script)
        for (const s of args.script)
            fp.push("<script type=\"text/javascript\" src=\"/js/" + s + "\"></script>")

    if (args.meta)
        for (const s of args.meta)
            fp.push(`<meta property=\"${s.property}\" content=\"${s.content}\" />`)

    for (const s of localLoadedScript)
        fp.push(`<script type=\"text/javascript\" src=\"/js/${s}\"></script>`)

    if (args.cssString)
        fp.push(`<style>${args.cssString}</style>`)

    const sp = [
        "<script async defer type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyBEEU8-4jzD85kIhtMuXcVIv5n61Rtbnt8\"></script>",
        "<meta charset=\"utf-8\">",
        "</head>"
    ]

    return fp.concat(sp).join("")

}


export function Body(args: { functionName: string, seo?: string[], args?: string }) {
    const ub = [
        `<script>var onloadparameter = ${args.args ? "JSON.parse(decodeURI(\"" + encodeURI(args.args) + "\"))" : "undefined"}</script>`,
        `<body onload="${args.functionName}(${args.args ? "onloadparameter" : ""})">`]

    if (args.seo)
        for (let s of args.seo)
            ub.push(s)
    ub.push("</body>")
    return ub.join("")
}

export function Footer(args: { name: string, address: string, codicefiscale: string, piva: string }) {
    return [
        "<div class=\"yca-footer\">",
        args.name,
        "<br />",
        args.address,
        "<br />",
        args.codicefiscale + " - " + args.piva,
        "<br />",
        "</div>",
        "</body>",
        "</html>"
    ].join("")
}