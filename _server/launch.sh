#!/bin/bash
date +'%H:%M:%S Stopping server'
pm2 stop Server
date +'%H:%M:%S Stopped'
date +'%H:%M:%S Starting server'
pm2 start ./bin/index.js --name Server
date +'%H:%M:%S Started'