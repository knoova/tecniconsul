(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const api = require("./api/_lib");
exports.api = api;
const tags = require("./tags");
exports.tags = tags;
const shards = require("./shards/_lib");
exports.shards = shards;
const blocks = require("./blocks/_lib");
exports.blocks = blocks;
const pages = require("./pages/_lib");
exports.pages = pages;
const template = require("./template/_lib");
exports.template = template;
const def = require("./definitions/_lib");
exports.def = def;
const models = require("./models/_lib");
exports.models = models;
const input = require("./fields/input/_lib");
exports.input = input;

},{"./api/_lib":2,"./blocks/_lib":19,"./definitions/_lib":24,"./fields/input/_lib":25,"./models/_lib":36,"./pages/_lib":37,"./shards/_lib":75,"./tags":90,"./template/_lib":91}],2:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const generic = require("./generic");
exports.generic = generic;
const model = require("./model");
exports.model = model;
const images = require("./images");
exports.images = images;
const regioni = require("./regioni");
exports.regioni = regioni;
const utils = require("./utils");
exports.utils = utils;
const faq = require("./faq");
exports.faq = faq;
const uffici = require("./uffici");
exports.uffici = uffici;
const news = require("./news");
exports.news = news;
const upload = require("./upload");
exports.upload = upload;
const utente = require("./utente");
exports.utente = utente;
const contatto = require("./contatto");
exports.contatto = contatto;
const mail = require("./mail");
exports.mail = mail;
const comuni = require("./comuni");
exports.comuni = comuni;
const landing = require("./landing");
exports.landing = landing;
const offerte = require("./offerte");
exports.offerte = offerte;
const migrazione = require("./migrazione");
exports.migrazione = migrazione;

},{"./comuni":3,"./contatto":4,"./faq":5,"./generic":6,"./images":7,"./landing":8,"./mail":9,"./migrazione":10,"./model":11,"./news":12,"./offerte":13,"./regioni":14,"./uffici":15,"./upload":16,"./utente":17,"./utils":18}],3:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function setComune(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/setcomune",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.setComune = setComune;
function getComuni(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/comuni",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getComuni = getComuni;
function getComune(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/comune/" + id,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getComune = getComune;
function deleteComuni(_id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/deletecomune",
            data: { _id },
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.deleteComuni = deleteComuni;
function changeComune(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/changecomuni/",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.changeComune = changeComune;
function setCabina(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/setcabina",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.setCabina = setCabina;
function deleteCabina(_id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/deletecabina",
            data: { _id },
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.deleteCabina = deleteCabina;
function changeCabina(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/changecabina/",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.changeCabina = changeCabina;
function getCabine(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/cabine",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getCabine = getCabine;
function getCabina(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/cabina/" + id,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getCabina = getCabina;

},{}],4:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getContatto() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/contatto/contatto",
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getContatto = getContatto;
function setContatto(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/contatto/setcontatto",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.setContatto = setContatto;
function changeContatto(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/contatto/changecontatto",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.changeContatto = changeContatto;
function deleteContatto(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/contatto/deletecontatto",
            data: { id },
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.deleteContatto = deleteContatto;

},{}],5:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getFAQ(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/faq/faqs",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getFAQ = getFAQ;
function setFAQ(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/faq/setfaq",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.setFAQ = setFAQ;
function changeFAQ(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/faq/changefaq",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.changeFAQ = changeFAQ;
function deleteFAQ(_id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/faq/deletefaq",
            data: { _id },
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.deleteFAQ = deleteFAQ;

},{}],6:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const md5 = require("md5");
function setProvincia(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/setprovincia",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.setProvincia = setProvincia;
function getProvince(regione) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/province/" + (regione ? regione : ""),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getProvince = getProvince;
function getProvincia(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/provincia/" + id,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getProvincia = getProvincia;
function deleteProvince(_id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/deleteprovince",
            data: { _id },
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.deleteProvince = deleteProvince;
function changeProvince(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/changeprovince/",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.changeProvince = changeProvince;
function setContatto(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/contatti/setcontatto",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.setContatto = setContatto;
function getContatti() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/contatti/getcontatto",
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getContatti = getContatti;
function createUser(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/login/createuser",
            data: Object.assign({}, args, { password: md5(args.password) }),
            success: data => {
                resolve(data);
            }
        });
    });
}
exports.createUser = createUser;
function changeUser(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/login/changeuser",
            data: args,
            success: data => {
                resolve(data);
            }
        });
    });
}
exports.changeUser = changeUser;

},{"md5":98}],7:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function putImage(image) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/images/put/",
            data: {
                base64Image: image
            },
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.putImage = putImage;

},{}],8:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getLanding(slug) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "GET",
            url: "/api/v1/landing/landing/" + slug,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getLanding = getLanding;
function getAllLanding() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/landing/landing",
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getAllLanding = getAllLanding;
function setLanding(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/landing/setlanding",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.setLanding = setLanding;
function changeLanding(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/landing/changelanding",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.changeLanding = changeLanding;
function deleteLanding(_id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/landing/deletelanding",
            data: { _id },
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.deleteLanding = deleteLanding;

},{}],9:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function sendMail(data) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/mail/sendcustomdata",
            data,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.sendMail = sendMail;

},{}],10:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function putImage(url) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/upload/migrate",
            data: { url },
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.putImage = putImage;
function getUtils() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/utils/utils",
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getUtils = getUtils;
function getUtenti() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/utente/getutente",
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getUtenti = getUtenti;
function getRegioni() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/regioni/regioni",
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getRegioni = getRegioni;
function getUffici() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/office/getufficio",
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getUffici = getUffici;
function getProvince() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/regioni/province/",
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getProvince = getProvince;
function getNews() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/news/news",
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getNews = getNews;
function getAllLanding() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/landing/landing",
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getAllLanding = getAllLanding;
function getCabine() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/cabine",
            data: {},
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getCabine = getCabine;
function getComuni() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/comuni/comuni",
            data: {},
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getComuni = getComuni;
function getContatto() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/contatto/contatto",
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getContatto = getContatto;
function getFAQ() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/faq/faqs",
            data: {},
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getFAQ = getFAQ;

},{}],11:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getAll() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/modelle",
            //data: ts.user.read().value._id,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getAll = getAll;
function getOne(_id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/modelle/:" + _id,
            //data: ts.user.read().value._id,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getOne = getOne;

},{}],12:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getNews() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/news/news",
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getNews = getNews;
function setNews(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/news/setnews",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.setNews = setNews;
function changeNews(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/news/changenews",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.changeNews = changeNews;
function deleteNews(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/news/deletenews",
            data: { id },
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.deleteNews = deleteNews;

},{}],13:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getOfferta(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            contentType: 'application/json',
            dataType: "json",
            url: "/api/v1/offerte/getofferta",
            data: JSON.stringify(args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getOfferta = getOfferta;
function setOfferta(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/api/v1/offerte/setofferta",
            data: JSON.stringify(args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.setOfferta = setOfferta;
function changeOfferta(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/faq/changefaq",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.changeOfferta = changeOfferta;
function deleteOfferta(_id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/faq/deletefaq",
            data: { _id },
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.deleteOfferta = deleteOfferta;

},{}],14:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getRegioni() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/regioni",
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getRegioni = getRegioni;
function setRegione(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/setregione",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.setRegione = setRegione;
function changeRegione(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/changeregione",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.changeRegione = changeRegione;
function deleteRegione(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/deleteregione",
            data: { id },
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.deleteRegione = deleteRegione;

},{}],15:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getUffici() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/office/getufficio",
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getUffici = getUffici;
function setUfficio(data) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/office/setufficio",
            data,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.setUfficio = setUfficio;
function changeUfficio(data) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/office/changeufficio",
            data,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.changeUfficio = changeUfficio;
function deleteUfficio(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/office/deleteufficio",
            data: { id },
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.deleteUfficio = deleteUfficio;

},{}],16:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function putImage(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/upload/put",
            data: {
                base64Image: args.image,
                name: args.name
            },
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.putImage = putImage;
function putCsv(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/upload/putcsv",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.putCsv = putCsv;
function putXls(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/upload/putxls",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.putXls = putXls;

},{}],17:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const md5 = require("md5");
function login(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/login/login",
            data: { user: args.user, password: md5(args.password) },
            success: data => {
                resolve(data);
            }
        });
    });
}
exports.login = login;
function logout() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/login/logout",
            success: data => {
                resolve(data);
            }
        });
    });
}
exports.logout = logout;
window.doLogout = logout;
function getUtenti(args) {
    return new Promise((resolve, reject) => {
        if (args)
            $.ajax({
                type: "POST",
                url: "/api/v1/utente/getutente",
                data: args,
                success: data => resolve(data),
                error: err => reject(err)
            });
        else
            $.ajax({
                type: "POST",
                url: "/api/v1/utente/getutente",
                success: data => resolve(data),
                error: err => reject(err)
            });
    });
}
exports.getUtenti = getUtenti;
function setUtente(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/utente/setutente",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.setUtente = setUtente;
function changeUtente(args) {
    if (args.password)
        args.password = md5(args.password);
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/utente/changeutente",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.changeUtente = changeUtente;
function deleteUtente(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/utente/deleteutente",
            data: { id },
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.deleteUtente = deleteUtente;

},{"md5":98}],18:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getUtils() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/utils/utils",
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.getUtils = getUtils;
function setUtils(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/utils/setutils",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.setUtils = setUtils;
function changeUtils(args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/utils/changeutils",
            data: Object.assign({}, args),
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.changeUtils = changeUtils;
function deleteUtils(_id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/utils/deleteutils",
            data: { _id },
            success: data => resolve(data),
            error: err => reject(err)
        });
    });
}
exports.deleteUtils = deleteUtils;

},{}],19:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const footer = require("./footer");
exports.footer = footer;
const menu = require("./menu");
exports.menu = menu;

},{"./footer":20,"./menu":21}],20:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const forms = require("../pages/form");
const shards = require("../shards/_lib");
function footer() {
    const wrapper = tags.div().addClass("yca-footer").attr({ id: "yca-footer" });
    const f1 = tags.div();
    const f2 = tags.div();
    const f3 = tags.div();
    const f4 = tags.div();
    f1.append(tags.h1().html("azienda").addClass("yca-footer-title"), tags.img().attr({ src: "/img/logo.jpg" }).css({ maxWidth: "50%" }), tags.p().addClass("yca-footer-small").html(`Tecniconsul Energia srl<br/>Via Gandhi, 22<br/>42123 Reggio emilia<br/>Tel. 0522 322031<br/>Fax 0522 376213<br/>Registro Imprese RE<br/>p. IVA 02400570350<br />Società sottoposta a controllo e direzione di Canarbino SpA`), tags.p().addClass("yca-footer-big").html(`- <a href="/utils/chisiamo">Chi siamo</a>`), tags.p().addClass("yca-footer-big").html(`- <a href="/utils/privacy">Informativa sulla privacy</a>`), tags.p().addClass("yca-footer-big").html(`- <a target="_blank" href="/pdf/Codice-Etico-Tecniconsul-Energia.pdf">Codice etico</a>`), tags.p().addClass("yca-footer-big").html(`- <a href="/utils/livelliqualita">Livelli di qualità commerciale</a>`));
    f2.append(tags.h1().html("le nostre offerte").addClass("yca-footer-title"), tags.p().addClass("yca-footer-big").html(`Casa`).append(tags.p().addClass("yca-footer-small").html(`<a href="/dettagli/luce">LUCE</a>`).css({ padding: "0 0 0 10px" }), tags.p().addClass("yca-footer-small").html(`<a href="/dettagli/gas">GAS</a>`).css({ padding: "0 0 0 10px" })), tags.p().addClass("yca-footer-big").html(`Azienda`).append(tags.p().addClass("yca-footer-small").html(`<a href="/dettagli/luceazienda">LUCE</a>`).css({ padding: "0 0 0 10px" }), tags.p().addClass("yca-footer-small").html(`<a href="/dettagli/gasazienda">GAS</a>`).css({ padding: "0 0 0 10px" })), tags.p().addClass("yca-footer-big").html(`<a href="/dettagli/placetluce">Tariffa placet luce</a>`), tags.p().addClass("yca-footer-big").html(`<a href="/dettagli/placetgas">Tariffa placet gas</a>`), tags.p().addClass("yca-footer-big").html(`<a href="/territori">Tariffa di tutela gas</a>`));
    const vieniConNoi = forms.formVieniConNoi();
    const myModal = shards.modal.panel(vieniConNoi);
    f3.append(myModal.el, tags.h1().html("il mio contratto").addClass("yca-footer-title"), tags.p().append(tags.p().addClass("yca-footer-big").append(tags.a().html(`- Informazioni utili`).attr({ href: "/faq/infoutili" })), tags.p().addClass("yca-footer-big").append(tags.a().html(`- Dati necessari`).attr({ href: "/faq/datinecessari" })), tags.p().addClass("yca-footer-big").append(tags.a().html(`- Modalità di pagamento`).attr({ href: "/faq/modalitapagamento" })), tags.p().addClass("yca-footer-big").append(tags.a().html(`- Bonus gas`).attr({ href: "/faq/bonusgas" })), tags.p().addClass("yca-footer-big").append(tags.a().html(`- Fatti chiamare`).click(() => {
        myModal.show();
    }), tags.br(), tags.br(), tags.br(), tags.br())));
    f4.append(tags.h1().html("modulistica").addClass("yca-footer-title"), tags.p().append(tags.p().addClass("yca-footer-big").append(tags.a().html(`- Accesso dati Del.AEGG n.157`).attr({ href: "/pdf/Deliberadel15707_100218113906.pdf", target: "_blank" })), tags.p().addClass("yca-footer-big").append(tags.a().html(`- Normativa di riferimento gas`).attr({ href: "https://www.arera.it/it/operatori/gas_testintegrati.htm" })), tags.p().addClass("yca-footer-big").append(tags.a().html(`- Normativa di riferimento luce`).attr({ href: "https://www.arera.it/it/operatori/ele_%20testintegrati.htm" })), tags.p().addClass("yca-footer-big").append(tags.a().html(`- Assicurazioni clienti finali`).attr({ href: "/utils/assicurazioniclienti" })), tags.p().addClass("yca-footer-big").append(tags.a().html(`- Reclami`).attr({ href: "/pdf/ModelloReclamo_TE_ReggioEmilia_100222122723.pdf", target: "_blank" }), tags.br(), tags.br(), tags.br(), tags.br(), tags.br(), tags.br(), tags.br()), tags.p().addClass("yca-footer-big").append(tags.a().html(`- Numero verde`).attr({ href: "tel:800589579" })), tags.p().addClass("yca-footer-big").append(tags.a().html(`- Contattaci`).attr({ href: "/contatti" }))));
    return wrapper.append(f1, f2, f3, f4);
}
exports.footer = footer;

},{"../pages/form":57,"../shards/_lib":75,"../tags":90}],21:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const shards = require("../shards/_lib");
const cookies = require("../cookies");
const utils = require("../utils");
const forms = require("../pages/form");
function menu() {
    const loggedIn = cookies.getCookie("user");
    const myModal = forms.buildLoginModal();
    const menuEls = [
        { image: "/img/logo.jpg", path: "/", class: "hamburger" },
        loggedIn ? { text: "Clienti", path: "/clienti" } : { text: "Clienti", callback: myModal.show },
        { text: "Informazioni", path: "/faq" },
        { text: "Contatti", path: "/contatti" },
        { image: "/img/numeroverde.png", path: "tel:800589579", class: "yca-menu-green-number" },
    ];
    const wrapper = tags.div().addClass("yca-menu-wrapper");
    const menuDiv = tags.div().addClass("yca-menu-wrapper-menu");
    for (let element of menuEls)
        if (element.text)
            menuDiv.append(tags.div().addClass("yca-menu-wrapper-menu-element").append(tags.a().html(element.text).attr("href", element.path).addClass(element.class).click(element.callback)));
        else if (element.image)
            menuDiv.append(tags.div().addClass("yca-menu-wrapper-menu-element").append(tags.a().attr("href", element.path).append(tags.img().attr("src", element.image).addClass(element.class))));
    return wrapper.append(menuDiv, myModal.el);
}
exports.menu = menu;
function mobile() {
    const loggedIn = cookies.getCookie("user");
    const prefix = "os-header";
    const startPos = { x: 0, y: 0 };
    let closingMenu = false;
    let openingMenu = false;
    const prefix2 = prefix + "-navmobile";
    const wrapper = tags.div().addClass(prefix2);
    const container = tags.div().addClass(prefix2 + "-container");
    const hamburger = shards.anchor.full({ image: "/img/hamburger.png", type: "light" }).addClass(prefix2 + "-hamburger").on("click", openMenu);
    const logo = tags.div().addClass(prefix2 + "-logo").css({
        backgroundImage: "url('/img/logo.jpg')",
        backgroundSize: "contain",
        backgroundPosition: "center",
        position: "absolute",
        top: "0",
        left: "0",
        height: "100%",
        width: "calc(100% - 200px)",
        backgroundRepeat: "no-repeat",
        margin: "0 100px"
    });
    const header = tags.div().addClass(prefix2 + "-header");
    const body = tags.div().addClass(prefix2 + "-body");
    container[0].addEventListener("touchstart", (e) => {
        startPos.x = e.touches[0].pageX;
        startPos.y = e.touches[0].pageY;
    });
    container[0].addEventListener("touchmove", (e) => {
        const nowX = e.changedTouches[e.changedTouches.length - 1].pageX;
        const nowY = e.changedTouches[e.changedTouches.length - 1].pageY;
        if (startPos.x > nowX && startPos.x - nowX > 60 && startPos.x - nowX > (startPos.y - nowY) * 2)
            closeMenu();
    });
    container.click((event) => {
        if (!$(event.target).closest(body).length || !(container.children("div").length > 0))
            closeMenu();
    });
    function openMenu() {
        if (openingMenu || closingMenu)
            return;
        openingMenu = true;
        $(document.body).css({ overflow: "hidden" });
        container.fadeIn(500, () => {
            openingMenu = false;
        });
        body.animate({
            left: "0"
        }, 500, () => {
            openingMenu = false;
        });
    }
    function closeMenu() {
        if (openingMenu || closingMenu)
            return;
        closingMenu = true;
        body.animate({
            left: "-300px"
        }, 500, () => {
            container.fadeOut(500, () => {
                $(document.body).css({ overflow: "visible" });
                closingMenu = false;
            });
        });
    }
    wrapper.append(hamburger, logo, container.append(body.append(header)));
    const myModal = forms.buildLoginModal();
    wrapper.append(myModal.el);
    const menuEls = [
        { text: "Home", path: "/" },
        loggedIn ? { text: "Clienti", path: "/clienti" } : { text: "Clienti", callback: myModal.show },
        { text: "Informazioni", path: "/faq" },
        { text: "Contatti", path: "/contatti" },
        { image: "/img/numeroverde.png", path: "tel:800589579", class: "yca-menu-green-number" },
    ];
    for (const element of menuEls) {
        // if (element.image && !element.submenu)
        //     continue;
        const db = element.callback ?
            shards.anchor.full({
                text: element.text,
                type: "light",
                action: () => {
                    closeMenu();
                    element.callback();
                }
            }) :
            shards.anchor.full({ text: element.text, type: "light", url: element.path });
        // if (element.active)
        //     db.addClass(prefix2 + "-nav-element-active")
        body.append(db.addClass("os-hq-menu-submenu-element"));
    }
    return wrapper;
}
exports.mobile = mobile;
function menuBackend() {
    const user = utils.userManager().get();
    const menuAdmin = [
        { text: "Add Faq", path: "/amministrazione/addfaq" },
        { text: "Change Faq", path: "/amministrazione/changefaq" },
        { text: "Add Regione", path: "/amministrazione/addregione" },
        { text: "Change Regione", path: "/amministrazione/changeregione" },
        { text: "Add Provincia", path: "/amministrazione/addprovincia" },
        { text: "Change Provincia", path: "/amministrazione/changeprovincia" },
        { text: "Add Comune", path: "/amministrazione/addcomune" },
        { text: "Change Comune", path: "/amministrazione/changecomune" },
        { text: "Add Cabina", path: "/amministrazione/addcabina" },
        { text: "Change Cabina", path: "/amministrazione/changecabina" },
        { text: "Add Listino", path: "/amministrazione/addlistino" },
        //{ text: "Change Listino", path: "/amministrazione/changelistino" },
        { text: "Add Ufficio", path: "/amministrazione/addufficio" },
        { text: "Change Ufficio", path: "/amministrazione/changeufficio" },
        { text: "Add Utils", path: "/amministrazione/addutils" },
        { text: "Change Utils", path: "/amministrazione/changeutils" },
        { text: "Add Landing", path: "/amministrazione/addlanding" },
        { text: "Change Landing", path: "/amministrazione/changelanding" },
        { text: "Add News", path: "/amministrazione/addnews" },
        { text: "Change News", path: "/amministrazione/changenews" },
        { text: "Add User", path: "/amministrazione/addusers" },
        { text: "Change User", path: "/amministrazione/changeusers" },
        { text: "Modifica offerte", path: "/amministrazione/addOfferte" }
    ];
    const wrapper = tags.div().addClass("yca-menu-wrapper");
    if (user.isAdmin)
        for (let element of menuAdmin) {
            if (element.path)
                wrapper.append(shards.anchor.full({ text: element.text, url: element.path }).addClass("yca-menu-buttons"));
            if (element.callback)
                wrapper.append(shards.anchor.full({ text: element.text, action: element.callback }).addClass("yca-menu-buttons"));
        }
    return wrapper;
}
exports.menuBackend = menuBackend;

},{"../cookies":23,"../pages/form":57,"../shards/_lib":75,"../tags":90,"../utils":94}],22:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getFromID(id) {
    let i = id;
    if (typeof id === "number")
        i = i.toString();
    for (const p of exports.province.province) {
        if (p.id == i)
            return p;
    }
}
exports.getFromID = getFromID;
exports.province = {
    province: [{
            "id": "84",
            "id_regione": "19",
            "codice_citta_metropolitana": null,
            "nome": "Agrigento",
            "sigla_automobilistica": "AG",
            "latitudine": 37.31109,
            "longitudine": 13.576548
        }, {
            "id": "6",
            "id_regione": "1",
            "codice_citta_metropolitana": null,
            "nome": "Alessandria",
            "sigla_automobilistica": "AL",
            "latitudine": 44.817559,
            "longitudine": 8.704663
        }, {
            "id": "42",
            "id_regione": "11",
            "codice_citta_metropolitana": null,
            "nome": "Ancona",
            "sigla_automobilistica": "AN",
            "latitudine": 43.549325,
            "longitudine": 13.266348
        }, {
            "id": "51",
            "id_regione": "9",
            "codice_citta_metropolitana": null,
            "nome": "Arezzo",
            "sigla_automobilistica": "AR",
            "latitudine": 43.466896,
            "longitudine": 11.88236
        }, {
            "id": "44",
            "id_regione": "11",
            "codice_citta_metropolitana": null,
            "nome": "Ascoli Piceno",
            "sigla_automobilistica": "AP",
            "latitudine": 42.863893,
            "longitudine": 13.589973
        }, {
            "id": "5",
            "id_regione": "1",
            "codice_citta_metropolitana": null,
            "nome": "Asti",
            "sigla_automobilistica": "AT",
            "latitudine": 44.900765,
            "longitudine": 8.206432
        }, {
            "id": "64",
            "id_regione": "15",
            "codice_citta_metropolitana": null,
            "nome": "Avellino",
            "sigla_automobilistica": "AV",
            "latitudine": 40.996451,
            "longitudine": 15.125896
        }, {
            "id": "72",
            "id_regione": "16",
            "codice_citta_metropolitana": "272",
            "nome": "Bari",
            "sigla_automobilistica": "BA",
            "latitudine": 41.117123,
            "longitudine": 16.871976
        }, {
            "id": "110",
            "id_regione": "16",
            "codice_citta_metropolitana": null,
            "nome": "Barletta-Andria-Trani",
            "sigla_automobilistica": "BT",
            "latitudine": 41.200454,
            "longitudine": 16.205148
        }, {
            "id": "25",
            "id_regione": "5",
            "codice_citta_metropolitana": null,
            "nome": "Belluno",
            "sigla_automobilistica": "BL",
            "latitudine": 46.249766,
            "longitudine": 12.196957
        }, {
            "id": "62",
            "id_regione": "15",
            "codice_citta_metropolitana": null,
            "nome": "Benevento",
            "sigla_automobilistica": "BN",
            "latitudine": 41.203509,
            "longitudine": 14.752094
        }, {
            "id": "16",
            "id_regione": "3",
            "codice_citta_metropolitana": null,
            "nome": "Bergamo",
            "sigla_automobilistica": "BG",
            "latitudine": 45.85783,
            "longitudine": 9.881998
        }, {
            "id": "96",
            "id_regione": "1",
            "codice_citta_metropolitana": null,
            "nome": "Biella",
            "sigla_automobilistica": "BI",
            "latitudine": 45.562818,
            "longitudine": 8.058272
        }, {
            "id": "37",
            "id_regione": "8",
            "codice_citta_metropolitana": "237",
            "nome": "Bologna",
            "sigla_automobilistica": "BO",
            "latitudine": 44.50051,
            "longitudine": 11.304784
        }, {
            "id": "21",
            "id_regione": "4",
            "codice_citta_metropolitana": null,
            "nome": "Bolzano/Bozen",
            "sigla_automobilistica": "BZ",
            "latitudine": 46.734096,
            "longitudine": 11.288802
        }, {
            "id": "17",
            "id_regione": "3",
            "codice_citta_metropolitana": null,
            "nome": "Brescia",
            "sigla_automobilistica": "BS",
            "latitudine": 45.659677,
            "longitudine": 10.385672
        }, {
            "id": "74",
            "id_regione": "16",
            "codice_citta_metropolitana": null,
            "nome": "Brindisi",
            "sigla_automobilistica": "BR",
            "latitudine": 40.611266,
            "longitudine": 17.763621
        }, {
            "id": "92",
            "id_regione": "20",
            "codice_citta_metropolitana": null,
            "nome": "Cagliari",
            "sigla_automobilistica": "CA",
            "latitudine": 39.223763,
            "longitudine": 9.121867
        }, {
            "id": "85",
            "id_regione": "19",
            "codice_citta_metropolitana": null,
            "nome": "Caltanissetta",
            "sigla_automobilistica": "CL",
            "latitudine": 37.490112,
            "longitudine": 14.062893
        }, {
            "id": "70",
            "id_regione": "14",
            "codice_citta_metropolitana": null,
            "nome": "Campobasso",
            "sigla_automobilistica": "CB",
            "latitudine": 41.673887,
            "longitudine": 14.752094
        }, {
            "id": "107",
            "id_regione": "20",
            "codice_citta_metropolitana": null,
            "nome": "Carbonia-Iglesias",
            "sigla_automobilistica": "CI",
            "latitudine": 39.253466,
            "longitudine": 8.572102
        }, {
            "id": "61",
            "id_regione": "15",
            "codice_citta_metropolitana": null,
            "nome": "Caserta",
            "sigla_automobilistica": "CE",
            "latitudine": 41.207835,
            "longitudine": 14.100133
        }, {
            "id": "87",
            "id_regione": "19",
            "codice_citta_metropolitana": null,
            "nome": "Catania",
            "sigla_automobilistica": "CT",
            "latitudine": 37.612598,
            "longitudine": 14.938885
        }, {
            "id": "79",
            "id_regione": "18",
            "codice_citta_metropolitana": null,
            "nome": "Catanzaro",
            "sigla_automobilistica": "CZ",
            "latitudine": 38.889635,
            "longitudine": 16.440587
        }, {
            "id": "69",
            "id_regione": "13",
            "codice_citta_metropolitana": null,
            "nome": "Chieti",
            "sigla_automobilistica": "CH",
            "latitudine": 42.033443,
            "longitudine": 14.379191
        }, {
            "id": "13",
            "id_regione": "3",
            "codice_citta_metropolitana": null,
            "nome": "Como",
            "sigla_automobilistica": "CO",
            "latitudine": 45.808042,
            "longitudine": 9.085179
        }, {
            "id": "78",
            "id_regione": "18",
            "codice_citta_metropolitana": null,
            "nome": "Cosenza",
            "sigla_automobilistica": "CS",
            "latitudine": 39.564411,
            "longitudine": 16.252214
        }, {
            "id": "19",
            "id_regione": "3",
            "codice_citta_metropolitana": null,
            "nome": "Cremona",
            "sigla_automobilistica": "CR",
            "latitudine": 45.201438,
            "longitudine": 9.983658
        }, {
            "id": "101",
            "id_regione": "18",
            "codice_citta_metropolitana": null,
            "nome": "Crotone",
            "sigla_automobilistica": "KR",
            "latitudine": 39.130986,
            "longitudine": 17.006703
        }, {
            "id": "4",
            "id_regione": "1",
            "codice_citta_metropolitana": null,
            "nome": "Cuneo",
            "sigla_automobilistica": "CN",
            "latitudine": 44.597031,
            "longitudine": 7.611422
        }, {
            "id": "86",
            "id_regione": "19",
            "codice_citta_metropolitana": null,
            "nome": "Enna",
            "sigla_automobilistica": "EN",
            "latitudine": 37.516481,
            "longitudine": 14.379191
        }, {
            "id": "109",
            "id_regione": "11",
            "codice_citta_metropolitana": null,
            "nome": "Fermo",
            "sigla_automobilistica": "FM",
            "latitudine": 43.093137,
            "longitudine": 13.589973
        }, {
            "id": "38",
            "id_regione": "8",
            "codice_citta_metropolitana": null,
            "nome": "Ferrara",
            "sigla_automobilistica": "FE",
            "latitudine": 44.766368,
            "longitudine": 11.764407
        }, {
            "id": "48",
            "id_regione": "9",
            "codice_citta_metropolitana": "248",
            "nome": "Firenze",
            "sigla_automobilistica": "FI",
            "latitudine": 43.767918,
            "longitudine": 11.252379
        }, {
            "id": "71",
            "id_regione": "16",
            "codice_citta_metropolitana": null,
            "nome": "Foggia",
            "sigla_automobilistica": "FG",
            "latitudine": 41.638448,
            "longitudine": 15.594339
        }, {
            "id": "40",
            "id_regione": "8",
            "codice_citta_metropolitana": null,
            "nome": "Forlì-Cesena",
            "sigla_automobilistica": "FC",
            "latitudine": 44.2225,
            "longitudine": 12.040833
        }, {
            "id": "60",
            "id_regione": "12",
            "codice_citta_metropolitana": null,
            "nome": "Frosinone",
            "sigla_automobilistica": "FR",
            "latitudine": 41.657653,
            "longitudine": 13.636272
        }, {
            "id": "10",
            "id_regione": "7",
            "codice_citta_metropolitana": "210",
            "nome": "Genova",
            "sigla_automobilistica": "GE",
            "latitudine": 44.446625,
            "longitudine": 9.145615
        }, {
            "id": "31",
            "id_regione": "6",
            "codice_citta_metropolitana": null,
            "nome": "Gorizia",
            "sigla_automobilistica": "GO",
            "latitudine": 45.90539,
            "longitudine": 13.516373
        }, {
            "id": "53",
            "id_regione": "9",
            "codice_citta_metropolitana": null,
            "nome": "Grosseto",
            "sigla_automobilistica": "GR",
            "latitudine": 42.851801,
            "longitudine": 11.252379
        }, {
            "id": "8",
            "id_regione": "7",
            "codice_citta_metropolitana": null,
            "nome": "Imperia",
            "sigla_automobilistica": "IM",
            "latitudine": 43.941866,
            "longitudine": 7.828637
        }, {
            "id": "94",
            "id_regione": "14",
            "codice_citta_metropolitana": null,
            "nome": "Isernia",
            "sigla_automobilistica": "IS",
            "latitudine": 41.589156,
            "longitudine": 14.193092
        }, {
            "id": "66",
            "id_regione": "13",
            "codice_citta_metropolitana": null,
            "nome": "L'Aquila",
            "sigla_automobilistica": "AQ",
            "latitudine": 42.349848,
            "longitudine": 13.399509
        }, {
            "id": "11",
            "id_regione": "7",
            "codice_citta_metropolitana": null,
            "nome": "La Spezia",
            "sigla_automobilistica": "SP",
            "latitudine": 44.10245,
            "longitudine": 9.824083
        }, {
            "id": "59",
            "id_regione": "12",
            "codice_citta_metropolitana": null,
            "nome": "Latina",
            "sigla_automobilistica": "LT",
            "latitudine": 41.408748,
            "longitudine": 13.08179
        }, {
            "id": "75",
            "id_regione": "16",
            "codice_citta_metropolitana": null,
            "nome": "Lecce",
            "sigla_automobilistica": "LE",
            "latitudine": 40.234739,
            "longitudine": 18.142867
        }, {
            "id": "97",
            "id_regione": "3",
            "codice_citta_metropolitana": null,
            "nome": "Lecco",
            "sigla_automobilistica": "LC",
            "latitudine": 45.938294,
            "longitudine": 9.385729
        }, {
            "id": "49",
            "id_regione": "9",
            "codice_citta_metropolitana": null,
            "nome": "Livorno",
            "sigla_automobilistica": "LI",
            "latitudine": 43.023985,
            "longitudine": 10.66471
        }, {
            "id": "98",
            "id_regione": "3",
            "codice_citta_metropolitana": null,
            "nome": "Lodi",
            "sigla_automobilistica": "LO",
            "latitudine": 45.240504,
            "longitudine": 9.529251
        }, {
            "id": "46",
            "id_regione": "9",
            "codice_citta_metropolitana": null,
            "nome": "Lucca",
            "sigla_automobilistica": "LU",
            "latitudine": 43.837674,
            "longitudine": 10.495053
        }, {
            "id": "43",
            "id_regione": "11",
            "codice_citta_metropolitana": null,
            "nome": "Macerata",
            "sigla_automobilistica": "MC",
            "latitudine": 43.245932,
            "longitudine": 13.266348
        }, {
            "id": "20",
            "id_regione": "3",
            "codice_citta_metropolitana": null,
            "nome": "Mantova",
            "sigla_automobilistica": "MN",
            "latitudine": 45.156417,
            "longitudine": 10.791375
        }, {
            "id": "45",
            "id_regione": "9",
            "codice_citta_metropolitana": null,
            "nome": "Massa-Carrara",
            "sigla_automobilistica": "MS",
            "latitudine": 44.079325,
            "longitudine": 10.097677
        }, {
            "id": "77",
            "id_regione": "17",
            "codice_citta_metropolitana": null,
            "nome": "Matera",
            "sigla_automobilistica": "MT",
            "latitudine": 40.66635,
            "longitudine": 16.604364
        }, {
            "id": "106",
            "id_regione": "20",
            "codice_citta_metropolitana": null,
            "nome": "Medio Campidano",
            "sigla_automobilistica": "VS",
            "latitudine": 39.531739,
            "longitudine": 8.704075
        }, {
            "id": "83",
            "id_regione": "19",
            "codice_citta_metropolitana": null,
            "nome": "Messina",
            "sigla_automobilistica": "ME",
            "latitudine": 38.06324,
            "longitudine": 14.985618
        }, {
            "id": "15",
            "id_regione": "3",
            "codice_citta_metropolitana": "215",
            "nome": "Milano",
            "sigla_automobilistica": "MI",
            "latitudine": 45.458626,
            "longitudine": 9.181873
        }, {
            "id": "36",
            "id_regione": "8",
            "codice_citta_metropolitana": null,
            "nome": "Modena",
            "sigla_automobilistica": "MO",
            "latitudine": 44.55138,
            "longitudine": 10.918048
        }, {
            "id": "108",
            "id_regione": "3",
            "codice_citta_metropolitana": null,
            "nome": "Monza e della Brianza",
            "sigla_automobilistica": "MB",
            "latitudine": 45.623599,
            "longitudine": 9.258802
        }, {
            "id": "63",
            "id_regione": "15",
            "codice_citta_metropolitana": "263",
            "nome": "Napoli",
            "sigla_automobilistica": "NA",
            "latitudine": 40.901975,
            "longitudine": 14.332644
        }, {
            "id": "3",
            "id_regione": "1",
            "codice_citta_metropolitana": null,
            "nome": "Novara",
            "sigla_automobilistica": "NO",
            "latitudine": 45.548513,
            "longitudine": 8.515079
        }, {
            "id": "91",
            "id_regione": "20",
            "codice_citta_metropolitana": null,
            "nome": "Nuoro",
            "sigla_automobilistica": "NU",
            "latitudine": 40.32869,
            "longitudine": 9.456155
        }, {
            "id": "105",
            "id_regione": "20",
            "codice_citta_metropolitana": null,
            "nome": "Ogliastra",
            "sigla_automobilistica": "OG",
            "latitudine": 39.841054,
            "longitudine": 9.456155
        }, {
            "id": "104",
            "id_regione": "20",
            "codice_citta_metropolitana": null,
            "nome": "Olbia-Tempio",
            "sigla_automobilistica": "OT",
            "latitudine": 40.826838,
            "longitudine": 9.278558
        }, {
            "id": "95",
            "id_regione": "20",
            "codice_citta_metropolitana": null,
            "nome": "Oristano",
            "sigla_automobilistica": "OR",
            "latitudine": 40.059907,
            "longitudine": 8.748117
        }, {
            "id": "28",
            "id_regione": "5",
            "codice_citta_metropolitana": null,
            "nome": "Padova",
            "sigla_automobilistica": "PD",
            "latitudine": 45.366186,
            "longitudine": 11.820914
        }, {
            "id": "82",
            "id_regione": "19",
            "codice_citta_metropolitana": null,
            "nome": "Palermo",
            "sigla_automobilistica": "PA",
            "latitudine": 38.115621,
            "longitudine": 13.361318
        }, {
            "id": "34",
            "id_regione": "8",
            "codice_citta_metropolitana": null,
            "nome": "Parma",
            "sigla_automobilistica": "PR",
            "latitudine": 44.801532,
            "longitudine": 10.327935
        }, {
            "id": "18",
            "id_regione": "3",
            "codice_citta_metropolitana": null,
            "nome": "Pavia",
            "sigla_automobilistica": "PV",
            "latitudine": 45.321817,
            "longitudine": 8.846624
        }, {
            "id": "54",
            "id_regione": "10",
            "codice_citta_metropolitana": null,
            "nome": "Perugia",
            "sigla_automobilistica": "PG",
            "latitudine": 42.938004,
            "longitudine": 12.621621
        }, {
            "id": "41",
            "id_regione": "11",
            "codice_citta_metropolitana": null,
            "nome": "Pesaro e Urbino",
            "sigla_automobilistica": "PU",
            "latitudine": 43.613012,
            "longitudine": 12.713512
        }, {
            "id": "68",
            "id_regione": "13",
            "codice_citta_metropolitana": null,
            "nome": "Pescara",
            "sigla_automobilistica": "PE",
            "latitudine": 42.357066,
            "longitudine": 13.960809
        }, {
            "id": "33",
            "id_regione": "8",
            "codice_citta_metropolitana": null,
            "nome": "Piacenza",
            "sigla_automobilistica": "PC",
            "latitudine": 44.826311,
            "longitudine": 9.529145
        }, {
            "id": "50",
            "id_regione": "9",
            "codice_citta_metropolitana": null,
            "nome": "Pisa",
            "sigla_automobilistica": "PI",
            "latitudine": 43.722832,
            "longitudine": 10.401719
        }, {
            "id": "47",
            "id_regione": "9",
            "codice_citta_metropolitana": null,
            "nome": "Pistoia",
            "sigla_automobilistica": "PT",
            "latitudine": 43.954373,
            "longitudine": 10.89031
        }, {
            "id": "93",
            "id_regione": "6",
            "codice_citta_metropolitana": null,
            "nome": "Pordenone",
            "sigla_automobilistica": "PN",
            "latitudine": 46.037886,
            "longitudine": 12.710835
        }, {
            "id": "76",
            "id_regione": "17",
            "codice_citta_metropolitana": null,
            "nome": "Potenza",
            "sigla_automobilistica": "PZ",
            "latitudine": 40.418219,
            "longitudine": 15.876004
        }, {
            "id": "100",
            "id_regione": "9",
            "codice_citta_metropolitana": null,
            "nome": "Prato",
            "sigla_automobilistica": "PO",
            "latitudine": 44.04539,
            "longitudine": 11.116445
        }, {
            "id": "88",
            "id_regione": "19",
            "codice_citta_metropolitana": null,
            "nome": "Ragusa",
            "sigla_automobilistica": "RG",
            "latitudine": 36.930622,
            "longitudine": 14.705431
        }, {
            "id": "39",
            "id_regione": "8",
            "codice_citta_metropolitana": null,
            "nome": "Ravenna",
            "sigla_automobilistica": "RA",
            "latitudine": 44.418444,
            "longitudine": 12.2036
        }, {
            "id": "80",
            "id_regione": "18",
            "codice_citta_metropolitana": null,
            "nome": "Reggio di Calabria",
            "sigla_automobilistica": "RC",
            "latitudine": 38.111301,
            "longitudine": 15.647291
        }, {
            "id": "35",
            "id_regione": "8",
            "codice_citta_metropolitana": null,
            "nome": "Reggio nell'Emilia",
            "sigla_automobilistica": "RE",
            "latitudine": 44.585658,
            "longitudine": 10.556474
        }, {
            "id": "57",
            "id_regione": "12",
            "codice_citta_metropolitana": null,
            "nome": "Rieti",
            "sigla_automobilistica": "RI",
            "latitudine": 42.367441,
            "longitudine": 12.89751
        }, {
            "id": "99",
            "id_regione": "8",
            "codice_citta_metropolitana": null,
            "nome": "Rimini",
            "sigla_automobilistica": "RN",
            "latitudine": 43.967605,
            "longitudine": 12.575703
        }, {
            "id": "58",
            "id_regione": "12",
            "codice_citta_metropolitana": "258",
            "nome": "Roma",
            "sigla_automobilistica": "RM",
            "latitudine": 41.872411,
            "longitudine": 12.480225
        }, {
            "id": "29",
            "id_regione": "5",
            "codice_citta_metropolitana": null,
            "nome": "Rovigo",
            "sigla_automobilistica": "RO",
            "latitudine": 45.024182,
            "longitudine": 11.823816
        }, {
            "id": "65",
            "id_regione": "15",
            "codice_citta_metropolitana": null,
            "nome": "Salerno",
            "sigla_automobilistica": "SA",
            "latitudine": 40.428783,
            "longitudine": 15.219481
        }, {
            "id": "90",
            "id_regione": "20",
            "codice_citta_metropolitana": null,
            "nome": "Sassari",
            "sigla_automobilistica": "SS",
            "latitudine": 40.796791,
            "longitudine": 8.575041
        }, {
            "id": "9",
            "id_regione": "7",
            "codice_citta_metropolitana": null,
            "nome": "Savona",
            "sigla_automobilistica": "SV",
            "latitudine": 44.2888,
            "longitudine": 8.265058
        }, {
            "id": "52",
            "id_regione": "9",
            "codice_citta_metropolitana": null,
            "nome": "Siena",
            "sigla_automobilistica": "SI",
            "latitudine": 43.293773,
            "longitudine": 11.433915
        }, {
            "id": "89",
            "id_regione": "19",
            "codice_citta_metropolitana": null,
            "nome": "Siracusa",
            "sigla_automobilistica": "SR",
            "latitudine": 37.075437,
            "longitudine": 15.286593
        }, {
            "id": "14",
            "id_regione": "3",
            "codice_citta_metropolitana": null,
            "nome": "Sondrio",
            "sigla_automobilistica": "SO",
            "latitudine": 46.172764,
            "longitudine": 9.799492
        }, {
            "id": "73",
            "id_regione": "16",
            "codice_citta_metropolitana": null,
            "nome": "Taranto",
            "sigla_automobilistica": "TA",
            "latitudine": 40.57409,
            "longitudine": 17.242998
        }, {
            "id": "67",
            "id_regione": "13",
            "codice_citta_metropolitana": null,
            "nome": "Teramo",
            "sigla_automobilistica": "TE",
            "latitudine": 42.589561,
            "longitudine": 13.636272
        }, {
            "id": "55",
            "id_regione": "10",
            "codice_citta_metropolitana": null,
            "nome": "Terni",
            "sigla_automobilistica": "TR",
            "latitudine": 42.563453,
            "longitudine": 12.529803
        }, {
            "id": "1",
            "id_regione": "1",
            "codice_citta_metropolitana": "201",
            "nome": "Torino",
            "sigla_automobilistica": "TO",
            "latitudine": 45.063299,
            "longitudine": 7.669289
        }, {
            "id": "81",
            "id_regione": "19",
            "codice_citta_metropolitana": null,
            "nome": "Trapani",
            "sigla_automobilistica": "TP",
            "latitudine": 37.87774,
            "longitudine": 12.713512
        }, {
            "id": "22",
            "id_regione": "4",
            "codice_citta_metropolitana": null,
            "nome": "Trento",
            "sigla_automobilistica": "TN",
            "latitudine": 46.0512,
            "longitudine": 11.117539
        }, {
            "id": "26",
            "id_regione": "5",
            "codice_citta_metropolitana": null,
            "nome": "Treviso",
            "sigla_automobilistica": "TV",
            "latitudine": 45.666852,
            "longitudine": 12.243062
        }, {
            "id": "32",
            "id_regione": "6",
            "codice_citta_metropolitana": null,
            "nome": "Trieste",
            "sigla_automobilistica": "TS",
            "latitudine": 45.689482,
            "longitudine": 13.783307
        }, {
            "id": "30",
            "id_regione": "6",
            "codice_citta_metropolitana": null,
            "nome": "Udine",
            "sigla_automobilistica": "UD",
            "latitudine": 46.140797,
            "longitudine": 13.16629
        }, {
            "id": "7",
            "id_regione": "2",
            "codice_citta_metropolitana": null,
            "nome": "Valle d'Aosta/Vallée d'Aoste",
            "sigla_automobilistica": "AO",
            "latitudine": 45.738888,
            "longitudine": 7.426187
        }, {
            "id": "12",
            "id_regione": "3",
            "codice_citta_metropolitana": null,
            "nome": "Varese",
            "sigla_automobilistica": "VA",
            "latitudine": 45.799026,
            "longitudine": 8.730095
        }, {
            "id": "27",
            "id_regione": "5",
            "codice_citta_metropolitana": "227",
            "nome": "Venezia",
            "sigla_automobilistica": "VE",
            "latitudine": 45.493048,
            "longitudine": 12.4177
        }, {
            "id": "103",
            "id_regione": "1",
            "codice_citta_metropolitana": null,
            "nome": "Verbano-Cusio-Ossola",
            "sigla_automobilistica": "VB",
            "latitudine": 46.139969,
            "longitudine": 8.272465
        }, {
            "id": "2",
            "id_regione": "1",
            "codice_citta_metropolitana": null,
            "nome": "Vercelli",
            "sigla_automobilistica": "VC",
            "latitudine": 45.32022,
            "longitudine": 8.418508
        }, {
            "id": "23",
            "id_regione": "5",
            "codice_citta_metropolitana": null,
            "nome": "Verona",
            "sigla_automobilistica": "VR",
            "latitudine": 45.44185,
            "longitudine": 11.073532
        }, {
            "id": "102",
            "id_regione": "18",
            "codice_citta_metropolitana": null,
            "nome": "Vibo Valentia",
            "sigla_automobilistica": "VV",
            "latitudine": 38.637857,
            "longitudine": 16.205148
        }, {
            "id": "24",
            "id_regione": "5",
            "codice_citta_metropolitana": null,
            "nome": "Vicenza",
            "sigla_automobilistica": "VI",
            "latitudine": 45.545479,
            "longitudine": 11.535421
        }, {
            "id": "56",
            "id_regione": "12",
            "codice_citta_metropolitana": null,
            "nome": "Viterbo",
            "sigla_automobilistica": "VT",
            "latitudine": 42.420677,
            "longitudine": 12.107669
        }]
};

},{}],23:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function setCookie(name, value, days) {
    let expires = "";
    if (days) {
        let date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toISOString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}
exports.setCookie = setCookie;
function getCookie(name) {
    let nameEQ = name + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return undefined;
}
exports.getCookie = getCookie;
function eraseCookie(name) {
    document.cookie = name + '=; Max-Age=-99999999;';
}
exports.eraseCookie = eraseCookie;

},{}],24:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });

},{}],25:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const string = require("./string");
exports.string = string;
const boolean = require("./boolean");
exports.boolean = boolean;
const radio = require("./radio");
exports.radio = radio;
const radioMulti = require("./radioMulti");
exports.radioMulti = radioMulti;
const upload = require("./upload");
exports.upload = upload;
const object = require("./object");
exports.object = object;
const number = require("./number");
exports.number = number;
const combo = require("./combo");
exports.combo = combo;
const date = require("./data");
exports.date = date;

},{"./boolean":26,"./combo":27,"./data":28,"./number":29,"./object":30,"./radio":31,"./radioMulti":32,"./string":33,"./upload":34}],26:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const models = require("../../models/_lib");
function booleanInput(args) {
    const title = tags.label().text(args.title);
    const input = tags.icheckbox();
    title.append(input, tags.span());
    const model = models.createModel({ state: "valid", value: false });
    input.on("click", () => {
        model.write({ state: "valid", value: !!input.prop('checked') });
    });
    function set(value) {
        model.write({ state: "valid", value });
    }
    model.watch(data => {
        if (data.state == "valid") {
            if (data.value)
                input.prop("checked");
            else
                input.removeProp("checked");
        }
    });
    return { model, el: title, set };
}
exports.booleanInput = booleanInput;

},{"../../models/_lib":36,"../../tags":90}],27:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const models = require("../../models/_lib");
function combo(args, visualName, valueName, values) {
    const wrapper = tags.div().append(tags.label().text(args.title));
    const comboBox = tags.select();
    const model = models.createModel(args.nullable ? { state: "valid", value: null } : { state: "invalid" });
    const trueCombo = comboBox[0];
    for (const value of values) {
        comboBox.append(tags.option().attr({ value: value[valueName] }).html(value[visualName]));
    }
    comboBox.change((e) => {
        const v = trueCombo.value;
        model.write({ state: "valid", value: v });
    });
    function set(data) {
        model.write({ state: "valid", value: data });
        comboBox.val(data);
    }
    wrapper.append(comboBox);
    return { model, el: wrapper, set };
}
exports.combo = combo;

},{"../../models/_lib":36,"../../tags":90}],28:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const models = require("../../models/_lib");
const driver = require("./number");
function date(args) {
    const yearSpan = 100;
    const completeDate = "T00:00:00.000Z";
    const me = {};
    const buildingDate = new Date();
    const minDay = 1;
    let maxDay = 31;
    const minMonth = 1;
    const maxMonth = 12;
    const minYear = buildingDate.getFullYear() - 100;
    const maxYear = buildingDate.getFullYear() + 100;
    const monthsArray = ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre"];
    const valueMod = models.createModel(args.nullable ? { state: "valid", value: null } : { state: "invalid" });
    const title = tags.label().text(args.title);
    const calendar = tags.div().addClass("os-pebble2-calendar");
    const dayDriver = driver.numberInput({ type: "number", title: "" });
    dayDriver.el.attr({ placeholder: "dd" });
    const monthDriver = driver.numberInput({ type: "number", title: "" });
    monthDriver.el.attr({ placeholder: "mm" });
    const yearDriver = driver.numberInput({ type: "number", title: "" });
    yearDriver.el.attr({ placeholder: "yyyy" });
    const calendarFields = tags.div().addClass("os-pebble2-calendar-fields");
    const testMobile = tags.span().addClass("only-mobile-test");
    const left = tags.div().addClass("os-pebble2-calendar-left");
    const right = tags.div().addClass("os-pebble2-calendar-right").append(tags.button().on("click", () => {
        setValue(null);
    }));
    function buildFields() {
        const daywrapper = tags.div().addClass("os-pebble2-calendar-fields-wrapper").addClass("flex-2");
        const dayTitle = tags.div().addClass("os-pebble2-calendar-fields-title").html("Giorno");
        const dayField = tags.div().addClass("os-pebble2-calendar-fields-field");
        daywrapper.append(dayTitle, dayField.append(dayDriver.el));
        const monthwrapper = tags.div().addClass("os-pebble2-calendar-fields-wrapper").addClass("flex-2");
        const monthTitle = tags.div().addClass("os-pebble2-calendar-fields-title").html("Mese");
        const monthField = tags.div().addClass("os-pebble2-calendar-fields-field");
        monthwrapper.append(monthTitle, monthField.append(monthDriver.el));
        const yearwrapper = tags.div().addClass("os-pebble2-calendar-fields-wrapper").addClass("flex-4");
        const yearTitle = tags.div().addClass("os-pebble2-calendar-fields-title").html("Anno");
        const yearField = tags.div().addClass("os-pebble2-calendar-fields-field");
        yearwrapper.append(yearTitle, yearField.append(yearDriver.el));
        const sep1 = tags.div().addClass("os-pebble2-calendar-fields-wrapper").addClass("flex-1").append(tags.div().addClass("os-pebble2-calendar-fields-title"), tags.div().addClass("os-pebble2-calendar-fields-separator").append(tags.p().html("/")));
        const sep2 = tags.div().addClass("os-pebble2-calendar-fields-wrapper").addClass("flex-1").append(tags.div().addClass("os-pebble2-calendar-fields-title"), tags.div().addClass("os-pebble2-calendar-fields-separator").append(tags.p().html("/")));
        return calendarFields.append(daywrapper, sep1, monthwrapper, sep2, yearwrapper);
    }
    let bufferValue = null;
    function isMobile() {
        return testMobile.is(":visible");
    }
    function setValue(value, stop) {
        valueMod.write({ state: "valid", value: value ? subsT0(value) : value });
        bufferValue = value ? subsT0(value) : value;
        if (!stop) {
            console.log("set value date", valueMod.read().value);
            const thisDate = new Date(value);
            dayDriver.set(thisDate.getDate());
            monthDriver.set((thisDate.getMonth() + 1));
            yearDriver.set(thisDate.getFullYear());
        }
    }
    dayDriver.model.watch(day => {
        console.log(day);
        if (!dayDriver.model.read().value && !monthDriver.model.read().value && !yearDriver.model.read().value) {
            console.log("was null");
            setValue(null, true);
            return;
        }
        else if (dayDriver.model.read().value && monthDriver.model.read().value && yearDriver.model.read().value) {
            const date = yearDriver.model.read().value + "-" +
                ((monthDriver.model.read().value < 10) ? "0" + monthDriver.model.read().value : monthDriver.model.read().value) + "-" +
                ((day.value < 10) ? "0" + day.value : day.value) + completeDate;
            setValue(date, true);
        }
    });
    monthDriver.model.watch(month => {
        console.log(month);
        if (!dayDriver.model.read().value && !monthDriver.model.read().value && !yearDriver.model.read().value) {
            console.log("was null");
            setValue(null, true);
            return;
        }
        else if (dayDriver.model.read().value && monthDriver.model.read().value && yearDriver.model.read().value) {
            const date = yearDriver.model.read().value + "-" +
                ((month.value < 10) ? "0" + month.value : month.value) + "-" +
                ((dayDriver.model.read().value < 10) ? "0" + dayDriver.model.read().value : dayDriver.model.read().value) + completeDate;
            setValue(date, true);
        }
    });
    yearDriver.model.watch(year => {
        console.log(year);
        if (!dayDriver.model.read().value && !monthDriver.model.read().value && !yearDriver.model.read().value) {
            console.log("was null");
            setValue(null, true);
            return;
        }
        else if (dayDriver.model.read().value && monthDriver.model.read().value && yearDriver.model.read().value) {
            const date = year.value + "-" +
                ((monthDriver.model.read().value < 10) ? "0" + monthDriver.model.read().value : monthDriver.model.read().value) + "-" +
                ((dayDriver.model.read().value < 10) ? "0" + dayDriver.model.read().value : dayDriver.model.read().value) + completeDate;
            setValue(date, true);
        }
    });
    // valueMod.watch((a) => {
    //     if (a.state == "valid") {
    //         if (a.value) {
    //             const thisDate = new Date(a.value)
    //            // buildCalendarBody(thisDate.toISOString(), calendarElement)
    //             dayDriver.model.write({ state: "valid", value: thisDate.getDate().toString() })
    //             monthDriver.model.write({ state: "valid", value: (thisDate.getMonth() + 1).toString() })
    //             yearDriver.model.write({ state: "valid", value: thisDate.getFullYear().toString() })
    //         }
    //         else {
    //             //buildCalendarBody(subsT0(new Date().toISOString()), calendarElement)
    //             dayDriver.model.write({ state: "valid", value: "" })
    //             monthDriver.model.write({ state: "valid", value: "" })
    //             yearDriver.model.write({ state: "valid", value: "" })
    //         }
    //     }
    // })
    function haveFocus() {
        if (dayDriver.el.is(":focus") || monthDriver.el.is(":focus") || yearDriver.el.is(":focus"))
            return true;
        return false;
    }
    function wrapInOrder() {
        buildFields();
        //buildCalendarBody(subsT0(new Date().toISOString()), calendarElement)
        return tags.div().append(title, calendar.append(left.append(calendarFields, testMobile), args.nullable ? right : null));
    }
    function subsT0(date) {
        const d = date.slice(0, 10);
        return d + completeDate;
    }
    return {
        el: wrapInOrder(),
        model: valueMod,
        set: setValue
    };
}
exports.date = date;

},{"../../models/_lib":36,"../../tags":90,"./number":29}],29:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const models = require("../../models/_lib");
function numberInput(args) {
    const title = tags.label().text(args.title);
    const input = tags.inumber();
    if (args.max)
        tags.inumber().attr({ max: args.max });
    if (args.min)
        tags.inumber().attr({ max: args.min });
    title.append(input);
    const model = models.createModel(args.nullable ? { state: "valid", value: null } : { state: "invalid" });
    input.on("change input", () => {
        model.write({ state: "valid", value: input.val() });
    });
    function set(val) {
        model.write({ state: "valid", value: val });
        input.val(val);
    }
    return { model, el: title, set };
}
exports.numberInput = numberInput;

},{"../../models/_lib":36,"../../tags":90}],30:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const models = require("../../models/_lib");
function objInput(i) {
    const el = tags.div();
    const model = models.createModel({ state: "invalid" });
    for (const inp in i) {
        // console.log(inp)
        // const input: core. = builder(i[inp])
        // el.append(input.el)
    }
    return { el };
}
exports.objInput = objInput;
// function builder<T>(inputDesc: core.InputArgs<T>): core.Input<T> {
//     switch (inputDesc.type) {
//         case "string":
//             return input.string.stringInput({ ...inputDesc });
//         case "password":
//             return input.string.stringInput({ ...inputDesc, password: true });
//         // case "mail":
//         //     return;
//         case "boolean":
//             return input.boolean.booleanInput({...inputDesc});
//         // case "radio":
//         //     return;
//         // case "upload":
//         //     return;
//     }
// }

},{"../../models/_lib":36,"../../tags":90}],31:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const models = require("../../models/_lib");
function radio(args) {
    const wrapper = tags.div().append(tags.label().text(args.title));
    const model = models.createModel(args.nullable ? { state: "valid", value: null } : { state: "invalid" });
    const title = tags.label();
    const radio = tags.iradio().prop({ name: args.name }).click(() => {
        model.write({ state: "valid", value: radio.prop("checked") });
    });
    wrapper.append(title.append(radio, tags.span(), args.description));
    function set(value) {
        model.write({ state: "valid", value });
    }
    model.watch(data => {
        if (data.state == "valid") {
            if (data.value)
                radio.prop("checked");
            else
                radio.removeProp("checked");
        }
    });
    return { model, el: wrapper, set };
}
exports.radio = radio;

},{"../../models/_lib":36,"../../tags":90}],32:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const models = require("../../models/_lib");
function radioMulti(args, property, values) {
    const wrapper = tags.div().append(tags.label().text(args.title));
    const input = tags.icheckbox();
    const model = models.createModel(args.nullable ? { state: "valid", value: null } : { state: "invalid" });
    for (const value of values) {
        const title = tags.label();
        const radio = tags.iradio().prop({ name: args.name }).click(() => {
            if (radio.prop("checked"))
                model.write({ state: "valid", value });
        });
        wrapper.append(title.append(radio, tags.span(), value[property]));
    }
    function set(data) { }
    return { model, el: wrapper, set };
}
exports.radioMulti = radioMulti;

},{"../../models/_lib":36,"../../tags":90}],33:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const models = require("../../models/_lib");
function stringInput(args) {
    const title = tags.label().text(args.title);
    const input = args.multiline ? tags.itextarea().attr({ rows: args.multiline }) : (args.password ? tags.ipassword() : tags.itext());
    title.append(input);
    const model = models.createModel(args.nullable ? { state: "valid", value: null } : { state: "invalid" });
    input.on("change input", () => {
        model.write({ state: "valid", value: input.val() });
    });
    function set(val) {
        model.write({ state: "valid", value: val });
    }
    model.watch(val => {
        if (val.state == "valid")
            input.val(val.value);
    });
    return { model, el: title, set };
}
exports.stringInput = stringInput;
function passwordInput(args) {
    return stringInput(Object.assign({}, args, { password: true }));
}
exports.passwordInput = passwordInput;
function multilineInput(args) {
    return stringInput(Object.assign({}, args, { multiline: 20 }));
}
exports.multilineInput = multilineInput;

},{"../../models/_lib":36,"../../tags":90}],34:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const models = require("../../models/_lib");
const api = require("../../api/_lib");
function upload(args) {
    const suffix = Math.floor(Math.random() * 1000000).toString();
    const label = tags.label().prop({ for: "buttonUpload" + suffix }).addClass("yca-custom-file-upload");
    const innerLabel = tags.span().text(args.title);
    const button = tags.ifile().attr({ id: "buttonUpload" + suffix });
    const model = models.createModel(args.nullable ? { state: "valid", value: null } : { state: "invalid" });
    if (args.files && args.files.length)
        button.attr({ accept: args.files.join(",") });
    const preview = tags.img();
    const wrapper = tags.div().append(label.append(innerLabel, preview), button);
    button.change(() => {
        var reader = new FileReader();
        const file = button[0].files[0];
        const name = file.name;
        reader.onloadend = e => {
            innerLabel.text("Uploading");
        };
        function imageIsLoaded(e) {
            preview.attr({ src: e.target.result });
            api.upload.putImage({ image: e.target.result, name }).then(val => {
                //preview.attr({ src: val.file })
                innerLabel.text("Uploaded");
                model.write({ state: "valid", value: val.url });
            });
        }
        ;
        if (file) {
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(file);
            console.log(file);
        }
        else {
            preview.attr({ src: reader.result });
        }
    });
    function set(url) {
        model.write({ state: "valid", value: url });
    }
    model.watch(data => {
        if (data.state == "valid")
            preview.attr({ src: data.value });
    });
    return { model, el: wrapper, set };
}
exports.upload = upload;
function uploadCSV(args, idCabina, type) {
    const suffix = Math.floor(Math.random() * 1000000).toString();
    const label = tags.label().prop({ for: "buttonUpload" + suffix }).addClass("yca-custom-file-upload");
    const innerLabel = tags.span().text(args.title);
    const button = tags.ifile().attr({ id: "buttonUpload" + suffix });
    const model = models.createModel(args.nullable ? { state: "valid", value: null } : { state: "invalid" });
    if (args.files && args.files.length)
        button.attr({ accept: args.files.join(",") });
    const preview = tags.img();
    const wrapper = tags.div().append(label.append(innerLabel, preview), button);
    button.change(() => {
        var reader = new FileReader();
        const file = button[0].files[0];
        const name = file.name;
        reader.onloadend = e => {
            innerLabel.text("Uploading");
        };
        function imageIsLoaded(e) {
            //preview.attr({ src: e.target.result })
            const csv = e.target.result;
            api.upload.putCsv({ csv, cabina: idCabina, type: type }).then(val => {
                //preview.attr({ src: val.file })
                innerLabel.text("Uploaded");
                model.write({ state: "valid", value: csv });
            });
        }
        ;
        if (file) {
            reader.onload = imageIsLoaded;
            reader.readAsText(file);
            console.log("file", file);
        }
    });
    function set(url) {
        model.write({ state: "valid", value: url });
    }
    model.watch(data => {
        if (data.state == "valid")
            console.log(type, data.value);
    });
    return { model, el: wrapper, set };
}
exports.uploadCSV = uploadCSV;
function uploadXLS(args, idCabina) {
    const suffix = Math.floor(Math.random() * 1000000).toString();
    const label = tags.label().prop({ for: "buttonUpload" + suffix }).addClass("yca-custom-file-upload");
    const innerLabel = tags.span().text(args.title);
    const button = tags.ifile().attr({ id: "buttonUpload" + suffix });
    const model = models.createModel(args.nullable ? { state: "valid", value: null } : { state: "invalid" });
    if (args.files && args.files.length)
        button.attr({ accept: args.files.join(",") });
    const preview = tags.img();
    const wrapper = tags.div().append(label.append(innerLabel, preview), button);
    button.change(() => {
        var reader = new FileReader();
        const file = button[0].files[0];
        const name = file.name;
        reader.onloadend = e => {
            innerLabel.text("Uploading");
        };
        function imageIsLoaded(e) {
            //preview.attr({ src: e.target.result })
            const xls = e.target.result;
            api.upload.putXls({ xls, name: idCabina }).then(val => {
                //preview.attr({ src: val.file })
                innerLabel.text("Uploaded");
                model.write({ state: "valid", value: xls });
            });
        }
        ;
        if (file) {
            reader.onload = imageIsLoaded;
            reader.readAsText(file);
            console.log("file", file);
        }
    });
    function set(url) {
        model.write({ state: "valid", value: url });
    }
    model.watch(data => {
        if (data.state == "valid")
            console.log(data.value);
    });
    return { model, el: wrapper, set };
}
exports.uploadXLS = uploadXLS;

},{"../../api/_lib":2,"../../models/_lib":36,"../../tags":90}],35:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pages = require("./pages/_lib");
const offline = true;
const test = true;
window.home = pages.home.build;
window.login = pages.login.build;
window.contatti = pages.contatti.build;
window.clienti = pages.clienti.build;
window.territori = pages.territori.build;
window.dettagli = pages.dettagli.build;
window.faq = (args) => {
    args ?
        pages.faq.build(args) :
        pages.faq.build();
};
window.offerte = (args) => {
    args ?
        pages.offerte.build(args) :
        pages.offerte.build();
};
window.form = pages.form.build;
window.thankyou = pages.thankyou.build;
window.news = (args) => {
    args ?
        pages.news.build(args) :
        pages.news.build();
};
window.utils = (args) => {
    args ?
        pages.utils.build(args) :
        pages.utils.build();
};
//____> amministrazione
window.amministrazione = pages.amministrazione.amministrazione;
window.migrazione = pages.migrazione.build;
window.addRegione = pages.regioniAdmin.addRegione;
window.changeRegione = pages.regioniAdmin.changeRegione;
window.addFaq = pages.faqAdmin.addFaq;
window.changeFaq = pages.faqAdmin.changeFaq;
window.addProvincia = pages.provinceAdmin.addProvincia;
window.changeProvincia = pages.provinceAdmin.changeProvincia;
window.addUfficio = pages.ufficiAdmin.addUffici;
window.changeUfficio = pages.ufficiAdmin.changeUfficio;
window.addUtils = pages.utilsAdmin.addUtilsPage;
window.changeUtils = pages.utilsAdmin.changeUtils;
window.addLanding = pages.landingAdmin.addLandingPage;
window.changeLanding = pages.landingAdmin.changeLanding;
window.addNews = pages.newsAdmin.addNewsPage;
window.changeNews = pages.newsAdmin.changeNews;
window.addUsers = pages.usersAdmin.addUsers;
window.changeUsers = pages.usersAdmin.changeUser;
window.addListino = pages.listinoAdmin.addListino;
window.changeListino = pages.listinoAdmin.changeListino;
window.addComune = pages.comuni.addComune;
window.changeComune = pages.comuni.changeComune;
window.addCabina = pages.cabine.addCabina;
window.changeCabina = pages.cabine.changeCabina;
window.offertaluce = pages.offertaluce.build;
window.offertagas = pages.offertagas.build;
window.offertaluceazienda = pages.offertaluceazienda.build;
window.offertagasazienda = pages.offertagasazienda.build;
window.offertaplacetluce = pages.offertaplacetluce.build;
window.offertaplacetgas = pages.offertaplacetgas.build;
window.datiPersonali = pages.datiPersonaliAdmin.build;
window.fatturazioneElettronica = pages.fatturazioneElettronicaAdmin.build;
window.leggerebollettaluce = pages.leggerebollettaluce.build;
window.leggerebollettagas = pages.leggerebollettagas.build;
window.addOfferte = pages.offerteLuce.addAndChangeOfferte;
window.changeOfferte = pages.offerteLuce.offerteChangeAll;
function alert(page) {
    if (!offline || (test && location.href.indexOf("localhost") > -1))
        return page;
    else
        return pages.offline.build;
}

},{"./pages/_lib":37}],36:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function createModel(starter) {
    let value = undefined;
    let watcher = undefined;
    function write(data) {
        value = data;
        if (watcher)
            watcher(value);
    }
    function read() {
        return value;
    }
    function watch(Watcher) {
        watcher = Watcher;
    }
    if (starter) {
        write(starter);
    }
    return { write, read, watch };
}
exports.createModel = createModel;

},{}],37:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const home = require("./home");
exports.home = home;
const login = require("./login");
exports.login = login;
const contatti = require("./contatti");
exports.contatti = contatti;
const clienti = require("./clienti");
exports.clienti = clienti;
const territori = require("./territori");
exports.territori = territori;
const faq = require("./faq");
exports.faq = faq;
const form = require("./form");
exports.form = form;
const thankyou = require("./thankyou");
exports.thankyou = thankyou;
const amministrazione = require("./administration/amministrazione");
exports.amministrazione = amministrazione;
const faqAdmin = require("./administration/faq");
exports.faqAdmin = faqAdmin;
const provinceAdmin = require("./administration/province");
exports.provinceAdmin = provinceAdmin;
const regioniAdmin = require("./administration/regioni");
exports.regioniAdmin = regioniAdmin;
const ufficiAdmin = require("./administration/uffici");
exports.ufficiAdmin = ufficiAdmin;
const usersAdmin = require("./administration/users");
exports.usersAdmin = usersAdmin;
const utilsAdmin = require("./administration/utils");
exports.utilsAdmin = utilsAdmin;
const landingAdmin = require("./administration/landing");
exports.landingAdmin = landingAdmin;
const newsAdmin = require("./administration/news");
exports.newsAdmin = newsAdmin;
const fatturazioneElettronicaAdmin = require("./administration/fatturazioneElettronica");
exports.fatturazioneElettronicaAdmin = fatturazioneElettronicaAdmin;
const datiPersonaliAdmin = require("./administration/datiPersonali");
exports.datiPersonaliAdmin = datiPersonaliAdmin;
const listinoAdmin = require("./administration/listino");
exports.listinoAdmin = listinoAdmin;
const offline = require("./offline");
exports.offline = offline;
const offerte = require("./offerte");
exports.offerte = offerte;
const news = require("./news");
exports.news = news;
const utils = require("./utils");
exports.utils = utils;
const dettagli = require("./dettagli");
exports.dettagli = dettagli;
const offertaplacetgas = require("./offerte/placetgas");
exports.offertaplacetgas = offertaplacetgas;
const offertaplacetluce = require("./offerte/placetluce");
exports.offertaplacetluce = offertaplacetluce;
const offertagas = require("./offerte/gas");
exports.offertagas = offertagas;
const offertaluce = require("./offerte/luce");
exports.offertaluce = offertaluce;
const offertagasazienda = require("./offerte/gasazienda");
exports.offertagasazienda = offertagasazienda;
const offertaluceazienda = require("./offerte/luceazienda");
exports.offertaluceazienda = offertaluceazienda;
const comuni = require("./administration/comuni");
exports.comuni = comuni;
const cabine = require("./administration/cabine");
exports.cabine = cabine;
const leggerebollettaluce = require("./leggerebollettaluce");
exports.leggerebollettaluce = leggerebollettaluce;
const leggerebollettagas = require("./leggerebollettagas");
exports.leggerebollettagas = leggerebollettagas;
const migrazione = require("./migrazione");
exports.migrazione = migrazione;
const offerteLuce = require("./administration/offerte");
exports.offerteLuce = offerteLuce;

},{"./administration/amministrazione":38,"./administration/cabine":39,"./administration/comuni":40,"./administration/datiPersonali":41,"./administration/faq":42,"./administration/fatturazioneElettronica":43,"./administration/landing":44,"./administration/listino":45,"./administration/news":46,"./administration/offerte":47,"./administration/province":48,"./administration/regioni":49,"./administration/uffici":50,"./administration/users":51,"./administration/utils":52,"./clienti":53,"./contatti":54,"./dettagli":55,"./faq":56,"./form":57,"./home":58,"./leggerebollettagas":59,"./leggerebollettaluce":60,"./login":61,"./migrazione":62,"./news":63,"./offerte":64,"./offerte/gas":65,"./offerte/gasazienda":66,"./offerte/luce":67,"./offerte/luceazienda":68,"./offerte/placetgas":69,"./offerte/placetluce":70,"./offline":71,"./territori":72,"./thankyou":73,"./utils":74}],38:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const cookies = require("../../cookies");
function amministrazione() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    console.log("ci sono");
    //======== FAQ ==========>
    const addFaq = tags.h3().text("Nuovo backend");
    const changeFaq = tags.p().text("Il menu ora è a sinistra , permanente");
    template.main.append(addFaq, changeFaq);
}
exports.amministrazione = amministrazione;

},{"../../cookies":23,"../../tags":90,"../../template/_lib":91}],39:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const input = require("../../fields/input/_lib");
const api = require("../../api/_lib");
const cookies = require("../../cookies");
function addCabina() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    const title = tags.h3().text("Aggiungi cabina");
    const sReg = tags.select();
    const sPro = tags.select();
    const sCom = tags.select();
    api.regioni.getRegioni().then(re => {
        for (const r of re)
            sReg.append(tags.option().text(r.name).attr({ value: r._id }));
        api.generic.getProvince(sReg.val()).then(prov => {
            for (const p of prov)
                sPro.append(tags.option().text(p.name).attr({ value: p._id }));
            api.comuni.getComuni({ provincia: sPro.val() }).then(com => {
                for (const c of com)
                    sCom.append(tags.option().text(c.name).attr({ value: c._id }));
                sCom.click();
            });
        });
    });
    sReg.change(e => {
        sPro.empty();
        api.generic.getProvince(sReg.val()).then(prov => {
            for (const p of prov)
                sPro.append(tags.option().text(p.name).attr({ value: p._id }));
            sPro.click();
        });
    });
    sPro.change(e => {
        sCom.empty();
        api.comuni.getComuni({ provincia: sPro.val() }).then(com => {
            for (const p of com)
                sCom.append(tags.option().text(p.name).attr({ value: p._id }));
            sCom.click();
        });
    });
    const nome = input.string.stringInput({ title: "Nome Cabina" });
    const submit = tags.isubmit().val("Invia").click(() => {
        if (nome.model.read().state == "invalid")
            return;
        console.log({ name: nome.model.read().value, regione: sReg.val() });
        api.comuni.setCabina({ name: nome.model.read().value, regione: sReg.val(), provincia: sPro.val(), comune: sCom.val() }).then(id => {
            nome.set("");
        });
    });
    template.main.append(title, sReg, sPro, sCom, nome.el, submit);
}
exports.addCabina = addCabina;
function changeCabina() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    const wrapper = tags.div();
    const sRegione = tags.select();
    api.regioni.getRegioni().then(re => {
        for (const r of re)
            sRegione.append(tags.option().text(r.name).attr({ value: r._id }));
        sRegione.click();
    });
    sRegione.change(e => {
        wrapper.empty();
        buildByRegione(sRegione.val());
    });
    template.main.append(sRegione, wrapper);
    function buildByRegione(mRegione) {
        api.comuni.getCabine({ regione: mRegione }).then(cabine => {
            const t = tags.h3().text("Modifica cabina");
            const name = input.string.stringInput({ title: "Nome" });
            const sReg = tags.select();
            const sCom = tags.select();
            const whichBrochure = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli cabina" }, "name", cabine);
            api.regioni.getRegioni().then(re => {
                for (const r of re)
                    sReg.append(tags.option().text(r.name).attr({ value: r._id }));
                sReg.click();
                api.comuni.getComuni({ regione: sReg.val() }).then(comuni => {
                    for (const p of comuni)
                        sCom.append(tags.option().text(p.name).attr({ value: p._id }));
                });
            });
            sReg.change(e => {
                sCom.empty();
                api.comuni.getComuni({ regione: sReg.val() }).then(prov => {
                    for (const p of prov)
                        sCom.append(tags.option().text(p.name).attr({ value: p._id }));
                    sCom.click();
                });
            });
            let _id = "";
            const submit = tags.isubmit().val("Invia").click(() => {
                if (name.model.read().state == "invalid")
                    return;
                api.comuni.getComune(sCom.val()).then(comune => {
                    api.comuni.changeCabina({
                        _id: _id,
                        name: name.model.read().value,
                        regione: comune.regione,
                        provincia: comune.provincia,
                        comune: comune._id
                    }).then(id => {
                        changeCabina();
                    });
                });
            });
            const cancella = tags.isubmit().val("Cancella").click(() => {
                api.comuni.deleteCabina(_id).then(removed => {
                    console.log(removed);
                    changeCabina();
                });
            });
            whichBrochure.model.watch(res => {
                if (res.state != "valid")
                    return;
                name.model.write({ state: "valid", value: res.value.name });
                _id = res.value._id;
            });
            wrapper.empty().append(whichBrochure.el, sReg, sCom, name.el, submit, cancella);
        });
    }
}
exports.changeCabina = changeCabina;

},{"../../api/_lib":2,"../../cookies":23,"../../fields/input/_lib":25,"../../tags":90,"../../template/_lib":91}],40:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const input = require("../../fields/input/_lib");
const api = require("../../api/_lib");
const cookies = require("../../cookies");
function addComune() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    const title = tags.h3().text("Aggiungi comune");
    const sReg = tags.select();
    const sPro = tags.select();
    api.regioni.getRegioni().then(re => {
        for (const r of re)
            sReg.append(tags.option().text(r.name).attr({ value: r._id }));
        api.generic.getProvince(sReg.val()).then(prov => {
            for (const p of prov)
                sPro.append(tags.option().text(p.name).attr({ value: p._id }));
        });
    });
    sReg.change(e => {
        sPro.empty();
        api.generic.getProvince(sReg.val()).then(prov => {
            for (const p of prov)
                sPro.append(tags.option().text(p.name).attr({ value: p._id }));
            sPro.click();
        });
    });
    const nome = input.string.stringInput({ title: "Nome Comune" });
    const submit = tags.isubmit().val("Invia").click(() => {
        if (nome.model.read().state == "invalid")
            return;
        console.log({ name: nome.model.read().value, regione: sReg.val() });
        api.comuni.setComune({ name: nome.model.read().value, regione: sReg.val(), provincia: sPro.val() }).then(id => {
            nome.set("");
        });
    });
    template.main.append(title, sReg, sPro, nome.el, submit);
}
exports.addComune = addComune;
function changeComune() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    const wrapper = tags.div();
    const sRegione = tags.select();
    api.regioni.getRegioni().then(re => {
        for (const r of re)
            sRegione.append(tags.option().text(r.name).attr({ value: r._id }));
        sRegione.click();
    });
    sRegione.change(e => {
        wrapper.empty();
        buildByRegione(sRegione.val());
    });
    template.main.append(sRegione, wrapper);
    function buildByRegione(mRegione) {
        api.comuni.getComuni({ regione: mRegione }).then(comuni => {
            const t = tags.h3().text("Modifica Comune");
            const name = input.string.stringInput({ title: "Nome" });
            const sReg = tags.select();
            const sPro = tags.select();
            const whichBrochure = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli comune" }, "name", comuni);
            api.regioni.getRegioni().then(re => {
                for (const r of re)
                    sReg.append(tags.option().text(r.name).attr({ value: r._id }));
                sReg.click();
                api.generic.getProvince(sReg.val()).then(province => {
                    for (const p of province)
                        sPro.append(tags.option().text(p.name).attr({ value: p._id }));
                });
            });
            sReg.change(e => {
                sPro.empty();
                api.generic.getProvince(sReg.val()).then(province => {
                    for (const p of province)
                        sPro.append(tags.option().text(p.name).attr({ value: p._id }));
                });
            });
            let _id = "";
            const submit = tags.isubmit().val("Invia").click(() => {
                if (name.model.read().state == "invalid")
                    return;
                api.generic.getProvincia(sPro.val()).then(provincia => {
                    api.comuni.changeComune({
                        _id: _id,
                        name: name.model.read().value,
                        regione: provincia.regione,
                        provincia: provincia._id
                    }).then(id => {
                        changeComune();
                    });
                });
            });
            const cancella = tags.isubmit().val("Cancella").click(() => {
                api.comuni.deleteComuni(_id).then(removed => {
                    console.log(removed);
                    changeComune();
                });
            });
            whichBrochure.model.watch(res => {
                if (res.state != "valid")
                    return;
                name.model.write({ state: "valid", value: res.value.name });
                _id = res.value._id;
            });
            wrapper.empty().append(whichBrochure.el, sReg, sPro, name.el, submit, cancella);
        });
    }
}
exports.changeComune = changeComune;

},{"../../api/_lib":2,"../../cookies":23,"../../fields/input/_lib":25,"../../tags":90,"../../template/_lib":91}],41:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const shards = require("../../shards/_lib");
const input = require("../../fields/input/_lib");
const api = require("../../api/_lib");
const utils = require("../../utils");
const province_1 = require("../../configurations/province");
function build(language) {
    const utente = utils.userManager();
    if (!utente.isLogged()) {
        location.href = "/login";
    }
    //CONFIG
    const blueBox = [
        { text: "Portale energia", logo: "/img/round_home.png", url: "https://tecniconsul-portcli.serviceict.it/argon-tecniconsul/portale.html?idFornitore=1" },
        { text: "Portale gas", logo: "/img/round_home.png", url: "http://www.tecniconsulenergia.it:8080/RetiVendita/Pages/Login.aspx" },
        //{ text: "Comunicazioni personali", logo: "/img/round_pencil.png" },
        { text: "Autolettura elettricità", logo: "/img/round_schermovuotol.png", url: "/clienti/autolettura" },
        { text: "Autolettura gas", logo: "/img/round_schermovuotol.png" },
        { text: "I miei dati personali", logo: "/img/round_pencil.png", url: "/clienti/datipersonali" },
        { text: "Attiva la fatturazione elettronica", logo: "/img/round_schermovuotol.png", url: "/clienti/fatturazioneelettronica" }
    ];
    if (utente.get().isAdmin)
        blueBox.push({ text: "Amministrazione", logo: "/img/round_schermovuotol.png", url: "/amministrazione" });
    const template = templates.main.main();
    const upperSection = tags.div().addClass("yca-wrapper").css({
        backgroundImage: "url(/img/lucecasamiddle.jpg)",
        backgroundSize: "cover",
        width: "80vw",
        height: "36vw",
        position: "relative"
    });
    const bottomFloater = tags.div().addClass("yca-home-bottomfloater");
    const usClaim = tags.h1().addClass("yca-home-claim").html("AREA<br />CLIENTI");
    upperSection.append(bottomFloater.append(usClaim));
    const blueSection = tags.div().addClass("yca-home-white");
    for (let b of blueBox)
        blueSection.append(shards.panel.whitePanel(b));
    const lower = tags.div().css({ margin: "150px 0" });
    const welcome = tags.h1().addClass("yca-login-title").html("Benvenuto nella tua area personale").css({ textAlign: "center", textTransform: "none", color: "#0000bb" });
    lower.append(welcome, tags.hr().css({ margin: "50px 0" }), blueSection);
    const container = tags.div().css({ maxWidth: "450px", margin: "auto" });
    const title = tags.h3().text("Modifica dati personali");
    //const codcliente = input.string.stringInput({ title: "Codice cliente" })
    const azienda = input.radio.radio({ title: "", name: "type", description: "Azienda", nullable: true });
    const privato = input.radio.radio({ title: "", name: "type", description: "Privato", nullable: true });
    const nome = input.string.stringInput({ title: "Nome", nullable: true });
    const cognome = input.string.stringInput({ title: "Cognome", nullable: true });
    const denominazione = input.string.stringInput({ title: "Denominazione sociale", nullable: true });
    const cf = input.string.stringInput({ title: "Codice fiscale" });
    const piva = input.string.stringInput({ title: "Partita IVA", nullable: true });
    const indirizzo = input.string.stringInput({ title: "Indirizzo (Via, Piazza ecc)", nullable: true });
    const provincia = input.combo.combo({ type: "combo", title: "Provincia" }, "nome", "id", province_1.province.province);
    const telefono = input.string.stringInput({ title: "Telefono", nullable: true });
    const email = input.string.stringInput({ title: "E-mail" });
    const nuovapassword = input.string.stringInput({ title: "Nuova password", nullable: true });
    const acconsentoObbl = input.boolean.booleanInput({ title: "Acconsento al trattamento dei dati personali e all'utilizzo dei cookie necessari per usufruire di questo sito e dei suoi servizi" });
    const errorDiv = tags.ul();
    container.append(title, privato.el, azienda.el, nome.el, cognome.el, denominazione.el, cf.el, piva.el, provincia.el, indirizzo.el, telefono.el, email.el, 
    //codcliente.el,
    nuovapassword.el, acconsentoObbl.el, errorDiv);
    api.utente.getUtenti({ _id: utente.get()._id }).then(utenti => {
        privato.set(utenti[0].privato);
        azienda.set(utenti[0].azienda);
        nome.set(utenti[0].nome);
        cognome.set(utenti[0].cognome);
        denominazione.set(utenti[0].denominazione);
        cf.set(utenti[0].cf);
        piva.set(utenti[0].piva);
        provincia.set(utenti[0].provincia);
        indirizzo.set(utenti[0].indirizzo);
        telefono.set(utenti[0].telefono);
        email.set(utenti[0].email);
        acconsentoObbl.set(utenti[0].acconsente);
        //codcliente.set(utenti[0].codcliente)
        const submit = tags.isubmit().val("Invia").click(() => {
            errorDiv.empty();
            let isValid = true;
            //condizioni
            // if (codcliente.model.read().state == "invalid") {
            //     errorDiv.append(tags.li().html("Devi inserire il codice cliente"))
            //     isValid = false
            // }
            if (email.model.read().state == "invalid") {
                errorDiv.append(tags.li().html("Devi inserire una mail"));
                isValid = false;
            }
            if (denominazione.model.read().state == "invalid" && (nome.model.read().state == "invalid" || cognome.model.read().state == "invalid")) {
                errorDiv.append(tags.li().html("Devi inserire una denominazione sociale o un nome e cognome"));
                isValid = false;
            }
            if (azienda.model.read().value && !denominazione.model.read().value) {
                errorDiv.append(tags.li().html("Se hai selezionato un cliente aziendale devi inserire una denominazione sociale"));
                isValid = false;
            }
            if (privato.model.read().value && !(!!nome.model.read().value && !!cognome.model.read().value)) {
                errorDiv.append(tags.li().html("Se hai selezionato un cliente privato devi inserire un un nome e cognome"));
                isValid = false;
            }
            if (isValid)
                api.utente.changeUtente({
                    _id: utenti[0]._id,
                    privato: privato.model.read().value,
                    azienda: azienda.model.read().value,
                    nome: nome.model.read().value,
                    cognome: cognome.model.read().value,
                    denominazione: denominazione.model.read().value,
                    cf: cf.model.read().value,
                    piva: piva.model.read().value,
                    provincia: provincia.model.read().value,
                    indirizzo: indirizzo.model.read().value,
                    telefono: telefono.model.read().value,
                    acconsente: acconsentoObbl.model.read().value,
                    email: email.model.read().value,
                    password: nuovapassword.model.read().value ? nuovapassword.model.read().value : undefined,
                }).then(id => {
                    location.reload();
                });
        });
        container.append(submit);
    });
    template.main.append(upperSection, lower, container);
}
exports.build = build;

},{"../../api/_lib":2,"../../configurations/province":22,"../../fields/input/_lib":25,"../../shards/_lib":75,"../../tags":90,"../../template/_lib":91,"../../utils":94}],42:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const shards = require("../../shards/_lib");
const input = require("../../fields/input/_lib");
const api = require("../../api/_lib");
const cookies = require("../../cookies");
function addFaq() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const categorie = [
        { name: "Utilità per te", _id: "utility" },
        { name: "Supporto per te", _id: "support" },
        { name: "Bollette per te", _id: "invoice" },
        { name: "Pagamenti per te", _id: "payments" },
        { name: "Alro per te", _id: "other" }
    ];
    const selectCategories = tags.select();
    for (const categoria of categorie)
        selectCategories.append(tags.option().text(categoria.name).attr({ value: categoria._id }));
    const template = templates.backend.build();
    const title = tags.h3().text("Aggiungi faq");
    const titolo = input.string.stringInput({ title: "Titolo" });
    const sottotitolo = input.string.stringInput({ title: "Sottotitolo", multiline: 5 });
    const descrizione = input.string.stringInput({ title: "Descrizione estesa", multiline: 15 });
    const logo = input.upload.upload({ title: "Logo" });
    const part = input.string.stringInput({ title: "Link (ID)" });
    const submit = shards.anchor.full({
        text: "Invia",
        action: () => {
            if (titolo.model.read().state == "invalid") {
                template.alert.append(tags.h1().html("Devi inserire un titolo"));
                return;
            }
            if (sottotitolo.model.read().state == "invalid") {
                template.alert.append(tags.h1().html("Devi inserire un sottotitolo"));
                return;
            }
            if (descrizione.model.read().state == "invalid") {
                template.alert.append(tags.h1().html("Devi inserire un descripion"));
                return;
            }
            if (logo.model.read().state == "invalid") {
                template.alert.append(tags.h1().html("Devi inserire un logo"));
                return;
            }
            if (part.model.read().state == "invalid") {
                template.alert.append(tags.h1().html("Devi inserire un ID"));
                return;
            }
            api.faq.setFAQ({
                title: titolo.model.read().value,
                subtitle: sottotitolo.model.read().value,
                description: descrizione.model.read().value,
                logo: logo.model.read().value,
                part: part.model.read().value,
                category: selectCategories.val()
            }).then(id => {
                addFaq();
            });
        }
    });
    template.main.append(title, selectCategories, titolo.el, sottotitolo.el, descrizione.el, logo.el, part.el, submit);
}
exports.addFaq = addFaq;
function changeFaq() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    api.faq.getFAQ({}).then(faq => {
        const title = tags.h3().text("Modifica faq");
        const titolo = input.string.stringInput({ title: "Titolo" });
        const sottotitolo = input.string.stringInput({ title: "Sottotitolo", multiline: 5 });
        const descrizione = input.string.stringInput({ title: "Descrizione estesa", multiline: 10 });
        const part = input.string.stringInput({ title: "Link (ID)" });
        const logo = input.upload.upload({ title: "Logo" });
        const categorie = [
            { name: "Utilità per te", _id: "utility" },
            { name: "Supporto per te", _id: "support" },
            { name: "Bollette per te", _id: "invoice" },
            { name: "Pagamenti per te", _id: "payments" },
            { name: "Alro per te", _id: "other" }
        ];
        const selectCategories = tags.select();
        for (const categoria of categorie)
            selectCategories.append(tags.option().text(categoria.name).attr({ value: categoria._id }));
        const whichFAQ = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli faq" }, "title", faq);
        let _id = "";
        const submit = tags.isubmit().val("Invia").click(() => {
            if (titolo.model.read().state == "invalid" || descrizione.model.read().state == "invalid" || logo.model.read().state == "invalid" || sottotitolo.model.read().state == "invalid" || part.model.read().state == "invalid")
                return;
            api.faq.changeFAQ({
                _id,
                title: titolo.model.read().value,
                subtitle: sottotitolo.model.read().value,
                description: descrizione.model.read().value,
                part: part.model.read().value,
                logo: logo.model.read().value,
                category: selectCategories.val()
            }).then(id => {
                changeFaq();
            });
        });
        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.faq.deleteFAQ(_id).then(removed => {
                console.log(removed);
                changeFaq();
            });
        });
        whichFAQ.model.watch(res => {
            if (res.state != "valid")
                return;
            titolo.model.write({ state: "valid", value: res.value.title });
            descrizione.model.write({ state: "valid", value: res.value.description });
            sottotitolo.model.write({ state: "valid", value: res.value.subtitle });
            logo.model.write({ state: "valid", value: res.value.logo });
            part.model.write({ state: "valid", value: res.value.part });
            selectCategories.val(res.value.category);
            _id = res.value._id;
        });
        template.main.append(whichFAQ.el, selectCategories, titolo.el, sottotitolo.el, descrizione.el, logo.el, part.el, submit, cancella);
    });
}
exports.changeFaq = changeFaq;

},{"../../api/_lib":2,"../../cookies":23,"../../fields/input/_lib":25,"../../shards/_lib":75,"../../tags":90,"../../template/_lib":91}],43:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const shards = require("../../shards/_lib");
const utils = require("../../utils");
const form = require("../form");
function build(language) {
    const utente = utils.userManager();
    if (!utente.isLogged()) {
        location.href = "/login";
    }
    //CONFIG
    const blueBox = [
        { text: "Portale energia", logo: "/img/round_home.png", url: "https://tecniconsul-portcli.serviceict.it/argon-tecniconsul/portale.html?idFornitore=1" },
        { text: "Portale gas", logo: "/img/round_home.png", url: "http://www.tecniconsulenergia.it:8080/RetiVendita/Pages/Login.aspx" },
        //{ text: "Comunicazioni personali", logo: "/img/round_pencil.png" },
        { text: "Autolettura elettricità", logo: "/img/round_schermovuotol.png", url: "/clienti/autolettura" },
        { text: "Autolettura gas", logo: "/img/round_schermovuotol.png" },
        { text: "I miei dati personali", logo: "/img/round_pencil.png", url: "/clienti/datipersonali" },
        { text: "Attiva la fatturazione elettronica", logo: "/img/round_schermovuotol.png", url: "/clienti/fatturazioneelettronica" }
    ];
    if (utente.get().isAdmin)
        blueBox.push({ text: "Amministrazione", logo: "/img/round_schermovuotol.png", url: "/amministrazione" });
    const template = templates.main.main();
    const upperSection = tags.div().addClass("yca-wrapper").css({
        backgroundImage: "url(/img/lucecasamiddle.jpg)",
        backgroundSize: "cover",
        width: "80vw",
        height: "36vw",
        position: "relative"
    });
    const bottomFloater = tags.div().addClass("yca-home-bottomfloater");
    const usClaim = tags.h1().addClass("yca-home-claim").html("AREA<br />CLIENTI");
    upperSection.append(bottomFloater.append(usClaim));
    const blueSection = tags.div().addClass("yca-home-white");
    for (let b of blueBox)
        blueSection.append(shards.panel.whitePanel(b));
    const lower = tags.div().css({ margin: "150px 0" });
    const welcome = tags.h1().addClass("yca-login-title").html("Benvenuto nella tua area personale").css({ textAlign: "center", textTransform: "none", color: "#0000bb" });
    lower.append(welcome, tags.hr().css({ margin: "50px 0" }), blueSection);
    console.log("fatturazione elettronica");
    const container = tags.div();
    const user = utils.userManager().get();
    container.append(form.fatturaElettronica(user));
    template.main.append(upperSection, lower, container);
}
exports.build = build;

},{"../../shards/_lib":75,"../../tags":90,"../../template/_lib":91,"../../utils":94,"../form":57}],44:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const input = require("../../fields/input/_lib");
const api = require("../../api/_lib");
const cookies = require("../../cookies");
function addLandingPage() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    const titolo = tags.h3().text("Aggiungi Landing page");
    const slug = input.string.stringInput({ title: "Slug" });
    const body = input.string.stringInput({ title: "HTML pagina", multiline: 20 });
    const submit = tags.isubmit().val("Invia").click(() => {
        if (slug.model.read().state == "invalid")
            return;
        if (body.model.read().state == "invalid")
            return;
        api.landing.setLanding({
            slug: slug.model.read().value,
            body: body.model.read().value,
        }).then(id => {
            slug.set("");
            body.set("");
        });
    });
    template.main.append(titolo, slug.el, body.el, submit);
}
exports.addLandingPage = addLandingPage;
function changeLanding() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    api.landing.getAllLanding().then(landing => {
        const t = tags.h3().text("Modifica pagina landing");
        const slug = input.string.stringInput({ title: "Slug" });
        const body = input.string.stringInput({ title: "Pagina", multiline: 20 });
        const chooseLanding = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli landing page" }, "slug", landing);
        let _id = "";
        const submit = tags.isubmit().val("Invia").click(() => {
            if (slug.model.read().state == "invalid")
                return;
            if (body.model.read().state == "invalid")
                return;
            api.landing.changeLanding({
                _id,
                slug: slug.model.read().value,
                body: body.model.read().value,
            }).then(id => {
                changeLanding();
            });
        });
        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.landing.deleteLanding(_id).then(removed => {
                console.log(removed);
                changeLanding();
            });
        });
        chooseLanding.model.watch(res => {
            if (res.state != "valid")
                return;
            slug.model.write({ state: "valid", value: res.value.slug });
            body.model.write({ state: "valid", value: res.value.body });
            _id = res.value._id;
        });
        template.main.append(chooseLanding.el, slug.el, body.el, submit, cancella);
    });
}
exports.changeLanding = changeLanding;

},{"../../api/_lib":2,"../../cookies":23,"../../fields/input/_lib":25,"../../tags":90,"../../template/_lib":91}],45:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const input = require("../../fields/input/_lib");
const api = require("../../api/_lib");
const cookies = require("../../cookies");
function addListino() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    const title = tags.h3().text("Aggiungi listino per comune");
    const container = tags.div();
    const upper = tags.div();
    api.comuni.getCabine({}).then(re => {
        const sReg = input.combo.combo({ title: "Cabina" }, "name", "_id", re);
        upper.append(sReg.el);
        if (re && re.length) {
            const uploadListino1 = input.upload.uploadXLS({ title: "Carica tabella in formato XLS" }, re[0]._id);
            container.empty().append(uploadListino1.el);
        }
        sReg.model.watch(v => {
            const uploadListino1 = input.upload.uploadXLS({ title: "Carica tabella in formato XLS" }, v.value);
            container.empty().append(uploadListino1.el);
        });
    });
    // const submit = tags.isubmit().val("Invia").click(() => {
    //     if (uploadListino1.model.read().state == "invalid")
    //         return
    //     console.log({ name: uploadListino1.model.read().value, regione: <string>sReg.val() })
    //     // api.generic.setProvincia({ name: uploadListino1.model.read().value, regione: <string>sReg.val() }).then(id => {
    //     //     uploadListino1.set("")
    //     // })
    // })
    template.main.append(title, upper, container);
}
exports.addListino = addListino;
function changeListino() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    api.generic.getProvince().then(regioni => {
        const t = tags.h3().text("Aggiungi slide");
        const name = input.string.stringInput({ title: "Nome" });
        const whichBrochure = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli brochure" }, "name", regioni);
        let _id = "";
        const submit = tags.isubmit().val("Invia").click(() => {
            if (name.model.read().state == "invalid")
                return;
            api.generic.changeProvince({
                _id: _id,
                name: name.model.read().value
            }).then(id => {
                changeListino();
            });
        });
        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.generic.deleteProvince(_id).then(removed => {
                console.log(removed);
                changeListino();
            });
        });
        whichBrochure.model.watch(res => {
            if (res.state != "valid")
                return;
            name.model.write({ state: "valid", value: res.value.name });
            _id = res.value._id;
        });
        template.main.append(whichBrochure.el, name.el, submit, cancella);
    });
}
exports.changeListino = changeListino;

},{"../../api/_lib":2,"../../cookies":23,"../../fields/input/_lib":25,"../../tags":90,"../../template/_lib":91}],46:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const input = require("../../fields/input/_lib");
const api = require("../../api/_lib");
const cookies = require("../../cookies");
function addNewsPage() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    const titolo = tags.h3().text("Aggiungi news");
    const title = input.string.stringInput({ title: "Titolo news" });
    const subtitle = input.string.stringInput({ title: "Subtitle" });
    const body = input.string.stringInput({ title: "News", multiline: 20 });
    const banner = input.upload.upload({ title: "Banner" });
    const date = input.string.stringInput({ title: "Data" });
    const submit = tags.isubmit().val("Invia").click(() => {
        if (title.model.read().state == "invalid")
            return;
        if (subtitle.model.read().state == "invalid")
            return;
        if (body.model.read().state == "invalid")
            return;
        // if (date.model.read().state == "invalid")
        //     return
        if (banner.model.read().state == "invalid")
            return;
        api.news.setNews({
            title: title.model.read().value,
            subtitle: subtitle.model.read().value,
            body: body.model.read().value,
            banner: banner.model.read().value,
            date: date.model.read().value
        }).then(id => {
            addNewsPage();
        });
    });
    template.main.append(titolo, title.el, date.el, subtitle.el, body.el, banner.el, submit);
}
exports.addNewsPage = addNewsPage;
function changeNews() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    api.news.getNews().then(news => {
        const titolo = tags.h3().text("Modifica pagina news");
        const title = input.string.stringInput({ title: "Titolo news" });
        const subtitle = input.string.stringInput({ title: "Subtitle" });
        const body = input.string.stringInput({ title: "News", multiline: 20 });
        const banner = input.upload.upload({ title: "Banner" });
        const date = input.string.stringInput({ title: "Data" });
        const chooseNews = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli brochure" }, "title", news);
        let _id = "";
        const submit = tags.isubmit().val("Invia").click(() => {
            if (title.model.read().state == "invalid")
                return;
            api.news.changeNews({
                _id,
                title: title.model.read().value,
                subtitle: subtitle.model.read().value,
                body: body.model.read().value,
                banner: banner.model.read().value,
                date: date.model.read().value
            }).then(id => {
                changeNews();
            });
        });
        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.news.deleteNews(_id).then(removed => {
                console.log(removed);
                changeNews();
            });
        });
        chooseNews.model.watch(res => {
            if (res.state != "valid")
                return;
            title.model.write({ state: "valid", value: res.value.title });
            subtitle.model.write({ state: "valid", value: res.value.subtitle });
            body.model.write({ state: "valid", value: res.value.body });
            banner.model.write({ state: "valid", value: res.value.banner });
            date.model.write({ state: "valid", value: res.value.date });
            _id = res.value._id;
        });
        template.main.append(titolo, chooseNews.el, title.el, date.el, subtitle.el, body.el, banner.el, submit, cancella);
    });
}
exports.changeNews = changeNews;

},{"../../api/_lib":2,"../../cookies":23,"../../fields/input/_lib":25,"../../tags":90,"../../template/_lib":91}],47:[function(require,module,exports){
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const shards = require("../../shards/_lib");
const input = require("../../fields/input/_lib");
const api = require("../../api/_lib");
const cookies = require("../../cookies");
function addAndChangeOfferte() {
    return __awaiter(this, void 0, void 0, function* () {
        const loggedIn = cookies.getCookie("user");
        if (!loggedIn) {
            location.href = "/login";
        }
        const template = templates.backend.build();
        console.log(location.pathname, location.pathname.split("/"));
        const myPathArray = location.pathname.split("/");
        const casaAziendaPlacet = myPathArray[myPathArray.length - 2];
        const luceGas = myPathArray[myPathArray.length - 1];
        const titleC1 = input.string.stringInput({ title: "Titolo offerta 1" });
        const introC1 = input.string.stringInput({ title: "Intro offerta 1" });
        const descriptionC1 = input.string.stringInput({ title: "Descrizione offerta 1", multiline: 20 });
        const titleC2 = input.string.stringInput({ title: "Titolo offerta 2" });
        const introC2 = input.string.stringInput({ title: "Intro offerta 2" });
        const descriptionC2 = input.string.stringInput({ title: "Descrizione offerta 2", multiline: 20 });
        const titleC3 = input.string.stringInput({ title: "Titolo offerta 3" });
        const introC3 = input.string.stringInput({ title: "Intro offerta 3" });
        const descriptionC3 = input.string.stringInput({ title: "Descrizione offerta 3", multiline: 20 });
        const titleC4 = input.string.stringInput({ title: "Titolo offerta 4" });
        const introC4 = input.string.stringInput({ title: "Intro offerta 4" });
        const descriptionC4 = input.string.stringInput({ title: "Descrizione offerta 4", multiline: 20 });
        const submit = tags.isubmit().val("Invia").click(() => __awaiter(this, void 0, void 0, function* () {
            const r1 = yield api.offerte.setOfferta({
                casaAziendaPlacet,
                luceGas,
                o1: {
                    title: titleC1.model.read().value,
                    description: descriptionC1.model.read().value,
                    intro: introC1.model.read().value
                },
                o2: {
                    title: titleC2.model.read().value,
                    description: descriptionC2.model.read().value,
                    intro: introC2.model.read().value
                },
                o3: {
                    title: titleC3.model.read().value,
                    description: descriptionC3.model.read().value,
                    intro: introC3.model.read().value
                },
                o4: {
                    title: titleC4.model.read().value,
                    description: descriptionC4.model.read().value,
                    intro: introC4.model.read().value
                }
            });
            if (r1) {
                location.reload();
            }
        }));
        template.main.append(tags.h2().text("Aggiungi o modifica le offerte " + luceGas), tags.h3().text("TARIFFA " + casaAziendaPlacet.toUpperCase()), titleC1.el, introC1.el, descriptionC1.el, tags.hr(), titleC2.el, introC2.el, descriptionC2.el, tags.hr(), titleC3.el, introC3.el, descriptionC3.el, tags.hr(), titleC4.el, introC4.el, descriptionC4.el, submit);
        const fill = yield api.offerte.getOfferta({
            casaAziendaPlacet,
            luceGas
        });
        if (fill.o1) {
            titleC1.set(fill.o1.title);
            introC1.set(fill.o1.intro);
            descriptionC1.set(fill.o1.description);
        }
        if (fill.o2) {
            titleC2.set(fill.o2.title);
            introC2.set(fill.o2.intro);
            descriptionC2.set(fill.o2.description);
        }
        if (fill.o3) {
            titleC3.set(fill.o3.title);
            introC3.set(fill.o3.intro);
            descriptionC3.set(fill.o3.description);
        }
        if (fill.o4) {
            titleC4.set(fill.o4.title);
            introC4.set(fill.o4.intro);
            descriptionC4.set(fill.o4.description);
        }
    });
}
exports.addAndChangeOfferte = addAndChangeOfferte;
function offerteChangeAll() {
    return __awaiter(this, void 0, void 0, function* () {
        const loggedIn = cookies.getCookie("user");
        if (!loggedIn) {
            location.href = "/login";
        }
        const template = templates.backend.build();
        template.main.append(tags.h2().text("Aggiungi o modifica le offerte "), tags.hr(), tags.h3().text("Casa"), shards.anchor.full({ text: "Gas", url: "/amministrazione/addOfferte/casa/gas" }).addClass("yca-menu-buttons"), shards.anchor.full({ text: "Luce", url: "/amministrazione/addOfferte/casa/luce" }).addClass("yca-menu-buttons"), tags.hr(), tags.h3().text("Azienda"), shards.anchor.full({ text: "Gas", url: "/amministrazione/addOfferte/azienda/gas" }).addClass("yca-menu-buttons"), shards.anchor.full({ text: "Luce", url: "/amministrazione/addOfferte/azienda/luce" }).addClass("yca-menu-buttons"), tags.hr(), tags.h3().text("Placet"), shards.anchor.full({ text: "Gas", url: "/amministrazione/addOfferte/placet/gas" }).addClass("yca-menu-buttons"), shards.anchor.full({ text: "Luce", url: "/amministrazione/addOfferte/placet/luce" }).addClass("yca-menu-buttons"));
    });
}
exports.offerteChangeAll = offerteChangeAll;

},{"../../api/_lib":2,"../../cookies":23,"../../fields/input/_lib":25,"../../shards/_lib":75,"../../tags":90,"../../template/_lib":91}],48:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const input = require("../../fields/input/_lib");
const api = require("../../api/_lib");
const cookies = require("../../cookies");
function addProvincia() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    const title = tags.h3().text("Aggiungi regione");
    const sReg = tags.select();
    api.regioni.getRegioni().then(re => {
        for (const r of re)
            sReg.append(tags.option().text(r.name).attr({ value: r._id }));
    });
    const nome = input.string.stringInput({ title: "Nome Provincia" });
    const submit = tags.isubmit().val("Invia").click(() => {
        if (nome.model.read().state == "invalid")
            return;
        console.log({ name: nome.model.read().value, regione: sReg.val() });
        api.generic.setProvincia({ name: nome.model.read().value, regione: sReg.val() }).then(id => {
            nome.set("");
        });
    });
    template.main.append(title, sReg, nome.el, submit);
}
exports.addProvincia = addProvincia;
function changeProvincia() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    api.generic.getProvince().then(regioni => {
        const t = tags.h3().text("Modifica provincia");
        const name = input.string.stringInput({ title: "Nome" });
        const whichBrochure = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli provincia" }, "name", regioni);
        let _id = "";
        const submit = tags.isubmit().val("Invia").click(() => {
            if (name.model.read().state == "invalid")
                return;
            api.generic.changeProvince({
                _id: _id,
                name: name.model.read().value
            }).then(id => {
                changeProvincia();
            });
        });
        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.generic.deleteProvince(_id).then(removed => {
                console.log(removed);
                changeProvincia();
            });
        });
        whichBrochure.model.watch(res => {
            if (res.state != "valid")
                return;
            name.model.write({ state: "valid", value: res.value.name });
            _id = res.value._id;
        });
        template.main.append(whichBrochure.el, name.el, submit, cancella);
    });
}
exports.changeProvincia = changeProvincia;

},{"../../api/_lib":2,"../../cookies":23,"../../fields/input/_lib":25,"../../tags":90,"../../template/_lib":91}],49:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const input = require("../../fields/input/_lib");
const api = require("../../api/_lib");
const cookies = require("../../cookies");
function addRegione() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    const title = tags.h3().text("Aggiungi regione");
    const nome = input.string.stringInput({ title: "Nome regione" });
    const submit = tags.isubmit().val("Invia").click(() => {
        if (nome.model.read().state == "invalid")
            return;
        api.regioni.setRegione({ name: nome.model.read().value }).then(id => {
            nome.set("");
        });
    });
    template.main.append(title, nome.el, submit);
}
exports.addRegione = addRegione;
function changeRegione() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    api.regioni.getRegioni().then(regioni => {
        const t = tags.h3().text("Aggiungi slide");
        const name = input.string.stringInput({ title: "Nome" });
        const whichBrochure = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli regione" }, "name", regioni);
        let _id = "";
        const submit = tags.isubmit().val("Invia").click(() => {
            if (name.model.read().state == "invalid")
                return;
            api.regioni.changeRegione({
                id: _id,
                name: name.model.read().value
            }).then(id => {
                changeRegione();
            });
        });
        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.regioni.deleteRegione(_id).then(removed => {
                console.log(removed);
                changeRegione();
            });
        });
        whichBrochure.model.watch(res => {
            if (res.state != "valid")
                return;
            name.model.write({ state: "valid", value: res.value.name });
            _id = res.value._id;
        });
        template.main.append(whichBrochure.el, name.el, submit, cancella);
    });
}
exports.changeRegione = changeRegione;

},{"../../api/_lib":2,"../../cookies":23,"../../fields/input/_lib":25,"../../tags":90,"../../template/_lib":91}],50:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const input = require("../../fields/input/_lib");
const api = require("../../api/_lib");
const cookies = require("../../cookies");
function addUffici() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    const title = tags.h3().text("Aggiungi ufficio");
    const city = input.string.stringInput({ title: "Provincia o città *" });
    const address = input.string.stringInput({ title: "Indirizzo *" });
    const phone = input.string.stringInput({ title: "Contatti telefonici *" });
    const hours = input.string.stringInput({ title: "Orari di apertura *", multiline: 10 });
    const submit = tags.isubmit().val("Invia").click(() => {
        api.uffici.setUfficio({ city: city.model.read().value, address: address.model.read().value, phone: phone.model.read().value, hours: hours.model.read().value, }).then(id => {
            city.set("");
            address.set("");
            phone.set("");
            hours.set("");
        });
    });
    template.main.append(title, city.el, address.el, phone.el, hours.el, submit);
}
exports.addUffici = addUffici;
function changeUfficio() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    api.uffici.getUffici().then(uffici => {
        const title = tags.h3().text("Aggiungi ufficio");
        const city = input.string.stringInput({ title: "Provincia o città *" });
        const address = input.string.stringInput({ title: "Indirizzo *" });
        const phone = input.string.stringInput({ title: "Contatti telefonici *" });
        const hours = input.string.stringInput({ title: "Orari di apertura *", multiline: 10 });
        const scegliufficio = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli brochure" }, "city", uffici);
        let _id = "";
        const submit = tags.isubmit().val("Invia").click(() => {
            if (city.model.read().state == "invalid")
                return;
            if (address.model.read().state == "invalid")
                return;
            if (phone.model.read().state == "invalid")
                return;
            if (hours.model.read().state == "invalid")
                return;
            api.uffici.changeUfficio({
                _id,
                city: city.model.read().value,
                address: address.model.read().value,
                phone: phone.model.read().value,
                hours: hours.model.read().value,
            }).then(id => {
                changeUfficio();
            });
        });
        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.uffici.deleteUfficio(_id).then(removed => {
                console.log(removed);
                changeUfficio();
            });
        });
        scegliufficio.model.watch(res => {
            if (res.state != "valid")
                return;
            city.model.write({ state: "valid", value: res.value.city });
            address.model.write({ state: "valid", value: res.value.address });
            phone.model.write({ state: "valid", value: res.value.phone });
            hours.model.write({ state: "valid", value: res.value.hours });
            _id = res.value._id;
        });
        template.main.append(title, scegliufficio.el, city.el, address.el, phone.el, hours.el, submit, cancella);
    });
}
exports.changeUfficio = changeUfficio;

},{"../../api/_lib":2,"../../cookies":23,"../../fields/input/_lib":25,"../../tags":90,"../../template/_lib":91}],51:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const input = require("../../fields/input/_lib");
const api = require("../../api/_lib");
const cookies = require("../../cookies");
const province_1 = require("../../configurations/province");
function addUsers() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    const title = tags.h3().text("Aggiungi utente");
    const codcliente = input.string.stringInput({ title: "Codice cliente" });
    const azienda = input.radio.radio({ title: "", name: "type", description: "Azienda", nullable: true });
    const privato = input.radio.radio({ title: "", name: "type", description: "Privato", nullable: true });
    const nome = input.string.stringInput({ title: "Nome", nullable: true });
    const cognome = input.string.stringInput({ title: "Cognome", nullable: true });
    const denominazione = input.string.stringInput({ title: "Denominazione sociale", nullable: true });
    const cf = input.string.stringInput({ title: "Codice fiscale" });
    const piva = input.string.stringInput({ title: "Partita IVA", nullable: true });
    const indirizzo = input.string.stringInput({ title: "Indirizzo (Via, Piazza ecc)", nullable: true });
    const provincia = input.combo.combo({ type: "combo", title: "Provincia" }, "nome", "id", province_1.province.province);
    const telefono = input.string.stringInput({ title: "Telefono", nullable: true });
    const email = input.string.stringInput({ title: "E-mail" });
    const password = input.string.stringInput({ title: "Password" });
    const errorDiv = tags.ul();
    const submit = tags.isubmit().val("Invia").click(() => {
        errorDiv.empty();
        let isValid = true;
        //condizioni
        if (codcliente.model.read().state == "invalid") {
            errorDiv.append(tags.li().html("Devi inserire il codice cliente"));
            isValid = false;
        }
        if (email.model.read().state == "invalid") {
            errorDiv.append(tags.li().html("Devi inserire una mail"));
            isValid = false;
        }
        if (password.model.read().state == "invalid") {
            errorDiv.append(tags.li().html("Devi inserire una password"));
            isValid = false;
        }
        if (denominazione.model.read().state == "invalid" && (nome.model.read().state == "invalid" || cognome.model.read().state == "invalid")) {
            errorDiv.append(tags.li().html("Devi inserire una denominazione sociale o un nome e cognome"));
            isValid = false;
        }
        if (azienda.model.read().value && !denominazione.model.read().value) {
            errorDiv.append(tags.li().html("Se hai selezionato un cliente aziendale devi inserire una denominazione sociale"));
            isValid = false;
        }
        if (privato.model.read().value && !(!!nome.model.read().value && !!cognome.model.read().value)) {
            errorDiv.append(tags.li().html("Se hai selezionato un cliente privato devi inserire un un nome e cognome"));
            isValid = false;
        }
        if (isValid)
            api.utente.setUtente({
                privato: privato.model.read().value,
                azienda: azienda.model.read().value,
                nome: nome.model.read().value,
                cognome: cognome.model.read().value,
                denominazione: denominazione.model.read().value,
                cf: cf.model.read().value,
                piva: piva.model.read().value,
                provincia: provincia.model.read().value,
                indirizzo: indirizzo.model.read().value,
                telefono: telefono.model.read().value,
                email: email.model.read().value,
                password: password.model.read().value,
                codcliente: codcliente.model.read().value
            }).then(id => {
                location.reload();
            });
    });
    template.main.append(title, privato.el, azienda.el, nome.el, cognome.el, denominazione.el, cf.el, piva.el, provincia.el, indirizzo.el, telefono.el, email.el, password.el, codcliente.el, errorDiv, submit);
}
exports.addUsers = addUsers;
function changeUser() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    api.utente.getUtenti().then(utenti => {
        const title = tags.h3().text("Modifica utente");
        const scegliutente = input.radioMulti.radioMulti({ name: "qutente", title: "Scegli utente" }, "codcliente", utenti);
        const codcliente = input.string.stringInput({ title: "Codice cliente" });
        const azienda = input.radio.radio({ title: "", name: "type", description: "Azienda", nullable: true });
        const privato = input.radio.radio({ title: "", name: "type", description: "Privato", nullable: true });
        const nome = input.string.stringInput({ title: "Nome", nullable: true });
        const cognome = input.string.stringInput({ title: "Cognome", nullable: true });
        const denominazione = input.string.stringInput({ title: "Denominazione sociale", nullable: true });
        const cf = input.string.stringInput({ title: "Codice fiscale" });
        const piva = input.string.stringInput({ title: "Partita IVA", nullable: true });
        const indirizzo = input.string.stringInput({ title: "Indirizzo (Via, Piazza ecc)", nullable: true });
        const provincia = input.combo.combo({ type: "combo", title: "Provincia" }, "nome", "id", province_1.province.province);
        const telefono = input.string.stringInput({ title: "Telefono", nullable: true });
        const email = input.string.stringInput({ title: "E-mail" });
        const nuovapassword = input.string.stringInput({ title: "Nuova password", nullable: true });
        const errorDiv = tags.ul();
        let _id = "";
        const submit = tags.isubmit().val("Invia").click(() => {
            errorDiv.empty();
            let isValid = true;
            //condizioni
            if (codcliente.model.read().state == "invalid") {
                errorDiv.append(tags.li().html("Devi inserire il codice cliente"));
                isValid = false;
            }
            if (email.model.read().state == "invalid") {
                errorDiv.append(tags.li().html("Devi inserire una mail"));
                isValid = false;
            }
            if (denominazione.model.read().state == "invalid" && (nome.model.read().state == "invalid" || cognome.model.read().state == "invalid")) {
                errorDiv.append(tags.li().html("Devi inserire una denominazione sociale o un nome e cognome"));
                isValid = false;
            }
            if (azienda.model.read().value && !denominazione.model.read().value) {
                errorDiv.append(tags.li().html("Se hai selezionato un cliente aziendale devi inserire una denominazione sociale"));
                isValid = false;
            }
            if (privato.model.read().value && !(!!nome.model.read().value && !!cognome.model.read().value)) {
                errorDiv.append(tags.li().html("Se hai selezionato un cliente privato devi inserire un un nome e cognome"));
                isValid = false;
            }
            if (isValid)
                api.utente.changeUtente({
                    _id,
                    privato: privato.model.read().value,
                    azienda: azienda.model.read().value,
                    nome: nome.model.read().value,
                    cognome: cognome.model.read().value,
                    denominazione: denominazione.model.read().value,
                    cf: cf.model.read().value,
                    piva: piva.model.read().value,
                    provincia: provincia.model.read().value,
                    indirizzo: indirizzo.model.read().value,
                    telefono: telefono.model.read().value,
                    email: email.model.read().value,
                    password: nuovapassword.model.read().value ? nuovapassword.model.read().value : undefined,
                    codcliente: codcliente.model.read().value
                }).then(id => {
                    location.reload();
                });
        });
        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.utente.deleteUtente(_id).then(removed => {
                console.log(removed);
                changeUser();
            });
        });
        scegliutente.model.watch(res => {
            if (res.state != "valid")
                return;
            privato.set(res.value.privato),
                azienda.set(res.value.azienda),
                nome.set(res.value.nome),
                cognome.set(res.value.cognome),
                denominazione.set(res.value.denominazione),
                cf.set(res.value.cf),
                piva.set(res.value.piva),
                provincia.set(res.value.provincia),
                indirizzo.set(res.value.indirizzo),
                telefono.set(res.value.telefono),
                email.set(res.value.email),
                codcliente.set(res.value.codcliente),
                _id = res.value._id;
        });
        template.main.append(title, scegliutente.el, tags.hr(), privato.el, azienda.el, nome.el, cognome.el, denominazione.el, cf.el, piva.el, provincia.el, indirizzo.el, telefono.el, email.el, codcliente.el, nuovapassword.el, errorDiv, submit, cancella);
    });
}
exports.changeUser = changeUser;

},{"../../api/_lib":2,"../../configurations/province":22,"../../cookies":23,"../../fields/input/_lib":25,"../../tags":90,"../../template/_lib":91}],52:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const input = require("../../fields/input/_lib");
const api = require("../../api/_lib");
const cookies = require("../../cookies");
function addUtilsPage() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    const titolo = tags.h3().text("Aggiungi pagina utils");
    const title = input.string.stringInput({ title: "Titolo pagina" });
    const slug = input.string.stringInput({ title: "Slug" });
    const body = input.string.stringInput({ title: "Pagina", multiline: 20 });
    const submit = tags.isubmit().val("Invia").click(() => {
        if (title.model.read().state == "invalid")
            return;
        if (slug.model.read().state == "invalid")
            return;
        if (body.model.read().state == "invalid")
            return;
        api.utils.setUtils({
            title: title.model.read().value,
            slug: slug.model.read().value,
            body: body.model.read().value,
        }).then(id => {
            title.set("");
            slug.set("");
            body.set("");
        });
    });
    template.main.append(titolo, title.el, slug.el, body.el, submit);
}
exports.addUtilsPage = addUtilsPage;
function changeUtils() {
    const loggedIn = cookies.getCookie("user");
    if (!loggedIn) {
        location.href = "/login";
    }
    const template = templates.backend.build();
    api.utils.getUtils().then(utils => {
        const t = tags.h3().text("Modifica pagina utils");
        const title = input.string.stringInput({ title: "Titolo" });
        const slug = input.string.stringInput({ title: "Slug" });
        const body = input.string.stringInput({ title: "Pagina", multiline: 20 });
        const chooseUtils = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli brochure" }, "title", utils);
        let _id = "";
        const submit = tags.isubmit().val("Invia").click(() => {
            if (title.model.read().state == "invalid")
                return;
            if (slug.model.read().state == "invalid")
                return;
            if (body.model.read().state == "invalid")
                return;
            api.utils.changeUtils({
                _id,
                title: title.model.read().value,
                slug: slug.model.read().value,
                body: body.model.read().value,
            }).then(id => {
                changeUtils();
            });
        });
        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.utils.deleteUtils(_id).then(removed => {
                console.log(removed);
                changeUtils();
            });
        });
        chooseUtils.model.watch(res => {
            if (res.state != "valid")
                return;
            title.model.write({ state: "valid", value: res.value.title });
            slug.model.write({ state: "valid", value: res.value.slug });
            body.model.write({ state: "valid", value: res.value.body });
            _id = res.value._id;
        });
        template.main.append(chooseUtils.el, title.el, slug.el, body.el, submit, cancella);
    });
}
exports.changeUtils = changeUtils;

},{"../../api/_lib":2,"../../cookies":23,"../../fields/input/_lib":25,"../../tags":90,"../../template/_lib":91}],53:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const templates = require("../template/_lib");
const shards = require("../shards/_lib");
const utils = require("../utils");
function build(language) {
    //CONFIG
    const blueBox = [
        { text: "Portale energia", logo: "/img/round_home.png", url: "https://tecniconsul-portcli.serviceict.it/argon-tecniconsul/portale.html?idFornitore=1" },
        { text: "Portale gas", logo: "/img/round_home.png", url: "http://www.tecniconsulenergia.it:8080/RetiVendita/Pages/Login.aspx" },
        //{ text: "Comunicazioni personali", logo: "/img/round_pencil.png" },
        { text: "Autolettura elettricità", logo: "/img/round_schermovuotol.png", url: "/clienti/autolettura" },
        { text: "Autolettura gas", logo: "/img/round_schermovuotol.png" },
        { text: "I miei dati personali", logo: "/img/round_pencil.png", url: "/clienti/datipersonali" },
        { text: "Attiva la fatturazione elettronica", logo: "/img/round_schermovuotol.png", url: "/clienti/fatturazioneelettronica" }
    ];
    const user = utils.userManager().get();
    if (user.isAdmin)
        blueBox.push({ text: "Amministrazione", logo: "/img/round_schermovuotol.png", url: "/amministrazione" });
    const template = templates.main.main();
    const upperSection = shards.bannerone.build();
    const bottomFloater = tags.div().addClass("yca-home-bottomfloater");
    const usClaim = tags.h1().addClass("yca-home-claim").html("AREA<br />CLIENTI");
    upperSection.append(bottomFloater.append(usClaim));
    const blueSection = tags.div().addClass("yca-home-white");
    for (let b of blueBox)
        blueSection.append(shards.panel.whitePanel(b));
    const lower = tags.div().css({ margin: "250px 0" });
    const welcome = tags.h1().addClass("yca-login-title").html("Benvenuto nella tua area personale").css({ textAlign: "center", textTransform: "none", color: "#0000bb" });
    lower.append(welcome, tags.hr().css({ margin: "50px 0" }), blueSection);
    template.main.append(upperSection, lower);
}
exports.build = build;

},{"../shards/_lib":75,"../tags":90,"../template/_lib":91,"../utils":94}],54:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const templates = require("../template/_lib");
const shards = require("../shards/_lib");
const input = require("../fields/input/_lib");
const api = require("../api/_lib");
const form = require("./form");
function build(language) {
    const template = templates.main.main();
    const upperSection = tags.div().addClass("yca-wrapper").css({
        backgroundImage: "url(/img/contatti.jpg)",
        backgroundSize: "cover",
        width: "80vw",
        height: "18vw",
        position: "relative",
        backgroundPosition: "top"
    });
    const bottomFloater = tags.div().addClass("yca-home-bottomfloater");
    const usClaim = tags.h1().addClass("yca-home-claim").html("NUOVA ENERGIA<BR />NELLA TUA GIORNATA!");
    upperSection.append(bottomFloater.append(usClaim));
    const middleSection = tags.div().addClass("yca-wrapper").css({ display: "flex" });
    const greenNumber = tags.div().addClass("yca-autoflex").addClass("yca-autoflex-column");
    greenNumber.append(tags.h3().addClass("yca-contatti-blacktitle").html("Numero Verde per comunicazioni autolettura"), tags.h2().addClass("yca-green-number-text").html("800172358"), tags.p().addClass("yca-contatti-greyparagraph").html("Per informazioni:"), tags.p().addClass("yca-contatti-greyparagraph").html(`Tecniconsul Energia s.r.l.<br>
Via M.K. Gandhi, 22 - 42123 REGGIO EMILIA<br>
Posta elettronica standard: <br>
<a href="mailto:info@tecniconsulenergia.it">info@tecniconsulenergia.it</a><br>
`), tags.p().addClass("yca-contatti-greyparagraph").html(`Posta elettronica certificata: <br>
<a href="mailto:tecniconsulenergia@postecert.it">tecniconsulenergia@postecert.it</a>`), tags.p().addClass("yca-contatti-greyparagraph").html(`Registro Imprese RE, Codice Fiscale <br>
e p. IVA n. 02400570350`));
    middleSection.append(form.formComunicazioniPersonali(greenNumber));
    const bottomSection = tags.div().addClass("yca-wrapper").css({ marginBottom: "100px" });
    bottomSection.append(tags.h1().html("Indirizzi e orari uffici di zona").css({ color: "#000" }));
    api.uffici.getUffici().then(offices => {
        const uffici = shards.related.uffici();
        const radioWrapper = input.radioMulti.radioMulti({ title: "Seleziona ufficio", name: "ufficio", }, "city", offices);
        bottomSection.append(radioWrapper.el, uffici.el);
        radioWrapper.model.watch(ds => {
            uffici.set(ds.value);
        });
        uffici.set(offices[0]);
    });
    template.main.append(upperSection, middleSection, bottomSection);
    window.scrollTo(0, 0);
}
exports.build = build;

},{"../api/_lib":2,"../fields/input/_lib":25,"../shards/_lib":75,"../tags":90,"../template/_lib":91,"./form":57}],55:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const templates = require("../template/_lib");
const shards = require("../shards/_lib");
const api = require("../api/_lib");
function build(args) {
    const template = templates.main.main();
    const upperSection = shards.bannerone.build();
    const bottomFloater = tags.div().addClass("yca-home-bottomfloater");
    const usClaim = tags.h1().addClass("yca-home-claim").html("LUCE E GAS<BR />OVUNQUE TU SIA");
    upperSection.append(bottomFloater.append(usClaim));
    const middleSection = tags.div().addClass("yca-wrapper");
    const ourOfferts = tags.h1().addClass("yca-home-underlined").text("Luce casa");
    const wrapperOfferte = tags.div();
    const offerte = tags.div().css({
        width: "50vw",
        flex: "1",
        height: "36vw",
        display: "inline-block"
    });
    const offLinkCont = tags.div();
    const link1 = tags.h2().css({ display: "inline-block" }).addClass("yca-home-casaimpresa").html("Luce 1");
    const link2 = tags.h2().css({ display: "inline-block" }).addClass("yca-home-casaimpresa").html("Luce 2");
    const threeButt = tags.div().css({
        float: "right",
        height: "36vw",
        backgroundColor: "#cccccccc",
        padding: "30px 20px"
    }).append(tags.div().addClass("yca-dettagli-sidebar").html("Scopri la tariffa<br />luce che meglio<br />si presta alla<br />gestione dei<br />consumi della<br />tua casa"));
    wrapperOfferte.append(offerte.append(offLinkCont.append(link1, link2)), threeButt).css({ backgroundImage: "url(/img/casa.jpg)" });
    link1.click(() => {
        link1.addClass("selected");
        link2.removeClass("selected");
        lowerTitleInner.html("Luce 1 casa");
        wrapperOfferte.css({ backgroundImage: "url(/img/lucecasamiddle.jpg)" });
        upperSection.css({ backgroundImage: "url(/img/lucecasaheader.jpg)" });
    });
    link2.click(() => {
        link1.removeClass("selected");
        link2.addClass("selected");
        lowerTitleInner.html("Luce 2 casa");
        wrapperOfferte.css({ backgroundImage: "url(/img/lucecasamiddle.jpg)" });
        upperSection.css({ backgroundImage: "url(/img/lucecasaheader.jpg)" });
    });
    middleSection.append(ourOfferts, wrapperOfferte);
    const segnapSlider = tags.h1().addClass("yca-home-underlined").html("News");
    const slider = shards.thumbbox.thumbBox({ scrollTime: 3000 });
    api.news.getNews().then(news => {
        const m = [];
        for (const n of news) {
            m.push(shards.news.thumbNews({
                image: n.banner,
                title: n.title,
                subtitle: n.title,
                date: n.date,
                body: n.body,
                id: n._id
            }));
        }
        slider.add(m);
    });
    const lowerTitle = tags.div().addClass("yca-dettagli-lowerTitleInner");
    const lowerTitleInner = tags.h1().css({
        fontSize: "4rem",
        color: "#666",
        textTransform: "none",
        fontFamily: "'Ubuntu', sans-serif"
    });
    lowerTitle.append(lowerTitleInner);
    const multicolumn = tags.div().html(`
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    `).css({
        columnCount: "3",
        columnGap: "30px",
        color: "#666",
        fontSize: "1.3rem",
        lineHeight: "1.9rem",
        fontFamily: "'Ubuntu', sans-serif"
    });
    const images = tags.div().css({ margin: "50px 0" });
    const img1 = tags.img().attr({ src: "/img/tariffa1.jpg" }).css({ display: "inline-block", maxWidth: "30%", minWidth: "30%", padding: "10px 20px", margin: "0 50px 0 0" });
    const img2 = tags.img().attr({ src: "/img/tariffa2.jpg" }).css({ display: "inline-block", maxWidth: "30%", minWidth: "30%", padding: "10px 20px", margin: "0 50px 0 0" });
    images.append(img1, img2);
    template.main.append(upperSection, middleSection, lowerTitle, multicolumn, images, segnapSlider, slider.el);
    link1.click();
    if (args.offerta == "casa") {
        link1.click();
    }
    if (args.offerta == "impresa") {
        link2.click();
    }
}
exports.build = build;

},{"../api/_lib":2,"../shards/_lib":75,"../tags":90,"../template/_lib":91}],56:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const templates = require("../template/_lib");
const shards = require("../shards/_lib");
const api = require("../api/_lib");
function build(args) {
    console.log("got param:", args);
    const template = templates.main.main();
    const upperSection = tags.div().addClass("yca-wrapper").css({
        backgroundImage: "url(/img/tramonto.jpg)",
        backgroundSize: "cover",
        width: "80vw",
        height: "18vw",
        position: "relative"
    });
    const categorie = [
        { name: "Utilità", _id: "utility", orientation: "left", logo: "/img/utilitaperte.png" },
        { name: "Supporto", _id: "support", orientation: "right", logo: "/img/supportoperte.png" },
        { name: "Bollette", _id: "invoice", orientation: "left", logo: "/img/bolletteperte.png" },
        { name: "Pagamenti", _id: "payments", orientation: "right", logo: "/img/pagamentiperte.png" },
        { name: "Altro", _id: "other", orientation: "left", logo: "/img/altroperte.png" }
    ];
    const lowerSection = tags.div().addClass("yca-wrapper");
    const if1 = [[0], [0, 0], [1, 0, 1], [1, 0, 0, 1], [2, 1, 0, 1, 2], [2, 1, 0, 0, 1, 2], [3, 2, 1, 0, 1, 2, 3]];
    const fArUtility = [];
    const fArSupport = [];
    const fArInvoice = [];
    const fArPayments = [];
    const fArOther = [];
    function toArray(arg) {
        const length = arg.length;
        const shiftArray = if1[length - 1];
        const myArr = [];
        for (let i = 0; i < length; i++) {
            arg[i].setShift(shiftArray[i] * 2);
            myArr.push(arg[i].el);
        }
        return myArr;
    }
    api.faq.getFAQ({}).then(faqs => {
        let c = 0;
        for (let i = 0; i < faqs.length; i++) {
            let orientation = "left";
            if (faqs[i].category == "support" || faqs[i].category == "payments")
                orientation = "right";
            const fe = shards.faq.faq({
                title: faqs[i].title,
                subitle: faqs[i].subtitle,
                text: faqs[i].description,
                logo: faqs[i].logo,
                hash: faqs[i].part,
                orientation
            });
            if (faqs[i].category == "utility")
                fArUtility.push(fe);
            if (faqs[i].category == "support")
                fArSupport.push(fe);
            if (faqs[i].category == "invoice")
                fArInvoice.push(fe);
            if (faqs[i].category == "payments")
                fArPayments.push(fe);
            if (faqs[i].category == "other")
                fArOther.push(fe);
        }
        for (const cat of categorie) {
            const faqContainer = shards.faq.faqCategory({ title: cat.name, logo: cat.logo, orientation: cat.orientation });
            lowerSection.append(faqContainer.el);
            if (cat._id == "utility")
                faqContainer.set(toArray(fArUtility));
            if (cat._id == "support")
                faqContainer.set(toArray(fArSupport));
            if (cat._id == "invoice")
                faqContainer.set(toArray(fArInvoice));
            if (cat._id == "payments")
                faqContainer.set(toArray(fArPayments));
            if (cat._id == "other")
                faqContainer.set(toArray(fArOther));
        }
        if (args && args.id) {
            if (location.hash)
                location.hash = "";
            location.hash = args.id;
        }
    });
    template.main.append(upperSection, lowerSection.css({ paddingBottom: "50px" }));
}
exports.build = build;

},{"../api/_lib":2,"../shards/_lib":75,"../tags":90,"../template/_lib":91}],57:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const input = require("../fields/input/_lib");
const template = require("../template/_lib");
const api = require("../api/_lib");
const shards = require("../shards/_lib");
function build() {
    const mTemplate = template.main.main();
    mTemplate.main.append(formVieniConNoi(), tags.hr(), formContatti(), tags.hr(), formComunicazioniPersonali(), tags.hr());
}
exports.build = build;
function formVieniConNoi() {
    const vieniConNoi = tags.div().addClass("yca-wrapper");
    const form1 = tags.div().addClass("yca-autoflex");
    const form2 = tags.div().addClass("yca-autoflex");
    const azienda = input.radio.radio({ title: "Azienda", name: "aziendaoprivato", nullable: true });
    const privato = input.radio.radio({ title: "Privato", name: "aziendaoprivato", nullable: true });
    const nome = input.string.stringInput({ title: "Nome *" });
    const cognome = input.string.stringInput({ title: "Cognome *" });
    const age = input.string.stringInput({ title: "Età", nullable: true });
    const professione = input.string.stringInput({ title: "Professione", nullable: true });
    const orario = input.string.stringInput({ title: "Orario in cui vuoi essere chiamato", nullable: true });
    const comune = input.string.stringInput({ title: "Comune di residenza *" });
    const telefono = input.string.stringInput({ title: "Numero di telefono *" });
    const acconsentoObbl = input.boolean.booleanInput({ title: "Acconsento al trattamento dei dati personali per essere ricontattato in merito alle offerte *" });
    const acconsentoNonObbl = input.boolean.booleanInput({ title: "Acconsento al trattamento dei dati personali per finalità di marketing" });
    const errorPanel = tags.ul().addClass("yca-error-panel");
    const submit = tags.isubmit().val("Invia").click(() => {
        let valid = true;
        errorPanel.empty();
        if (nome.model.read().state == "invalid" && cognome.model.read().state == "invalid" || comune.model.read().state == "invalid" || telefono.model.read().state == "invalid") {
            errorPanel.append(tags.li().html("Errore: i campi obbligatori non sono compilati"));
            valid = false;
        }
        if (acconsentoObbl.model.read().value != true) {
            errorPanel.append(tags.li().html("Errore: devi accettare il trattamento dei dati personali per esere ricontattato"));
            valid = false;
        }
        if (valid)
            api.generic.setContatto({
                azienda: azienda.model.read().value,
                privato: privato.model.read().value,
                nome: nome.model.read().value,
                cognome: cognome.model.read().value,
                age: age.model.read().value,
                professione: professione.model.read().value,
                orario: orario.model.read().value,
                comune: comune.model.read().value,
                telefono: telefono.model.read().value,
                acconsento: acconsentoObbl.model.read().value,
                acconsentoNonObbl: acconsentoNonObbl.model.read().value,
                tipo: "vieniconnoi-territori"
            }).then(r => {
                api.mail.sendMail({
                    obj: "Form vieni con noi in pagina territori", body: JSON.stringify({
                        nome: nome.model.read().value,
                        cognome: cognome.model.read().value,
                        acconsento: acconsentoObbl.model.read().value,
                        email: telefono.model.read().value,
                        tipo: "paginacontatti",
                        oggetto: r
                    })
                }).then(a => {
                    location.href = "/thankyou";
                });
            }).catch(e => { errorPanel.html(e); });
    });
    form1.append(azienda.el, nome.el, cognome.el, age.el, professione.el, orario.el);
    form2.append(privato.el, comune.el, telefono.el, acconsentoObbl.el, acconsentoNonObbl.el, errorPanel, submit);
    const formWrapper = tags.div().css({ display: "flex", padding: "0" }).append(form1, form2);
    return vieniConNoi.append(formWrapper);
}
exports.formVieniConNoi = formVieniConNoi;
function formContatti() {
    const nome = input.string.stringInput({ title: "Nome" });
    const cognome = input.string.stringInput({ title: "Cognome" });
    const email = input.string.stringInput({ title: "Email" });
    const acconsento = input.boolean.booleanInput({ title: "Acconsento al trattamento dei dati personali *" });
    const submit = tags.isubmit().val("Invia").click(() => {
        api.generic.setContatto({
            nome: nome.model.read().value,
            cognome: cognome.model.read().value,
            acconsento: acconsento.model.read().value,
            email: email.model.read().value,
            tipo: "form contatti"
        }).then(r => {
            api.mail.sendMail({
                obj: "Form contatti rapidi", body: JSON.stringify({
                    nome: nome.model.read().value,
                    cognome: cognome.model.read().value,
                    acconsento: acconsento.model.read().value,
                    email: email.model.read().value,
                    tipo: "paginacontatti"
                })
            }).then(a => {
                location.href = "/thankyou";
            });
        });
    });
    const form = tags.div().addClass("yca-autoflex");
    return form.append(nome.el, cognome.el, email.el, acconsento.el, submit);
}
exports.formContatti = formContatti;
function formComunicazioniPersonali(rightPanel) {
    const wrapTop = tags.div().css({ display: "flex" }).css({ width: "100%" });
    const leftTop = tags.div().css({ flex: "auto" });
    const rightTop = tags.div().css({ flex: "auto" });
    const azienda = input.radio.radio({ title: "", name: "type", description: "Azienda" });
    const privato = input.radio.radio({ title: "", name: "type", description: "Privato" });
    const wrapBottom = tags.div().css({ display: "flex" });
    const leftBottom = tags.div().css({ flex: "auto", padding: "0 175px" });
    const rightBottom = tags.div().css({ flex: "auto", padding: "0 175px" });
    wrapTop.append(leftTop.append(azienda.el), rightTop.append(privato.el));
    const errorPanel = tags.div().addClass("yca-error-panel");
    const wrapCenter = tags.div().css({ display: "flex" });
    const leftCenter = tags.div();
    const rightCenter = tags.div().css({ flex: "auto" });
    const nome = input.string.stringInput({ title: "Nome" });
    const cognome = input.string.stringInput({ title: "Cognome" });
    const email = input.string.stringInput({ title: "Email" });
    const acconsento = input.boolean.booleanInput({ title: "Acconsento al trattamento dei dati personali *" });
    wrapCenter.append(leftCenter.append(wrapTop, nome.el, cognome.el, email.el, acconsento.el));
    const commenti = input.string.stringInput({ title: "Lascia qui i tuoi commenti *", multiline: 10 });
    const submit = tags.isubmit().val("Invia").click(() => {
        if (acconsento.model.read().state == "invalid" || nome.model.read().state == "invalid" || cognome.model.read().state == "invalid")
            errorPanel.html("Errore: i campi obbligatori non sono compilati");
        else
            api.generic.setContatto({
                azienda: azienda.model.read().value,
                privato: privato.model.read().value,
                nome: nome.model.read().value,
                cognome: cognome.model.read().value,
                acconsento: acconsento.model.read().value,
                commenti: commenti.model.read().value,
                tipo: "paginacontatti"
            }).then(r => {
                api.mail.sendMail({
                    obj: "Pagina contatti", body: JSON.stringify({
                        azienda: azienda.model.read().value,
                        privato: privato.model.read().value,
                        nome: nome.model.read().value,
                        cognome: cognome.model.read().value,
                        acconsento: acconsento.model.read().value,
                        commenti: commenti.model.read().value,
                        tipo: "paginacontatti"
                    })
                }).then(a => {
                    location.href = "/thankyou";
                });
            });
    });
    const file = input.upload.upload({ title: "Carica un file" });
    const form = tags.div().addClass("yca-autoflex").addClass("yca-autoflex-columnifmobile");
    wrapBottom.append(leftBottom.append(submit), rightBottom.append(file.el));
    if (rightPanel) {
        wrapCenter.append(rightCenter.append(rightPanel));
    }
    form.append(wrapCenter, commenti.el, wrapBottom);
    commenti.set("");
    return form;
}
exports.formComunicazioniPersonali = formComunicazioniPersonali;
function fatturaElettronica(user) {
    const title = tags.h1().text("Fattura Elettronica").css({ textTransform: "none", color: "#3260ab" });
    const text = tags.p().html("Se vuoi attivare il servizio di invio della fattura in modalità elettronica a costo zero (il documento cartaceo non sarà più inviato) compila i campi sottostanti inserendo oltre alla tua email anche i tuoi dati personali quali: Nome Cognome e Codice Contratto che puoi trovare nella prima pagina della bolletta e poi clicca su “Invia”.")
        .css({
        display: "block",
        fontSize: "24px",
        margin: "3em 0em"
    });
    const elettogas = input.combo.combo({ title: "Elettricità o gas" }, "name", "name", [{ name: "Elettricità" }, { name: "Gas" }]);
    elettogas.set("Elettricità");
    const intestatario = input.string.stringInput({ title: "Intestatario fornitura (richiesto)" });
    const mail = input.string.stringInput({ title: "La tua email (richiesto)" });
    const codice = input.string.stringInput({ title: "Codice Contratto (richiesto)" });
    const nota = input.string.stringInput({ title: "Nota facoltativa" });
    const submit = tags.isubmit().val("Invia").click(() => {
        api.mail.sendMail({
            obj: "Fattura elettronica", body: JSON.stringify({
                tipo: elettogas.model.read().value,
                intestatario: intestatario.model.read().value,
                mail: mail.model.read().value,
                codice: codice.model.read().value,
                nota: nota.model.read().value,
            })
        }).then(a => {
            location.href = "/clienti";
        });
    });
    intestatario.set(user.denominazione ? user.denominazione : (user.nome + " " + user.cognome));
    mail.set(user.email);
    codice.set(user.codcliente);
    const form = tags.div().addClass("yca-autoflex").addClass("yca-slimform");
    return form.append(title, text, intestatario.el, mail.el, codice.el, nota.el, submit);
}
exports.fatturaElettronica = fatturaElettronica;
function buildLoginModal() {
    const title = tags.h1().addClass("yca-login-title").html("Login").css({ textAlign: "center", color: "#0000bb" });
    // const form = tags.div().css({ margin: "auto" })
    // const username = input.string.stringInput({ title: "Codice cliente *" })
    // const password = input.string.passwordInput({ title: "Password *" })
    // const middl = tags.div().css({ backgroundColor: "rgba(255, 255, 255, 0.6)" })
    // const errorDiv = tags.ul()
    // const button = tags.isubmit().text("LOGIN").click(() => {
    //     errorDiv.empty()
    //     let isValid = true
    //     if (username.model.read().state == "invalid") {
    //         errorDiv.append(tags.li().html("Devi inserire email/username"))
    //         isValid = false
    //     }
    //     if (password.model.read().state == "invalid") {
    //         errorDiv.append(tags.li().html("Devi inserire email/username"))
    //         isValid = false
    //     }
    //     if (isValid)
    //         api.utente.login({ user: username.model.read().value, password: password.model.read().value }).then(user => {
    //             if (user && !(user as any).error) {
    //                 //cookies.setCookie("user", user, 30)
    //                 location.href = "/clienti"
    //             }
    //             else {
    //                 errorDiv.append(tags.li().html("Codice cliente e/o password errati"))
    //             }
    //         }).catch(e => {
    //             errorDiv.append(tags.li().html("Error: " + JSON.stringify(e)))
    //         })
    // })
    const under = tags.div().css({ display: "flex" });
    const buttonE = shards.anchor.full({ text: "Portale energia", url: "https://tecniconsul-portcli.serviceict.it/argon-tecniconsul/portale.html?idFornitore=1" }).css({ flex: "1", margin: "0 20px 0 0", textAlign: "center", padding: "16px 32px" });
    const buttonG = shards.anchor.full({ text: "Portale gas", url: "http://www.tecniconsulenergia.it:8080/RetiVendita/Pages/Login.aspx" }).css({ flex: "1", margin: "0 0 0 20px", textAlign: "center", padding: "16px 32px" });
    under.append(buttonE, buttonG);
    // middl.append(title, form.append(username.el, password.el, button))
    const modal = shards.modal.panel(under);
    return modal;
}
exports.buildLoginModal = buildLoginModal;

},{"../api/_lib":2,"../fields/input/_lib":25,"../shards/_lib":75,"../tags":90,"../template/_lib":91}],58:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const templates = require("../template/_lib");
const shards = require("../shards/_lib");
const api = require("../api/_lib");
const forms = require("./form");
function build(language) {
    //CONFIG
    const blueBox = [
        {
            text: "leggere la bolletta", logo: "/img/white_schermo.png", url: [
                { name: "Leggere bolletta luce", url: "/leggerebollettaluce" },
                { name: "Leggere bolletta gas", url: "/leggerebollettagas" }
            ]
        },
        { text: "agevolazioni", logo: "/img/white_colonna.png", url: "/utils/agevolazioni" },
        { text: "come risparmiare", logo: "/img/white_money.png", url: "/utils/come_risparmiare" }
    ];
    const template = templates.main.main();
    const upperSection = shards.bannerone.build();
    const bottomFloater = tags.div().addClass("yca-home-bottomfloater");
    const usClaim = tags.h1().addClass("yca-home-claim").html("NUOVA ENERGIA<BR />NELLA TUA GIORNATA");
    const usButton = tags.div().addClass("yca-home-bigbluepanel-button").append(tags.a().html("Vieni con noi")).css({
        width: "50%", maxWidth: "50%", margin: "0 25%"
    });
    upperSection.append(bottomFloater.append(usClaim, usButton));
    const middleSection = tags.div().addClass("yca-wrapper");
    const ourOfferts = tags.h1().addClass("yca-home-underlined").text("Le nostre offerte");
    const wrapperOfferte = tags.div().addClass("yca-home-offerte-wrapper");
    const offertaFabbrica = tags.a().attr({ href: "/offerte/impresa" }).addClass("yca-home-offerte-offerta").css({
        backgroundImage: "url(/img/fabbrica.jpg)"
    });
    const offertaHome = tags.a().attr({ href: "/offerte/casa" }).addClass("yca-home-offerte-offerta").css({
        backgroundImage: "url(/img/casa.jpg)"
    });
    const offertaHomeText = tags.p().addClass("yca-home-offerte-offerta-testo").html("casa");
    const offertaFabbricaText = tags.p().addClass("yca-home-offerte-offerta-testo").html("impresa");
    offertaHome.append(offertaHomeText);
    offertaFabbrica.append(offertaFabbricaText);
    wrapperOfferte.append(offertaHome, offertaFabbrica);
    const blueSection = tags.div().addClass("yca-home-blue");
    for (let b of blueBox)
        blueSection.append(shards.panel.bluePanel(b));
    const bigBluePanel = tags.div().addClass("yca-home-bigbluepanel-wrapper");
    const bigBluePanelTitle = tags.div().addClass("yca-home-bigbluepanel-title").append(tags.p().html("Ti risolviamo ogni dubbio"));
    const bigBluePanelButton = tags.div().addClass("yca-home-bigbluepanel-button");
    const bigBluePanelImage = tags.div().addClass("yca-home-bigbluepanel-image");
    const bbb = shards.anchor.full({ text: "clicca qua", url: "/faq" });
    bigBluePanelButton.append(bbb);
    bigBluePanelImage.addClass("yca-home-bigbluepanel-image").css({ backgroundImage: "url(/img/puntointerrogativo.png)" });
    bigBluePanel.append(bigBluePanelTitle, bigBluePanelButton, bigBluePanelImage);
    const vieniConNoi = forms.formVieniConNoi();
    const myModal = shards.modal.panel(vieniConNoi);
    usButton.click(() => {
        myModal.show();
    });
    middleSection.append(myModal.el, ourOfferts, wrapperOfferte, blueSection, bigBluePanel);
    const segnapSlider = tags.h1().addClass("yca-home-underlined").html("News");
    //const errSlider = tags.p().html("Error: MongoDB Error: no element for slider in database.")
    const slider = shards.thumbbox.thumbBox({ scrollTime: 3000 });
    api.news.getNews().then(news => {
        const m = [];
        for (const n of news) {
            m.push(shards.news.thumbNews({
                image: n.banner,
                title: n.title,
                subtitle: n.subtitle,
                date: n.date,
                body: n.body,
                id: n._id
            }));
        }
        slider.add(m);
    });
    template.main.append(upperSection, middleSection, segnapSlider, slider.el, shards.bannerone.lowerBanner());
}
exports.build = build;

},{"../api/_lib":2,"../shards/_lib":75,"../tags":90,"../template/_lib":91,"./form":57}],59:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const templates = require("../template/_lib");
const shards = require("../shards/_lib");
function build() {
    const template = templates.main.main();
    const buttonHeader = tags.div().addClass("yca-leggerebolletta-buttonheader").append(shards.anchor.full({
        text: "Prima pagina", action: () => {
            first.show();
            second.hide();
            third.hide();
        }
    }), shards.anchor.full({
        text: "Seconda pagina", action: () => {
            first.hide();
            second.show();
            third.hide();
        }
    }), shards.anchor.full({
        text: "Arera", action: () => {
            first.hide();
            second.hide();
            third.show();
        }
    }));
    const buttonHeader2 = tags.div();
    const bprima = tags.h2().css({ display: "inline-block" }).addClass("yca-home-casaimpresa").addClass("selected").html("Prima pagina").click(() => {
        first.show();
        second.hide();
        third.hide();
        bprima.addClass("selected");
        bseconda.removeClass("selected");
        barera.removeClass("selected");
    });
    const bseconda = tags.h2().css({ display: "inline-block" }).addClass("yca-home-casaimpresa").html("Seconda pagina").click(() => {
        first.hide();
        second.show();
        third.hide();
        bprima.removeClass("selected");
        bseconda.addClass("selected");
        barera.removeClass("selected");
    });
    const barera = tags.h2().css({ display: "inline-block" }).addClass("yca-home-casaimpresa").html("Arera").click(() => {
        first.hide();
        second.hide();
        third.show();
        bprima.removeClass("selected");
        bseconda.removeClass("selected");
        barera.addClass("selected");
    });
    buttonHeader2.append(bprima, bseconda, barera);
    const i01 = tags.img().attr({ src: "/img/leggerebolletta/01.jpg" }).css({ maxWidth: "100%" });
    const t01 = tags.div().html(`<h3>META&rsquo; SINISTRA: Informazioni Generali</h3>
    <p><strong>CODICE CLIENTE</strong><br />Identifica il tuo codice personale univoco. Tienilo a portata di mano ogni volta che ci contatti: ci permetter&agrave; di darti velocemente tutte le informazioni di cui hai bisogno.</p>
    <p><strong>INTESTATARIO CONTRATTO</strong><br />&Egrave; l&rsquo;intestatario del contratto, titolare degli obblighi e dei diritti relativi alla fornitura.</p>
    <p><strong>CODICE CONTRATTO</strong><br />Identifica il tuo contratto di fornitura per una specifica utenza</p>
    <p><strong>INDIRIZZO FORNITURA</strong><br />&Egrave; l&rsquo;indirizzo in cui fisicamente viene consegnato il gas ovvero l&rsquo;indirizzo dell&rsquo;allacciamento. Pu&ograve; essere diverso dall&rsquo;indirizzo dell&rsquo;abitazione o dell&rsquo;azienda.</p>
    <p><strong>PDR</strong><br />&Egrave; un codice che identifica il Punto Di Riconsegna, cio&egrave; il punto fisico dove Tecniconsul Energia consegna il gas al cliente finale. Questo codice &egrave; univoco a livello nazionale: &egrave; sempre lo stesso anche se cambi fornitore. Se invece cambi casa, avrai un numero di PDR diverso.</p>
    <p><strong>CODICE REMI</strong><br />&Egrave; l&rsquo;identificativo dell&rsquo;impianto che serve il PDR.</p>
    <p><strong>TIPO OFFERTA</strong><br />&Egrave; il nome dell&rsquo;offerta del mercato libero che hai sottoscritto.</p>
    <p><strong>DATA INIZIO CONTRATTO</strong><br />&Egrave; la data di entrata in fornitura, cio&egrave; il momento dal quale Tecniconsul Energia comincia a fatturare i tuoi consumi.</p>
    <p><strong>TIPO CLIENTE</strong><br />&Egrave; il tipo di cliente intestatario della fornitura. Il &ldquo;cliente domestico&rdquo; &egrave; un privato che in genere usa il gas per la propria abitazione e locali annessi. Negli &ldquo;altri usi&rdquo; sono ricompresi tutti i clienti con partita IVA.</p>
    <p>&nbsp;</p>
    <h3>META&rsquo; DESTRA: Dati Bolletta</h3>
    <p><strong>DATI DI RECAPITO</strong><br />Questo &egrave; l&rsquo;indirizzo di spedizione della fattura. Il nome e cognome indicati servono solo per il recapito e possono essere diversi da quelli riportati a fianco. Assicurati che l&rsquo;indirizzo sia corretto e comunicaci con tempestivit&agrave; ogni variazione per evitare ritardi e disguidi nella consegna della fattura. Se per&ograve; non vuoi pensieri, registrati all&rsquo;area clienti web e passa alla bolletta online: la consegna &egrave; immediata e sicura! Scegli la tranquillit&agrave; e la semplicit&agrave;.</p>
    <p><strong>IMPORTO DA PAGARE</strong><br />&Egrave; l&rsquo;importo complessivo della fattura.</p>
    <p><strong>SCADENZA</strong><br />&Egrave; la data di scadenza del pagamento.</p>`);
    const i02 = tags.img().attr({ src: "/img/leggerebolletta/02.jpg" }).css({ maxWidth: "100%" });
    const t02 = tags.div().html(`<h3>I Miei Consumi</h3>
    <p><strong>CLASSE DEL MISURATORE</strong><br />Indica la portata del misuratore. Le abitazioni con riscaldamento individuale hanno in linea generale contatori di classe G4 (sono i pi&ugrave; piccoli).</p>
    <p><strong>COEFFICIENTE CORRETTIVO (C)</strong><br />&Egrave; il coefficiente che converte il consumo misurato dal contatore, espresso in metri cubi, nell&rsquo;unit&agrave; di misura utilizzata per la fatturazione, cio&egrave; gli standard metri cubi. Tale conversione permette che tu paghi solo per l&rsquo;effettiva quantit&agrave; di gas consumata in base alla pressione e alla temperatura di consegna. Il coefficiente &egrave; fissato dal Distributore locale che definisce le condizioni tecniche di erogazione del gas.</p>
    <p><strong>POTERE CALORIFICO SUPERIORE (P)</strong><br />&Egrave; il valore convenzionale che permette di fatturare in Smc alcune componenti di prezzo relative alla materia gas naturale che invece sono espresse in Giga/Joule.</p>
    <p><strong>TIPOLOGIA D&rsquo;USO</strong><br />Qui trovi il tipo di utilizzo. La terminologia deriva dalla normativa ed &egrave; standard</p>
    <p><strong>MATRICOLA DEL CONTATORE</strong><br />&Egrave; il codice che il produttore mette come identificazione del contatore. Generalmente si trova sotto un codice a barre.</p>
    <p><strong>CONSUMO FATTURATO</strong><br />Sono gli standard metri cubi che effettivamente vengono fatturati in bolletta. Possono essere effettivi, stimati o entrambe le tipologie di consumo.</p>
    <p><strong>GRAFICO CONSUMI</strong><br />Un semplice istogramma che visualizza a colpo d&rsquo;occhio il consumo diviso mese per mese.</p>`);
    const i03 = tags.img().attr({ src: "/img/leggerebolletta/03.jpg" }).css({ maxWidth: "100%" });
    const t03 = tags.div().html(`<h3>META&rsquo; SINISTRA: Riepilogo e importi fatturati</h3>
    <p><strong>MATERIA GAS NATURALE</strong><br />&Egrave; l&rsquo;importo richiesto a seguito delle diverse attivit&agrave; di approvvigionamento e commercializzazione svolte da Tecniconsul Energia per fornirti il gas. Per questa offerta del mercato libero, la voce &egrave; composta da:<br />&ndash; Prezzo (espresso in &euro;/Standard metri cubi, ovvero Smc);<br />&ndash; Altre componenti cos&igrave; come stabilite dall&rsquo;autorit&agrave; per il mercato di tutela, ossia:<br />&mdash; Quota di vendita al dettaglio (QVD) suddivisa in una parte fissa espressa in &euro;/anno e in una variabile in &euro;/Smc;<br />&mdash; Oneri di gradualit&agrave; (GRAD e QVDV) espressi in &euro;/Smc.<br />Per maggiori dettagli invece sulle altre componenti applicate consulta il sito dell&rsquo;autorit&agrave;.</p>
    <p class="text-gas">Per l&rsquo;offerta del mercato libero Tariffa di Riferimento, il prezzo &egrave; aggiornato con cadenza trimestrale in base a quanto stabilito dalla Delibera dell&rsquo;Autorit&agrave; (AEEGSI) n. 196/2013 relativamente alla Componente Materia Prima per il Mercato di Tutela.</p>
    <p><strong>TRASPORTO E GESTIONE CONTATORE</strong><br />&Egrave; l&rsquo;importo richiesto a seguito dei costi sostenuti dal distributore per:<br />&ndash; trasportare il gas attraverso la rete di distruzione nazionale e locale fino al tuo punto di fornitura;<br />&ndash; gestire il tuo contatore con i relativi dati di lettura.<br />Questi costi non dipendono da Tecniconsul Energia ma sono determinati dall&rsquo;Autorit&agrave; (AEEGSI) per tutti i venditori. Questa voce contiene le seguenti componenti:<br />&ndash; Quota trasporto (Qt) espressa in &euro;/Smc e differenziata per le diverse aree del territorio nazionale;<br />&ndash; Tariffa di distribuzione e misura (&tau;1, &tau;3) suddivisa in una parte fissa (espressa in &euro;/anno) e in una parte variabile (espressa in &euro;/Smc) e differenziata per le diverse aree del territorio nazionale<br />&ndash; Componenti RS e UG1 espresse in &euro;/Smc.</p>
    <p><strong>ONERI DI SISTEMA</strong><br />&Egrave; l&rsquo;importo relativo alla copertura di costi per attivit&agrave; di interesse generale per il sistema gas, come ad esempio lo sviluppo delle fonti rinnovabili e progetti per il risparmio energetico. Questi costi non dipendono da Tecniconsul Energia ma sono determinati dall&rsquo;Autorit&agrave; (AEEGSI). Quindi questa voce contiene le stesse componenti previste per il mercato di tutela, ossia:<br />&ndash; Componenti GS, RE e UG3 espresse in &euro;/Smc;<br />&ndash; Componente UG2 suddivisa in una parte fissa (espressa in &euro;/anno) e in una parte variabile (espressa in &euro;/Smc).</p>
    <h3>META&rsquo; DESTRA: Informazioni e Recapiti</h3>
    <p><strong>CONTATTI</strong><br />Questa sezione riporta tutti i riferimenti utili per restare in contatto con Tecniconsul Energia.</p>`);
    const i04 = tags.img().attr({ src: "/img/leggerebolletta/04.jpg" }).css({ maxWidth: "100%" });
    const t04 = tags.div().html(`<p><strong>DETTAGLIO IMPOSTE</strong><br />Il totale dovuto per le imposte &egrave; una delle voci del &ldquo;Riepilogo Importi Fatturati&rdquo; in prima pagina. Questo riquadro mostra il dettaglio della loro applicazione, in particolare vengono indicate l&rsquo;aliquota prevista dalla normativa fiscale, le quantit&agrave; alle quali l&rsquo;aliquota &egrave; applicata e l&rsquo;importo dovuto. Con la bolletta del gas si pagano l&rsquo;imposta di consumo, l&rsquo;addizionale regionale e l&rsquo;imposta sul valore aggiunto (IVA). Ti ricordiamo che le imposte non dipendono dalla societ&agrave; di vendita ma vengono da questa riversate allo Stato o alle Regioni secondo quanto previsto dalla normativa.</p>
    <p><strong>QUADRO LETTURE E CONSUMI</strong><br />Le letture &ldquo;Rilevate&rdquo; (prese dalla societ&agrave; di distribuzione) e le &ldquo;Autoletture&rdquo; (comunicate dal cliente attraverso i canali indicati in prima pagina) sono letture effettive e danno luogo a eventuali conguagli. In mancanza di queste, in fattura vengono indicati i metri cubi stimati ovvero si stima quanto dovrebbe segnare il contatore ad una certa data. Di conseguenza i consumi fatturati in bolletta possono essere effettivi o stimati.</p>
    <p><strong>CONSUMO ANNUO</strong><br />&Egrave; il tuo consumo relativo a 12 mesi di fornitura. Se non abbiamo le letture effettive il dato si basa su stime. Se sei cliente da meno di un anno, invece del &ldquo;Consumo annuo&rdquo; il valore si riferir&agrave; al &ldquo;Consumo da inizio fornitura&rdquo; che appunto &egrave; il consumo rilevato o stimato da quando sei entrato in fornitura.</p>
    <p><strong>ALTRE PARTITE</strong><br />In questo riquadro trovi il dettaglio di cosa ti &egrave; stato fatturato sotto la voce &ldquo;Altre Partite&rdquo; nella &ldquo;Riepilogo Importi Fatturati&rdquo; in prima pagina.</p>
    <p><strong>BOLLETTE NON PAGATE</strong><br />In questa sezione &egrave; riportato il dettaglio delle fatture scadute e non pagate con relativo numero, scadenza e importo. La presenza di fatture &egrave; un dato su cui ti invitiamo a riporre la massima attenzione per evitare interruzioni del servizio.</p>
    <p><strong>INTERESSI DI MORA</strong><br />Questo DATO &egrave; presente solo nel caso in cui ti siano stati addebitati interessi di mora per il ritardato pagamento di fatture precedenti. Gli interessi vengono applicati sulla base dei giorni di ritardo a partire dal giorno di scadenza applicando gli interessi di legge.</p>
    <p><strong>INFORMAZIONI UTILI</strong><br />Questa sezione contiene numerose informazioni utili su molteplici argomenti: sui pagamenti come e dove effettuarli senza commissioni, come e quando richiedere una rateizzazione. Trovi anche le Comunicazioni dell&rsquo;Autorit&agrave; (AEEGSI) e, nel caso di aggiornamento dei corrispettivi dovuti, l&rsquo;eventuale fonte normativa o contrattuale dalla quale derivano. Non possono mancare neanche delle preziose avvertenze sull&rsquo;uso del metano e molto altro.</p>`);
    const arera = tags.div().html(`<h2>Bolletta 2.0</h2>
    <p>&nbsp;</p>
    <p><strong>Informazioni presenti nella bolletta sintetica dell&rsquo;elettricit&agrave;</strong></p>
    <p><a href="https://bolletta.autorita.energia.it/bolletta20/index.php/home/elettricita/dati-del-cliente-e-della-fornitura" target="_blank" rel="noopener">ARERA &ndash;&nbsp;Come leggere la tua bolletta</a></p>
    <p>&nbsp;</p>
    <p><strong>Glossario della bolletta per la fornitura di Luce e Gas</strong></p>
    <p><a href="http://www.tecniconsulenergia.it/allegati/Glossario_Luce_Gas.pdf" target="_blank" rel="noopener">ARERA &ndash;&nbsp;Glossario</a></p>
    <p>&nbsp;</p>
    <p><strong>Voci di spesa</strong><br /><a href="https://bolletta.autorita.energia.it/bolletta20/index.php/guida-voci-di-spesa/elettricita" target="_blank" rel="noopener">ARERA &ndash; Guida alla lettura delle voci di spesa</a></p>`);
    const first = tags.div().show().append(i01, t01, i02, t02, i03, t03);
    const second = tags.div().hide().append(i04, t04);
    const third = tags.div().hide().append(arera);
    template.main.append(buttonHeader2, first, second, third);
}
exports.build = build;

},{"../shards/_lib":75,"../tags":90,"../template/_lib":91}],60:[function(require,module,exports){
arguments[4][59][0].apply(exports,arguments)
},{"../shards/_lib":75,"../tags":90,"../template/_lib":91,"dup":59}],61:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const templates = require("../template/_lib");
const forms = require("./form");
function build() {
    const template = templates.main.main();
    const modal = forms.buildLoginModal();
    template.main.append(modal.el);
    window.scrollTo(0, 0);
    modal.show();
}
exports.build = build;

},{"../template/_lib":91,"./form":57}],62:[function(require,module,exports){
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const tag = require("../tags");
const templates = require("../template/_lib");
const api = require("../api/_lib");
function build() {
    const template = templates.main.main();
    const reportNews = tag.div();
    const buttonNews = tag.button().text("Sincronizza news").on("click", () => __awaiter(this, void 0, void 0, function* () {
        const news = yield api.migrazione.getNews();
        reportNews.append(tag.p().text("Ci sono da scaricare " + news.length + " elementi"));
        let count = 1;
        for (const n of news) {
            try {
                if (n.banner) {
                    const pi = yield api.migrazione.putImage(n.banner);
                    n.banner = n.banner.replace("https://tecniconsul.s3.amazonaws.com/", "/uploadedImg/");
                }
                const res = yield api.news.setNews(n);
                console.log("Put", res.id);
                reportNews.append(tag.p().text("Elemento " + count + " migrata"));
            }
            catch (e) {
                reportNews.append(tag.p().text("Elemento " + count + " impossibile da migrare"));
            }
            count++;
        }
    }));
    const reportComuni = tag.div();
    const buttonComuni = tag.button().text("Sincronizza comuni").on("click", () => __awaiter(this, void 0, void 0, function* () {
        const comuni = yield api.migrazione.getComuni();
        reportComuni.append(tag.p().text("Ci sono da scaricare " + comuni.length + " elementi"));
        let count = 1;
        for (const n of comuni) {
            try {
                const res = yield api.comuni.setComune(n);
                console.log("Put", res.id);
                reportComuni.append(tag.p().text("Elemento " + count + " migrata"));
            }
            catch (e) {
                reportComuni.append(tag.p().text("Elemento " + count + " impossibile da migrare"));
            }
            count++;
        }
    }));
    const reportCabine = tag.div();
    const buttonCabine = tag.button().text("Sincronizza cabine").on("click", () => __awaiter(this, void 0, void 0, function* () {
        const cabine = yield api.migrazione.getCabine();
        reportCabine.append(tag.p().text("Ci sono da scaricare " + cabine.length + " elementi"));
        let count = 1;
        for (const n of cabine) {
            try {
                const res = yield api.comuni.setCabina(n);
                console.log("Put", res.id);
                reportCabine.append(tag.p().text("Elemento " + count + " migrata"));
            }
            catch (e) {
                reportCabine.append(tag.p().text("Elemento " + count + " impossibile da migrare"));
            }
            count++;
        }
    }));
    const reportRegioni = tag.div();
    const buttonRegioni = tag.button().text("Sincronizza regioni").on("click", () => __awaiter(this, void 0, void 0, function* () {
        const regioni = yield api.migrazione.getRegioni();
        reportRegioni.append(tag.p().text("Ci sono da scaricare " + regioni.length + " elementi"));
        let count = 1;
        for (const n of regioni) {
            try {
                const res = yield api.regioni.setRegione(n);
                console.log("Put", res.id);
                reportRegioni.append(tag.p().text("Elemento " + count + " migrata"));
            }
            catch (e) {
                reportRegioni.append(tag.p().text("Elemento " + count + " impossibile da migrare"));
            }
            count++;
        }
    }));
    const reportProvince = tag.div();
    const buttonProvince = tag.button().text("Sincronizza province").on("click", () => __awaiter(this, void 0, void 0, function* () {
        const province = yield api.migrazione.getProvince();
        reportProvince.append(tag.p().text("Ci sono da scaricare " + province.length + " elementi"));
        let count = 1;
        for (const n of province) {
            try {
                const res = yield api.generic.setProvincia(n);
                console.log("Put", res.id);
                reportProvince.append(tag.p().text("Elemento " + count + " migrata"));
            }
            catch (e) {
                reportProvince.append(tag.p().text("Elemento " + count + " impossibile da migrare"));
            }
            count++;
        }
    }));
    const reportFaq = tag.div();
    const buttonFaq = tag.button().text("Sincronizza Faq").on("click", () => __awaiter(this, void 0, void 0, function* () {
        const faqs = yield api.migrazione.getFAQ();
        reportFaq.append(tag.p().text("Ci sono da scaricare " + faqs.length + " elementi"));
        let count = 1;
        for (const n of faqs) {
            try {
                if (n.logo) {
                    const pi = yield api.migrazione.putImage(n.logo);
                    n.logo = n.logo.replace("https://tecniconsul.s3.amazonaws.com/", "/uploadedImg/");
                }
                const res = yield api.faq.setFAQ(n);
                console.log("Put", res);
                reportFaq.append(tag.p().text("Elemento " + count + " migrata"));
            }
            catch (e) {
                reportFaq.append(tag.p().text("Elemento " + count + " impossibile da migrare"));
            }
            count++;
        }
    }));
    const reportUtils = tag.div();
    const buttonUtils = tag.button().text("Sincronizza utils").on("click", () => __awaiter(this, void 0, void 0, function* () {
        const utils = yield api.migrazione.getUtils();
        reportUtils.append(tag.p().text("Ci sono da scaricare " + utils.length + " elementi"));
        let count = 1;
        for (const n of utils) {
            try {
                const res = yield api.utils.setUtils(n);
                console.log("Put", res.id);
                reportUtils.append(tag.p().text("Elemento " + count + " migrata"));
            }
            catch (e) {
                reportUtils.append(tag.p().text("Elemento " + count + " impossibile da migrare"));
            }
            count++;
        }
    }));
    const reportUffici = tag.div();
    const buttonUffici = tag.button().text("Sincronizza uffici").on("click", () => __awaiter(this, void 0, void 0, function* () {
        const uffici = yield api.migrazione.getUffici();
        reportUffici.append(tag.p().text("Ci sono da scaricare " + uffici.length + " elementi"));
        let count = 1;
        for (const n of uffici) {
            try {
                const res = yield api.uffici.setUfficio(n);
                console.log("Put", res.id);
                reportUffici.append(tag.p().text("Elemento " + count + " migrata"));
            }
            catch (e) {
                reportUffici.append(tag.p().text("Elemento " + count + " impossibile da migrare"));
            }
            count++;
        }
    }));
    const buttonAll = tag.button().text("Sincronizza TUTTO").on("click", () => __awaiter(this, void 0, void 0, function* () {
        buttonNews.click();
        buttonUtils.click();
        buttonUffici.click();
        buttonFaq.click();
        buttonRegioni.click();
        buttonProvince.click();
        buttonComuni.click();
        buttonCabine.click();
    }));
    template.main.append(buttonNews, reportNews, tag.hr(), buttonRegioni, reportRegioni, tag.hr(), buttonProvince, reportProvince, tag.hr(), buttonComuni, reportComuni, tag.hr(), buttonCabine, reportCabine, tag.hr(), buttonFaq, reportFaq, tag.hr(), buttonUtils, reportUtils, tag.hr(), buttonUffici, reportUffici, tag.hr(), buttonAll);
}
exports.build = build;

},{"../api/_lib":2,"../tags":90,"../template/_lib":91}],63:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const templates = require("../template/_lib");
const shards = require("../shards/_lib");
const api = require("../api/_lib");
function build(args) {
    console.log("got param:", args);
    const template = templates.main.main();
    const upperSection = tags.div().addClass("yca-wrapper");
    const title = tags.h1().html("BLOG NEWS").addClass("yca-blognews-title");
    upperSection.append(title);
    const lowerSection = tags.div().addClass("yca-wrapper");
    api.news.getNews().then(news => {
        for (const n of news) {
            lowerSection.append(shards.news.news({ title: n.title, subtitle: n.subtitle, image: n.banner, date: n.date, body: n.body, id: n._id }));
        }
        template.main.append(upperSection, lowerSection);
        if (args) {
            location.hash = "#" + args.id;
        }
    });
}
exports.build = build;

},{"../api/_lib":2,"../shards/_lib":75,"../tags":90,"../template/_lib":91}],64:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const templates = require("../template/_lib");
const shards = require("../shards/_lib");
const api = require("../api/_lib");
function build(args) {
    //CONFIG
    const blueBox = [
        { text: "leggere la bolletta", logo: "/img/white_schermo.png", url: [
                { name: "Leggere bolletta luce", url: "/leggerebollettaluce" },
                { name: "Leggere bolletta gas", url: "/leggerebollettagas" }
            ] },
        { text: "agevolazioni", logo: "/img/white_colonna.png", url: "/utils/agevolazioni" },
        { text: "come risparmiare", logo: "/img/white_money.png", url: "/utils/come_risparmiare" }
    ];
    const template = templates.main.main();
    const upperSection = tags.div().addClass("yca-wrapper").css({
        backgroundImage: "url(/img/lavoro.jpg)",
        backgroundSize: "cover",
        width: "80vw",
        height: "36vw",
        position: "relative"
    });
    const bottomFloater = tags.div().addClass("yca-home-bottomfloater");
    const usClaim = tags.h1().addClass("yca-home-claim").html("NUOVA ENERGIA<BR />NELLA TUA GIORNATA");
    upperSection.append(bottomFloater.append(usClaim));
    const middleSection = tags.div().addClass("yca-wrapper");
    const ourOfferts = tags.h1().addClass("yca-home-underlined").text("Le nostre offerte");
    const wrapperOfferte = tags.div();
    const offerte = tags.div().css({
        width: "60vw",
        flex: "1",
        height: "36vw",
        display: "inline-block"
    });
    const offLinkCont = tags.div();
    const linkCasa = tags.h2().css({ display: "inline-block" }).addClass("yca-home-casaimpresa").html("casa");
    const linkImpresa = tags.h2().css({ display: "inline-block" }).addClass("yca-home-casaimpresa").html("impresa");
    const myByttStyle = {
        color: "rgb(255, 255, 255)",
        borderRadius: "10px",
        backgroundColor: "rgb(102, 102, 102)",
        width: "270px",
        height: "90px",
        fontSize: "24px",
        border: "0px",
        display: "block",
        margin: "10px",
        textAlign: "center",
        cursor: "pointer"
    };
    const threeButt = tags.div().addClass("yca-offerte-threebuttons").css({ float: "right", /*height: "36vw",*/ display: "flex", margin: "8vw 0", flexDirection: "column" }).append(tags.div().css({ flex: "1" }).append(tags.button().css(myByttStyle).text("luce").on("click", () => { location.href = "/dettagli/luce"; })), tags.div().css({ flex: "1" }).append(tags.button().css(myByttStyle).text("gas").on("click", () => { location.href = "/dettagli/gas"; })));
    const threeButtAzienda = tags.div().addClass("yca-offerte-threebuttons").css({ float: "right", /*height: "36vw",*/ display: "flex", margin: "8vw 0", flexDirection: "column" }).append(tags.div().css({ flex: "1" }).append(tags.button().css(myByttStyle).text("luce").on("click", () => { location.href = "/dettagli/luceazienda"; })), tags.div().css({ flex: "1" }).append(tags.button().css(myByttStyle).text("gas").on("click", () => { location.href = "/dettagli/gasazienda"; })));
    wrapperOfferte.addClass("yca-offerte-wrapper").append(offerte.addClass("yca-offerte-wrapper-offerte").append(offLinkCont.append(linkCasa, linkImpresa)), threeButt, threeButtAzienda).css({ backgroundImage: "url(/img/casa.jpg)" });
    linkCasa.click(() => {
        linkCasa.addClass("selected");
        linkImpresa.removeClass("selected");
        wrapperOfferte.css({ backgroundImage: "url(/img/casa.jpg)" });
        usClaim.html("LUCE E GAS<br />OVUNQUE TU SIA");
        upperSection.css({ backgroundImage: "url(/img/Lavoro_1979.jpg)" });
        threeButtAzienda.hide();
        threeButt.show();
    });
    linkImpresa.click(() => {
        linkCasa.removeClass("selected");
        linkImpresa.addClass("selected");
        wrapperOfferte.css({ backgroundImage: "url(/img/fabbrica.jpg)" });
        usClaim.html("LUCE E GAS<br />CHIUNQUE TU SIA");
        upperSection.css({ backgroundImage: "url(/img/lavoro.jpg)" });
        threeButtAzienda.show();
        threeButt.hide();
    });
    const blueSection = tags.div().addClass("yca-home-blue");
    for (let b of blueBox)
        blueSection.append(shards.panel.bluePanel(b));
    middleSection.append(ourOfferts, wrapperOfferte, blueSection);
    const segnapSlider = tags.h1().addClass("yca-home-underlined").html("News");
    //const errSlider = tags.p().html("Error: MongoDB Error: no element for slider in database.")
    const slider = shards.thumbbox.thumbBox({ scrollTime: 3000 });
    api.news.getNews().then(news => {
        const m = [];
        for (const n of news) {
            m.push(shards.news.thumbNews({
                image: n.banner,
                title: n.title,
                subtitle: n.title,
                date: n.date,
                body: n.body,
                id: n._id
            }));
        }
        slider.add(m);
    });
    template.main.append(upperSection, middleSection, segnapSlider, slider.el);
    linkCasa.click();
    if (args.offerta == "casa") {
        linkCasa.click();
    }
    if (args.offerta == "impresa") {
        linkImpresa.click();
    }
}
exports.build = build;

},{"../api/_lib":2,"../shards/_lib":75,"../tags":90,"../template/_lib":91}],65:[function(require,module,exports){
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const shards = require("../../shards/_lib");
const api = require("../../api/_lib");
function build(args) {
    return __awaiter(this, void 0, void 0, function* () {
        const template = templates.main.main();
        const upperSection = shards.bannerone.build();
        const bottomFloater = tags.div().addClass("yca-home-bottomfloater");
        const usClaim = tags.h1().addClass("yca-home-claim").html("LUCE E GAS<BR />OVUNQUE TU SIA");
        upperSection.append(bottomFloater.append(usClaim));
        const middleSection = tags.div().addClass("yca-wrapper");
        const ourOfferts = tags.h1().addClass("yca-home-underlined").text("Gas casa");
        const wrapperOfferte = tags.div().addClass("yca-offerte-wrapper").css({ display: "table-row" });
        const offerte = tags.div().css({
            // width: "50vw",
            // flex: "1",
            padding: "30px 20px",
            display: "table-cell",
        });
        const offLinkCont = tags.div();
        const images1 = tags.div().addClass("yca-offerte-containerofferte");
        const images2 = tags.div().addClass("yca-offerte-containerofferte");
        const fill = yield api.offerte.getOfferta({
            casaAziendaPlacet: "casa",
            luceGas: "gas"
        });
        const varLuce1 = fill.o1 ? {
            titolo: fill.o1.title || "",
            pre: fill.o1.intro || "",
            testo: fill.o1.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce2 = fill.o2 ? {
            titolo: fill.o2.title || "",
            pre: fill.o2.intro || "",
            testo: fill.o2.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce3 = fill.o3 ? {
            titolo: fill.o3.title || "",
            pre: fill.o3.intro || "",
            testo: fill.o3.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce4 = fill.o4 ? {
            titolo: fill.o4.title || "",
            pre: fill.o4.intro || "",
            testo: fill.o4.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        images1.append(shards.priceBadge.description(varLuce1), shards.priceBadge.description(varLuce2));
        images2.append(shards.priceBadge.description(varLuce3), shards.priceBadge.description(varLuce4));
        offLinkCont.append(images1, images2).css({});
        const threeButt = tags.div().css({
            padding: "30px 20px",
            backgroundColor: "#cccccccc",
            display: "table-cell",
            minWidth: "25vw"
        }).append(tags.div().addClass("yca-dettagli-sidebar").html("Scopri la tariffa<br />gas che meglio<br />si presta alla<br />gestione dei<br />tuoi consumi"));
        wrapperOfferte.append(offerte.append(offLinkCont).css({
            padding: "30px 20px",
            backgroundColor: "#cccccccc",
            display: "table-cell"
        }), tags.div().addClass("yca-offerte-wrapper-sep").append(tags.div()), threeButt).css({ backgroundImage: "url(/img/casa.jpg)" });
        middleSection.append(ourOfferts, wrapperOfferte);
        const segnapSlider = tags.h1().addClass("yca-home-underlined").html("News");
        const slider = shards.thumbbox.thumbBox({ scrollTime: 3000 });
        api.news.getNews().then(news => {
            const m = [];
            for (const n of news) {
                m.push(shards.news.thumbNews({
                    image: n.banner,
                    title: n.title,
                    subtitle: n.title,
                    date: n.date,
                    body: n.body,
                    id: n._id
                }));
            }
            slider.add(m);
        });
        template.main.append(upperSection, middleSection, segnapSlider, slider.el);
    });
}
exports.build = build;

},{"../../api/_lib":2,"../../shards/_lib":75,"../../tags":90,"../../template/_lib":91}],66:[function(require,module,exports){
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const shards = require("../../shards/_lib");
const api = require("../../api/_lib");
function build(args) {
    return __awaiter(this, void 0, void 0, function* () {
        const template = templates.main.main();
        const upperSection = shards.bannerone.build();
        const bottomFloater = tags.div().addClass("yca-home-bottomfloater");
        const usClaim = tags.h1().addClass("yca-home-claim").html("LUCE E GAS<BR />OVUNQUE TU SIA");
        upperSection.append(bottomFloater.append(usClaim));
        const middleSection = tags.div().addClass("yca-wrapper");
        const ourOfferts = tags.h1().addClass("yca-home-underlined").text("Gas azienda");
        const wrapperOfferte = tags.div().addClass("yca-offerte-wrapper").css({ display: "table-row" });
        const offerte = tags.div().css({
            // width: "50vw",
            // flex: "1",
            padding: "30px 20px",
            display: "table-cell",
        });
        const offLinkCont = tags.div();
        const images1 = tags.div().addClass("yca-offerte-containerofferte");
        const images2 = tags.div().addClass("yca-offerte-containerofferte");
        const fill = yield api.offerte.getOfferta({
            casaAziendaPlacet: "azienda",
            luceGas: "gas"
        });
        const varLuce1 = fill.o1 ? {
            titolo: fill.o1.title || "",
            pre: fill.o1.intro || "",
            testo: fill.o1.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce2 = fill.o2 ? {
            titolo: fill.o2.title || "",
            pre: fill.o2.intro || "",
            testo: fill.o2.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce3 = fill.o3 ? {
            titolo: fill.o3.title || "",
            pre: fill.o3.intro || "",
            testo: fill.o3.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce4 = fill.o4 ? {
            titolo: fill.o4.title || "",
            pre: fill.o4.intro || "",
            testo: fill.o4.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        images1.append(shards.priceBadge.description(varLuce1), shards.priceBadge.description(varLuce2));
        images2.append(shards.priceBadge.description(varLuce3), shards.priceBadge.description(varLuce4));
        offLinkCont.append(images1, images2).css({});
        const threeButt = tags.div().css({
            padding: "30px 20px",
            backgroundColor: "#cccccccc",
            display: "table-cell",
            minWidth: "25vw"
        }).append(tags.div().addClass("yca-dettagli-sidebar").html("Scopri la tariffa<br />gas che meglio<br />si presta alla<br />gestione dei<br />tuoi consumi"));
        wrapperOfferte.append(offerte.append(offLinkCont).css({
            padding: "30px 20px",
            backgroundColor: "#cccccccc",
            display: "table-cell"
        }), tags.div().addClass("yca-offerte-wrapper-sep").append(tags.div()), threeButt).css({ backgroundImage: "url(/img/fabbrica.jpg)" });
        middleSection.append(ourOfferts, wrapperOfferte);
        const segnapSlider = tags.h1().addClass("yca-home-underlined").html("News");
        const slider = shards.thumbbox.thumbBox({ scrollTime: 3000 });
        api.news.getNews().then(news => {
            const m = [];
            for (const n of news) {
                m.push(shards.news.thumbNews({
                    image: n.banner,
                    title: n.title,
                    subtitle: n.title,
                    date: n.date,
                    body: n.body,
                    id: n._id
                }));
            }
            slider.add(m);
        });
        template.main.append(upperSection, middleSection, segnapSlider, slider.el);
    });
}
exports.build = build;

},{"../../api/_lib":2,"../../shards/_lib":75,"../../tags":90,"../../template/_lib":91}],67:[function(require,module,exports){
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const shards = require("../../shards/_lib");
const api = require("../../api/_lib");
function build(args) {
    return __awaiter(this, void 0, void 0, function* () {
        const template = templates.main.main();
        const upperSection = shards.bannerone.build();
        const bottomFloater = tags.div().addClass("yca-home-bottomfloater");
        const usClaim = tags.h1().addClass("yca-home-claim").html("LUCE E GAS<BR />OVUNQUE TU SIA");
        upperSection.append(bottomFloater.append(usClaim));
        const middleSection = tags.div().addClass("yca-wrapper");
        const ourOfferts = tags.h1().addClass("yca-home-underlined").text("Luce casa");
        const wrapperOfferte = tags.div().addClass("yca-offerte-wrapper").css({ display: "table-row" });
        const offerte = tags.div().css({
            // width: "50vw",
            // flex: "1",
            padding: "30px 20px",
            display: "table-cell",
        });
        const offLinkCont = tags.div();
        const images1 = tags.div().addClass("yca-offerte-containerofferte");
        const images2 = tags.div().addClass("yca-offerte-containerofferte");
        const fill = yield api.offerte.getOfferta({
            casaAziendaPlacet: "casa",
            luceGas: "luce"
        });
        const varLuce1 = fill.o1 ? {
            titolo: fill.o1.title || "",
            pre: fill.o1.intro || "",
            testo: fill.o1.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce2 = fill.o2 ? {
            titolo: fill.o2.title || "",
            pre: fill.o2.intro || "",
            testo: fill.o2.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce3 = fill.o3 ? {
            titolo: fill.o3.title || "",
            pre: fill.o3.intro || "",
            testo: fill.o3.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce4 = fill.o4 ? {
            titolo: fill.o4.title || "",
            pre: fill.o4.intro || "",
            testo: fill.o4.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        images1.append(shards.priceBadge.description(varLuce1), shards.priceBadge.description(varLuce2));
        images2.append(shards.priceBadge.description(varLuce3), shards.priceBadge.description(varLuce4));
        offLinkCont.append(images1, images2).css({});
        const threeButt = tags.div().css({
            padding: "30px 20px",
            backgroundColor: "#cccccccc",
            display: "table-cell",
            minWidth: "25vw"
        }).append(tags.div().addClass("yca-dettagli-sidebar").html("Scopri la tariffa<br />luce che meglio<br />si presta alla<br />gestione dei<br />tuoi consumi"));
        wrapperOfferte.append(offerte.append(offLinkCont).css({
            padding: "30px 20px",
            backgroundColor: "#cccccccc",
            display: "table-cell"
        }), tags.div().addClass("yca-offerte-wrapper-sep").append(tags.div()), threeButt).css({ backgroundImage: "url(/img/casa.jpg)" });
        middleSection.append(ourOfferts, wrapperOfferte);
        const segnapSlider = tags.h1().addClass("yca-home-underlined").html("News");
        const slider = shards.thumbbox.thumbBox({ scrollTime: 3000 });
        api.news.getNews().then(news => {
            const m = [];
            for (const n of news) {
                m.push(shards.news.thumbNews({
                    image: n.banner,
                    title: n.title,
                    subtitle: n.title,
                    date: n.date,
                    body: n.body,
                    id: n._id
                }));
            }
            slider.add(m);
        });
        template.main.append(upperSection, middleSection, segnapSlider, slider.el);
    });
}
exports.build = build;

},{"../../api/_lib":2,"../../shards/_lib":75,"../../tags":90,"../../template/_lib":91}],68:[function(require,module,exports){
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const shards = require("../../shards/_lib");
const api = require("../../api/_lib");
function build(args) {
    return __awaiter(this, void 0, void 0, function* () {
        const template = templates.main.main();
        const upperSection = shards.bannerone.build();
        const bottomFloater = tags.div().addClass("yca-home-bottomfloater");
        const usClaim = tags.h1().addClass("yca-home-claim").html("LUCE E GAS<BR />OVUNQUE TU SIA");
        upperSection.append(bottomFloater.append(usClaim));
        const middleSection = tags.div().addClass("yca-wrapper");
        const ourOfferts = tags.h1().addClass("yca-home-underlined").text("Luce azienda");
        const wrapperOfferte = tags.div().addClass("yca-offerte-wrapper").css({ display: "table-row" });
        const offerte = tags.div().css({
            // width: "50vw",
            // flex: "1",
            padding: "30px 20px",
            display: "table-cell",
        });
        const offLinkCont = tags.div();
        const images1 = tags.div().addClass("yca-offerte-containerofferte");
        const images2 = tags.div().addClass("yca-offerte-containerofferte");
        const fill = yield api.offerte.getOfferta({
            casaAziendaPlacet: "azienda",
            luceGas: "luce"
        });
        const varLuce1 = fill.o1 ? {
            titolo: fill.o1.title || "",
            pre: fill.o1.intro || "",
            testo: fill.o1.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce2 = fill.o2 ? {
            titolo: fill.o2.title || "",
            pre: fill.o2.intro || "",
            testo: fill.o2.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce3 = fill.o3 ? {
            titolo: fill.o3.title || "",
            pre: fill.o3.intro || "",
            testo: fill.o3.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce4 = fill.o4 ? {
            titolo: fill.o4.title || "",
            pre: fill.o4.intro || "",
            testo: fill.o4.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        images1.append(shards.priceBadge.description(varLuce1), shards.priceBadge.description(varLuce2));
        images2.append(shards.priceBadge.description(varLuce3), shards.priceBadge.description(varLuce4));
        offLinkCont.append(images1, images2).css({});
        const threeButt = tags.div().css({
            padding: "30px 20px",
            backgroundColor: "#cccccccc",
            display: "table-cell",
            minWidth: "25vw"
        }).append(tags.div().addClass("yca-dettagli-sidebar").html("Scopri la tariffa<br />luce che meglio<br />si presta alla<br />gestione dei<br />tuoi consumi"));
        wrapperOfferte.append(offerte.append(offLinkCont).css({
            padding: "30px 20px",
            backgroundColor: "#cccccccc",
            display: "table-cell"
        }), tags.div().addClass("yca-offerte-wrapper-sep").append(tags.div()), threeButt).css({ backgroundImage: "url(/img/fabbrica.jpg)" });
        middleSection.append(ourOfferts, wrapperOfferte);
        const segnapSlider = tags.h1().addClass("yca-home-underlined").html("News");
        const slider = shards.thumbbox.thumbBox({ scrollTime: 3000 });
        api.news.getNews().then(news => {
            const m = [];
            for (const n of news) {
                m.push(shards.news.thumbNews({
                    image: n.banner,
                    title: n.title,
                    subtitle: n.title,
                    date: n.date,
                    body: n.body,
                    id: n._id
                }));
            }
            slider.add(m);
        });
        template.main.append(upperSection, middleSection, segnapSlider, slider.el);
    });
}
exports.build = build;

},{"../../api/_lib":2,"../../shards/_lib":75,"../../tags":90,"../../template/_lib":91}],69:[function(require,module,exports){
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const shards = require("../../shards/_lib");
const api = require("../../api/_lib");
function build(args) {
    return __awaiter(this, void 0, void 0, function* () {
        const template = templates.main.main();
        const upperSection = shards.bannerone.build();
        const bottomFloater = tags.div().addClass("yca-home-bottomfloater");
        const usClaim = tags.h1().addClass("yca-home-claim").html("LUCE E GAS<BR />OVUNQUE TU SIA");
        upperSection.append(bottomFloater.append(usClaim));
        const middleSection = tags.div().addClass("yca-wrapper");
        const ourOfferts = tags.h1().addClass("yca-home-underlined").text("Gas casa e azienda");
        const wrapperOfferte = tags.div();
        const offerte = tags.div().css({
            width: "50vw",
            flex: "1",
            height: "36vw",
            display: "inline-block"
        });
        const offLinkCont = tags.div();
        const threeButt = tags.div().css({
            float: "right",
            height: "36vw",
            backgroundColor: "#cccccccc",
            padding: "30px 20px"
        }).append(tags.div().addClass("yca-dettagli-sidebar").html("Scopri la tariffa<br />gas che meglio<br />si presta alla<br />gestione dei<br />tuoi consumi"));
        wrapperOfferte.append(offerte.append(offLinkCont), threeButt).css({ backgroundImage: "url(/img/casa.jpg)" });
        middleSection.append(ourOfferts, wrapperOfferte);
        const segnapSlider = tags.h1().addClass("yca-home-underlined").html("News");
        const slider = shards.thumbbox.thumbBox({ scrollTime: 3000 });
        api.news.getNews().then(news => {
            const m = [];
            for (const n of news) {
                m.push(shards.news.thumbNews({
                    image: n.banner,
                    title: n.title,
                    subtitle: n.title,
                    date: n.date,
                    body: n.body,
                    id: n._id
                }));
            }
            slider.add(m);
        });
        const multicolumn = tags.div().addClass("yca-offerte-multicolumn");
        const images1 = tags.div().addClass("yca-offerte-containerofferte");
        const images2 = tags.div().addClass("yca-offerte-containerofferte");
        const fill = yield api.offerte.getOfferta({
            casaAziendaPlacet: "placet",
            luceGas: "gas"
        });
        const varLuce1 = fill.o1 ? {
            titolo: fill.o1.title || "",
            pre: fill.o1.intro || "",
            testo: fill.o1.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce2 = fill.o2 ? {
            titolo: fill.o2.title || "",
            pre: fill.o2.intro || "",
            testo: fill.o2.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce3 = fill.o3 ? {
            titolo: fill.o3.title || "",
            pre: fill.o3.intro || "",
            testo: fill.o3.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce4 = fill.o4 ? {
            titolo: fill.o4.title || "",
            pre: fill.o4.intro || "",
            testo: fill.o4.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        images1.append(shards.priceBadge.description(varLuce1), shards.priceBadge.description(varLuce2));
        images2.append(shards.priceBadge.description(varLuce3), shards.priceBadge.description(varLuce4));
        const div = tags.div().addClass("yca-placet").append(tags.h1().html("Vantaggi").css({ color: "#3260ab", textAlign: "center" }), tags.h3().html("Servizio bolletta online"), tags.hr(), tags.h3().html("Prezzo variabile che segue l'andamento dei mercati all'ingrosso"), tags.hr(), tags.h3().html("Condizioni generali di fornitura fissate dall'autorità"), tags.h1().html("Come si attiva").css({ color: "#3260ab", textAlign: "center" }), tags.strong().html("Sportello T.E."), tags.br(), tags.span().html("Recati presso i nostri sportelli della tua zona per saperne di più sulle nostre offerte ed entrare in un mondo di vantaggi"), tags.br(), tags.br(), tags.strong().html("Chiamaci"), tags.br(), tags.span().html("Contattaci al nostro numero verde: un consulente risponderà ad ogni tua domanda o ti raggiungerà a domicilio per illustrarti le offerte a te dedicate."), tags.br(), tags.br(), tags.strong().html("Contattaci online"), tags.br(), tags.span().html("Scrivici all'indirizzo e-mail indicato nella sezione \"contatti\": ti risponderemo al più presto."), tags.br(), tags.br(), tags.hr(), tags.strong().append(tags.a().attr({ href: "https://www.arera.it/it/consumatori/placet.htm" }).html("L'autorità")));
        template.main.append(upperSection, middleSection, multicolumn, images1, images2, div, segnapSlider, slider.el);
    });
}
exports.build = build;

},{"../../api/_lib":2,"../../shards/_lib":75,"../../tags":90,"../../template/_lib":91}],70:[function(require,module,exports){
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../../tags");
const templates = require("../../template/_lib");
const shards = require("../../shards/_lib");
const api = require("../../api/_lib");
function build(args) {
    return __awaiter(this, void 0, void 0, function* () {
        const template = templates.main.main();
        const upperSection = shards.bannerone.build();
        const bottomFloater = tags.div().addClass("yca-home-bottomfloater");
        const usClaim = tags.h1().addClass("yca-home-claim").html("LUCE E GAS<BR />OVUNQUE TU SIA");
        upperSection.append(bottomFloater.append(usClaim));
        const middleSection = tags.div().addClass("yca-wrapper");
        const ourOfferts = tags.h1().addClass("yca-home-underlined").text("Luce casa e azienda");
        const wrapperOfferte = tags.div();
        const offerte = tags.div().css({
            width: "50vw",
            flex: "1",
            height: "36vw",
            display: "inline-block"
        });
        const offLinkCont = tags.div();
        const threeButt = tags.div().css({
            float: "right",
            height: "36vw",
            backgroundColor: "#cccccccc",
            padding: "30px 20px"
        }).append(tags.div().addClass("yca-dettagli-sidebar").html("Scopri la tariffa<br />luce che meglio<br />si presta alla<br />gestione dei<br />tuoi consumi"));
        wrapperOfferte.append(offerte.append(offLinkCont), threeButt).css({ backgroundImage: "url(/img/casa.jpg)" });
        middleSection.append(ourOfferts, wrapperOfferte);
        const segnapSlider = tags.h1().addClass("yca-home-underlined").html("News");
        const slider = shards.thumbbox.thumbBox({ scrollTime: 3000 });
        api.news.getNews().then(news => {
            const m = [];
            for (const n of news) {
                m.push(shards.news.thumbNews({
                    image: n.banner,
                    title: n.title,
                    subtitle: n.title,
                    date: n.date,
                    body: n.body,
                    id: n._id
                }));
            }
            slider.add(m);
        });
        const multicolumn = tags.div().addClass("yca-offerte-multicolumn");
        const images1 = tags.div().addClass("yca-offerte-containerofferte");
        const images2 = tags.div().addClass("yca-offerte-containerofferte");
        const fill = yield api.offerte.getOfferta({
            casaAziendaPlacet: "placet",
            luceGas: "luce"
        });
        const varLuce1 = fill.o1 ? {
            titolo: fill.o1.title || "",
            pre: fill.o1.intro || "",
            testo: fill.o1.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce2 = fill.o2 ? {
            titolo: fill.o2.title || "",
            pre: fill.o2.intro || "",
            testo: fill.o2.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce3 = fill.o3 ? {
            titolo: fill.o3.title || "",
            pre: fill.o3.intro || "",
            testo: fill.o3.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        const varLuce4 = fill.o4 ? {
            titolo: fill.o4.title || "",
            pre: fill.o4.intro || "",
            testo: fill.o4.description || ""
        } : {
            titolo: "",
            pre: "",
            testo: ""
        };
        images1.append(shards.priceBadge.description(varLuce1), shards.priceBadge.description(varLuce2));
        images2.append(shards.priceBadge.description(varLuce3), shards.priceBadge.description(varLuce4));
        const div = tags.div().addClass("yca-placet").append(tags.h1().html("Vantaggi").css({ color: "#3260ab", textAlign: "center" }), tags.h3().html("Servizio bolletta online"), tags.hr(), tags.h3().html("Prezzo variabile che segue l'andamento dei mercati all'ingrosso"), tags.hr(), tags.h3().html("Condizioni generali di fornitura fissate dall'autorità"), tags.h1().html("Come si attiva").css({ color: "#3260ab", textAlign: "center" }), tags.strong().html("Sportello T.E."), tags.br(), tags.span().html("Recati presso i nostri sportelli della tua zona per saperne di più sulle nostre offerte ed entrare in un mondo di vantaggi"), tags.br(), tags.br(), tags.strong().html("Chiamaci"), tags.br(), tags.span().html("Contattaci al nostro numero verde: un consulente risponderà ad ogni tua domanda o ti raggiungerà a domicilio per illustrarti le offerte a te dedicate."), tags.br(), tags.br(), tags.strong().html("Contattaci online"), tags.br(), tags.span().html("Scrivici all'indirizzo e-mail indicato nella sezione \"contatti\": ti risponderemo al più presto."), tags.br(), tags.br(), tags.hr(), tags.strong().append(tags.a().attr({ href: "https://www.arera.it/it/consumatori/placet.htm" }).html("L'autorità")));
        template.main.append(upperSection, middleSection, multicolumn, images1, images2, div, segnapSlider, slider.el);
    });
}
exports.build = build;

},{"../../api/_lib":2,"../../shards/_lib":75,"../../tags":90,"../../template/_lib":91}],71:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const templates = require("../template/_lib");
function build(language) {
    const template = templates.main.main();
    const lowerSection = tags.div().addClass("yca-wrapper").append(tags.p().html("Configuring backup data to server. Retry in few hours.<br />Configurazione degli aggiornamenti/backup su server. Riprovare tra qualche ora."));
    template.main.append(lowerSection);
}
exports.build = build;

},{"../tags":90,"../template/_lib":91}],72:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const templates = require("../template/_lib");
const shards = require("../shards/_lib");
const api = require("../api/_lib");
const _lib_1 = require("../_lib");
function build(language) {
    const template = templates.main.main();
    const upperSection = tags.div().addClass("yca-wrapper").css({
        backgroundImage: "url(/img/tubi.jpg)",
        backgroundSize: "cover",
        width: "80vw",
        height: "20vw",
        position: "relative"
    });
    const bottomFloater = tags.div().addClass("yca-home-bottomfloater");
    const usClaim = tags.h1().addClass("yca-home-claim").html("Luce e Gas<br/>Ovunque tu sia");
    upperSection.append(bottomFloater.append(usClaim));
    const lowerSection = tags.div().addClass("yca-wrapper");
    const regTitle = tags.h4().addClass("yca-territori-squarepre").text("Regione");
    const comTitle = tags.h4().addClass("yca-territori-squarepre").text("Comune");
    const cabTitle = tags.h4().addClass("yca-territori-squarepre").text("Cabina");
    const sReg = tags.select();
    const sComune = tags.select();
    const sCabine = tags.select();
    api.regioni.getRegioni().then(re => {
        for (const r of re)
            sReg.append(tags.option().text(r.name).attr({ value: r._id }));
        api.comuni.getComuni({ regione: sReg.val() }).then(comuni => {
            for (const p of comuni)
                sComune.append(tags.option().text(p.name).attr({ value: p._id }));
            api.comuni.getCabine({ comune: sComune.val() }).then(cabine => {
                for (const c of cabine)
                    sCabine.append(tags.option().text(c.name).attr({ value: c._id }));
                sCabine.click();
                api.comuni.getCabina(sCabine.val()).then(cabina => {
                    // setGreenTable(cabina.tabella1[0])
                    // setPurpleTable(cabina.tabella2[0])
                    // setGreyTable(cabina.tabella3)
                    // setBlueTable(cabina.tabella4)
                });
            });
        });
    });
    sReg.change(e => {
        sComune.empty();
        api.comuni.getComuni({ regione: sReg.val() }).then(prov => {
            for (const p of prov)
                sComune.append(tags.option().text(p.name).attr({ value: p._id }));
            sComune.click();
            api.comuni.getCabine({ comune: sComune.val() }).then(cabine => {
                for (const c of cabine)
                    sCabine.append(tags.option().text(c.name).attr({ value: c._id }));
                api.comuni.getCabina(sCabine.val()).then(cabina => {
                    // setGreenTable(cabina.tabella1[0])
                    // setPurpleTable(cabina.tabella2[0])
                    // setGreyTable(cabina.tabella3)
                    // setBlueTable(cabina.tabella4)
                });
            });
        });
    });
    sComune.change(e => {
        sCabine.empty();
        api.comuni.getCabine({ comune: sComune.val() }).then(comune => {
            for (const p of comune)
                sCabine.append(tags.option().text(p.name).attr({ value: p._id }));
            sComune.click();
            api.comuni.getCabine({ comune: sCabine.val() }).then(cabine => {
                for (const c of cabine)
                    sCabine.append(tags.option().text(c.name).attr({ value: c._id }));
                api.comuni.getCabina(sCabine.val()).then(cabina => {
                    // setGreenTable(cabina.tabella1[0])
                    // setPurpleTable(cabina.tabella2[0])
                    // setGreyTable(cabina.tabella3)
                    // setBlueTable(cabina.tabella4)
                });
            });
        });
    });
    sCabine.on("change click", e => {
        api.comuni.getCabina(sCabine.val()).then(cabina => {
            // setGreenTable(cabina.tabella1[0])
            // setPurpleTable(cabina.tabella2[0])
            // setGreyTable(cabina.tabella3)
            // setBlueTable(cabina.tabella4)
            const nome = cabina.name.toLowerCase().split(' ').map((s) => s.charAt(0).toUpperCase() + s.substring(1)).join('');
            buttonWrapper.empty().append(shards.anchor.full({ text: "Clienti domestici", url: "/pdf/cabine/" + nome + "D.pdf" }), shards.anchor.full({ text: "Clienti non domestici", url: "/pdf/cabine/" + nome + "ND.pdf" }));
        });
    });
    const affianca = tags.div().addClass("yca-territori-affianca");
    const affiancaLeft = tags.div().addClass("yca-territori-affianca-left");
    const affiancaRight = tags.div().addClass("yca-territori-affianca-left");
    const affiancaAfter = tags.div().addClass("yca-territori-affianca-left");
    affiancaLeft.append(regTitle, sReg);
    affiancaRight.append(comTitle, sComune);
    affiancaAfter.append(cabTitle, sCabine);
    const centerToTables = tags.div().css({ width: "60vw", margin: "auto" }).append(tags.h4().addClass("yca-territori-title").text("Inserisci la regione a cui sei interessato"), affianca.append(affiancaLeft, affiancaRight, affiancaAfter));
    const buttonWrapper = tags.div().addClass("yca-territori-buttonwrapper");
    const disclaimer = tags.p().addClass("yca-territori-note").css({ fontSize: "10px", padding: "20px 0px" }).html("Note:<br />PCS: è il Potere calorifico superiore ovvero la quantità di calore realizzata nella combustione completa, a pressione costante di 1,01325 bar, di una unità di massa o di volume di combustibile secondo quanto previsto dalla delibera Arg/gas 64/09 e s.m.i. Le condizioni economiche di fornitura indicate, sono da ritenersi \"salvo conguaglio\" e diverse disposizioni dell'Impresa di Distri");
    const noi = tags.a().append(tags.img().addClass("yca-territori-connoi").attr({ src: "/img/noi.jpg" }).css({ width: "26vw", margin: "50px auto", display: "block", cursor: "pointer" }));
    const vieniConNoi = tags.div().addClass("yca-wrapper").css({ display: "none" });
    const modal = shards.modal.panel(_lib_1.pages.form.formVieniConNoi());
    noi.on("click", () => {
        console.log("cliccked");
        modal.show();
    });
    lowerSection.append(centerToTables, buttonWrapper, 
    // greenTable,
    // purpleTable,
    // greyTable,
    disclaimer.css({ width: "60vw", margin: "auto" }), 
    // blueTable,
    noi, vieniConNoi, modal.el);
    template.main.append(upperSection, lowerSection);
}
exports.build = build;

},{"../_lib":1,"../api/_lib":2,"../shards/_lib":75,"../tags":90,"../template/_lib":91}],73:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const templates = require("../template/_lib");
const shards = require("../shards/_lib");
function build() {
    const template = templates.main.main();
    const upperSection = tags.div().addClass("yca-wrapper").css({
        backgroundImage: "url(/img/thanks.jpg)",
        backgroundSize: "cover",
        width: "80vw",
        height: "42.5vw",
        position: "relative"
    });
    const middleSection = tags.div().addClass("yca-wrapper").css({ padding: "50px 50px" });
    middleSection.append(tags.h1().css({ textTransform: "none", fontSize: "4.8rem", color: "#3260ab" }).text("E adesso?"), tags.div().css({ display: "flex" }).append(shards.panel.stdPanel({ logo: "/img/h_mail.jpg", text: "Scopri i documenti che ti serviranno", url: "/faq/documentinecessari" })), tags.div().css({ display: "flex" }).append(shards.panel.stdPanel({ logo: "/img/h_ufficio_2.jpg", text: "L'ufficio più vicino a te", url: "/contatti" })));
    template.main.append(upperSection, middleSection);
}
exports.build = build;

},{"../shards/_lib":75,"../tags":90,"../template/_lib":91}],74:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const templates = require("../template/_lib");
function build(args) {
    console.log("got param:", args);
    const template = templates.main.main();
    const upperSection = tags.div().addClass("yca-wrapper").append(tags.h1().html(args.title));
    const lowerSection = tags.div().addClass("yca-wrapper").append(tags.div().html(args.body));
    template.main.append(upperSection, lowerSection).addClass(`yca-utils-${args.slug}`);
}
exports.build = build;

},{"../tags":90,"../template/_lib":91}],75:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const banner = require("./banner");
exports.banner = banner;
const bannerone = require("./bannerone");
exports.bannerone = bannerone;
const smallBanner = require("./smallBanner");
exports.smallBanner = smallBanner;
const related = require("./related");
exports.related = related;
const slider = require("./slider");
exports.slider = slider;
const sliderElement = require("./sliderElement");
exports.sliderElement = sliderElement;
const panel = require("./panel");
exports.panel = panel;
const modal = require("./modal");
exports.modal = modal;
const map = require("./map");
exports.map = map;
const anchor = require("./anchor");
exports.anchor = anchor;
const faq = require("./faq");
exports.faq = faq;
const thumbbox = require("./thumbBox");
exports.thumbbox = thumbbox;
const news = require("./news");
exports.news = news;
const priceBadge = require("./priceBadge");
exports.priceBadge = priceBadge;

},{"./anchor":76,"./banner":77,"./bannerone":78,"./faq":79,"./map":80,"./modal":81,"./news":82,"./panel":83,"./priceBadge":84,"./related":85,"./slider":86,"./sliderElement":87,"./smallBanner":88,"./thumbBox":89}],76:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
/**crea un bottone standard esponendo i metodi per modificarlo */
function dynamic(args) {
    const anchor = tags.a().addClass('yca-anchor-main');
    let isEnabled = true;
    let isCalling = false;
    set(args);
    function disableAnchorForCalling() {
        isCalling = true;
        anchor.addClass("yca-anchor-disabled");
    }
    function enableAnchorForCalling() {
        isCalling = false;
        if (isEnabled)
            anchor.removeClass("yca-anchor-disabled");
    }
    anchor.click(e => {
        if (args.action && !isCalling && !args.url) {
            e.preventDefault();
            disableAnchorForCalling();
            const prom = args.action();
            if (!prom)
                enableAnchorForCalling();
            else
                prom.then(r => {
                    enableAnchorForCalling();
                }).catch(r => {
                    enableAnchorForCalling();
                });
        }
        else if (!isEnabled && !args.url) {
            e.preventDefault();
        }
    });
    function set(data) {
        function setText(args) {
            anchor.empty();
            renderContent({ el: anchor, text: args.message, image: args.image });
        }
        function setAction(action) {
            if (action)
                args.action = action;
            else
                args.action = undefined;
        }
        function setUrl(url) {
            if (url)
                anchor.attr({ href: url });
            else
                anchor.removeAttr("href");
        }
        function setStatus(disabled) {
            isEnabled = !disabled;
            if (!isEnabled)
                anchor.addClass("yca-anchor-disabled");
            else
                anchor.removeClass("yca-anchor-disabled");
        }
        function setType(type) {
            anchor.removeClass("yca-anchor-main").removeClass("yca-anchor-light");
            anchor.addClass((!type || type == "yca-anchor-main") ? "yca-anchor-main" : "yca-anchor-light");
        }
        setText({ message: data.text, image: data.image });
        setUrl(data.url);
        setAction(data.action);
        setStatus(data.disabled);
        setType(data.type);
    }
    return { anchor, set, args };
}
exports.dynamic = dynamic;
function full(args) {
    const el = dynamic(args).anchor;
    return el;
}
exports.full = full;
function renderContent(args) {
    if (args.text)
        args.el.append(tags.span().html(args.text)).css('white-space', 'nowrap');
    else if (args.text && args.image)
        args.el.append(tags.img().attr({ src: args.image }), tags.span().html(args.text)).css('white-space', 'nowrap');
    else if (args.image)
        args.el.append(tags.img().attr({ src: args.image })).css('white-space', 'nowrap');
}

},{"../tags":90}],77:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
function banner(args) {
    const wrapper = tags.div().addClass("yca-banner-wrapper");
    const img = tags.div().addClass("yca-banner-image").append(tags.img().attr("src", args.image));
    const inner = tags.div().addClass("yca-banner-text-inner").css({ backgroundColor: args.bgColor, color: args.textColor }).html(args.text);
    const desc = tags.p().html(args.description);
    if (args.textPositionHigh) {
        inner.addClass("yca-banner-text-high");
    }
    return wrapper.append(img);
}
exports.banner = banner;

},{"../tags":90}],78:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
function build() {
    const n = Math.floor(Math.random() * 6);
    return tags.div().addClass("yca-wrapper").css({
        backgroundImage: `url(/img/random2/${n.toString()}.jpg)`,
        backgroundSize: "cover",
        width: "80vw",
        height: "36vw",
        position: "relative"
    });
}
exports.build = build;
function lowerBanner() {
    return tags.a().addClass("yca-banner").css({
        display: "block",
        backgroundImage: `url(/img/banner.png)`,
        backgroundSize: "cover",
        backgroundPosition: "center",
        width: "80vw",
        height: "100px",
        margin: "auto",
        position: "relative",
        borderLeft: "2px solid #3260abff",
        borderRight: "2px solid #3260abff"
    }).attr({ href: "https://www.arera.it/it/index.htm" });
}
exports.lowerBanner = lowerBanner;

},{"../tags":90}],79:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const modal = require("./modal");
function faq(args) {
    const myModal = modal.panel(tags.div().html(args.text));
    const wrapper = tags.div().addClass("yca-shard-faq").attr({ id: args.hash });
    const logo = tags.div().addClass("yca-shard-faq-logo").append(tags.img().attr({ src: args.logo }));
    const inner = tags.div().addClass("yca-shard-faq-inner").on("click touchend", () => {
        if (!myModal.shown)
            myModal.show();
        else
            myModal.close();
    });
    const left = tags.div().addClass("yca-shard-faq-left").append(tags.div().addClass("yca-shard-faq-left-inner").append(tags.h1().html(args.title).addClass("yca-shard-faq-left-title"), tags.p().html(args.subitle).addClass("yca-shard-faq-left-subtitle")));
    function setShift(shift) {
        if (args.orientation == "left")
            inner.css({ left: `-${(5 * (Math.pow(shift, 2.3)))}px` });
        if (args.orientation == "right")
            inner.css({ right: `-${(5 * (Math.pow(shift, 2.3)))}px` });
        return 5 * (Math.pow(shift, 2.3));
    }
    if (args.orientation == "left")
        wrapper.append(inner.append(logo.addClass("yca-to-left"), left.addClass("yca-to-left")).addClass("yca-to-left"), myModal.el);
    else
        wrapper.append(inner.append(left.addClass("yca-to-right"), logo.addClass("yca-to-right")).addClass("yca-to-right"), myModal.el);
    return { el: wrapper, setShift };
}
exports.faq = faq;
function faqCategory(args) {
    const el = tags.div().addClass("yca-shard-faqcategory");
    const title = tags.div().addClass("yca-shard-faqcategory-title").append(tags.h1().html(args.title));
    const container = tags.div().addClass("yca-shard-faqcategory-container");
    const logo = tags.div().addClass("yca-shard-faqcategory-container-logo").append(tags.img().attr({ src: args.logo }));
    const faqContainer = tags.div().addClass("yca-shard-faqcategory-container-faq");
    if (args.orientation == "left") {
        title.addClass("yca-to-left");
        container.append(logo.addClass("yca-to-left"), faqContainer.addClass("yca-to-left")).addClass("yca-to-left");
    }
    else {
        title.addClass("yca-to-right");
        container.append(faqContainer.addClass("yca-to-right"), logo.addClass("yca-to-right")).addClass("yca-to-right");
    }
    el.append(title, container);
    function set(faq) {
        for (const f of faq) {
            faqContainer.append(f);
        }
    }
    return { el, set };
}
exports.faqCategory = faqCategory;

},{"../tags":90,"./modal":81}],80:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
function simple() {
    const wrapper = tags.div().css({ minHeight: "100%" });
    function set(address) {
        new google.maps.Geocoder().geocode({ address }, (coordinates, status) => {
            console.log("GoogleMapsResults: ", status, coordinates, coordinates[0].geometry.location.toJSON());
            if (status != google.maps.GeocoderStatus.OK)
                throw new Error(`Error in parsing api MAPS, status: ${status}`);
            const position = coordinates[0].geometry.location.toJSON();
            const gMap = new google.maps.Map(wrapper[0], {
                center: position,
                zoom: 14
            });
            const marker = new google.maps.Marker({
                position,
                map: gMap
            });
        });
    }
    return { el: wrapper, set };
}
exports.simple = simple;

},{"../tags":90}],81:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
function panel(item, onClose) {
    const wrapper = tags.div().addClass("yca-modal").hide();
    const modal = tags.div().addClass("yca-modal-content");
    const cross = tags.button().addClass("yca-modal-cross").html("&times;").css({ backgroundColor: "#fff", border: "0px solid transparent" });
    let shown = false;
    wrapper.append(modal.append(cross, item));
    function show() {
        wrapper.show(450, () => {
            shown = true;
        });
    }
    function close() {
        if (onClose)
            onClose();
        wrapper.hide(450, () => {
            shown = false;
        });
        console.log("chiusura");
    }
    cross.on("click touchend", close);
    return { el: wrapper, show, close, shown };
}
exports.panel = panel;

},{"../tags":90}],82:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const _lib_1 = require("./_lib");
function news(args) {
    const wrapper = tags.div().addClass("yca-news-wrapper").attr({ id: args.id });
    const myModal = _lib_1.modal.panel(tags.p().html(args.body));
    const img = tags.div().addClass("yca-news-image").css({ backgroundImage: `url(${args.image})`, backgroundSize: "cover" });
    //const date = tags.div().addClass("yca-news-date").html(args.date)
    const ti = tags.h1().addClass("yca-news-title").html(args.title);
    const desc = tags.p().html(args.body).addClass("yca-news-description");
    const button = _lib_1.anchor.full({
        text: "Leggi di più",
        action: () => {
            myModal.show();
        }
    }).css({ padding: "15px 30px", display: "none" });
    return wrapper.append(ti, /*date,*/ img, desc, button, myModal.el);
}
exports.news = news;
function thumbNews(args) {
    const wrapper = tags.div().addClass("yca-thumbnews-wrapper");
    const myModal = _lib_1.modal.panel(tags.p().html(args.body));
    const img = tags.div().addClass("yca-thumbnews-image").css({ backgroundImage: `url(${args.image})`, backgroundSize: "cover" });
    const ti = tags.h1().addClass("yca-thumbnews-title").html(args.title);
    const desc = tags.p().html(args.subtitle).addClass("yca-thumbnews-description");
    const texts = tags.div().addClass("yca-thumbnews-textwrapper");
    const button = _lib_1.anchor.full({
        text: "Leggi di più",
        url: "/news/" + args.id
    }).css({ padding: "15px 30px" });
    return wrapper.append(img, texts.append(ti, desc, button, myModal.el.css({ color: "#000" })));
}
exports.thumbNews = thumbNews;

},{"../tags":90,"./_lib":75}],83:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const anchor = require("./anchor");
const modal = require("./modal");
function bluePanel(args) {
    const wrapper = tags.div().addClass("yca-bluepanel-wrapper");
    const img = tags.img().addClass("yca-bluepanel-image").attr("src", args.logo);
    const txt = tags.p().addClass("yca-bluepanel-text").text(args.text);
    const link = tags.a().addClass("yca-bluepanel-link");
    if (args.url) {
        if (typeof args.url === "string") {
            link.attr({ href: args.url });
        }
        else {
            const mWrapper = tags.div().css({ padding: "40px 5px" });
            for (const su of args.url) {
                mWrapper.append(tags.div().css({ padding: "15px 0" }).append(anchor.full({ text: su.name, url: su.url })));
            }
            const mm = modal.panel(mWrapper);
            wrapper.append(mm.el);
            link.on("click touchend", mm.show);
        }
    }
    const imgWrapper = tags.div().addClass("yca-bluepanel-imagewrapper");
    link.append(imgWrapper.append(img), txt);
    return wrapper.append(link);
}
exports.bluePanel = bluePanel;
function whitePanel(args) {
    const wrapper = tags.div().addClass("yca-whitepanel-wrapper");
    const img = tags.img().addClass("yca-whitepanel-image").attr("src", args.logo);
    const txt = tags.p().addClass("yca-whitepanel-text").text(args.text);
    const link = tags.a().attr({ href: args.url ? args.url : "#" }).addClass("yca-whitepanel-link");
    const imgWrapper = tags.div().addClass("yca-whitepanel-imagewrapper");
    link.append(imgWrapper.append(img), txt);
    return wrapper.append(link);
}
exports.whitePanel = whitePanel;
function stdPanel(args) {
    const wrapper = tags.div().addClass("yca-stdpanel-wrapper");
    const img = tags.img().addClass("yca-stdpanel-image").attr("src", args.logo);
    const txt = tags.p().addClass("yca-stdpanel-text").text(args.text);
    const link = tags.a().attr({ href: args.url ? args.url : "#" }).addClass("yca-stdpanel-link");
    const imgWrapper = tags.div().addClass("yca-stdpanel-imagewrapper");
    link.append(imgWrapper.append(img), txt);
    return wrapper.append(link);
}
exports.stdPanel = stdPanel;

},{"../tags":90,"./anchor":76,"./modal":81}],84:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tag = require("../tags");
function badge(args) {
    const wrapper = tag.div().addClass("yca-pricebadge-badge");
    const prezzo = tag.div().addClass("yca-pricebadge-badge-prezzo").html(buildPrezzo(args.prezzo));
    const typeTariffa = tag.div().addClass("yca-pricebadge-badge-tariffa").html(args.typeTariffa);
    const fasciaOraria = tag.div().addClass("yca-pricebadge-badge-orario").html(args.fasciaOraria);
    function buildPrezzo(p) {
        return p.toString() + "€/kWh";
    }
    return wrapper.append(prezzo, typeTariffa, fasciaOraria);
}
exports.badge = badge;
function description(args) {
    const wrapper = tag.div().addClass("yca-pricebadge-description");
    const titolo = tag.p().addClass("yca-pricebadge-description-titolo").html(args.titolo);
    const container = tag.p().addClass("yca-pricebadge-description-container");
    const pre = tag.strong().addClass("yca-pricebadge-description-container-pre").html(args.pre);
    const testo = tag.span().addClass("yca-pricebadge-description-container-testo").html(args.testo);
    container.append(pre, testo);
    return wrapper.append(titolo, container);
}
exports.description = description;

},{"../tags":90}],85:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const map = require("./map");
function related(image, title, description, pdf) {
    const wrapper = tags.div().addClass("yca-related-wrapper");
    const img = tags.div().addClass("yca-related-image").append(tags.img().attr("src", image));
    const txt = tags.div().addClass("yca-related-text");
    const ti = tags.h1().addClass("yca-related-title").html(title);
    const desc = tags.p().html(description).addClass("yca-related-description");
    for (let file of pdf) {
        txt.append(tags.a().addClass("yca-pdf-wrapper").attr("href", file.url).append(tags.img().attr("src", "pdf.jpg"), tags.p().html(file.lang)));
    }
    return wrapper;
}
exports.related = related;
function uffici() {
    const wrapper = tags.div().css({ display: "flex", minHeight: "20vw" });
    const right = tags.div().css({ flex: "1", padding: "20px", backgroundColor: "#667a85" });
    const left = tags.div().css({ flex: "3", backgroundColor: "#ccc", minHeight: "100%" });
    const mappa = map.simple();
    left.append(mappa.el);
    function set(args) {
        right.empty().append(tags.p().addClass("yca-contatti-whiteparagraph").html(args.address), tags.p().addClass("yca-contatti-whiteparagraph").html(args.phone), args.email ? tags.p().addClass("yca-contatti-whiteparagraph").append(tags.a().text(args.email).attr({ href: "mailto:" + args.email })) : null, tags.p().addClass("yca-contatti-whiteparagraph").html(args.hours));
        mappa.set(args.address);
    }
    return { el: wrapper.append(left, right), set };
}
exports.uffici = uffici;

},{"../tags":90,"./map":80}],86:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
function build(args) {
    let time = 2550;
    const wrapper = tags.div().addClass("yca-slider");
    const internal = tags.div().addClass("yca-slider-internal");
    const elements = [];
    const numbers = { total: 0, actual: 0 };
    let started = false;
    let timeout = 0;
    const play = tags.div().addClass("yca-slider-internal-play").append(tags.img().attr("src", "img/play.png").addClass("yca-slider-internal-play-button"));
    wrapper.append(play, internal);
    if (args && args.els)
        add(args.els);
    function add(els) {
        for (let e of els) {
            elements.push(e.css({ whiteSpace: "pre" }));
        }
        numbers.total = elements.length;
        load(numbers.actual);
    }
    function empty() {
        internal.empty();
    }
    function manageTimer() {
        if (started) {
            play.show();
            started = false;
            clearTimeout(timeout);
        }
        else {
            play.hide();
            started = true;
            timeout = window.setTimeout(goNext, time);
        }
    }
    internal.click(() => {
        manageTimer();
    });
    play.click(() => {
        manageTimer();
    });
    function start(mTime) {
        time = mTime ? mTime : time;
        manageTimer();
    }
    function goNext() {
        clearTimeout(timeout);
        numbers.actual = (numbers.actual + 1) % numbers.total;
        internal.empty();
        load(numbers.actual);
        timeout = window.setTimeout(goNext, time);
    }
    function load(num) {
        internal.append(elements[num]);
    }
    return { el: wrapper, add, empty, start };
}
exports.build = build;

},{"../tags":90}],87:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
function sliderElement(image, text) {
    const wrapper = tags.div().addClass("yca-sliderelement-wrapper");
    const img = tags.div().addClass("yca-sliderelement-image").append(tags.img().attr("src", image));
    if (text)
        return wrapper.append(tags.div().addClass("yca-sliderelement-text").html(text), img);
    return wrapper.append(img);
}
exports.sliderElement = sliderElement;

},{"../tags":90}],88:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
function smallBanner(args) {
    const wrapper = tags.div().addClass("yca-smallbanner-wrapper");
    const img = tags.div().addClass("yca-smallbanner-image").append(tags.img().attr("src", args.image));
    const inner = tags.div().addClass("yca-smallbanner-text-inner").css({ backgroundColor: args.bgColor, color: args.textColor }).html(args.text);
    return wrapper.append(img, inner);
}
exports.smallBanner = smallBanner;

},{"../tags":90}],89:[function(require,module,exports){
"use strict";
/*/// <reference path="../../typings/index.d.ts" />*/
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const time = 450;
let defaultScrollTime = 0;
function thumbBox(args) {
    const wrapper = tags.div().addClass("os-thumbbox");
    const internal = tags.div().addClass("os-thumbbox-internal");
    const points = tags.div().addClass("os-thumbbox-points");
    let started = false;
    let timeout = 0;
    let actualSelected = 0;
    let elements = [];
    let balls = [];
    if (args && args.scrollTime) {
        defaultScrollTime = args.scrollTime;
        startTimer();
    }
    const bPrev = tags.div()
        .append(tags.img().attr({ src: "/img/arrow_left.jpg" }))
        .addClass("os-thumbbox-previous")
        .click((e) => {
        stopTimer();
        previous();
    });
    const bNext = tags.div()
        .append(tags.img().attr({ src: "/img/arrow_right.jpg" }))
        .addClass("os-thumbbox-next")
        .click((e) => {
        stopTimer();
        next();
    });
    if (args && args.arrowHeight) {
        bPrev.css("top", args.arrowHeight);
        bNext.css("top", args.arrowHeight);
    }
    wrapper.append(bPrev, bNext, internal, points);
    if (args && args.els)
        add(args.els);
    function startTimer() {
        if (!started) {
            started = true;
            timeout = window.setTimeout(goNext, defaultScrollTime);
        }
    }
    function stopTimer() {
        if (started) {
            started = false;
            clearTimeout(timeout);
        }
    }
    let resizeTimer = 0;
    wrapper
        .hover(() => checkPosition())
        .click(() => stopTimer())
        .on("touchend", () => {
        stopTimer();
        internal.scroll(() => {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(checkPosition, 25);
        });
    });
    function scrollGap() {
        return (args && args.paginationWidth) ? args.paginationWidth : internal.width();
    }
    $(window).bind('resize', function () {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(previous, 10);
    });
    function add(els) {
        elements.push(...els);
        for (let e of elements) {
            const b = tags.span().addClass("os-thumbox-ball").addClass("os-thumbox-ball-" + els.indexOf(e).toString()).click(() => {
                goTo(els.indexOf(e));
            });
            balls.push(b);
            points.append(b);
            internal.append(e.addClass("os-thumbox-element-" + els.indexOf(e).toString()));
        }
        changeBall(0);
        checkPosition();
    }
    function empty() {
        internal.empty();
    }
    function goTo(go) {
        let scroll = getActualScroll().actual;
        if (go > actualSelected) {
            for (let i = 0; i < go - actualSelected; i++) {
                scroll = getActualScroll(scroll).next;
            }
        }
        else if (go < actualSelected) {
            for (let i = 0; i < actualSelected - go; i++) {
                scroll = getActualScroll(scroll).previous;
            }
        }
        internal.stop().animate({ scrollLeft: scroll }, time, checkPosition);
        checkPosition();
    }
    function getActualScroll(now) {
        const actual = now ? now : internal.scrollLeft();
        const scroll = { actual, previous: 0, next: 0, slideBegin: 0 };
        if (actual % scrollGap() != 0) {
            scroll.previous = Math.floor(actual / scrollGap()) * scrollGap();
            scroll.next = (Math.floor(actual / scrollGap()) + 1) * scrollGap();
            scroll.slideBegin = scroll.previous;
        }
        else {
            scroll.slideBegin = actual;
            scroll.previous = ((actual / scrollGap()) - 1) * scrollGap();
            scroll.next = ((actual / scrollGap()) + 1) * scrollGap();
        }
        return scroll;
    }
    function next() {
        internal.stop().animate({ scrollLeft: getActualScroll().next }, time, checkPosition);
    }
    function previous() {
        internal.stop().animate({ scrollLeft: getActualScroll().previous }, time, checkPosition);
    }
    function goNext() {
        clearTimeout(timeout);
        internal.stop().animate({ scrollLeft: (internal.scrollLeft() >= (internal[0].scrollWidth - internal.width())) ? 0 : getActualScroll().next }, time, checkPosition);
        timeout = window.setTimeout(goNext, defaultScrollTime);
    }
    function changeBall(now) {
        for (let i = 0; i < balls.length; i++) {
            if (i == now)
                balls[i].addClass("os-thumbox-ball-selected");
            else
                balls[i].removeClass("os-thumbox-ball-selected");
        }
        actualSelected = now;
    }
    function checkPosition() {
        if (internal.scrollLeft() <= 0) {
            bPrev.addClass("os-thumbbox-keephidden");
        }
        else {
            bPrev.removeClass("os-thumbbox-keephidden");
        }
        if (internal.scrollLeft() >= (internal[0].scrollWidth - internal.width())) {
            bNext.addClass("os-thumbbox-keephidden");
        }
        else {
            bNext.removeClass("os-thumbbox-keephidden");
        }
        for (const e of elements) {
            if (checkInView(internal, e))
                changeBall(elements.indexOf(e));
        }
    }
    return { el: wrapper, add, empty };
}
exports.thumbBox = thumbBox;
function checkInView(container, elem) {
    const contCenter = container.width() / 2; //container.scrollLeft() + ();
    const elemLeft = elem.offset().left - container.offset().left;
    const elemRight = elemLeft + elem.width();
    const isCentered = (elemLeft <= contCenter && contCenter < elemRight);
    // console.log("data", elem, contCenter, elemLeft, elemRight, isCentered)
    return isCentered;
}

},{"../tags":90}],90:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function tag(name, inputType) {
    const t = $(document.createElement(name));
    if (inputType)
        t.prop("type", inputType);
    return t;
}
exports.div = () => tag("div");
exports.button = () => tag("button");
exports.img = () => tag("img");
exports.span = () => tag("span");
exports.script = () => tag("script");
exports.p = () => tag("p");
exports.br = () => tag("br");
exports.strong = () => tag("strong");
exports.a = () => tag("a");
exports.h1 = () => tag("h1");
exports.h2 = () => tag("h2");
exports.h3 = () => tag("h3");
exports.h4 = () => tag("h4");
exports.hr = () => tag("hr");
exports.ul = () => tag("ul");
exports.li = () => tag("li");
exports.select = () => tag("select");
exports.option = () => tag("option");
exports.table = () => tag("table");
exports.tbody = () => tag("tbody");
exports.th = () => tag("th");
exports.tr = () => tag("tr");
exports.td = () => tag("td");
exports.label = () => tag("label");
exports.itext = () => tag("input", "text");
exports.iemail = () => tag("input", "mail");
exports.ipassword = () => tag("input", "password");
exports.inumber = () => tag("input", "number");
exports.ifile = () => tag("input", "file");
exports.isubmit = () => tag("input", "submit");
exports.icheckbox = () => tag("input", "checkbox");
exports.iradio = () => tag("input", "radio");
exports.itextarea = () => tag("textarea");

},{}],91:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main = require("./main");
exports.main = main;
const backend = require("./backend");
exports.backend = backend;

},{"./backend":92,"./main":93}],92:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const blocks = require("../blocks/_lib");
function build() {
    const body = $("body").empty();
    const header = tags.div().attr("id", "yca-header").append(blocks.menu.menu(), blocks.menu.mobile());
    const main = tags.div().attr({ id: "yca-main" }).addClass("yca-main-main");
    const side = tags.div().attr({ id: "yca-main-side" }).addClass("yca-main-side").append(blocks.menu.menuBackend());
    const alert = tags.div().addClass("yca-alert");
    const wrapper = tags.div().addClass("yca-main-wrapper").append(side, main);
    main.append(alert);
    body.append(header, wrapper);
    return { main, side, alert };
}
exports.build = build;

},{"../blocks/_lib":19,"../tags":90}],93:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("../tags");
const blocks = require("../blocks/_lib");
const cookie = require("../utils");
const shards = require("../shards/_lib");
function main() {
    const body = $("body").empty();
    const header = tags.div().attr("id", "yca-header").append(blocks.menu.menu(), blocks.menu.mobile());
    const main = tags.div().attr("id", "yca-main");
    const alert = tags.div().addClass("yca-alert");
    main.append(alert);
    const w = tags.div();
    const t = tags.div().html("Questo sito utilizza cookie di profilazione, anche di terze parti, per inviarti messaggi pubblicitari mirati e servizi in linea con le tue preferenze. Se vuoi saperne di più o negare il consenso a tutti o ad alcuni cookie clicca qui il pulsante “<a href=\"/utils/privacy\">Cookie policy</a>”. Proseguendo la navigazione acconsenti all’uso dei cookie. Il consenso può essere espresso anche cliccando sul tasto ok o sul tasto × oppure proseguendo la navigazione mediante scrolling.");
    const p = shards.modal.panel(w.append(t), setCookieBanner);
    const b = shards.anchor.full({
        text: "Ok", action: () => {
            p.close();
        }
    }).css({
        padding: "10px 40px",
        display: "block",
        width: "200px",
        textAlign: "center",
        margin: "10px auto"
    });
    w.append(b);
    main.append(p.el);
    body.append(header, main, blocks.footer.footer());
    const c = cookie.getCookie("acceptedCookie");
    if (!c)
        p.show();
    return { main, alert };
}
exports.main = main;
function setCookieBanner() {
    cookie.setCookie("acceptedCookie", "true", 30);
}

},{"../blocks/_lib":19,"../shards/_lib":75,"../tags":90,"../utils":94}],94:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tags = require("./tags");
const api = require("./api/_lib");
function getParameterByName(name) {
    const url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex.exec(url);
    if (!results)
        return "";
    if (!results[2])
        return "";
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
exports.getParameterByName = getParameterByName;
function setCookie(name, value, days) {
    let expires = "";
    if (days) {
        let date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toISOString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}
exports.setCookie = setCookie;
function getCookie(name) {
    let nameEQ = name + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return undefined;
}
exports.getCookie = getCookie;
function deleteCookie(name) {
    let endDate = new Date();
    endDate.setFullYear(1200);
    document.cookie = `${name} = ${endDate.toISOString()}`;
}
exports.deleteCookie = deleteCookie;
function createTableBy2DArray(tableData) {
    var tableBody = tags.tbody();
    tableData.forEach(function (rowData) {
        var row = tags.tr();
        rowData.forEach(function (cellData) {
            var cell = tags.td();
            cell.html(cellData);
            row.append(cell);
        });
        tableBody.append(row);
    });
    return tableBody;
}
exports.createTableBy2DArray = createTableBy2DArray;
function userManager() {
    function set(u) {
        setCookie("user", JSON.stringify(u), 30);
    }
    function get() {
        try {
            return JSON.parse(decodeURIComponent(getCookie("user")));
        }
        catch (err) {
            console.log("unable to read cookie user", err);
            return undefined;
        }
    }
    function getPromise() {
        const _id = get()._id;
        if (!_id)
            return Promise.resolve(undefined);
        else {
            api.utente.getUtenti({ _id }).then(u => {
                set(u[0]);
                console.log(u);
                return u;
            });
        }
    }
    function isLogged() {
        return !!getCookie("user");
    }
    return { set, get, getPromise, isLogged };
}
exports.userManager = userManager;
window.refreshUser = userManager().getPromise;

},{"./api/_lib":2,"./tags":90}],95:[function(require,module,exports){
var charenc = {
  // UTF-8 encoding
  utf8: {
    // Convert a string to a byte array
    stringToBytes: function(str) {
      return charenc.bin.stringToBytes(unescape(encodeURIComponent(str)));
    },

    // Convert a byte array to a string
    bytesToString: function(bytes) {
      return decodeURIComponent(escape(charenc.bin.bytesToString(bytes)));
    }
  },

  // Binary encoding
  bin: {
    // Convert a string to a byte array
    stringToBytes: function(str) {
      for (var bytes = [], i = 0; i < str.length; i++)
        bytes.push(str.charCodeAt(i) & 0xFF);
      return bytes;
    },

    // Convert a byte array to a string
    bytesToString: function(bytes) {
      for (var str = [], i = 0; i < bytes.length; i++)
        str.push(String.fromCharCode(bytes[i]));
      return str.join('');
    }
  }
};

module.exports = charenc;

},{}],96:[function(require,module,exports){
(function() {
  var base64map
      = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/',

  crypt = {
    // Bit-wise rotation left
    rotl: function(n, b) {
      return (n << b) | (n >>> (32 - b));
    },

    // Bit-wise rotation right
    rotr: function(n, b) {
      return (n << (32 - b)) | (n >>> b);
    },

    // Swap big-endian to little-endian and vice versa
    endian: function(n) {
      // If number given, swap endian
      if (n.constructor == Number) {
        return crypt.rotl(n, 8) & 0x00FF00FF | crypt.rotl(n, 24) & 0xFF00FF00;
      }

      // Else, assume array and swap all items
      for (var i = 0; i < n.length; i++)
        n[i] = crypt.endian(n[i]);
      return n;
    },

    // Generate an array of any length of random bytes
    randomBytes: function(n) {
      for (var bytes = []; n > 0; n--)
        bytes.push(Math.floor(Math.random() * 256));
      return bytes;
    },

    // Convert a byte array to big-endian 32-bit words
    bytesToWords: function(bytes) {
      for (var words = [], i = 0, b = 0; i < bytes.length; i++, b += 8)
        words[b >>> 5] |= bytes[i] << (24 - b % 32);
      return words;
    },

    // Convert big-endian 32-bit words to a byte array
    wordsToBytes: function(words) {
      for (var bytes = [], b = 0; b < words.length * 32; b += 8)
        bytes.push((words[b >>> 5] >>> (24 - b % 32)) & 0xFF);
      return bytes;
    },

    // Convert a byte array to a hex string
    bytesToHex: function(bytes) {
      for (var hex = [], i = 0; i < bytes.length; i++) {
        hex.push((bytes[i] >>> 4).toString(16));
        hex.push((bytes[i] & 0xF).toString(16));
      }
      return hex.join('');
    },

    // Convert a hex string to a byte array
    hexToBytes: function(hex) {
      for (var bytes = [], c = 0; c < hex.length; c += 2)
        bytes.push(parseInt(hex.substr(c, 2), 16));
      return bytes;
    },

    // Convert a byte array to a base-64 string
    bytesToBase64: function(bytes) {
      for (var base64 = [], i = 0; i < bytes.length; i += 3) {
        var triplet = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];
        for (var j = 0; j < 4; j++)
          if (i * 8 + j * 6 <= bytes.length * 8)
            base64.push(base64map.charAt((triplet >>> 6 * (3 - j)) & 0x3F));
          else
            base64.push('=');
      }
      return base64.join('');
    },

    // Convert a base-64 string to a byte array
    base64ToBytes: function(base64) {
      // Remove non-base-64 characters
      base64 = base64.replace(/[^A-Z0-9+\/]/ig, '');

      for (var bytes = [], i = 0, imod4 = 0; i < base64.length;
          imod4 = ++i % 4) {
        if (imod4 == 0) continue;
        bytes.push(((base64map.indexOf(base64.charAt(i - 1))
            & (Math.pow(2, -2 * imod4 + 8) - 1)) << (imod4 * 2))
            | (base64map.indexOf(base64.charAt(i)) >>> (6 - imod4 * 2)));
      }
      return bytes;
    }
  };

  module.exports = crypt;
})();

},{}],97:[function(require,module,exports){
/*!
 * Determine if an object is a Buffer
 *
 * @author   Feross Aboukhadijeh <https://feross.org>
 * @license  MIT
 */

// The _isBuffer check is for Safari 5-7 support, because it's missing
// Object.prototype.constructor. Remove this eventually
module.exports = function (obj) {
  return obj != null && (isBuffer(obj) || isSlowBuffer(obj) || !!obj._isBuffer)
}

function isBuffer (obj) {
  return !!obj.constructor && typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj)
}

// For Node v0.10 support. Remove this eventually.
function isSlowBuffer (obj) {
  return typeof obj.readFloatLE === 'function' && typeof obj.slice === 'function' && isBuffer(obj.slice(0, 0))
}

},{}],98:[function(require,module,exports){
(function(){
  var crypt = require('crypt'),
      utf8 = require('charenc').utf8,
      isBuffer = require('is-buffer'),
      bin = require('charenc').bin,

  // The core
  md5 = function (message, options) {
    // Convert to byte array
    if (message.constructor == String)
      if (options && options.encoding === 'binary')
        message = bin.stringToBytes(message);
      else
        message = utf8.stringToBytes(message);
    else if (isBuffer(message))
      message = Array.prototype.slice.call(message, 0);
    else if (!Array.isArray(message))
      message = message.toString();
    // else, assume byte array already

    var m = crypt.bytesToWords(message),
        l = message.length * 8,
        a =  1732584193,
        b = -271733879,
        c = -1732584194,
        d =  271733878;

    // Swap endian
    for (var i = 0; i < m.length; i++) {
      m[i] = ((m[i] <<  8) | (m[i] >>> 24)) & 0x00FF00FF |
             ((m[i] << 24) | (m[i] >>>  8)) & 0xFF00FF00;
    }

    // Padding
    m[l >>> 5] |= 0x80 << (l % 32);
    m[(((l + 64) >>> 9) << 4) + 14] = l;

    // Method shortcuts
    var FF = md5._ff,
        GG = md5._gg,
        HH = md5._hh,
        II = md5._ii;

    for (var i = 0; i < m.length; i += 16) {

      var aa = a,
          bb = b,
          cc = c,
          dd = d;

      a = FF(a, b, c, d, m[i+ 0],  7, -680876936);
      d = FF(d, a, b, c, m[i+ 1], 12, -389564586);
      c = FF(c, d, a, b, m[i+ 2], 17,  606105819);
      b = FF(b, c, d, a, m[i+ 3], 22, -1044525330);
      a = FF(a, b, c, d, m[i+ 4],  7, -176418897);
      d = FF(d, a, b, c, m[i+ 5], 12,  1200080426);
      c = FF(c, d, a, b, m[i+ 6], 17, -1473231341);
      b = FF(b, c, d, a, m[i+ 7], 22, -45705983);
      a = FF(a, b, c, d, m[i+ 8],  7,  1770035416);
      d = FF(d, a, b, c, m[i+ 9], 12, -1958414417);
      c = FF(c, d, a, b, m[i+10], 17, -42063);
      b = FF(b, c, d, a, m[i+11], 22, -1990404162);
      a = FF(a, b, c, d, m[i+12],  7,  1804603682);
      d = FF(d, a, b, c, m[i+13], 12, -40341101);
      c = FF(c, d, a, b, m[i+14], 17, -1502002290);
      b = FF(b, c, d, a, m[i+15], 22,  1236535329);

      a = GG(a, b, c, d, m[i+ 1],  5, -165796510);
      d = GG(d, a, b, c, m[i+ 6],  9, -1069501632);
      c = GG(c, d, a, b, m[i+11], 14,  643717713);
      b = GG(b, c, d, a, m[i+ 0], 20, -373897302);
      a = GG(a, b, c, d, m[i+ 5],  5, -701558691);
      d = GG(d, a, b, c, m[i+10],  9,  38016083);
      c = GG(c, d, a, b, m[i+15], 14, -660478335);
      b = GG(b, c, d, a, m[i+ 4], 20, -405537848);
      a = GG(a, b, c, d, m[i+ 9],  5,  568446438);
      d = GG(d, a, b, c, m[i+14],  9, -1019803690);
      c = GG(c, d, a, b, m[i+ 3], 14, -187363961);
      b = GG(b, c, d, a, m[i+ 8], 20,  1163531501);
      a = GG(a, b, c, d, m[i+13],  5, -1444681467);
      d = GG(d, a, b, c, m[i+ 2],  9, -51403784);
      c = GG(c, d, a, b, m[i+ 7], 14,  1735328473);
      b = GG(b, c, d, a, m[i+12], 20, -1926607734);

      a = HH(a, b, c, d, m[i+ 5],  4, -378558);
      d = HH(d, a, b, c, m[i+ 8], 11, -2022574463);
      c = HH(c, d, a, b, m[i+11], 16,  1839030562);
      b = HH(b, c, d, a, m[i+14], 23, -35309556);
      a = HH(a, b, c, d, m[i+ 1],  4, -1530992060);
      d = HH(d, a, b, c, m[i+ 4], 11,  1272893353);
      c = HH(c, d, a, b, m[i+ 7], 16, -155497632);
      b = HH(b, c, d, a, m[i+10], 23, -1094730640);
      a = HH(a, b, c, d, m[i+13],  4,  681279174);
      d = HH(d, a, b, c, m[i+ 0], 11, -358537222);
      c = HH(c, d, a, b, m[i+ 3], 16, -722521979);
      b = HH(b, c, d, a, m[i+ 6], 23,  76029189);
      a = HH(a, b, c, d, m[i+ 9],  4, -640364487);
      d = HH(d, a, b, c, m[i+12], 11, -421815835);
      c = HH(c, d, a, b, m[i+15], 16,  530742520);
      b = HH(b, c, d, a, m[i+ 2], 23, -995338651);

      a = II(a, b, c, d, m[i+ 0],  6, -198630844);
      d = II(d, a, b, c, m[i+ 7], 10,  1126891415);
      c = II(c, d, a, b, m[i+14], 15, -1416354905);
      b = II(b, c, d, a, m[i+ 5], 21, -57434055);
      a = II(a, b, c, d, m[i+12],  6,  1700485571);
      d = II(d, a, b, c, m[i+ 3], 10, -1894986606);
      c = II(c, d, a, b, m[i+10], 15, -1051523);
      b = II(b, c, d, a, m[i+ 1], 21, -2054922799);
      a = II(a, b, c, d, m[i+ 8],  6,  1873313359);
      d = II(d, a, b, c, m[i+15], 10, -30611744);
      c = II(c, d, a, b, m[i+ 6], 15, -1560198380);
      b = II(b, c, d, a, m[i+13], 21,  1309151649);
      a = II(a, b, c, d, m[i+ 4],  6, -145523070);
      d = II(d, a, b, c, m[i+11], 10, -1120210379);
      c = II(c, d, a, b, m[i+ 2], 15,  718787259);
      b = II(b, c, d, a, m[i+ 9], 21, -343485551);

      a = (a + aa) >>> 0;
      b = (b + bb) >>> 0;
      c = (c + cc) >>> 0;
      d = (d + dd) >>> 0;
    }

    return crypt.endian([a, b, c, d]);
  };

  // Auxiliary functions
  md5._ff  = function (a, b, c, d, x, s, t) {
    var n = a + (b & c | ~b & d) + (x >>> 0) + t;
    return ((n << s) | (n >>> (32 - s))) + b;
  };
  md5._gg  = function (a, b, c, d, x, s, t) {
    var n = a + (b & d | c & ~d) + (x >>> 0) + t;
    return ((n << s) | (n >>> (32 - s))) + b;
  };
  md5._hh  = function (a, b, c, d, x, s, t) {
    var n = a + (b ^ c ^ d) + (x >>> 0) + t;
    return ((n << s) | (n >>> (32 - s))) + b;
  };
  md5._ii  = function (a, b, c, d, x, s, t) {
    var n = a + (c ^ (b | ~d)) + (x >>> 0) + t;
    return ((n << s) | (n >>> (32 - s))) + b;
  };

  // Package private blocksize
  md5._blocksize = 16;
  md5._digestsize = 16;

  module.exports = function (message, options) {
    if (message === undefined || message === null)
      throw new Error('Illegal argument ' + message);

    var digestbytes = crypt.wordsToBytes(md5(message, options));
    return options && options.asBytes ? digestbytes :
        options && options.asString ? bin.bytesToString(digestbytes) :
        crypt.bytesToHex(digestbytes);
  };

})();

},{"charenc":95,"crypt":96,"is-buffer":97}]},{},[35]);
