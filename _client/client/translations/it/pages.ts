export const home = {
    title: "YouCan Agency",
    claim: "TU: al centro del nostro mondo",
    description: `YouCan Agency è un'agenzia di eventi e spettacolo in grado di coprire l'intero territorio nazionale e internazionale.<br/>
Nasce dall'idea di creare un brand forte e d’impatto, che possa descrivere a pieno i concetti di originalità e professionalità nell’ambito dello spettacolo e degli eventi pubblici e/o privati.<br/>
L'obiettivo è quello di sviluppare una sinergia con il cliente mettendo a disposizione il nostro know-how per la realizzazione delle sue richieste, avvalendoci della collaborazione di una rete di affermati 
professionisti per l’ideazione e la realizzazione di progetti perfettamente in linea con le esigenze e le particolarità del cliente e della ricorrenza.<br/>
Il nostro servizio è frutto di attenzione ed esperienza, la cura del dettaglio e l'attenzione nei confronti delle esigenze più particolari rendono il nostro servizio indicato per Enti Pubblici e Privati e 
per tutti coloro i quali vogliano rendere unico ed indimenticabile il loro evento attraverso spettacoli creati su misura e dallo straordinario impatto emotivo e visivo.<br/>
Tutto questo perché la soddisfazione dei nostri clienti è prima di tutto la nostra.<br/>
Ideare, progettare, organizzare eventi e realizzare sogni sono il nostro leitmotiv.<br/>
Scopri gli artisti di YouCan Agency: siamo in grado di offrire una vasta scelta tra: artisti, modelli/e, ballerine, presentatrici, attori e attrici, gruppi musicali e d'attrazione, cabarettisti e animatori.<br/>
Entra nel sito scoprirai originalità e talento... e fai la differenza!`
}

export const associazione = {
    title: "YouCan Agency",
    storia: `Nasce a Parma l’Associazione ricreativa culturale YouCan.<br />
Prendendo spunto dalla filosofia che aveva ispirato la YouCan Agency di Ketty Lalli, 
talentuoso social media marketer, l’associazione si prefigge di aiutare giovani artisti interessati al mondo dello spettacolo ad affinare 
il loro talento e ad avvicinarsi a questo mondo in modo sicuro e protetto.<br />
L’associazione si occupa della formazione e della promozione degli artisti, della gestione dei loro ingaggi, e della gestione della loro immagine.<br/>
Oltre ad avvalersi della collaborazione di team esterni, l’associazione spinge le modelle e gli artisti più esperti ad aiutare i più govani e meno esperti nel loro 
percorso: i nostri artisti di successo aiutano e insegnano ai nuovi arrivati, trasmettendo competenza e amore per l’arte.`,
    organigramma: `<h1>Organigramma dell’associazione:</h1>
    <h2>Presidente:</h2>
    Sergio Ferrari
    <h2>Consiglieri:</h2>
    <ul><li>Tino Serraiocco</li>
    <li>Chiara Summer</li>
    <li>Fabio Riolo</li>
    <li>Francesco Galleano</li></ul>`,
    contatti: `Telefono:<br/>
Presidente: <a href="tel:320 481 3036">320 481 3036</a><br/>
E-mail:<br/>
casting e iscrizioni: <a href="mailto:casting@youcanagency.it">casting@youcanagency.it</a><br/>
eventi: <a href="mailto:eventi@youcanagency.it">eventi@youcanagency.it</a><br/>
informazioni: <a href="mailto:info@youcanagency.it">info@youcanagency.it</a><br/>
book e fotografie: <a href="mailto:tino@youcanagency.it">tino@youcanagency.it</a>`
}