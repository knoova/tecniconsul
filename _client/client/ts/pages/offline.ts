import * as tags from "../tags"
import * as templates from "../template/_lib"
import * as shards from "../shards/_lib"
import * as blocks from "../blocks/_lib"
import * as input from "../fields/input/_lib"
import * as api from "../api/_lib"

export function build(language?: string) {
    const template = templates.main.main()

    const lowerSection = tags.div().addClass("yca-wrapper").append(tags.p().html("Configuring backup data to server. Retry in few hours.<br />Configurazione degli aggiornamenti/backup su server. Riprovare tra qualche ora."))



    template.main.append(lowerSection)
}