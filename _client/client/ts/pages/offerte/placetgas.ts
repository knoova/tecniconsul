import * as tags from "../../tags"
import * as templates from "../../template/_lib"
import * as shards from "../../shards/_lib"
import * as blocks from "../../blocks/_lib"
import * as api from "../../api/_lib"
import * as forms from "../form"

export async function build(args?: { offerta: string }) {

    const template = templates.main.main()
    const upperSection = shards.bannerone.build()

    const bottomFloater = tags.div().addClass("yca-home-bottomfloater")
    const usClaim = tags.h1().addClass("yca-home-claim").html("LUCE E GAS<BR />OVUNQUE TU SIA")


    upperSection.append(bottomFloater.append(usClaim))


    const middleSection = tags.div().addClass("yca-wrapper")
    const ourOfferts = tags.h1().addClass("yca-home-underlined").text("Gas casa e azienda")


    const wrapperOfferte = tags.div()

    const offerte = tags.div().css({
        width: "50vw",
        flex: "1",
        height: "36vw",
        display: "inline-block"
    })

    const offLinkCont = tags.div()



    const threeButt = tags.div().css({
        float: "right",
        height: "36vw",
        backgroundColor: "#cccccccc",
        padding: "30px 20px"
    }).append(
        tags.div().addClass("yca-dettagli-sidebar").html("Scopri la tariffa<br />gas che meglio<br />si presta alla<br />gestione dei<br />tuoi consumi")
    )


    wrapperOfferte.append(offerte.append(offLinkCont), threeButt).css({ backgroundImage: "url(/img/casa.jpg)" })




    middleSection.append(ourOfferts, wrapperOfferte)


    const segnapSlider = tags.h1().addClass("yca-home-underlined").html("News")
    const slider = shards.thumbbox.thumbBox({ scrollTime: 3000 })

    api.news.getNews().then(news => {
        const m: JQuery[] = []
        for (const n of news) {
            m.push(shards.news.thumbNews({
                image: n.banner,
                title: n.title,
                subtitle: n.title,
                date: n.date,
                body: n.body,
                id: n._id
            }))
        }
        slider.add(m)
    })


    const multicolumn = tags.div().addClass("yca-offerte-multicolumn")



    const images1 = tags.div().addClass("yca-offerte-containerofferte")
    const images2 = tags.div().addClass("yca-offerte-containerofferte")

    const fill = await api.offerte.getOfferta({
        casaAziendaPlacet: "placet",
        luceGas: "gas"
    })

    const varLuce1 = fill.o1 ? {
        titolo: fill.o1.title || "",
        pre: fill.o1.intro || "",
        testo: fill.o1.description || ""
    } : {
            titolo: "",
            pre: "",
            testo: ""
        }
    const varLuce2 = fill.o2 ? {
        titolo: fill.o2.title || "",
        pre: fill.o2.intro || "",
        testo: fill.o2.description || ""
    } : {
            titolo: "",
            pre: "",
            testo: ""
        }
    const varLuce3 = fill.o3 ? {
        titolo: fill.o3.title || "",
        pre: fill.o3.intro || "",
        testo: fill.o3.description || ""
    } : {
            titolo: "",
            pre: "",
            testo: ""
        }
    const varLuce4 = fill.o4 ? {
        titolo: fill.o4.title || "",
        pre: fill.o4.intro || "",
        testo: fill.o4.description || ""
    } : {
            titolo: "",
            pre: "",
            testo: ""
        }


    images1.append(shards.priceBadge.description(varLuce1), shards.priceBadge.description(varLuce2))
    images2.append(shards.priceBadge.description(varLuce3), shards.priceBadge.description(varLuce4))

    const div = tags.div().addClass("yca-placet").append(
        tags.h1().html("Vantaggi").css({ color: "#3260ab", textAlign: "center" }),
        tags.h3().html("Servizio bolletta online"),
        tags.hr(),
        tags.h3().html("Prezzo variabile che segue l'andamento dei mercati all'ingrosso"),
        tags.hr(),
        tags.h3().html("Condizioni generali di fornitura fissate dall'autorità"),
        tags.h1().html("Come si attiva").css({ color: "#3260ab", textAlign: "center" }),
        tags.strong().html("Sportello T.E."), tags.br(),
        tags.span().html("Recati presso i nostri sportelli della tua zona per saperne di più sulle nostre offerte ed entrare in un mondo di vantaggi"), tags.br(), tags.br(),
        tags.strong().html("Chiamaci"), tags.br(),
        tags.span().html("Contattaci al nostro numero verde: un consulente risponderà ad ogni tua domanda o ti raggiungerà a domicilio per illustrarti le offerte a te dedicate."), tags.br(), tags.br(),
        tags.strong().html("Contattaci online"), tags.br(),
        tags.span().html("Scrivici all'indirizzo e-mail indicato nella sezione \"contatti\": ti risponderemo al più presto."), tags.br(), tags.br(),
        tags.hr(),
        tags.strong().append(tags.a().attr({ href: "https://www.arera.it/it/consumatori/placet.htm" }).html("L'autorità"))
    )



    template.main.append(upperSection, middleSection, multicolumn, images1, images2, div, segnapSlider, slider.el)



}



