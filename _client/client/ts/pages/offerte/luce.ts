import * as tags from "../../tags"
import * as templates from "../../template/_lib"
import * as shards from "../../shards/_lib"
import * as blocks from "../../blocks/_lib"
import * as api from "../../api/_lib"
import * as forms from "../form"

export async function build(args?: { offerta: string }) {

    const template = templates.main.main()
    const upperSection = shards.bannerone.build()

    const bottomFloater = tags.div().addClass("yca-home-bottomfloater")
    const usClaim = tags.h1().addClass("yca-home-claim").html("LUCE E GAS<BR />OVUNQUE TU SIA")


    upperSection.append(bottomFloater.append(usClaim))


    const middleSection = tags.div().addClass("yca-wrapper")
    const ourOfferts = tags.h1().addClass("yca-home-underlined").text("Luce casa")


    const wrapperOfferte = tags.div().addClass("yca-offerte-wrapper").css({ display: "table-row" })

    const offerte = tags.div().css({
        // width: "50vw",
        // flex: "1",
        padding: "30px 20px",
        display: "table-cell",
        //borderRight: "4px solid #3260ab"
    })

    const offLinkCont = tags.div()


    const images1 = tags.div().addClass("yca-offerte-containerofferte")
    const images2 = tags.div().addClass("yca-offerte-containerofferte")

   
    const fill = await api.offerte.getOfferta({
        casaAziendaPlacet: "casa",
        luceGas: "luce"
    })

    const varLuce1 = fill.o1 ? {
        titolo: fill.o1.title || "",
        pre: fill.o1.intro || "",
        testo: fill.o1.description || ""
    } : {
            titolo: "",
            pre: "",
            testo: ""
        }
    const varLuce2 = fill.o2 ? {
        titolo: fill.o2.title || "",
        pre: fill.o2.intro || "",
        testo: fill.o2.description || ""
    } : {
            titolo: "",
            pre: "",
            testo: ""
        }
    const varLuce3 = fill.o3 ? {
        titolo: fill.o3.title || "",
        pre: fill.o3.intro || "",
        testo: fill.o3.description || ""
    } : {
            titolo: "",
            pre: "",
            testo: ""
        }
    const varLuce4 = fill.o4 ? {
        titolo: fill.o4.title || "",
        pre: fill.o4.intro || "",
        testo: fill.o4.description || ""
    } : {
            titolo: "",
            pre: "",
            testo: ""
        }


    images1.append(shards.priceBadge.description(varLuce1), shards.priceBadge.description(varLuce2))
    images2.append(shards.priceBadge.description(varLuce3), shards.priceBadge.description(varLuce4))


    offLinkCont.append(images1, images2).css({})

    const threeButt = tags.div().css({
        padding: "30px 20px",
        backgroundColor: "#cccccccc",
        display: "table-cell",
        minWidth: "25vw"
    }).append(
        tags.div().addClass("yca-dettagli-sidebar").html("Scopri la tariffa<br />luce che meglio<br />si presta alla<br />gestione dei<br />tuoi consumi")
    )



    wrapperOfferte.append(offerte.append(offLinkCont).css({
        padding: "30px 20px",
        backgroundColor: "#cccccccc",
        display: "table-cell"
    }), tags.div().addClass("yca-offerte-wrapper-sep").append(tags.div()), threeButt).css({ backgroundImage: "url(/img/casa.jpg)" })




    middleSection.append(ourOfferts, wrapperOfferte)


    const segnapSlider = tags.h1().addClass("yca-home-underlined").html("News")
    const slider = shards.thumbbox.thumbBox({ scrollTime: 3000 })

    api.news.getNews().then(news => {
        const m: JQuery[] = []
        for (const n of news) {
            m.push(shards.news.thumbNews({
                image: n.banner,
                title: n.title,
                subtitle: n.title,
                date: n.date,
                body: n.body,
                id: n._id
            }))
        }
        slider.add(m)
    })



    template.main.append(upperSection, middleSection, segnapSlider, slider.el)



}



