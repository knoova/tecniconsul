import * as tags from "../tags"
import * as templates from "../template/_lib"
import * as shards from "../shards/_lib"
import * as blocks from "../blocks/_lib"
import * as api from "../api/_lib"
import * as forms from "./form"

export function build(language?: string) {
    //CONFIG
    const blueBox = [
        {
            text: "leggere la bolletta", logo: "/img/white_schermo.png", url: [
                { name: "Leggere bolletta luce", url: "/leggerebollettaluce" },
                { name: "Leggere bolletta gas", url: "/leggerebollettagas" }
            ]
        },
        { text: "agevolazioni", logo: "/img/white_colonna.png", url: "/utils/agevolazioni" },
        { text: "come risparmiare", logo: "/img/white_money.png", url: "/utils/come_risparmiare" }
    ]

    const template = templates.main.main()
    const upperSection = shards.bannerone.build()

    const bottomFloater = tags.div().addClass("yca-home-bottomfloater")
    const usClaim = tags.h1().addClass("yca-home-claim").html("NUOVA ENERGIA<BR />NELLA TUA GIORNATA")
    const usButton = tags.div().addClass("yca-home-bigbluepanel-button").append(tags.a().html("Vieni con noi")).css({
        width: "50%", maxWidth: "50%", margin: "0 25%"
    })


    upperSection.append(bottomFloater.append(usClaim, usButton))


    const middleSection = tags.div().addClass("yca-wrapper")
    const ourOfferts = tags.h1().addClass("yca-home-underlined").text("Le nostre offerte")


    const wrapperOfferte = tags.div().addClass("yca-home-offerte-wrapper")

    const offertaFabbrica = tags.a().attr({ href: "/offerte/impresa" }).addClass("yca-home-offerte-offerta").css({
        backgroundImage: "url(/img/fabbrica.jpg)"
    })

    const offertaHome = tags.a().attr({ href: "/offerte/casa" }).addClass("yca-home-offerte-offerta").css({
        backgroundImage: "url(/img/casa.jpg)"
    })


    const offertaHomeText = tags.p().addClass("yca-home-offerte-offerta-testo").html("casa")
    const offertaFabbricaText = tags.p().addClass("yca-home-offerte-offerta-testo").html("impresa")


    offertaHome.append(offertaHomeText)
    offertaFabbrica.append(offertaFabbricaText)



    wrapperOfferte.append(offertaHome, offertaFabbrica)





    const blueSection = tags.div().addClass("yca-home-blue")

    for (let b of blueBox)
        blueSection.append(shards.panel.bluePanel(b))

    const bigBluePanel = tags.div().addClass("yca-home-bigbluepanel-wrapper")
    const bigBluePanelTitle = tags.div().addClass("yca-home-bigbluepanel-title").append(
        tags.p().html("Ti risolviamo ogni dubbio")
    )
    const bigBluePanelButton = tags.div().addClass("yca-home-bigbluepanel-button")
    const bigBluePanelImage = tags.div().addClass("yca-home-bigbluepanel-image")

    const bbb = shards.anchor.full({ text: "clicca qua", url: "/faq" })

    bigBluePanelButton.append(bbb)

    bigBluePanelImage.addClass("yca-home-bigbluepanel-image").css({ backgroundImage: "url(/img/puntointerrogativo.png)" })

    bigBluePanel.append(bigBluePanelTitle, bigBluePanelButton, bigBluePanelImage)


    const vieniConNoi = forms.formVieniConNoi()
    const myModal = shards.modal.panel(vieniConNoi)


    usButton.click(() => {
        myModal.show()
    })

    middleSection.append(myModal.el, ourOfferts, wrapperOfferte, blueSection, bigBluePanel)


    const segnapSlider = tags.h1().addClass("yca-home-underlined").html("News")
    //const errSlider = tags.p().html("Error: MongoDB Error: no element for slider in database.")
    const slider = shards.thumbbox.thumbBox({ scrollTime: 3000 })

    api.news.getNews().then(news => {
        const m: JQuery[] = []
        for (const n of news) {
            m.push(shards.news.thumbNews({
                image: n.banner,
                title: n.title,
                subtitle: n.subtitle,
                date: n.date,
                body: n.body,
                id: n._id
            }))
        }
        slider.add(m)
    })




    template.main.append(upperSection, middleSection, segnapSlider, slider.el, shards.bannerone.lowerBanner())


}



