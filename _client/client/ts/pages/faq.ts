import * as tags from "../tags"
import * as templates from "../template/_lib"
import * as shards from "../shards/_lib"
import * as blocks from "../blocks/_lib"
import * as input from "../fields/input/_lib"
import * as api from "../api/_lib"

export function build(args?: { id: string }) {
    console.log("got param:", args)
    const template = templates.main.main()
    const upperSection = tags.div().addClass("yca-wrapper").css({
        backgroundImage: "url(/img/tramonto.jpg)",
        backgroundSize: "cover",
        width: "80vw",
        height: "18vw",
        position: "relative"
    })

    const categorie: { name: string, _id: string, logo: string, orientation: "left" | "right" }[] = [
        { name: "Utilità", _id: "utility", orientation: "left", logo: "/img/utilitaperte.png" },
        { name: "Supporto", _id: "support", orientation: "right", logo: "/img/supportoperte.png" },
        { name: "Bollette", _id: "invoice", orientation: "left", logo: "/img/bolletteperte.png" },
        { name: "Pagamenti", _id: "payments", orientation: "right", logo: "/img/pagamentiperte.png" },
        { name: "Altro", _id: "other", orientation: "left", logo: "/img/altroperte.png" }
    ]

    const lowerSection = tags.div().addClass("yca-wrapper")

    const if1 = [[0], [0, 0], [1, 0, 1], [1, 0, 0, 1], [2, 1, 0, 1, 2], [2, 1, 0, 0, 1, 2], [3, 2, 1, 0, 1, 2, 3]]


    const fArUtility: { el: JQuery, setShift: (shift: number) => number }[] = []
    const fArSupport: { el: JQuery, setShift: (shift: number) => number }[] = []
    const fArInvoice: { el: JQuery, setShift: (shift: number) => number }[] = []
    const fArPayments: { el: JQuery, setShift: (shift: number) => number }[] = []
    const fArOther: { el: JQuery, setShift: (shift: number) => number }[] = []


    function toArray(arg: { el: JQuery, setShift: (shift: number) => number }[]): JQuery[] {
        const length = arg.length
        const shiftArray = if1[length - 1]
        const myArr: JQuery[] = []
        for (let i = 0; i < length; i++) {
            arg[i].setShift(shiftArray[i] * 2)
            myArr.push(arg[i].el)
        }
        return myArr
    }

    api.faq.getFAQ({}).then(faqs => {
        let c = 0
        for (let i = 0; i < faqs.length; i++) {
            let orientation: "left" | "right" = "left"
            if (faqs[i].category == "support" || faqs[i].category == "payments")
                orientation = "right"
            const fe = shards.faq.faq({
                title: faqs[i].title,
                subitle: faqs[i].subtitle,
                text: faqs[i].description,
                logo: faqs[i].logo,
                hash: faqs[i].part,
                orientation
            })
            if (faqs[i].category == "utility")
                fArUtility.push(fe)
            if (faqs[i].category == "support")
                fArSupport.push(fe)
            if (faqs[i].category == "invoice")
                fArInvoice.push(fe)
            if (faqs[i].category == "payments")
                fArPayments.push(fe)
            if (faqs[i].category == "other")
                fArOther.push(fe)
        }


        for (const cat of categorie) {
            const faqContainer = shards.faq.faqCategory({ title: cat.name, logo: cat.logo, orientation: cat.orientation })
            lowerSection.append(faqContainer.el)

            if (cat._id == "utility")
                faqContainer.set(toArray(fArUtility))
            if (cat._id == "support")
                faqContainer.set(toArray(fArSupport))
            if (cat._id == "invoice")
                faqContainer.set(toArray(fArInvoice))
            if (cat._id == "payments")
                faqContainer.set(toArray(fArPayments))
            if (cat._id == "other")
                faqContainer.set(toArray(fArOther))

        }


        if (args && args.id) {
            if (location.hash)
                location.hash = ""
            location.hash = args.id
        }

    })

    template.main.append(upperSection, lowerSection.css({ paddingBottom: "50px" }))
}
