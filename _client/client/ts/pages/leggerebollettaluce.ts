import * as tags from "../tags"
import * as templates from "../template/_lib"
import * as input from "../fields/input/_lib"
import * as cookies from "../cookies"
import * as shards from "../shards/_lib"
import * as api from "../api/_lib"
import * as forms from "./form"

export function build() {
    const template = templates.main.main()

    const buttonHeader = tags.div().addClass("yca-leggerebolletta-buttonheader").append(
        shards.anchor.full({
            text: "Prima pagina", action: () => {
                first.show()
                second.hide()
                third.hide()
            }
        }),
        shards.anchor.full({
            text: "Seconda pagina", action: () => {
                first.hide()
                second.show()
                third.hide()
            }
        }),
        shards.anchor.full({
            text: "Arera", action: () => {
                first.hide()
                second.hide()
                third.show()
            }
        })
    )


    const buttonHeader2 = tags.div()
    const bprima = tags.h2().css({ display: "inline-block" }).addClass("yca-home-casaimpresa").addClass("selected").html("Prima pagina").click(() => {
        first.show()
        second.hide()
        third.hide()
        bprima.addClass("selected")
        bseconda.removeClass("selected")
        barera.removeClass("selected")
    })
    const bseconda = tags.h2().css({ display: "inline-block" }).addClass("yca-home-casaimpresa").html("Seconda pagina").click(() => {
        first.hide()
        second.show()
        third.hide()
        bprima.removeClass("selected")
        bseconda.addClass("selected")
        barera.removeClass("selected")
    })
    const barera = tags.h2().css({ display: "inline-block" }).addClass("yca-home-casaimpresa").html("Arera").click(() => {
        first.hide()
        second.hide()
        third.show()
        bprima.removeClass("selected")
        bseconda.removeClass("selected")
        barera.addClass("selected")
    })
    buttonHeader2.append(bprima, bseconda, barera)

    const i01 = tags.img().attr({ src: "/img/leggerebolletta/01.jpg" }).css({ maxWidth: "100%" })
    const t01 = tags.div().html(`<h3>META&rsquo; SINISTRA: Informazioni Generali</h3>
    <p><strong>CODICE CLIENTE</strong><br />Identifica il tuo codice personale univoco. Tienilo a portata di mano ogni volta che ci contatti: ci permetter&agrave; di darti velocemente tutte le informazioni di cui hai bisogno.</p>
    <p><strong>INTESTATARIO CONTRATTO</strong><br />&Egrave; l&rsquo;intestatario del contratto, titolare degli obblighi e dei diritti relativi alla fornitura.</p>
    <p><strong>CODICE CONTRATTO</strong><br />Identifica il tuo contratto di fornitura per una specifica utenza</p>
    <p><strong>INDIRIZZO FORNITURA</strong><br />&Egrave; l&rsquo;indirizzo in cui fisicamente viene consegnato il gas ovvero l&rsquo;indirizzo dell&rsquo;allacciamento. Pu&ograve; essere diverso dall&rsquo;indirizzo dell&rsquo;abitazione o dell&rsquo;azienda.</p>
    <p><strong>PDR</strong><br />&Egrave; un codice che identifica il Punto Di Riconsegna, cio&egrave; il punto fisico dove Tecniconsul Energia consegna il gas al cliente finale. Questo codice &egrave; univoco a livello nazionale: &egrave; sempre lo stesso anche se cambi fornitore. Se invece cambi casa, avrai un numero di PDR diverso.</p>
    <p><strong>CODICE REMI</strong><br />&Egrave; l&rsquo;identificativo dell&rsquo;impianto che serve il PDR.</p>
    <p><strong>TIPO OFFERTA</strong><br />&Egrave; il nome dell&rsquo;offerta del mercato libero che hai sottoscritto.</p>
    <p><strong>DATA INIZIO CONTRATTO</strong><br />&Egrave; la data di entrata in fornitura, cio&egrave; il momento dal quale Tecniconsul Energia comincia a fatturare i tuoi consumi.</p>
    <p><strong>TIPO CLIENTE</strong><br />&Egrave; il tipo di cliente intestatario della fornitura. Il &ldquo;cliente domestico&rdquo; &egrave; un privato che in genere usa il gas per la propria abitazione e locali annessi. Negli &ldquo;altri usi&rdquo; sono ricompresi tutti i clienti con partita IVA.</p>
    <p>&nbsp;</p>
    <h3>META&rsquo; DESTRA: Dati Bolletta</h3>
    <p><strong>DATI DI RECAPITO</strong><br />Questo &egrave; l&rsquo;indirizzo di spedizione della fattura. Il nome e cognome indicati servono solo per il recapito e possono essere diversi da quelli riportati a fianco. Assicurati che l&rsquo;indirizzo sia corretto e comunicaci con tempestivit&agrave; ogni variazione per evitare ritardi e disguidi nella consegna della fattura. Se per&ograve; non vuoi pensieri, registrati all&rsquo;area clienti web e passa alla bolletta online: la consegna &egrave; immediata e sicura! Scegli la tranquillit&agrave; e la semplicit&agrave;.</p>
    <p><strong>IMPORTO DA PAGARE</strong><br />&Egrave; l&rsquo;importo complessivo della fattura.</p>
    <p><strong>SCADENZA</strong><br />&Egrave; la data di scadenza del pagamento.</p>`)
    const i02 = tags.img().attr({ src: "/img/leggerebolletta/02.jpg" }).css({ maxWidth: "100%" })
    const t02 = tags.div().html(`<h3>I Miei Consumi</h3>
    <p><strong>CLASSE DEL MISURATORE</strong><br />Indica la portata del misuratore. Le abitazioni con riscaldamento individuale hanno in linea generale contatori di classe G4 (sono i pi&ugrave; piccoli).</p>
    <p><strong>COEFFICIENTE CORRETTIVO (C)</strong><br />&Egrave; il coefficiente che converte il consumo misurato dal contatore, espresso in metri cubi, nell&rsquo;unit&agrave; di misura utilizzata per la fatturazione, cio&egrave; gli standard metri cubi. Tale conversione permette che tu paghi solo per l&rsquo;effettiva quantit&agrave; di gas consumata in base alla pressione e alla temperatura di consegna. Il coefficiente &egrave; fissato dal Distributore locale che definisce le condizioni tecniche di erogazione del gas.</p>
    <p><strong>POTERE CALORIFICO SUPERIORE (P)</strong><br />&Egrave; il valore convenzionale che permette di fatturare in Smc alcune componenti di prezzo relative alla materia gas naturale che invece sono espresse in Giga/Joule.</p>
    <p><strong>TIPOLOGIA D&rsquo;USO</strong><br />Qui trovi il tipo di utilizzo. La terminologia deriva dalla normativa ed &egrave; standard</p>
    <p><strong>MATRICOLA DEL CONTATORE</strong><br />&Egrave; il codice che il produttore mette come identificazione del contatore. Generalmente si trova sotto un codice a barre.</p>
    <p><strong>CONSUMO FATTURATO</strong><br />Sono gli standard metri cubi che effettivamente vengono fatturati in bolletta. Possono essere effettivi, stimati o entrambe le tipologie di consumo.</p>
    <p><strong>GRAFICO CONSUMI</strong><br />Un semplice istogramma che visualizza a colpo d&rsquo;occhio il consumo diviso mese per mese.</p>`)
    const i03 = tags.img().attr({ src: "/img/leggerebolletta/03.jpg" }).css({ maxWidth: "100%" })
    const t03 = tags.div().html(`<h3>META&rsquo; SINISTRA: Riepilogo e importi fatturati</h3>
    <p><strong>MATERIA GAS NATURALE</strong><br />&Egrave; l&rsquo;importo richiesto a seguito delle diverse attivit&agrave; di approvvigionamento e commercializzazione svolte da Tecniconsul Energia per fornirti il gas. Per questa offerta del mercato libero, la voce &egrave; composta da:<br />&ndash; Prezzo (espresso in &euro;/Standard metri cubi, ovvero Smc);<br />&ndash; Altre componenti cos&igrave; come stabilite dall&rsquo;autorit&agrave; per il mercato di tutela, ossia:<br />&mdash; Quota di vendita al dettaglio (QVD) suddivisa in una parte fissa espressa in &euro;/anno e in una variabile in &euro;/Smc;<br />&mdash; Oneri di gradualit&agrave; (GRAD e QVDV) espressi in &euro;/Smc.<br />Per maggiori dettagli invece sulle altre componenti applicate consulta il sito dell&rsquo;autorit&agrave;.</p>
    <p class="text-gas">Per l&rsquo;offerta del mercato libero Tariffa di Riferimento, il prezzo &egrave; aggiornato con cadenza trimestrale in base a quanto stabilito dalla Delibera dell&rsquo;Autorit&agrave; (AEEGSI) n. 196/2013 relativamente alla Componente Materia Prima per il Mercato di Tutela.</p>
    <p><strong>TRASPORTO E GESTIONE CONTATORE</strong><br />&Egrave; l&rsquo;importo richiesto a seguito dei costi sostenuti dal distributore per:<br />&ndash; trasportare il gas attraverso la rete di distruzione nazionale e locale fino al tuo punto di fornitura;<br />&ndash; gestire il tuo contatore con i relativi dati di lettura.<br />Questi costi non dipendono da Tecniconsul Energia ma sono determinati dall&rsquo;Autorit&agrave; (AEEGSI) per tutti i venditori. Questa voce contiene le seguenti componenti:<br />&ndash; Quota trasporto (Qt) espressa in &euro;/Smc e differenziata per le diverse aree del territorio nazionale;<br />&ndash; Tariffa di distribuzione e misura (&tau;1, &tau;3) suddivisa in una parte fissa (espressa in &euro;/anno) e in una parte variabile (espressa in &euro;/Smc) e differenziata per le diverse aree del territorio nazionale<br />&ndash; Componenti RS e UG1 espresse in &euro;/Smc.</p>
    <p><strong>ONERI DI SISTEMA</strong><br />&Egrave; l&rsquo;importo relativo alla copertura di costi per attivit&agrave; di interesse generale per il sistema gas, come ad esempio lo sviluppo delle fonti rinnovabili e progetti per il risparmio energetico. Questi costi non dipendono da Tecniconsul Energia ma sono determinati dall&rsquo;Autorit&agrave; (AEEGSI). Quindi questa voce contiene le stesse componenti previste per il mercato di tutela, ossia:<br />&ndash; Componenti GS, RE e UG3 espresse in &euro;/Smc;<br />&ndash; Componente UG2 suddivisa in una parte fissa (espressa in &euro;/anno) e in una parte variabile (espressa in &euro;/Smc).</p>
    <h3>META&rsquo; DESTRA: Informazioni e Recapiti</h3>
    <p><strong>CONTATTI</strong><br />Questa sezione riporta tutti i riferimenti utili per restare in contatto con Tecniconsul Energia.</p>`)
    const i04 = tags.img().attr({ src: "/img/leggerebolletta/04.jpg" }).css({ maxWidth: "100%" })
    const t04 = tags.div().html(`<p><strong>DETTAGLIO IMPOSTE</strong><br />Il totale dovuto per le imposte &egrave; una delle voci del &ldquo;Riepilogo Importi Fatturati&rdquo; in prima pagina. Questo riquadro mostra il dettaglio della loro applicazione, in particolare vengono indicate l&rsquo;aliquota prevista dalla normativa fiscale, le quantit&agrave; alle quali l&rsquo;aliquota &egrave; applicata e l&rsquo;importo dovuto. Con la bolletta del gas si pagano l&rsquo;imposta di consumo, l&rsquo;addizionale regionale e l&rsquo;imposta sul valore aggiunto (IVA). Ti ricordiamo che le imposte non dipendono dalla societ&agrave; di vendita ma vengono da questa riversate allo Stato o alle Regioni secondo quanto previsto dalla normativa.</p>
    <p><strong>QUADRO LETTURE E CONSUMI</strong><br />Le letture &ldquo;Rilevate&rdquo; (prese dalla societ&agrave; di distribuzione) e le &ldquo;Autoletture&rdquo; (comunicate dal cliente attraverso i canali indicati in prima pagina) sono letture effettive e danno luogo a eventuali conguagli. In mancanza di queste, in fattura vengono indicati i metri cubi stimati ovvero si stima quanto dovrebbe segnare il contatore ad una certa data. Di conseguenza i consumi fatturati in bolletta possono essere effettivi o stimati.</p>
    <p><strong>CONSUMO ANNUO</strong><br />&Egrave; il tuo consumo relativo a 12 mesi di fornitura. Se non abbiamo le letture effettive il dato si basa su stime. Se sei cliente da meno di un anno, invece del &ldquo;Consumo annuo&rdquo; il valore si riferir&agrave; al &ldquo;Consumo da inizio fornitura&rdquo; che appunto &egrave; il consumo rilevato o stimato da quando sei entrato in fornitura.</p>
    <p><strong>ALTRE PARTITE</strong><br />In questo riquadro trovi il dettaglio di cosa ti &egrave; stato fatturato sotto la voce &ldquo;Altre Partite&rdquo; nella &ldquo;Riepilogo Importi Fatturati&rdquo; in prima pagina.</p>
    <p><strong>BOLLETTE NON PAGATE</strong><br />In questa sezione &egrave; riportato il dettaglio delle fatture scadute e non pagate con relativo numero, scadenza e importo. La presenza di fatture &egrave; un dato su cui ti invitiamo a riporre la massima attenzione per evitare interruzioni del servizio.</p>
    <p><strong>INTERESSI DI MORA</strong><br />Questo DATO &egrave; presente solo nel caso in cui ti siano stati addebitati interessi di mora per il ritardato pagamento di fatture precedenti. Gli interessi vengono applicati sulla base dei giorni di ritardo a partire dal giorno di scadenza applicando gli interessi di legge.</p>
    <p><strong>INFORMAZIONI UTILI</strong><br />Questa sezione contiene numerose informazioni utili su molteplici argomenti: sui pagamenti come e dove effettuarli senza commissioni, come e quando richiedere una rateizzazione. Trovi anche le Comunicazioni dell&rsquo;Autorit&agrave; (AEEGSI) e, nel caso di aggiornamento dei corrispettivi dovuti, l&rsquo;eventuale fonte normativa o contrattuale dalla quale derivano. Non possono mancare neanche delle preziose avvertenze sull&rsquo;uso del metano e molto altro.</p>`)
    const arera = tags.div().html(`<h2>Bolletta 2.0</h2>
    <p>&nbsp;</p>
    <p><strong>Informazioni presenti nella bolletta sintetica dell&rsquo;elettricit&agrave;</strong></p>
    <p><a href="https://bolletta.autorita.energia.it/bolletta20/index.php/home/elettricita/dati-del-cliente-e-della-fornitura" target="_blank" rel="noopener">ARERA &ndash;&nbsp;Come leggere la tua bolletta</a></p>
    <p>&nbsp;</p>
    <p><strong>Glossario della bolletta per la fornitura di Luce e Gas</strong></p>
    <p><a href="http://www.tecniconsulenergia.it/allegati/Glossario_Luce_Gas.pdf" target="_blank" rel="noopener">ARERA &ndash;&nbsp;Glossario</a></p>
    <p>&nbsp;</p>
    <p><strong>Voci di spesa</strong><br /><a href="https://bolletta.autorita.energia.it/bolletta20/index.php/guida-voci-di-spesa/elettricita" target="_blank" rel="noopener">ARERA &ndash; Guida alla lettura delle voci di spesa</a></p>`)



    const first = tags.div().show().append(
        i01, t01, i02, t02, i03, t03
    )
    const second = tags.div().hide().append(
        i04, t04
    )
    const third = tags.div().hide().append(arera)



    template.main.append(buttonHeader2, first, second, third)
}



