import * as tag from "../tags"
import * as templates from "../template/_lib"
import * as input from "../fields/input/_lib"
import * as cookies from "../cookies"
import * as shards from "../shards/_lib"
import * as api from "../api/_lib"
import * as forms from "./form"

export function build() {
    const template = templates.main.main()


    const reportNews = tag.div()
    const buttonNews = tag.button().text("Sincronizza news").on("click", async () => {
        const news = await api.migrazione.getNews()
        reportNews.append(tag.p().text("Ci sono da scaricare " + news.length + " elementi"))
        let count = 1
        for (const n of news) {
            try {
                if (n.banner) {
                    const pi = await api.migrazione.putImage(n.banner)
                    n.banner = n.banner.replace("https://tecniconsul.s3.amazonaws.com/", "/uploadedImg/")
                }
                const res = await api.news.setNews(n)
                console.log("Put", res.id)
                reportNews.append(tag.p().text("Elemento " + count + " migrata"))
            } catch (e) {
                reportNews.append(tag.p().text("Elemento " + count + " impossibile da migrare"))
            }
            count++

        }
    })

    const reportComuni = tag.div()
    const buttonComuni = tag.button().text("Sincronizza comuni").on("click", async () => {
        const comuni = await api.migrazione.getComuni()
        reportComuni.append(tag.p().text("Ci sono da scaricare " + comuni.length + " elementi"))
        let count = 1
        for (const n of comuni) {
            try {
                const res = await api.comuni.setComune(n)
                console.log("Put", res.id)
                reportComuni.append(tag.p().text("Elemento " + count + " migrata"))
            } catch (e) {
                reportComuni.append(tag.p().text("Elemento " + count + " impossibile da migrare"))
            }
            count++
        }
    })

    const reportCabine = tag.div()
    const buttonCabine = tag.button().text("Sincronizza cabine").on("click", async () => {
        const cabine = await api.migrazione.getCabine()
        reportCabine.append(tag.p().text("Ci sono da scaricare " + cabine.length + " elementi"))
        let count = 1
        for (const n of cabine) {
            try {
                const res = await api.comuni.setCabina(n)
                console.log("Put", res.id)
                reportCabine.append(tag.p().text("Elemento " + count + " migrata"))
            } catch (e) {
                reportCabine.append(tag.p().text("Elemento " + count + " impossibile da migrare"))
            }
            count++
        }
    })

    const reportRegioni = tag.div()
    const buttonRegioni = tag.button().text("Sincronizza regioni").on("click", async () => {
        const regioni = await api.migrazione.getRegioni()
        reportRegioni.append(tag.p().text("Ci sono da scaricare " + regioni.length + " elementi"))
        let count = 1
        for (const n of regioni) {
            try {
                const res = await api.regioni.setRegione(n)
                console.log("Put", res.id)
                reportRegioni.append(tag.p().text("Elemento " + count + " migrata"))
            } catch (e) {
                reportRegioni.append(tag.p().text("Elemento " + count + " impossibile da migrare"))
            }
            count++
        }
    })

    const reportProvince = tag.div()
    const buttonProvince = tag.button().text("Sincronizza province").on("click", async () => {
        const province = await api.migrazione.getProvince()
        reportProvince.append(tag.p().text("Ci sono da scaricare " + province.length + " elementi"))
        let count = 1
        for (const n of province) {
            try {
                const res = await api.generic.setProvincia(n)
                console.log("Put", res.id)
                reportProvince.append(tag.p().text("Elemento " + count + " migrata"))
            } catch (e) {
                reportProvince.append(tag.p().text("Elemento " + count + " impossibile da migrare"))
            }
            count++
        }
    })

    const reportFaq = tag.div()
    const buttonFaq = tag.button().text("Sincronizza Faq").on("click", async () => {
        const faqs = await api.migrazione.getFAQ()
        reportFaq.append(tag.p().text("Ci sono da scaricare " + faqs.length + " elementi"))
        let count = 1
        for (const n of faqs) {
            try {
                if (n.logo) {
                    const pi = await api.migrazione.putImage(n.logo)
                    n.logo = n.logo.replace("https://tecniconsul.s3.amazonaws.com/", "/uploadedImg/")
                }
                const res = await api.faq.setFAQ(n)
                console.log("Put", res)
                reportFaq.append(tag.p().text("Elemento " + count + " migrata"))
            } catch (e) {
                reportFaq.append(tag.p().text("Elemento " + count + " impossibile da migrare"))
            }
            count++
        }
    })

    const reportUtils = tag.div()
    const buttonUtils = tag.button().text("Sincronizza utils").on("click", async () => {
        const utils = await api.migrazione.getUtils()
        reportUtils.append(tag.p().text("Ci sono da scaricare " + utils.length + " elementi"))
        let count = 1
        for (const n of utils) {
            try {
                const res = await api.utils.setUtils(n)
                console.log("Put", res.id)
                reportUtils.append(tag.p().text("Elemento " + count + " migrata"))
            } catch (e) {
                reportUtils.append(tag.p().text("Elemento " + count + " impossibile da migrare"))
            }
            count++
        }
    })

    const reportUffici = tag.div()
    const buttonUffici = tag.button().text("Sincronizza uffici").on("click", async () => {
        const uffici = await api.migrazione.getUffici()
        reportUffici.append(tag.p().text("Ci sono da scaricare " + uffici.length + " elementi"))
        let count = 1
        for (const n of uffici) {
            try {
                const res = await api.uffici.setUfficio(n)
                console.log("Put", res.id)
                reportUffici.append(tag.p().text("Elemento " + count + " migrata"))
            } catch (e) {
                reportUffici.append(tag.p().text("Elemento " + count + " impossibile da migrare"))
            }
            count++
        }
    })


    const buttonAll = tag.button().text("Sincronizza TUTTO").on("click", async () => {
        buttonNews.click()
        buttonUtils.click()
        buttonUffici.click()
        buttonFaq.click()
        buttonRegioni.click()
        buttonProvince.click()
        buttonComuni.click()
        buttonCabine.click()
    })


    template.main.append(
        buttonNews,
        reportNews,
        tag.hr(),
        buttonRegioni,
        reportRegioni,
        tag.hr(),
        buttonProvince,
        reportProvince,
        tag.hr(),
        buttonComuni,
        reportComuni,
        tag.hr(),
        buttonCabine,
        reportCabine,
        tag.hr(),
        buttonFaq,
        reportFaq,
        tag.hr(),
        buttonUtils,
        reportUtils,
        tag.hr(),
        buttonUffici,
        reportUffici,
        tag.hr(),
        buttonAll
    )
}



