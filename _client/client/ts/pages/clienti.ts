import * as tags from "../tags"
import * as templates from "../template/_lib"
import * as shards from "../shards/_lib"
import * as blocks from "../blocks/_lib"
import * as input from "../fields/input/_lib"
import * as utils from "../utils";

export function build(language?: string) {
    //CONFIG
    const blueBox = [
        { text: "Portale energia", logo: "/img/round_home.png", url: "https://tecniconsul-portcli.serviceict.it/argon-tecniconsul/portale.html?idFornitore=1" },
        { text: "Portale gas", logo: "/img/round_home.png", url: "http://www.tecniconsulenergia.it:8080/RetiVendita/Pages/Login.aspx" },
        //{ text: "Comunicazioni personali", logo: "/img/round_pencil.png" },
        { text: "Autolettura elettricità", logo: "/img/round_schermovuotol.png", url: "/clienti/autolettura" },
        { text: "Autolettura gas", logo: "/img/round_schermovuotol.png" },
        { text: "I miei dati personali", logo: "/img/round_pencil.png", url: "/clienti/datipersonali" },
        { text: "Attiva la fatturazione elettronica", logo: "/img/round_schermovuotol.png", url: "/clienti/fatturazioneelettronica" }
    ]


    const user = utils.userManager().get()

    if (user.isAdmin)
        blueBox.push({ text: "Amministrazione", logo: "/img/round_schermovuotol.png", url: "/amministrazione" })





    const template = templates.main.main()
    const upperSection = shards.bannerone.build()

    const bottomFloater = tags.div().addClass("yca-home-bottomfloater")
    const usClaim = tags.h1().addClass("yca-home-claim").html("AREA<br />CLIENTI")

    upperSection.append(bottomFloater.append(usClaim))



    const blueSection = tags.div().addClass("yca-home-white")

    for (let b of blueBox)
        blueSection.append(shards.panel.whitePanel(b))


    const lower = tags.div().css({ margin: "250px 0" })

    const welcome = tags.h1().addClass("yca-login-title").html("Benvenuto nella tua area personale").css({ textAlign: "center", textTransform: "none", color: "#0000bb" })

    lower.append(welcome, tags.hr().css({ margin: "50px 0" }), blueSection)


    template.main.append(upperSection, lower)


}



