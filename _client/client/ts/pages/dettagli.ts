import * as tags from "../tags"
import * as templates from "../template/_lib"
import * as shards from "../shards/_lib"
import * as blocks from "../blocks/_lib"
import * as api from "../api/_lib"
import * as forms from "./form"

export function build(args?: { offerta: string }) {
   
    const template = templates.main.main()
    const upperSection = shards.bannerone.build()

    const bottomFloater = tags.div().addClass("yca-home-bottomfloater")
    const usClaim = tags.h1().addClass("yca-home-claim").html("LUCE E GAS<BR />OVUNQUE TU SIA")


    upperSection.append(bottomFloater.append(usClaim))


    const middleSection = tags.div().addClass("yca-wrapper")
    const ourOfferts = tags.h1().addClass("yca-home-underlined").text("Luce casa")


    const wrapperOfferte = tags.div()

    const offerte = tags.div().css({
        width: "50vw",
        flex: "1",
        height: "36vw",
        display: "inline-block"
    })

    const offLinkCont = tags.div()
    const link1 = tags.h2().css({ display: "inline-block" }).addClass("yca-home-casaimpresa").html("Luce 1")
    const link2 = tags.h2().css({ display: "inline-block" }).addClass("yca-home-casaimpresa").html("Luce 2")


    const threeButt = tags.div().css({
        float: "right",
        height: "36vw",
        backgroundColor: "#cccccccc",
        padding: "30px 20px"
    }).append(
        tags.div().addClass("yca-dettagli-sidebar").html("Scopri la tariffa<br />luce che meglio<br />si presta alla<br />gestione dei<br />consumi della<br />tua casa")
    )


    wrapperOfferte.append(offerte.append(offLinkCont.append(link1, link2)), threeButt).css({ backgroundImage: "url(/img/casa.jpg)" })

    link1.click(() => {
        link1.addClass("selected")
        link2.removeClass("selected")
        lowerTitleInner.html("Luce 1 casa")
        wrapperOfferte.css({ backgroundImage: "url(/img/lucecasamiddle.jpg)" })
        upperSection.css({ backgroundImage: "url(/img/lucecasaheader.jpg)" })
    })
    link2.click(() => {
        link1.removeClass("selected")
        link2.addClass("selected")
        lowerTitleInner.html("Luce 2 casa")
        wrapperOfferte.css({ backgroundImage: "url(/img/lucecasamiddle.jpg)" })
        upperSection.css({ backgroundImage: "url(/img/lucecasaheader.jpg)" })
    })




    middleSection.append(ourOfferts, wrapperOfferte)


    const segnapSlider = tags.h1().addClass("yca-home-underlined").html("News")
    const slider = shards.thumbbox.thumbBox({ scrollTime: 3000 })

    api.news.getNews().then(news => {
        const m: JQuery[] = []
        for (const n of news) {
            m.push(shards.news.thumbNews({
                image: n.banner,
                title: n.title,
                subtitle: n.title,
                date: n.date,
                body: n.body,
                id: n._id
            }))
        }
        slider.add(m)
    })


    const lowerTitle = tags.div().addClass("yca-dettagli-lowerTitleInner")
    const lowerTitleInner = tags.h1().css({
        fontSize: "4rem",
        color: "#666",
        textTransform: "none",
        fontFamily: "'Ubuntu', sans-serif"
    })
    lowerTitle.append(lowerTitleInner)

    const multicolumn = tags.div().html(`
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    `).css({
            columnCount: "3",
            columnGap: "30px",
            color: "#666",
            fontSize: "1.3rem",
            lineHeight: "1.9rem",
            fontFamily: "'Ubuntu', sans-serif"
        })

    const images = tags.div().css({ margin: "50px 0" })
    const img1 = tags.img().attr({ src: "/img/tariffa1.jpg" }).css({ display: "inline-block", maxWidth: "30%", minWidth: "30%", padding: "10px 20px", margin: "0 50px 0 0" })
    const img2 = tags.img().attr({ src: "/img/tariffa2.jpg" }).css({ display: "inline-block", maxWidth: "30%", minWidth: "30%", padding: "10px 20px", margin: "0 50px 0 0" })

    images.append(img1, img2)



    template.main.append(upperSection, middleSection, lowerTitle, multicolumn, images, segnapSlider, slider.el)

    link1.click()

    if (args.offerta == "casa") {
        link1.click()
    }

    if (args.offerta == "impresa") {
        link2.click()
    }

}



