import * as tags from "../tags"
import * as templates from "../template/_lib"
import * as shards from "../shards/_lib"
import * as blocks from "../blocks/_lib"
import * as input from "../fields/input/_lib"
import * as api from "../api/_lib"

export function build(args?: { title: string, slug: string, body: string, _id?: string }) {
    console.log("got param:", args)
    const template = templates.main.main()
    const upperSection = tags.div().addClass("yca-wrapper").append(tags.h1().html(args.title))
    const lowerSection = tags.div().addClass("yca-wrapper").append(tags.div().html(args.body))





    template.main.append(upperSection, lowerSection).addClass(`yca-utils-${args.slug}`)
}