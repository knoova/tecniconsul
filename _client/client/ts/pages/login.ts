import * as tags from "../tags"
import * as templates from "../template/_lib"
import * as input from "../fields/input/_lib"
import * as cookies from "../cookies"
import * as shards from "../shards/_lib"
import * as api from "../api/_lib"
import * as forms from "./form"

export function build() {
    const template = templates.main.main()
    const modal = forms.buildLoginModal()
    template.main.append(modal.el)
    window.scrollTo(0, 0)
    modal.show()
}



