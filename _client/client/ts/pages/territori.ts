import * as tags from "../tags"
import * as templates from "../template/_lib"
import * as shards from "../shards/_lib"
import * as api from "../api/_lib"
import { pages } from "../_lib";

export function build(language?: string) {
    const template = templates.main.main()
    const upperSection = tags.div().addClass("yca-wrapper").css({
        backgroundImage: "url(/img/tubi.jpg)",
        backgroundSize: "cover",
        width: "80vw",
        height: "20vw",
        position: "relative"
    })

    const bottomFloater = tags.div().addClass("yca-home-bottomfloater")
    const usClaim = tags.h1().addClass("yca-home-claim").html("Luce e Gas<br/>Ovunque tu sia")

    upperSection.append(bottomFloater.append(usClaim))

    const lowerSection = tags.div().addClass("yca-wrapper")


    const regTitle = tags.h4().addClass("yca-territori-squarepre").text("Regione")
    const comTitle = tags.h4().addClass("yca-territori-squarepre").text("Comune")
    const cabTitle = tags.h4().addClass("yca-territori-squarepre").text("Cabina")
    const sReg = tags.select()
    const sComune = tags.select()
    const sCabine = tags.select()

    api.regioni.getRegioni().then(re => {
        for (const r of re)
            sReg.append(tags.option().text(r.name).attr({ value: r._id }))
        api.comuni.getComuni({ regione: <string>sReg.val() }).then(comuni => {
            for (const p of comuni)
                sComune.append(tags.option().text(p.name).attr({ value: p._id }))
            api.comuni.getCabine({ comune: <string>sComune.val() }).then(cabine => {
                for (const c of cabine)
                    sCabine.append(tags.option().text(c.name).attr({ value: c._id }))
                sCabine.click()
                api.comuni.getCabina(<string>sCabine.val()).then(cabina => {
                    // setGreenTable(cabina.tabella1[0])
                    // setPurpleTable(cabina.tabella2[0])
                    // setGreyTable(cabina.tabella3)
                    // setBlueTable(cabina.tabella4)
                })
            })
        })
    })

    sReg.change(e => {
        sComune.empty()
        api.comuni.getComuni({ regione: <string>sReg.val() }).then(prov => {
            for (const p of prov)
                sComune.append(tags.option().text(p.name).attr({ value: p._id }))
            sComune.click()
            api.comuni.getCabine({ comune: <string>sComune.val() }).then(cabine => {
                for (const c of cabine)
                    sCabine.append(tags.option().text(c.name).attr({ value: c._id }))
                api.comuni.getCabina(<string>sCabine.val()).then(cabina => {
                    // setGreenTable(cabina.tabella1[0])
                    // setPurpleTable(cabina.tabella2[0])
                    // setGreyTable(cabina.tabella3)
                    // setBlueTable(cabina.tabella4)
                })
            })

        })
    })

    sComune.change(e => {
        sCabine.empty()
        api.comuni.getCabine({ comune: <string>sComune.val() }).then(comune => {
            for (const p of comune)
                sCabine.append(tags.option().text(p.name).attr({ value: p._id }))
            sComune.click()
            api.comuni.getCabine({ comune: <string>sCabine.val() }).then(cabine => {
                for (const c of cabine)
                    sCabine.append(tags.option().text(c.name).attr({ value: c._id }))
                api.comuni.getCabina(<string>sCabine.val()).then(cabina => {
                    // setGreenTable(cabina.tabella1[0])
                    // setPurpleTable(cabina.tabella2[0])
                    // setGreyTable(cabina.tabella3)
                    // setBlueTable(cabina.tabella4)
                })
            })
        })
    })

    sCabine.on("change click", e => {
        api.comuni.getCabina(<string>sCabine.val()).then(cabina => {
            // setGreenTable(cabina.tabella1[0])
            // setPurpleTable(cabina.tabella2[0])
            // setGreyTable(cabina.tabella3)
            // setBlueTable(cabina.tabella4)
            const nome = cabina.name.toLowerCase().split(' ').map((s) => s.charAt(0).toUpperCase() + s.substring(1)).join('')
            buttonWrapper.empty().append(
                shards.anchor.full({text: "Clienti domestici", url: "/pdf/cabine/"+nome+"D.pdf"}),
                shards.anchor.full({text: "Clienti non domestici", url: "/pdf/cabine/"+nome+"ND.pdf"})
            )
        })
    })

    const affianca = tags.div().addClass("yca-territori-affianca")
    const affiancaLeft = tags.div().addClass("yca-territori-affianca-left")
    const affiancaRight = tags.div().addClass("yca-territori-affianca-left")
    const affiancaAfter = tags.div().addClass("yca-territori-affianca-left")
    affiancaLeft.append(
        regTitle,
        sReg
    )
    affiancaRight.append(
        comTitle,
        sComune
    )
    affiancaAfter.append(
        cabTitle,
        sCabine
    )

    const centerToTables = tags.div().css({ width: "60vw", margin: "auto" }).append(
        tags.h4().addClass("yca-territori-title").text("Inserisci la regione a cui sei interessato"),
        affianca.append(affiancaLeft, affiancaRight, affiancaAfter)
    )


    const buttonWrapper = tags.div().addClass("yca-territori-buttonwrapper")
    

    const disclaimer = tags.p().addClass("yca-territori-note").css({ fontSize: "10px", padding: "20px 0px" }).html("Note:<br />PCS: è il Potere calorifico superiore ovvero la quantità di calore realizzata nella combustione completa, a pressione costante di 1,01325 bar, di una unità di massa o di volume di combustibile secondo quanto previsto dalla delibera Arg/gas 64/09 e s.m.i. Le condizioni economiche di fornitura indicate, sono da ritenersi \"salvo conguaglio\" e diverse disposizioni dell'Impresa di Distri")


    const noi = tags.a().append(tags.img().addClass("yca-territori-connoi").attr({ src: "/img/noi.jpg" }).css({ width: "26vw", margin: "50px auto", display: "block", cursor: "pointer" }))

    const vieniConNoi = tags.div().addClass("yca-wrapper").css({ display: "none" })
    

    const modal = shards.modal.panel(pages.form.formVieniConNoi())

    noi.on("click", () => {
        console.log("cliccked")
        modal.show()
    })


    lowerSection.append(
        centerToTables,
        buttonWrapper,
        // greenTable,
        // purpleTable,
        // greyTable,
        disclaimer.css({ width: "60vw", margin: "auto" }),
        // blueTable,
        noi,
        vieniConNoi,
        modal.el
    )

    template.main.append(upperSection, lowerSection)

}

