import * as tags from "../tags"
import * as templates from "../template/_lib"
import * as shards from "../shards/_lib"
import * as blocks from "../blocks/_lib"
import * as input from "../fields/input/_lib"
import * as api from "../api/_lib"
import * as form from "./form"

export function build(language?: string) {
    const template = templates.main.main()

    const upperSection = tags.div().addClass("yca-wrapper").css({
        backgroundImage: "url(/img/contatti.jpg)",
        backgroundSize: "cover",
        width: "80vw",
        height: "18vw",
        position: "relative",
        backgroundPosition: "top"
    })

    const bottomFloater = tags.div().addClass("yca-home-bottomfloater")
    const usClaim = tags.h1().addClass("yca-home-claim").html("NUOVA ENERGIA<BR />NELLA TUA GIORNATA!")



    upperSection.append(bottomFloater.append(usClaim))


    const middleSection = tags.div().addClass("yca-wrapper").css({ display: "flex" })

    const greenNumber = tags.div().addClass("yca-autoflex").addClass("yca-autoflex-column")




    greenNumber.append(
        tags.h3().addClass("yca-contatti-blacktitle").html("Numero Verde per comunicazioni autolettura"),
        tags.h2().addClass("yca-green-number-text").html("800172358"),
        tags.p().addClass("yca-contatti-greyparagraph").html("Per informazioni:"),
        tags.p().addClass("yca-contatti-greyparagraph").html(`Tecniconsul Energia s.r.l.<br>
Via M.K. Gandhi, 22 - 42123 REGGIO EMILIA<br>
Posta elettronica standard: <br>
<a href="mailto:info@tecniconsulenergia.it">info@tecniconsulenergia.it</a><br>
`),
        tags.p().addClass("yca-contatti-greyparagraph").html(`Posta elettronica certificata: <br>
<a href="mailto:tecniconsulenergia@postecert.it">tecniconsulenergia@postecert.it</a>`),
        tags.p().addClass("yca-contatti-greyparagraph").html(`Registro Imprese RE, Codice Fiscale <br>
e p. IVA n. 02400570350`)
    )


    middleSection.append(form.formComunicazioniPersonali(greenNumber))

    const bottomSection = tags.div().addClass("yca-wrapper").css({ marginBottom: "100px" })

    bottomSection.append(tags.h1().html("Indirizzi e orari uffici di zona").css({ color: "#000" }))


    api.uffici.getUffici().then(offices => {
        const uffici = shards.related.uffici()
        const radioWrapper = input.radioMulti.radioMulti({ title: "Seleziona ufficio", name: "ufficio", }, "city", offices)
        bottomSection.append(radioWrapper.el, uffici.el)
        radioWrapper.model.watch(ds => {
            uffici.set(ds.value)
        })
        uffici.set(offices[0])
    })

    template.main.append(upperSection, middleSection, bottomSection)
    window.scrollTo(0, 0)
}



