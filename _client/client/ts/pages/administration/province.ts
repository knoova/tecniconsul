import * as tags from "../../tags"
import * as templates from "../../template/_lib"
import * as shards from "../../shards/_lib"
import * as blocks from "../../blocks/_lib"
import * as input from "../../fields/input/_lib"
import * as api from "../../api/_lib"
import * as cookies from "../../cookies"


export function addProvincia() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()
    const title = tags.h3().text("Aggiungi regione")
    const sReg = tags.select()
    api.regioni.getRegioni().then(re => {
        for (const r of re)
            sReg.append(tags.option().text(r.name).attr({ value: r._id }))
    })
    const nome = input.string.stringInput({ title: "Nome Provincia" })

    const submit = tags.isubmit().val("Invia").click(() => {
        if (nome.model.read().state == "invalid")
            return

        console.log({ name: nome.model.read().value, regione: <string>sReg.val() })
        api.generic.setProvincia({ name: nome.model.read().value, regione: <string>sReg.val() }).then(id => {
            nome.set("")
        })

    })

    template.main.append(title, sReg, nome.el, submit)
}




export function changeProvincia() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()

    api.generic.getProvince().then(regioni => {

        const t = tags.h3().text("Modifica provincia")
        const name = input.string.stringInput({ title: "Nome" })
        const whichBrochure = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli provincia" }, "name", regioni)

        let _id = ""

        const submit = tags.isubmit().val("Invia").click(() => {
            if (name.model.read().state == "invalid")
                return
            api.generic.changeProvince({
                _id: _id,
                name: name.model.read().value
            }).then(id => {
                changeProvincia()
            })

        })

        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.generic.deleteProvince(_id).then(removed => {
                console.log(removed)
                changeProvincia()
            })
        })

        whichBrochure.model.watch(res => {
            if (res.state != "valid")
                return
            name.model.write({ state: "valid", value: res.value.name })
            _id = res.value._id
        })

        template.main.append(whichBrochure.el, name.el, submit, cancella)
    })

}
