import * as tags from "../../tags"
import * as templates from "../../template/_lib"
import * as shards from "../../shards/_lib"
import * as blocks from "../../blocks/_lib"
import * as input from "../../fields/input/_lib"
import * as api from "../../api/_lib"
import * as cookies from "../../cookies"



export function addUffici() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()
    const title = tags.h3().text("Aggiungi ufficio")
    const city = input.string.stringInput({ title: "Provincia o città *" })
    const address = input.string.stringInput({ title: "Indirizzo *" })
    const phone = input.string.stringInput({ title: "Contatti telefonici *" })
    const hours = input.string.stringInput({ title: "Orari di apertura *", multiline: 10 })

    const submit = tags.isubmit().val("Invia").click(() => {
        api.uffici.setUfficio({ city: city.model.read().value, address: address.model.read().value, phone: phone.model.read().value, hours: hours.model.read().value, }).then(id => {
            city.set("")
            address.set("")
            phone.set("")
            hours.set("")
        })

    })

    template.main.append(title, city.el, address.el, phone.el, hours.el, submit)


}


export function changeUfficio() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()

    api.uffici.getUffici().then(uffici => {

        const title = tags.h3().text("Aggiungi ufficio")
        const city = input.string.stringInput({ title: "Provincia o città *" })
        const address = input.string.stringInput({ title: "Indirizzo *" })
        const phone = input.string.stringInput({ title: "Contatti telefonici *" })
        const hours = input.string.stringInput({ title: "Orari di apertura *", multiline: 10 })
        const scegliufficio = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli brochure" }, "city", uffici)

        let _id = ""

        const submit = tags.isubmit().val("Invia").click(() => {
            if (city.model.read().state == "invalid")
                return
            if (address.model.read().state == "invalid")
                return
            if (phone.model.read().state == "invalid")
                return
            if (hours.model.read().state == "invalid")
                return
            api.uffici.changeUfficio({
                _id,
                city: city.model.read().value,
                address: address.model.read().value,
                phone: phone.model.read().value,
                hours: hours.model.read().value,
            }).then(id => {
                changeUfficio()
            })

        })

        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.uffici.deleteUfficio(_id).then(removed => {
                console.log(removed)
                changeUfficio()
            })
        })

        scegliufficio.model.watch(res => {
            if (res.state != "valid")
                return
            city.model.write({ state: "valid", value: res.value.city })
            address.model.write({ state: "valid", value: res.value.address })
            phone.model.write({ state: "valid", value: res.value.phone })
            hours.model.write({ state: "valid", value: res.value.hours })
            _id = res.value._id
        })

        template.main.append(title, scegliufficio.el, city.el, address.el, phone.el, hours.el, submit, cancella)
    })

}

