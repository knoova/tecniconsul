import * as tags from "../../tags"
import * as templates from "../../template/_lib"
import * as shards from "../../shards/_lib"
import * as blocks from "../../blocks/_lib"
import * as input from "../../fields/input/_lib"
import * as api from "../../api/_lib"
import * as cookies from "../../cookies"


export function addCabina() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()
    const title = tags.h3().text("Aggiungi cabina")
    const sReg = tags.select()
    const sPro = tags.select()
    const sCom = tags.select()

    api.regioni.getRegioni().then(re => {
        for (const r of re)
            sReg.append(tags.option().text(r.name).attr({ value: r._id }))
        api.generic.getProvince(<string>sReg.val()).then(prov => {
            for (const p of prov)
                sPro.append(tags.option().text(p.name).attr({ value: p._id }))
            api.comuni.getComuni({ provincia: <string>sPro.val() }).then(com => {
                for (const c of com)
                    sCom.append(tags.option().text(c.name).attr({ value: c._id }))
                sCom.click()
            })
        })
    })

    sReg.change(e => {
        sPro.empty()
        api.generic.getProvince(<string>sReg.val()).then(prov => {
            for (const p of prov)
                sPro.append(tags.option().text(p.name).attr({ value: p._id }))
            sPro.click()
        })
    })
    sPro.change(e => {
        sCom.empty()
        api.comuni.getComuni({ provincia: <string>sPro.val() }).then(com => {
            for (const p of com)
                sCom.append(tags.option().text(p.name).attr({ value: p._id }))
            sCom.click()
        })
    })

    const nome = input.string.stringInput({ title: "Nome Cabina" })

    const submit = tags.isubmit().val("Invia").click(() => {
        if (nome.model.read().state == "invalid")
            return

        console.log({ name: nome.model.read().value, regione: <string>sReg.val() })
        api.comuni.setCabina({ name: nome.model.read().value, regione: <string>sReg.val(), provincia: <string>sPro.val(), comune: <string>sCom.val() }).then(id => {
            nome.set("")
        })

    })

    template.main.append(title, sReg, sPro, sCom, nome.el, submit)
}




export function changeCabina() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()
    const wrapper = tags.div()

    const sRegione = tags.select()
    api.regioni.getRegioni().then(re => {
        for (const r of re)
            sRegione.append(tags.option().text(r.name).attr({ value: r._id }))
        sRegione.click()
    })
    sRegione.change(e => {
        wrapper.empty()
        buildByRegione(<string>sRegione.val())
    })

    template.main.append(sRegione, wrapper)

    function buildByRegione(mRegione: string) {
        api.comuni.getCabine({regione: mRegione}).then(cabine => {

            const t = tags.h3().text("Modifica cabina")
            const name = input.string.stringInput({ title: "Nome" })
            const sReg = tags.select()
            const sCom = tags.select()
            const whichBrochure = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli cabina" }, "name", cabine)
    
            api.regioni.getRegioni().then(re => {
                for (const r of re)
                    sReg.append(tags.option().text(r.name).attr({ value: r._id }))
                sReg.click()
                api.comuni.getComuni({ regione: <string>sReg.val() }).then(comuni => {
                    for (const p of comuni)
                        sCom.append(tags.option().text(p.name).attr({ value: p._id }))
                })
            })
    
            sReg.change(e => {
                sCom.empty()
                api.comuni.getComuni({ regione: <string>sReg.val() }).then(prov => {
                    for (const p of prov)
                        sCom.append(tags.option().text(p.name).attr({ value: p._id }))
                    sCom.click()
                })
            })
    
            let _id = ""
    
            const submit = tags.isubmit().val("Invia").click(() => {
                if (name.model.read().state == "invalid")
                    return
                api.comuni.getComune(<string>sCom.val()).then(comune => {
    
                    api.comuni.changeCabina({
                        _id: _id,
                        name: name.model.read().value,
                        regione: comune.regione,
                        provincia: comune.provincia,
                        comune: comune._id
                    }).then(id => {
                        changeCabina()
                    })
                })
    
            })
    
            const cancella = tags.isubmit().val("Cancella").click(() => {
                api.comuni.deleteCabina(_id).then(removed => {
                    console.log(removed)
                    changeCabina()
                })
            })
    
            whichBrochure.model.watch(res => {
                if (res.state != "valid")
                    return
                name.model.write({ state: "valid", value: res.value.name })
                _id = res.value._id
            })
    
            wrapper.empty().append(whichBrochure.el, sReg, sCom, name.el, submit, cancella)
        })
    }

    

}
