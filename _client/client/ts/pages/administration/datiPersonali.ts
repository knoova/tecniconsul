import * as tags from "../../tags"
import * as templates from "../../template/_lib"
import * as shards from "../../shards/_lib"
import * as blocks from "../../blocks/_lib"
import * as input from "../../fields/input/_lib"
import * as api from "../../api/_lib"
import * as utils from "../../utils";

import { province } from "../../configurations/province"

export function build(language?: string) {
    const utente = utils.userManager()

    if (!utente.isLogged()) {
        location.href = "/login"
    }
    //CONFIG
    const blueBox = [
        { text: "Portale energia", logo: "/img/round_home.png", url: "https://tecniconsul-portcli.serviceict.it/argon-tecniconsul/portale.html?idFornitore=1" },
        { text: "Portale gas", logo: "/img/round_home.png", url: "http://www.tecniconsulenergia.it:8080/RetiVendita/Pages/Login.aspx" },
        //{ text: "Comunicazioni personali", logo: "/img/round_pencil.png" },
        { text: "Autolettura elettricità", logo: "/img/round_schermovuotol.png", url: "/clienti/autolettura" },
        { text: "Autolettura gas", logo: "/img/round_schermovuotol.png" },
        { text: "I miei dati personali", logo: "/img/round_pencil.png", url: "/clienti/datipersonali" },
        { text: "Attiva la fatturazione elettronica", logo: "/img/round_schermovuotol.png", url: "/clienti/fatturazioneelettronica" }
    ]


    if (utente.get().isAdmin)
        blueBox.push({ text: "Amministrazione", logo: "/img/round_schermovuotol.png", url: "/amministrazione" })


    const template = templates.main.main()
    const upperSection = tags.div().addClass("yca-wrapper").css({
        backgroundImage: "url(/img/lucecasamiddle.jpg)",
        backgroundSize: "cover",
        width: "80vw",
        height: "36vw",
        position: "relative"
    })

    const bottomFloater = tags.div().addClass("yca-home-bottomfloater")
    const usClaim = tags.h1().addClass("yca-home-claim").html("AREA<br />CLIENTI")

    upperSection.append(bottomFloater.append(usClaim))



    const blueSection = tags.div().addClass("yca-home-white")

    for (let b of blueBox)
        blueSection.append(shards.panel.whitePanel(b))


    const lower = tags.div().css({ margin: "150px 0" })

    const welcome = tags.h1().addClass("yca-login-title").html("Benvenuto nella tua area personale").css({ textAlign: "center", textTransform: "none", color: "#0000bb" })

    lower.append(welcome, tags.hr().css({ margin: "50px 0" }), blueSection)

    const container = tags.div().css({ maxWidth: "450px", margin: "auto" })



    const title = tags.h3().text("Modifica dati personali")
    //const codcliente = input.string.stringInput({ title: "Codice cliente" })
    const azienda = input.radio.radio({ title: "", name: "type", description: "Azienda", nullable: true })
    const privato = input.radio.radio({ title: "", name: "type", description: "Privato", nullable: true })
    const nome = input.string.stringInput({ title: "Nome", nullable: true })
    const cognome = input.string.stringInput({ title: "Cognome", nullable: true })
    const denominazione = input.string.stringInput({ title: "Denominazione sociale", nullable: true })
    const cf = input.string.stringInput({ title: "Codice fiscale" })
    const piva = input.string.stringInput({ title: "Partita IVA", nullable: true })
    const indirizzo = input.string.stringInput({ title: "Indirizzo (Via, Piazza ecc)", nullable: true })
    const provincia = input.combo.combo({ type: "combo", title: "Provincia" }, "nome", "id", province.province)
    const telefono = input.string.stringInput({ title: "Telefono", nullable: true })
    const email = input.string.stringInput({ title: "E-mail" })
    const nuovapassword = input.string.stringInput({ title: "Nuova password", nullable: true })
    const acconsentoObbl = input.boolean.booleanInput({ title: "Acconsento al trattamento dei dati personali e all'utilizzo dei cookie necessari per usufruire di questo sito e dei suoi servizi" })
    const errorDiv = tags.ul()



    container.append(
        title,
        privato.el,
        azienda.el,
        nome.el,
        cognome.el,
        denominazione.el,
        cf.el,
        piva.el,
        provincia.el,
        indirizzo.el,
        telefono.el,
        email.el,
        //codcliente.el,
        nuovapassword.el,
        acconsentoObbl.el,
        errorDiv
    )


    api.utente.getUtenti({ _id: utente.get()._id }).then(utenti => {
        privato.set(utenti[0].privato)
        azienda.set(utenti[0].azienda)
        nome.set(utenti[0].nome)
        cognome.set(utenti[0].cognome)
        denominazione.set(utenti[0].denominazione)
        cf.set(utenti[0].cf)
        piva.set(utenti[0].piva)
        provincia.set(utenti[0].provincia)
        indirizzo.set(utenti[0].indirizzo)
        telefono.set(utenti[0].telefono)
        email.set(utenti[0].email)
        acconsentoObbl.set(utenti[0].acconsente)
        //codcliente.set(utenti[0].codcliente)
        const submit = tags.isubmit().val("Invia").click(() => {
            errorDiv.empty()
            let isValid = true
            //condizioni
            // if (codcliente.model.read().state == "invalid") {
            //     errorDiv.append(tags.li().html("Devi inserire il codice cliente"))
            //     isValid = false
            // }
            if (email.model.read().state == "invalid") {
                errorDiv.append(tags.li().html("Devi inserire una mail"))
                isValid = false
            }
            if (denominazione.model.read().state == "invalid" && (nome.model.read().state == "invalid" || cognome.model.read().state == "invalid")) {
                errorDiv.append(tags.li().html("Devi inserire una denominazione sociale o un nome e cognome"))
                isValid = false
            }
            if (azienda.model.read().value && !denominazione.model.read().value) {
                errorDiv.append(tags.li().html("Se hai selezionato un cliente aziendale devi inserire una denominazione sociale"))
                isValid = false
            }
            if (privato.model.read().value && !(!!nome.model.read().value && !!cognome.model.read().value)) {
                errorDiv.append(tags.li().html("Se hai selezionato un cliente privato devi inserire un un nome e cognome"))
                isValid = false
            }
            if (isValid)
                api.utente.changeUtente({
                    _id: utenti[0]._id,
                    privato: privato.model.read().value,
                    azienda: azienda.model.read().value,
                    nome: nome.model.read().value,
                    cognome: cognome.model.read().value,
                    denominazione: denominazione.model.read().value,
                    cf: cf.model.read().value,
                    piva: piva.model.read().value,
                    provincia: provincia.model.read().value,
                    indirizzo: indirizzo.model.read().value,
                    telefono: telefono.model.read().value,
                    acconsente: acconsentoObbl.model.read().value,
                    email: email.model.read().value,
                    password: nuovapassword.model.read().value ? nuovapassword.model.read().value : undefined,
                    //codcliente: codcliente.model.read().value
                }).then(id => {
                    location.reload()
                })
        })

        container.append(submit)
    })



    template.main.append(upperSection, lower, container)


}



