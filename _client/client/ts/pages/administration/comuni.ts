import * as tags from "../../tags"
import * as templates from "../../template/_lib"
import * as shards from "../../shards/_lib"
import * as blocks from "../../blocks/_lib"
import * as input from "../../fields/input/_lib"
import * as api from "../../api/_lib"
import * as cookies from "../../cookies"


export function addComune() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()
    const title = tags.h3().text("Aggiungi comune")
    const sReg = tags.select()
    const sPro = tags.select()

    api.regioni.getRegioni().then(re => {
        for (const r of re)
            sReg.append(tags.option().text(r.name).attr({ value: r._id }))
        api.generic.getProvince(<string>sReg.val()).then(prov => {
            for (const p of prov)
                sPro.append(tags.option().text(p.name).attr({ value: p._id }))
        })
    })

    sReg.change(e => {
        sPro.empty()
        api.generic.getProvince(<string>sReg.val()).then(prov => {
            for (const p of prov)
                sPro.append(tags.option().text(p.name).attr({ value: p._id }))
            sPro.click()
        })
    })
    const nome = input.string.stringInput({ title: "Nome Comune" })

    const submit = tags.isubmit().val("Invia").click(() => {
        if (nome.model.read().state == "invalid")
            return

        console.log({ name: nome.model.read().value, regione: <string>sReg.val() })
        api.comuni.setComune({ name: nome.model.read().value, regione: <string>sReg.val(), provincia: <string>sPro.val() }).then(id => {
            nome.set("")
        })

    })

    template.main.append(title, sReg, sPro, nome.el, submit)
}




export function changeComune() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()
    const wrapper = tags.div()

    const sRegione = tags.select()
    api.regioni.getRegioni().then(re => {
        for (const r of re)
            sRegione.append(tags.option().text(r.name).attr({ value: r._id }))
        sRegione.click()
    })
    sRegione.change(e => {
        wrapper.empty()
        buildByRegione(<string>sRegione.val())
    })

    template.main.append(sRegione, wrapper)

    function buildByRegione(mRegione: string) {
        api.comuni.getComuni({ regione: mRegione }).then(comuni => {

            const t = tags.h3().text("Modifica Comune")
            const name = input.string.stringInput({ title: "Nome" })
            const sReg = tags.select()
            const sPro = tags.select()
            const whichBrochure = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli comune" }, "name", comuni)

            api.regioni.getRegioni().then(re => {
                for (const r of re)
                    sReg.append(tags.option().text(r.name).attr({ value: r._id }))
                sReg.click()
                api.generic.getProvince(<string>sReg.val()).then(province => {
                    for (const p of province)
                        sPro.append(tags.option().text(p.name).attr({ value: p._id }))
                })
            })

            sReg.change(e => {
                sPro.empty()
                api.generic.getProvince(<string>sReg.val()).then(province => {
                    for (const p of province)
                        sPro.append(tags.option().text(p.name).attr({ value: p._id }))
                })
            })

            let _id = ""

            const submit = tags.isubmit().val("Invia").click(() => {
                if (name.model.read().state == "invalid")
                    return
                api.generic.getProvincia(<string>sPro.val()).then(provincia => {

                    api.comuni.changeComune({
                        _id: _id,
                        name: name.model.read().value,
                        regione: provincia.regione,
                        provincia: provincia._id
                    }).then(id => {
                        changeComune()
                    })
                })

            })

            const cancella = tags.isubmit().val("Cancella").click(() => {
                api.comuni.deleteComuni(_id).then(removed => {
                    console.log(removed)
                    changeComune()
                })
            })

            whichBrochure.model.watch(res => {
                if (res.state != "valid")
                    return
                name.model.write({ state: "valid", value: res.value.name })
                _id = res.value._id
            })

            wrapper.empty().append(whichBrochure.el, sReg, sPro, name.el, submit, cancella)
        })
    }
}
