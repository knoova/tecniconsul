import * as tags from "../../tags"
import * as templates from "../../template/_lib"
import * as shards from "../../shards/_lib"
import * as blocks from "../../blocks/_lib"
import * as input from "../../fields/input/_lib"
import * as api from "../../api/_lib"
import * as cookies from "../../cookies"



export function addNewsPage() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()
    const titolo = tags.h3().text("Aggiungi news")
    const title = input.string.stringInput({ title: "Titolo news" })
    const subtitle = input.string.stringInput({ title: "Subtitle" })
    const body = input.string.stringInput({ title: "News", multiline: 20 })
    const banner = input.upload.upload({ title: "Banner" })
    const date = input.string.stringInput({ title: "Data" })
    const submit = tags.isubmit().val("Invia").click(() => {
        if (title.model.read().state == "invalid")
            return
        if (subtitle.model.read().state == "invalid")
            return
        if (body.model.read().state == "invalid")
            return
        // if (date.model.read().state == "invalid")
        //     return
        if (banner.model.read().state == "invalid")
            return
        api.news.setNews({
            title: title.model.read().value,
            subtitle: subtitle.model.read().value,
            body: body.model.read().value,
            banner: banner.model.read().value,
            date: date.model.read().value
        }).then(id => {
            addNewsPage()
        })

    })

    template.main.append(titolo, title.el, date.el, subtitle.el, body.el, banner.el, submit)


}


export function changeNews() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()

    api.news.getNews().then(news => {

        const titolo = tags.h3().text("Modifica pagina news")
        const title = input.string.stringInput({ title: "Titolo news" })
        const subtitle = input.string.stringInput({ title: "Subtitle" })
        const body = input.string.stringInput({ title: "News", multiline: 20 })
        const banner = input.upload.upload({ title: "Banner" })
        const date = input.string.stringInput({ title: "Data" })
        const chooseNews = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli brochure" }, "title", news)

        let _id = ""

        const submit = tags.isubmit().val("Invia").click(() => {
            if (title.model.read().state == "invalid")
                return
            api.news.changeNews({
                _id,
                title: title.model.read().value,
                subtitle: subtitle.model.read().value,
                body: body.model.read().value,
                banner: banner.model.read().value,
                date: date.model.read().value
            }).then(id => {
                changeNews()
            })

        })

        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.news.deleteNews(_id).then(removed => {
                console.log(removed)
                changeNews()
            })
        })

        chooseNews.model.watch(res => {
            if (res.state != "valid")
                return
            title.model.write({ state: "valid", value: res.value.title })
            subtitle.model.write({ state: "valid", value: res.value.subtitle })
            body.model.write({ state: "valid", value: res.value.body })
            banner.model.write({ state: "valid", value: res.value.banner })
            date.model.write({ state: "valid", value: res.value.date })
            _id = res.value._id
        })

        template.main.append(titolo, chooseNews.el, title.el, date.el, subtitle.el, body.el, banner.el, submit, cancella)
    })

}
