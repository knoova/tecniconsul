import * as tags from "../../tags"
import * as templates from "../../template/_lib"
import * as shards from "../../shards/_lib"
import * as blocks from "../../blocks/_lib"
import * as input from "../../fields/input/_lib"
import * as api from "../../api/_lib"
import * as cookies from "../../cookies"



export function addRegione() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()
    const title = tags.h3().text("Aggiungi regione")
    const nome = input.string.stringInput({ title: "Nome regione" })
    const submit = tags.isubmit().val("Invia").click(() => {
        if (nome.model.read().state == "invalid")
            return
        api.regioni.setRegione({name: nome.model.read().value}).then(id => {
            nome.set("")
        })

    })

    template.main.append(title, nome.el, submit)


}


export function changeRegione() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()

    api.regioni.getRegioni().then(regioni => {

        const t = tags.h3().text("Aggiungi slide")
        const name = input.string.stringInput({ title: "Nome" })
        const whichBrochure = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli regione" }, "name", regioni)

        let _id = ""

        const submit = tags.isubmit().val("Invia").click(() => {
            if (name.model.read().state == "invalid")
                return
            api.regioni.changeRegione({
                id: _id,
                name: name.model.read().value
            }).then(id => {
                changeRegione()
            })

        })

        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.regioni.deleteRegione(_id).then(removed => {
                console.log(removed)
                changeRegione()
            })
        })

        whichBrochure.model.watch(res => {
            if (res.state != "valid")
                return
            name.model.write({ state: "valid", value: res.value.name })
            _id = res.value._id
        })

        template.main.append(whichBrochure.el, name.el, submit, cancella)
    })

}
