import * as tags from "../../tags"
import * as templates from "../../template/_lib"
import * as shards from "../../shards/_lib"
import * as blocks from "../../blocks/_lib"
import * as input from "../../fields/input/_lib"
import * as api from "../../api/_lib"
import * as cookies from "../../cookies"
import { province } from "../../configurations/province"



export function addUsers() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()
    const title = tags.h3().text("Aggiungi utente")
    const codcliente = input.string.stringInput({ title: "Codice cliente" })
    const azienda = input.radio.radio({ title: "", name: "type", description: "Azienda", nullable: true })
    const privato = input.radio.radio({ title: "", name: "type", description: "Privato", nullable: true })
    const nome = input.string.stringInput({ title: "Nome", nullable: true })
    const cognome = input.string.stringInput({ title: "Cognome", nullable: true })
    const denominazione = input.string.stringInput({ title: "Denominazione sociale", nullable: true })
    const cf = input.string.stringInput({ title: "Codice fiscale" })
    const piva = input.string.stringInput({ title: "Partita IVA", nullable: true })
    const indirizzo = input.string.stringInput({ title: "Indirizzo (Via, Piazza ecc)", nullable: true })
    const provincia = input.combo.combo({ type: "combo", title: "Provincia" }, "nome", "id", province.province)
    const telefono = input.string.stringInput({ title: "Telefono", nullable: true })
    const email = input.string.stringInput({ title: "E-mail" })
    const password = input.string.stringInput({ title: "Password" })
    const errorDiv = tags.ul()


    const submit = tags.isubmit().val("Invia").click(() => {
        errorDiv.empty()
        let isValid = true
        //condizioni
        if (codcliente.model.read().state == "invalid") {
            errorDiv.append(tags.li().html("Devi inserire il codice cliente"))
            isValid = false
        }
        if (email.model.read().state == "invalid") {
            errorDiv.append(tags.li().html("Devi inserire una mail"))
            isValid = false
        }
        if (password.model.read().state == "invalid") {
            errorDiv.append(tags.li().html("Devi inserire una password"))
            isValid = false
        }
        if (denominazione.model.read().state == "invalid" && (nome.model.read().state == "invalid" || cognome.model.read().state == "invalid")) {
            errorDiv.append(tags.li().html("Devi inserire una denominazione sociale o un nome e cognome"))
            isValid = false
        }
        if (azienda.model.read().value && !denominazione.model.read().value) {
            errorDiv.append(tags.li().html("Se hai selezionato un cliente aziendale devi inserire una denominazione sociale"))
            isValid = false
        }
        if (privato.model.read().value && !(!!nome.model.read().value && !!cognome.model.read().value)) {
            errorDiv.append(tags.li().html("Se hai selezionato un cliente privato devi inserire un un nome e cognome"))
            isValid = false
        }
        if (isValid)
            api.utente.setUtente({
                privato: privato.model.read().value,
                azienda: azienda.model.read().value,
                nome: nome.model.read().value,
                cognome: cognome.model.read().value,
                denominazione: denominazione.model.read().value,
                cf: cf.model.read().value,
                piva: piva.model.read().value,
                provincia: provincia.model.read().value,
                indirizzo: indirizzo.model.read().value,
                telefono: telefono.model.read().value,
                email: email.model.read().value,
                password: password.model.read().value,
                codcliente: codcliente.model.read().value
            }).then(id => {
                location.reload()
            })

    })

    template.main.append(title,
        privato.el,
        azienda.el,
        nome.el,
        cognome.el,
        denominazione.el,
        cf.el,
        piva.el,
        provincia.el,
        indirizzo.el,
        telefono.el,
        email.el,
        password.el,
        codcliente.el,
        errorDiv,
        submit)


}


export function changeUser() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()

    api.utente.getUtenti().then(utenti => {

        const title = tags.h3().text("Modifica utente")
        const scegliutente = input.radioMulti.radioMulti({ name: "qutente", title: "Scegli utente" }, "codcliente", utenti)
        const codcliente = input.string.stringInput({ title: "Codice cliente" })
        const azienda = input.radio.radio({ title: "", name: "type", description: "Azienda", nullable: true })
        const privato = input.radio.radio({ title: "", name: "type", description: "Privato", nullable: true })
        const nome = input.string.stringInput({ title: "Nome", nullable: true })
        const cognome = input.string.stringInput({ title: "Cognome", nullable: true })
        const denominazione = input.string.stringInput({ title: "Denominazione sociale", nullable: true })
        const cf = input.string.stringInput({ title: "Codice fiscale" })
        const piva = input.string.stringInput({ title: "Partita IVA", nullable: true })
        const indirizzo = input.string.stringInput({ title: "Indirizzo (Via, Piazza ecc)", nullable: true })
        const provincia = input.combo.combo({ type: "combo", title: "Provincia" }, "nome", "id", province.province)
        const telefono = input.string.stringInput({ title: "Telefono", nullable: true })
        const email = input.string.stringInput({ title: "E-mail" })
        const nuovapassword = input.string.stringInput({ title: "Nuova password", nullable: true })
        const errorDiv = tags.ul()

        let _id = ""

        const submit = tags.isubmit().val("Invia").click(() => {
            errorDiv.empty()
            let isValid = true
            //condizioni
            if (codcliente.model.read().state == "invalid") {
                errorDiv.append(tags.li().html("Devi inserire il codice cliente"))
                isValid = false
            }
            if (email.model.read().state == "invalid") {
                errorDiv.append(tags.li().html("Devi inserire una mail"))
                isValid = false
            }
            if (denominazione.model.read().state == "invalid" && (nome.model.read().state == "invalid" || cognome.model.read().state == "invalid")) {
                errorDiv.append(tags.li().html("Devi inserire una denominazione sociale o un nome e cognome"))
                isValid = false
            }
            if (azienda.model.read().value && !denominazione.model.read().value) {
                errorDiv.append(tags.li().html("Se hai selezionato un cliente aziendale devi inserire una denominazione sociale"))
                isValid = false
            }
            if (privato.model.read().value && !(!!nome.model.read().value && !!cognome.model.read().value)) {
                errorDiv.append(tags.li().html("Se hai selezionato un cliente privato devi inserire un un nome e cognome"))
                isValid = false
            }
            if (isValid)
                api.utente.changeUtente({
                    _id,
                    privato: privato.model.read().value,
                    azienda: azienda.model.read().value,
                    nome: nome.model.read().value,
                    cognome: cognome.model.read().value,
                    denominazione: denominazione.model.read().value,
                    cf: cf.model.read().value,
                    piva: piva.model.read().value,
                    provincia: provincia.model.read().value,
                    indirizzo: indirizzo.model.read().value,
                    telefono: telefono.model.read().value,
                    email: email.model.read().value,
                    password: nuovapassword.model.read().value ? nuovapassword.model.read().value : undefined,
                    codcliente: codcliente.model.read().value
                }).then(id => {
                    location.reload()
                })
        })

        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.utente.deleteUtente(_id).then(removed => {
                console.log(removed)
                changeUser()
            })
        })

        scegliutente.model.watch(res => {
            if (res.state != "valid")
                return
            privato.set(res.value.privato),
                azienda.set(res.value.azienda),
                nome.set(res.value.nome),
                cognome.set(res.value.cognome),
                denominazione.set(res.value.denominazione),
                cf.set(res.value.cf),
                piva.set(res.value.piva),
                provincia.set(res.value.provincia),
                indirizzo.set(res.value.indirizzo),
                telefono.set(res.value.telefono),
                email.set(res.value.email),
                codcliente.set(res.value.codcliente),
                _id = res.value._id
        })

        template.main.append(
            title,
            scegliutente.el,
            tags.hr(),
            privato.el,
            azienda.el,
            nome.el,
            cognome.el,
            denominazione.el,
            cf.el,
            piva.el,
            provincia.el,
            indirizzo.el,
            telefono.el,
            email.el,
            codcliente.el,
            nuovapassword.el,
            errorDiv,
            submit,
            cancella
        )
    })

}

