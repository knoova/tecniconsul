import * as tags from "../../tags"
import * as templates from "../../template/_lib"
import * as shards from "../../shards/_lib"
import * as blocks from "../../blocks/_lib"
import * as input from "../../fields/input/_lib"
import * as api from "../../api/_lib"
import * as cookies from "../../cookies"


export function addListino() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()
    const title = tags.h3().text("Aggiungi listino per comune")
    const container = tags.div()
    const upper = tags.div()
    api.comuni.getCabine({}).then(re => {
        const sReg = input.combo.combo({ title: "Cabina" }, "name", "_id", re)
        upper.append(sReg.el)

        if (re && re.length) {
            const uploadListino1 = input.upload.uploadXLS({ title: "Carica tabella in formato XLS" }, re[0]._id)
            container.empty().append(uploadListino1.el)
        }

        sReg.model.watch(v => {

            const uploadListino1 = input.upload.uploadXLS({ title: "Carica tabella in formato XLS" }, v.value)
            container.empty().append(uploadListino1.el)
        })
    })

    // const submit = tags.isubmit().val("Invia").click(() => {
    //     if (uploadListino1.model.read().state == "invalid")
    //         return

    //     console.log({ name: uploadListino1.model.read().value, regione: <string>sReg.val() })
    //     // api.generic.setProvincia({ name: uploadListino1.model.read().value, regione: <string>sReg.val() }).then(id => {
    //     //     uploadListino1.set("")
    //     // })

    // })

    template.main.append(title, upper, container)
}




export function changeListino() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()

    api.generic.getProvince().then(regioni => {

        const t = tags.h3().text("Aggiungi slide")
        const name = input.string.stringInput({ title: "Nome" })
        const whichBrochure = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli brochure" }, "name", regioni)

        let _id = ""

        const submit = tags.isubmit().val("Invia").click(() => {
            if (name.model.read().state == "invalid")
                return
            api.generic.changeProvince({
                _id: _id,
                name: name.model.read().value
            }).then(id => {
                changeListino()
            })

        })

        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.generic.deleteProvince(_id).then(removed => {
                console.log(removed)
                changeListino()
            })
        })

        whichBrochure.model.watch(res => {
            if (res.state != "valid")
                return
            name.model.write({ state: "valid", value: res.value.name })
            _id = res.value._id
        })

        template.main.append(whichBrochure.el, name.el, submit, cancella)
    })

}
