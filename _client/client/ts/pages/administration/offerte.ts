import * as tags from "../../tags"
import * as templates from "../../template/_lib"
import * as shards from "../../shards/_lib"
import * as blocks from "../../blocks/_lib"
import * as input from "../../fields/input/_lib"
import * as api from "../../api/_lib"
import * as cookies from "../../cookies"



export async function addAndChangeOfferte() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()


    console.log(location.pathname, location.pathname.split("/"))

    const myPathArray = location.pathname.split("/")

    const casaAziendaPlacet = <"casa" | "azienda" | "placet">myPathArray[myPathArray.length - 2]
    const luceGas = <"luce" | "gas">myPathArray[myPathArray.length - 1]


    const titleC1 = input.string.stringInput({ title: "Titolo offerta 1" })
    const introC1 = input.string.stringInput({ title: "Intro offerta 1" })
    const descriptionC1 = input.string.stringInput({ title: "Descrizione offerta 1", multiline: 20 })

    const titleC2 = input.string.stringInput({ title: "Titolo offerta 2" })
    const introC2 = input.string.stringInput({ title: "Intro offerta 2" })
    const descriptionC2 = input.string.stringInput({ title: "Descrizione offerta 2", multiline: 20 })

    const titleC3 = input.string.stringInput({ title: "Titolo offerta 3" })
    const introC3 = input.string.stringInput({ title: "Intro offerta 3" })
    const descriptionC3 = input.string.stringInput({ title: "Descrizione offerta 3", multiline: 20 })

    const titleC4 = input.string.stringInput({ title: "Titolo offerta 4" })
    const introC4 = input.string.stringInput({ title: "Intro offerta 4" })
    const descriptionC4 = input.string.stringInput({ title: "Descrizione offerta 4", multiline: 20 })



    const submit = tags.isubmit().val("Invia").click(async () => {



        const r1 = await api.offerte.setOfferta({
            casaAziendaPlacet,
            luceGas,
            o1: {
                title: titleC1.model.read().value,
                description: descriptionC1.model.read().value,
                intro: introC1.model.read().value
            },
            o2: {
                title: titleC2.model.read().value,
                description: descriptionC2.model.read().value,
                intro: introC2.model.read().value
            },
            o3: {
                title: titleC3.model.read().value,
                description: descriptionC3.model.read().value,
                intro: introC3.model.read().value
            },
            o4: {
                title: titleC4.model.read().value,
                description: descriptionC4.model.read().value,
                intro: introC4.model.read().value
            }
        })

        if (r1) {
            location.reload()
        }


    })

    template.main.append(
        tags.h2().text("Aggiungi o modifica le offerte " + luceGas),
        tags.h3().text("TARIFFA " + casaAziendaPlacet.toUpperCase()),
        titleC1.el,
        introC1.el,
        descriptionC1.el,
        tags.hr(),
        titleC2.el,
        introC2.el,
        descriptionC2.el,
        tags.hr(),
        titleC3.el,
        introC3.el,
        descriptionC3.el,
        tags.hr(),
        titleC4.el,
        introC4.el,
        descriptionC4.el,
        submit
    )


    const fill = await api.offerte.getOfferta({
        casaAziendaPlacet,
        luceGas
    })

    if (fill.o1) {
        titleC1.set(fill.o1.title)
        introC1.set(fill.o1.intro)
        descriptionC1.set(fill.o1.description)
    }

    if (fill.o2) {
        titleC2.set(fill.o2.title)
        introC2.set(fill.o2.intro)
        descriptionC2.set(fill.o2.description)
    }

    if (fill.o3) {
        titleC3.set(fill.o3.title)
        introC3.set(fill.o3.intro)
        descriptionC3.set(fill.o3.description)
    }

    if (fill.o4) {
        titleC4.set(fill.o4.title)
        introC4.set(fill.o4.intro)
        descriptionC4.set(fill.o4.description)
    }


}




export async function offerteChangeAll() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()
    template.main.append(
        tags.h2().text("Aggiungi o modifica le offerte "),
        tags.hr(),
        tags.h3().text("Casa"),
        shards.anchor.full({ text: "Gas", url: "/amministrazione/addOfferte/casa/gas" }).addClass("yca-menu-buttons"),
        shards.anchor.full({ text: "Luce", url: "/amministrazione/addOfferte/casa/luce" }).addClass("yca-menu-buttons"),
        tags.hr(),
        tags.h3().text("Azienda"),
        shards.anchor.full({ text: "Gas", url: "/amministrazione/addOfferte/azienda/gas" }).addClass("yca-menu-buttons"),
        shards.anchor.full({ text: "Luce", url: "/amministrazione/addOfferte/azienda/luce" }).addClass("yca-menu-buttons"),
        tags.hr(),
        tags.h3().text("Placet"),
        shards.anchor.full({ text: "Gas", url: "/amministrazione/addOfferte/placet/gas" }).addClass("yca-menu-buttons"),
        shards.anchor.full({ text: "Luce", url: "/amministrazione/addOfferte/placet/luce" }).addClass("yca-menu-buttons")
    )
}