import * as tags from "../../tags"
import * as templates from "../../template/_lib"
import * as shards from "../../shards/_lib"
import * as blocks from "../../blocks/_lib"
import * as input from "../../fields/input/_lib"
import * as api from "../../api/_lib"
import * as cookies from "../../cookies"



export function addLandingPage() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()
    const titolo = tags.h3().text("Aggiungi Landing page")
    const slug = input.string.stringInput({ title: "Slug" })
    const body = input.string.stringInput({ title: "HTML pagina", multiline: 20 })
    const submit = tags.isubmit().val("Invia").click(() => {
        if (slug.model.read().state == "invalid")
            return
        if (body.model.read().state == "invalid")
            return
        api.landing.setLanding({
            slug: slug.model.read().value,
            body: body.model.read().value,
        }).then(id => {
            slug.set("")
            body.set("")
        })

    })

    template.main.append(titolo, slug.el, body.el, submit)


}


export function changeLanding() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()

    api.landing.getAllLanding().then(landing => {

        const t = tags.h3().text("Modifica pagina landing")
        const slug = input.string.stringInput({ title: "Slug" })
        const body = input.string.stringInput({ title: "Pagina", multiline: 20 })
        const chooseLanding = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli landing page" }, "slug", landing)

        let _id = ""

        const submit = tags.isubmit().val("Invia").click(() => {
            if (slug.model.read().state == "invalid")
                return
            if (body.model.read().state == "invalid")
                return
            api.landing.changeLanding({
                _id,
                slug: slug.model.read().value,
                body: body.model.read().value,
            }).then(id => {
                changeLanding()
            })

        })

        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.landing.deleteLanding(_id).then(removed => {
                console.log(removed)
                changeLanding()
            })
        })

        chooseLanding.model.watch(res => {
            if (res.state != "valid")
                return
            slug.model.write({ state: "valid", value: res.value.slug })
            body.model.write({ state: "valid", value: res.value.body })
            _id = res.value._id
        })

        template.main.append(chooseLanding.el, slug.el, body.el, submit, cancella)
    })

}
