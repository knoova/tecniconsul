import * as tags from "../../tags"
import * as templates from "../../template/_lib"
import * as shards from "../../shards/_lib"
import * as blocks from "../../blocks/_lib"
import * as input from "../../fields/input/_lib"
import * as api from "../../api/_lib"
import * as cookies from "../../cookies"



export function addFaq() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }

    const categorie: { name: string, _id: string }[] = [
        { name: "Utilità per te", _id: "utility" },
        { name: "Supporto per te", _id: "support" },
        { name: "Bollette per te", _id: "invoice" },
        { name: "Pagamenti per te", _id: "payments" },
        { name: "Alro per te", _id: "other" }
    ]
    const selectCategories = tags.select()

    for (const categoria of categorie)
        selectCategories.append(tags.option().text(categoria.name).attr({ value: categoria._id }))

    const template = templates.backend.build()
    const title = tags.h3().text("Aggiungi faq")
    const titolo = input.string.stringInput({ title: "Titolo" })
    const sottotitolo = input.string.stringInput({ title: "Sottotitolo", multiline: 5 })
    const descrizione = input.string.stringInput({ title: "Descrizione estesa", multiline: 15 })
    const logo = input.upload.upload({ title: "Logo" })
    const part = input.string.stringInput({ title: "Link (ID)" })
    const submit = shards.anchor.full({
        text: "Invia",
        action: () => {
            if (titolo.model.read().state == "invalid") {
                template.alert.append(tags.h1().html("Devi inserire un titolo"))
                return
            }
            if (sottotitolo.model.read().state == "invalid") {
                template.alert.append(tags.h1().html("Devi inserire un sottotitolo"))
                return
            }
            if (descrizione.model.read().state == "invalid") {
                template.alert.append(tags.h1().html("Devi inserire un descripion"))
                return
            }
            if (logo.model.read().state == "invalid") {
                template.alert.append(tags.h1().html("Devi inserire un logo"))
                return
            }
            if (part.model.read().state == "invalid") {
                template.alert.append(tags.h1().html("Devi inserire un ID"))
                return
            }
            api.faq.setFAQ({
                title: titolo.model.read().value,
                subtitle: sottotitolo.model.read().value,
                description: descrizione.model.read().value,
                logo: logo.model.read().value,
                part: part.model.read().value,
                category: <string>selectCategories.val()
            }).then(id => {
                addFaq()
            })

        }
    })

    template.main.append(title, selectCategories, titolo.el, sottotitolo.el, descrizione.el, logo.el, part.el, submit)


}

export function changeFaq() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()

    api.faq.getFAQ({}).then(faq => {

        const title = tags.h3().text("Modifica faq")
        const titolo = input.string.stringInput({ title: "Titolo" })
        const sottotitolo = input.string.stringInput({ title: "Sottotitolo", multiline: 5 })
        const descrizione = input.string.stringInput({ title: "Descrizione estesa", multiline: 10 })
        const part = input.string.stringInput({ title: "Link (ID)" })
        const logo = input.upload.upload({ title: "Logo" })


        const categorie: { name: string, _id: string }[] = [
            { name: "Utilità per te", _id: "utility" },
            { name: "Supporto per te", _id: "support" },
            { name: "Bollette per te", _id: "invoice" },
            { name: "Pagamenti per te", _id: "payments" },
            { name: "Alro per te", _id: "other" }
        ]

        const selectCategories = tags.select()

        for (const categoria of categorie)
            selectCategories.append(tags.option().text(categoria.name).attr({ value: categoria._id }))


        const whichFAQ = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli faq" }, "title", faq)
        let _id = ""

        const submit = tags.isubmit().val("Invia").click(() => {
            if (titolo.model.read().state == "invalid" || descrizione.model.read().state == "invalid" || logo.model.read().state == "invalid" || sottotitolo.model.read().state == "invalid" || part.model.read().state == "invalid")
                return
            api.faq.changeFAQ({
                _id,
                title: titolo.model.read().value,
                subtitle: sottotitolo.model.read().value,
                description: descrizione.model.read().value,
                part: part.model.read().value,
                logo: logo.model.read().value,
                category: <string>selectCategories.val()
            }).then(id => {
                changeFaq()
            })

        })

        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.faq.deleteFAQ(_id).then(removed => {
                console.log(removed)
                changeFaq()
            })
        })

        whichFAQ.model.watch(res => {
            if (res.state != "valid")
                return
            titolo.model.write({ state: "valid", value: res.value.title })
            descrizione.model.write({ state: "valid", value: res.value.description })
            sottotitolo.model.write({ state: "valid", value: res.value.subtitle })
            logo.model.write({ state: "valid", value: res.value.logo })
            part.model.write({ state: "valid", value: res.value.part })
            selectCategories.val(res.value.category)
            _id = res.value._id
        })

        template.main.append(whichFAQ.el, selectCategories, titolo.el, sottotitolo.el, descrizione.el, logo.el, part.el, submit, cancella)
    })

}
