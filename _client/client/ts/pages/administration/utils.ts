import * as tags from "../../tags"
import * as templates from "../../template/_lib"
import * as shards from "../../shards/_lib"
import * as blocks from "../../blocks/_lib"
import * as input from "../../fields/input/_lib"
import * as api from "../../api/_lib"
import * as cookies from "../../cookies"



export function addUtilsPage() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()
    const titolo = tags.h3().text("Aggiungi pagina utils")
    const title = input.string.stringInput({ title: "Titolo pagina" })
    const slug = input.string.stringInput({ title: "Slug" })
    const body = input.string.stringInput({ title: "Pagina", multiline: 20 })
    const submit = tags.isubmit().val("Invia").click(() => {
        if (title.model.read().state == "invalid")
            return
        if (slug.model.read().state == "invalid")
            return
        if (body.model.read().state == "invalid")
            return
        api.utils.setUtils({
            title: title.model.read().value,
            slug: slug.model.read().value,
            body: body.model.read().value,
        }).then(id => {
            title.set("")
            slug.set("")
            body.set("")
        })

    })

    template.main.append(titolo, title.el, slug.el, body.el, submit)


}


export function changeUtils() {
    const loggedIn = cookies.getCookie("user")
    if (!loggedIn) {
        location.href = "/login"
    }
    const template = templates.backend.build()

    api.utils.getUtils().then(utils => {

        const t = tags.h3().text("Modifica pagina utils")
        const title = input.string.stringInput({ title: "Titolo" })
        const slug = input.string.stringInput({ title: "Slug" })
        const body = input.string.stringInput({ title: "Pagina", multiline: 20 })
        const chooseUtils = input.radioMulti.radioMulti({ name: "whichbrochure", title: "Scegli brochure" }, "title", utils)

        let _id = ""

        const submit = tags.isubmit().val("Invia").click(() => {
            if (title.model.read().state == "invalid")
                return
            if (slug.model.read().state == "invalid")
                return
            if (body.model.read().state == "invalid")
                return
            api.utils.changeUtils({
                _id,
                title: title.model.read().value,
                slug: slug.model.read().value,
                body: body.model.read().value,
            }).then(id => {
                changeUtils()
            })

        })

        const cancella = tags.isubmit().val("Cancella").click(() => {
            api.utils.deleteUtils(_id).then(removed => {
                console.log(removed)
                changeUtils()
            })
        })

        chooseUtils.model.watch(res => {
            if (res.state != "valid")
                return
            title.model.write({ state: "valid", value: res.value.title })
            slug.model.write({ state: "valid", value: res.value.slug })
            body.model.write({ state: "valid", value: res.value.body })
            _id = res.value._id
        })

        template.main.append(chooseUtils.el, title.el, slug.el, body.el, submit, cancella)
    })

}
