import * as tags from "../tags"
import * as templates from "../template/_lib"
import * as shards from "../shards/_lib"
import * as blocks from "../blocks/_lib"
import * as input from "../fields/input/_lib"
import * as api from "../api/_lib"

export function build(args?: { id: string }) {
    console.log("got param:", args)
    const template = templates.main.main()
    const upperSection = tags.div().addClass("yca-wrapper")

    const title = tags.h1().html("BLOG NEWS").addClass("yca-blognews-title")

    upperSection.append(title)


    const lowerSection = tags.div().addClass("yca-wrapper")

    api.news.getNews().then(news => {
        for (const n of news) {
            lowerSection.append(shards.news.news({ title: n.title, subtitle: n.subtitle, image: n.banner, date: n.date, body: n.body, id: n._id }))
        }

        template.main.append(upperSection, lowerSection)
        
        if (args) {
            location.hash = "#" + args.id
        }
    })



}