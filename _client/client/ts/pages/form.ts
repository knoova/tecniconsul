import * as tags from "../tags"
import * as input from "../fields/input/_lib"
import * as template from "../template/_lib"
import * as api from "../api/_lib"
import * as pages from "./_lib"
import * as shards from "../shards/_lib"



export function build() {
    const mTemplate = template.main.main()
    mTemplate.main.append(
        formVieniConNoi(),
        tags.hr(),
        formContatti(),
        tags.hr(),
        formComunicazioniPersonali(),
        tags.hr(),
        // fatturaElettronica(),
        // tags.hr()
    )
}

export function formVieniConNoi(): JQuery {
    const vieniConNoi = tags.div().addClass("yca-wrapper")
    const form1 = tags.div().addClass("yca-autoflex")
    const form2 = tags.div().addClass("yca-autoflex")

    const azienda = input.radio.radio({ title: "Azienda", name: "aziendaoprivato", nullable: true })
    const privato = input.radio.radio({ title: "Privato", name: "aziendaoprivato", nullable: true })
    const nome = input.string.stringInput({ title: "Nome *" })
    const cognome = input.string.stringInput({ title: "Cognome *" })
    const age = input.string.stringInput({ title: "Età", nullable: true })
    const professione = input.string.stringInput({ title: "Professione", nullable: true })
    const orario = input.string.stringInput({ title: "Orario in cui vuoi essere chiamato", nullable: true })
    const comune = input.string.stringInput({ title: "Comune di residenza *" })
    const telefono = input.string.stringInput({ title: "Numero di telefono *" })
    const acconsentoObbl = input.boolean.booleanInput({ title: "Acconsento al trattamento dei dati personali per essere ricontattato in merito alle offerte *" })
    const acconsentoNonObbl = input.boolean.booleanInput({ title: "Acconsento al trattamento dei dati personali per finalità di marketing" })
    const errorPanel = tags.ul().addClass("yca-error-panel")
    const submit = tags.isubmit().val("Invia").click(() => {
        let valid = true
        errorPanel.empty()
        if (nome.model.read().state == "invalid" && cognome.model.read().state == "invalid" || comune.model.read().state == "invalid" || telefono.model.read().state == "invalid") {
            errorPanel.append(tags.li().html("Errore: i campi obbligatori non sono compilati"))
            valid = false;
        }
        if (acconsentoObbl.model.read().value != true) {
            errorPanel.append(tags.li().html("Errore: devi accettare il trattamento dei dati personali per esere ricontattato"))
            valid = false;
        }
        if (valid)
            api.generic.setContatto({
                azienda: azienda.model.read().value,
                privato: privato.model.read().value,
                nome: nome.model.read().value,
                cognome: cognome.model.read().value,
                age: age.model.read().value,
                professione: professione.model.read().value,
                orario: orario.model.read().value,
                comune: comune.model.read().value,
                telefono: telefono.model.read().value,
                acconsento: acconsentoObbl.model.read().value,
                acconsentoNonObbl: acconsentoNonObbl.model.read().value,
                tipo: "vieniconnoi-territori"
            }).then(r => {
                api.mail.sendMail({
                    obj: "Form vieni con noi in pagina territori", body: JSON.stringify({
                        nome: nome.model.read().value,
                        cognome: cognome.model.read().value,
                        acconsento: acconsentoObbl.model.read().value,
                        email: telefono.model.read().value,
                        tipo: "paginacontatti",
                        oggetto: r
                    })
                }).then(a => {
                    location.href = "/thankyou"
                })
            }).catch(e => { errorPanel.html(e) })

    })

    form1.append(
        azienda.el,
        nome.el,
        cognome.el,
        age.el,
        professione.el,
        orario.el
    )

    form2.append(
        privato.el,
        comune.el,
        telefono.el,
        acconsentoObbl.el,
        acconsentoNonObbl.el,
        errorPanel,
        submit
    )

    const formWrapper = tags.div().css({ display: "flex", padding: "0" }).append(form1, form2)
    return vieniConNoi.append(formWrapper)
}

export function formContatti(): JQuery {

    const nome = input.string.stringInput({ title: "Nome" })
    const cognome = input.string.stringInput({ title: "Cognome" })
    const email = input.string.stringInput({ title: "Email" })
    const acconsento = input.boolean.booleanInput({ title: "Acconsento al trattamento dei dati personali *" })
    const submit = tags.isubmit().val("Invia").click(() => {
        api.generic.setContatto({
            nome: nome.model.read().value,
            cognome: cognome.model.read().value,
            acconsento: acconsento.model.read().value,
            email: email.model.read().value,
            tipo: "form contatti"
        }).then(r => {
            api.mail.sendMail({
                obj: "Form contatti rapidi", body: JSON.stringify({
                    nome: nome.model.read().value,
                    cognome: cognome.model.read().value,
                    acconsento: acconsento.model.read().value,
                    email: email.model.read().value,
                    tipo: "paginacontatti"
                })
            }).then(a => {
                location.href = "/thankyou"
            })
        })
    })
    const form = tags.div().addClass("yca-autoflex")

    return form.append(
        nome.el,
        cognome.el,
        email.el,
        acconsento.el,
        submit
    )
}

export function formComunicazioniPersonali(rightPanel?: JQuery): JQuery {
    const wrapTop = tags.div().css({ display: "flex" }).css({ width: "100%" })
    const leftTop = tags.div().css({ flex: "auto" })
    const rightTop = tags.div().css({ flex: "auto" })
    const azienda = input.radio.radio({ title: "", name: "type", description: "Azienda" })
    const privato = input.radio.radio({ title: "", name: "type", description: "Privato" })

    const wrapBottom = tags.div().css({ display: "flex" })
    const leftBottom = tags.div().css({ flex: "auto", padding: "0 175px" })
    const rightBottom = tags.div().css({ flex: "auto", padding: "0 175px" })

    wrapTop.append(leftTop.append(azienda.el), rightTop.append(privato.el))


    const errorPanel = tags.div().addClass("yca-error-panel")


    const wrapCenter = tags.div().css({ display: "flex" })
    const leftCenter = tags.div()
    const rightCenter = tags.div().css({ flex: "auto" })

    const nome = input.string.stringInput({ title: "Nome" })
    const cognome = input.string.stringInput({ title: "Cognome" })
    const email = input.string.stringInput({ title: "Email" })
    const acconsento = input.boolean.booleanInput({ title: "Acconsento al trattamento dei dati personali *" })
    wrapCenter.append(leftCenter.append(wrapTop, nome.el, cognome.el, email.el, acconsento.el))


    const commenti = input.string.stringInput({ title: "Lascia qui i tuoi commenti *", multiline: 10 })
    const submit = tags.isubmit().val("Invia").click(() => {
        if (acconsento.model.read().state == "invalid" || nome.model.read().state == "invalid" || cognome.model.read().state == "invalid")
            errorPanel.html("Errore: i campi obbligatori non sono compilati")
        else
            api.generic.setContatto({
                azienda: azienda.model.read().value,
                privato: privato.model.read().value,
                nome: nome.model.read().value,
                cognome: cognome.model.read().value,
                acconsento: acconsento.model.read().value,
                commenti: commenti.model.read().value,
                tipo: "paginacontatti"
            }).then(r => {
                api.mail.sendMail({
                    obj: "Pagina contatti", body: JSON.stringify({
                        azienda: azienda.model.read().value,
                        privato: privato.model.read().value,
                        nome: nome.model.read().value,
                        cognome: cognome.model.read().value,
                        acconsento: acconsento.model.read().value,
                        commenti: commenti.model.read().value,
                        tipo: "paginacontatti"
                    })
                }).then(a => {
                    location.href = "/thankyou"
                })
            })
    })

    const file = input.upload.upload({ title: "Carica un file" })
    const form = tags.div().addClass("yca-autoflex").addClass("yca-autoflex-columnifmobile")

    wrapBottom.append(leftBottom.append(submit), rightBottom.append(file.el))

    if (rightPanel) {
        wrapCenter.append(rightCenter.append(rightPanel))
    }

    form.append(
        wrapCenter,
        commenti.el,
        wrapBottom
    )

    commenti.set("")

    return form;
}


export function fatturaElettronica(user: api.utente.Utente): JQuery {
    const title = tags.h1().text("Fattura Elettronica").css({ textTransform: "none", color: "#3260ab" })
    const text = tags.p().html("Se vuoi attivare il servizio di invio della fattura in modalità elettronica a costo zero (il documento cartaceo non sarà più inviato) compila i campi sottostanti inserendo oltre alla tua email anche i tuoi dati personali quali: Nome Cognome e Codice Contratto che puoi trovare nella prima pagina della bolletta e poi clicca su “Invia”.")
        .css({
            display: "block",
            fontSize: "24px",
            margin: "3em 0em"
        })
    const elettogas = input.combo.combo({ title: "Elettricità o gas" }, "name", "name", [{ name: "Elettricità" }, { name: "Gas" }])
    elettogas.set("Elettricità")
    const intestatario = input.string.stringInput({ title: "Intestatario fornitura (richiesto)" })
    const mail = input.string.stringInput({ title: "La tua email (richiesto)" })
    const codice = input.string.stringInput({ title: "Codice Contratto (richiesto)" })
    const nota = input.string.stringInput({ title: "Nota facoltativa" })
    const submit = tags.isubmit().val("Invia").click(() => {
        api.mail.sendMail({
            obj: "Fattura elettronica", body: JSON.stringify({
                tipo: elettogas.model.read().value,
                intestatario: intestatario.model.read().value,
                mail: mail.model.read().value,
                codice: codice.model.read().value,
                nota: nota.model.read().value,
            })
        }).then(a => {
            location.href = "/clienti"
        })
    })

    intestatario.set(user.denominazione ? user.denominazione : (user.nome + " " + user.cognome))
    mail.set(user.email)
    codice.set(user.codcliente)

    const form = tags.div().addClass("yca-autoflex").addClass("yca-slimform")

    return form.append(
        title,
        text,
        intestatario.el,
        mail.el,
        codice.el,
        nota.el,
        submit
    )
}

export function buildLoginModal() {
    const title = tags.h1().addClass("yca-login-title").html("Login").css({ textAlign: "center", color: "#0000bb" })
    // const form = tags.div().css({ margin: "auto" })
    // const username = input.string.stringInput({ title: "Codice cliente *" })
    // const password = input.string.passwordInput({ title: "Password *" })
    // const middl = tags.div().css({ backgroundColor: "rgba(255, 255, 255, 0.6)" })
    // const errorDiv = tags.ul()
    // const button = tags.isubmit().text("LOGIN").click(() => {
    //     errorDiv.empty()
    //     let isValid = true
    //     if (username.model.read().state == "invalid") {
    //         errorDiv.append(tags.li().html("Devi inserire email/username"))
    //         isValid = false
    //     }
    //     if (password.model.read().state == "invalid") {
    //         errorDiv.append(tags.li().html("Devi inserire email/username"))
    //         isValid = false
    //     }
    //     if (isValid)
    //         api.utente.login({ user: username.model.read().value, password: password.model.read().value }).then(user => {
    //             if (user && !(user as any).error) {
    //                 //cookies.setCookie("user", user, 30)
    //                 location.href = "/clienti"
    //             }
    //             else {
    //                 errorDiv.append(tags.li().html("Codice cliente e/o password errati"))
    //             }
    //         }).catch(e => {
    //             errorDiv.append(tags.li().html("Error: " + JSON.stringify(e)))
    //         })
    // })

    const under = tags.div().css({ display: "flex" })
    const buttonE = shards.anchor.full({ text: "Portale energia", url: "https://tecniconsul-portcli.serviceict.it/argon-tecniconsul/portale.html?idFornitore=1" }).css({ flex: "1", margin: "0 20px 0 0", textAlign: "center", padding: "16px 32px" })
    const buttonG = shards.anchor.full({ text: "Portale gas", url: "http://www.tecniconsulenergia.it:8080/RetiVendita/Pages/Login.aspx" }).css({ flex: "1", margin: "0 0 0 20px", textAlign: "center", padding: "16px 32px" })
    under.append(buttonE, buttonG)

    // middl.append(title, form.append(username.el, password.el, button))
    const modal = shards.modal.panel(under)
    return modal
}