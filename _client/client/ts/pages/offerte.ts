import * as tags from "../tags"
import * as templates from "../template/_lib"
import * as shards from "../shards/_lib"
import * as blocks from "../blocks/_lib"
import * as api from "../api/_lib"
import * as forms from "./form"

export function build(args?: { offerta: string }) {
    //CONFIG
    const blueBox = [
        { text: "leggere la bolletta", logo: "/img/white_schermo.png", url: [
            { name: "Leggere bolletta luce", url: "/leggerebollettaluce" },
            { name: "Leggere bolletta gas", url: "/leggerebollettagas" }
        ] },
        { text: "agevolazioni", logo: "/img/white_colonna.png", url: "/utils/agevolazioni" },
        { text: "come risparmiare", logo: "/img/white_money.png", url: "/utils/come_risparmiare" }
    ]




    const template = templates.main.main()
    const upperSection = tags.div().addClass("yca-wrapper").css({
        backgroundImage: "url(/img/lavoro.jpg)",
        backgroundSize: "cover",
        width: "80vw",
        height: "36vw",
        position: "relative"
    })

    const bottomFloater = tags.div().addClass("yca-home-bottomfloater")
    const usClaim = tags.h1().addClass("yca-home-claim").html("NUOVA ENERGIA<BR />NELLA TUA GIORNATA")


    upperSection.append(bottomFloater.append(usClaim))


    const middleSection = tags.div().addClass("yca-wrapper")
    const ourOfferts = tags.h1().addClass("yca-home-underlined").text("Le nostre offerte")


    const wrapperOfferte = tags.div()

    const offerte = tags.div().css({
        width: "60vw",
        flex: "1",
        height: "36vw",
        display: "inline-block"
    })

    const offLinkCont = tags.div()
    const linkCasa = tags.h2().css({ display: "inline-block" }).addClass("yca-home-casaimpresa").html("casa")
    const linkImpresa = tags.h2().css({ display: "inline-block" }).addClass("yca-home-casaimpresa").html("impresa")


    const myByttStyle = {
        color: "rgb(255, 255, 255)",
        borderRadius: "10px",
        backgroundColor: "rgb(102, 102, 102)",
        width: "270px",
        height: "90px",
        fontSize: "24px",
        border: "0px",
        display: "block",
        margin: "10px",
        textAlign: "center",
        cursor: "pointer"
    }

    const threeButt = tags.div().addClass("yca-offerte-threebuttons").css({ float: "right", /*height: "36vw",*/ display: "flex", margin: "8vw 0", flexDirection: "column" }).append(
        tags.div().css({ flex: "1" }).append(tags.button().css(myByttStyle).text("luce").on("click", () => { location.href = "/dettagli/luce" })),
        tags.div().css({ flex: "1" }).append(tags.button().css(myByttStyle).text("gas").on("click", () => { location.href = "/dettagli/gas" })),
        //tags.div().css({ flex: "1" }).append(tags.button().css(myByttStyle).text("luce + gas").on("click", () => { location.href = "/offerte/lucegas" })),
    )
    const threeButtAzienda = tags.div().addClass("yca-offerte-threebuttons").css({ float: "right", /*height: "36vw",*/ display: "flex", margin: "8vw 0", flexDirection: "column" }).append(
        tags.div().css({ flex: "1" }).append(tags.button().css(myByttStyle).text("luce").on("click", () => { location.href = "/dettagli/luceazienda" })),
        tags.div().css({ flex: "1" }).append(tags.button().css(myByttStyle).text("gas").on("click", () => { location.href = "/dettagli/gasazienda" })),
        //tags.div().css({ flex: "1" }).append(tags.button().css(myByttStyle).text("luce + gas").on("click", () => { location.href = "/offerte/lucegas" })),
    )


    wrapperOfferte.addClass("yca-offerte-wrapper").append(offerte.addClass("yca-offerte-wrapper-offerte").append(offLinkCont.append(linkCasa, linkImpresa)), threeButt, threeButtAzienda).css({ backgroundImage: "url(/img/casa.jpg)" })

    linkCasa.click(() => {
        linkCasa.addClass("selected")
        linkImpresa.removeClass("selected")
        wrapperOfferte.css({ backgroundImage: "url(/img/casa.jpg)" })
        usClaim.html("LUCE E GAS<br />OVUNQUE TU SIA")
        upperSection.css({ backgroundImage: "url(/img/Lavoro_1979.jpg)" })
        threeButtAzienda.hide()
        threeButt.show()
    })
    linkImpresa.click(() => {
        linkCasa.removeClass("selected")
        linkImpresa.addClass("selected")
        wrapperOfferte.css({ backgroundImage: "url(/img/fabbrica.jpg)" })
        usClaim.html("LUCE E GAS<br />CHIUNQUE TU SIA")
        upperSection.css({ backgroundImage: "url(/img/lavoro.jpg)" })
        threeButtAzienda.show()
        threeButt.hide()
    })




    const blueSection = tags.div().addClass("yca-home-blue")

    for (let b of blueBox)
        blueSection.append(shards.panel.bluePanel(b))


    middleSection.append(ourOfferts, wrapperOfferte, blueSection)


    const segnapSlider = tags.h1().addClass("yca-home-underlined").html("News")
    //const errSlider = tags.p().html("Error: MongoDB Error: no element for slider in database.")
    const slider = shards.thumbbox.thumbBox({ scrollTime: 3000 })

    api.news.getNews().then(news => {
        const m: JQuery[] = []
        for (const n of news) {
            m.push(shards.news.thumbNews({
                image: n.banner,
                title: n.title,
                subtitle: n.title,
                date: n.date,
                body: n.body,
                id: n._id
            }))
        }
        slider.add(m)
    })




    template.main.append(upperSection, middleSection, segnapSlider, slider.el)

    linkCasa.click()

    if (args.offerta == "casa") {
        linkCasa.click()
    }
    if (args.offerta == "impresa") {
        linkImpresa.click()
    }

}



