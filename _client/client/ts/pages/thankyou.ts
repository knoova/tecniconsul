import * as tags from "../tags"
import * as templates from "../template/_lib"
import * as shards from "../shards/_lib"

export function build() {
    const template = templates.main.main()
    const upperSection = tags.div().addClass("yca-wrapper").css({
        backgroundImage: "url(/img/thanks.jpg)",
        backgroundSize: "cover",
        width: "80vw",
        height: "42.5vw",
        position: "relative"
    })



    const middleSection = tags.div().addClass("yca-wrapper").css({ padding: "50px 50px" })

    middleSection.append(
        tags.h1().css({ textTransform: "none", fontSize: "4.8rem", color: "#3260ab" }).text("E adesso?"),
        tags.div().css({ display: "flex" }).append(
            shards.panel.stdPanel({ logo: "/img/h_mail.jpg", text: "Scopri i documenti che ti serviranno", url: "/faq/documentinecessari" })
        ),
        tags.div().css({ display: "flex" }).append(
            shards.panel.stdPanel({ logo: "/img/h_ufficio_2.jpg", text: "L'ufficio più vicino a te", url: "/contatti" })
        )
    )


    template.main.append(upperSection, middleSection)


}



