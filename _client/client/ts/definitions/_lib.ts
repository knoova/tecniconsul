export interface id {
    _id: string
}

export type Menu = MenuElement[]

// export const AgencyData = {
//     name: "Associazione ricreativa culturale \"YouCan\"",
//     shortName: "YouCan Agency",
//     piva: "02806310344",
//     codicefiscale: "92187720344",
//     phone: "",
//     mobile: "+39 320 481 3036",
//     address: "Via Bolzoni 2,\n43123 Parma"
// }
export interface AgencyData {
    name: string,
    shortName: string
    piva: string
    codicefiscale: string
    phone: string
    mobile: string
    address: string
}

export interface MenuElement {
    _id: id
    text?: string
    image?: string
    path: string
    submenu?: Menu
}

export interface User {
    _id: id
    email: string
    password: string
    name: string
    surname: string
    birthday: string
    country: string
    address?: string,
    cap?: string,
    town?: string,
    city?: string,
    contacts?: Contacts,
    cf?: string,
    measurement?: Measurements
}

export interface Role {
    _id: id
    name: string
    admin: boolean
    moderator: boolean
    member: boolean
    host: boolean
}

export interface Contacts {
    _id: string
    userId: id
    phone?: string
    mobile?: string
    mail?: string
    agencyMail?: string
}

export interface Measurements {
    _id: string
    userId: id
    chest?: number
    waist?: number
    hips?: number
    height?: number
    weight?: number
    shoes?: number
}

export interface Page {
    _id: string,
    name: string,
    title: string,
    functionName: string,
    path: string[]
}