import * as tags from "../tags"
import * as blocks from "../blocks/_lib"


export function build() {
    const body = $("body").empty();
    const header = tags.div().attr("id", "yca-header").append(blocks.menu.menu(), blocks.menu.mobile())
    const main = tags.div().attr({ id: "yca-main" }).addClass("yca-main-main")
    const side = tags.div().attr({ id: "yca-main-side" }).addClass("yca-main-side").append(blocks.menu.menuBackend())
    const alert = tags.div().addClass("yca-alert")
    const wrapper = tags.div().addClass("yca-main-wrapper").append(side, main)
    main.append(alert)
    body.append(header, wrapper)

    return { main, side, alert }
}