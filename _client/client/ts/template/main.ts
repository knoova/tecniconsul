import * as tags from "../tags"
import * as blocks from "../blocks/_lib"
import * as cookie from "../utils";
import * as shards from "../shards/_lib";

export function main() {
    const body = $("body").empty();
    const header = tags.div().attr("id", "yca-header").append(blocks.menu.menu(), blocks.menu.mobile())
    const main = tags.div().attr("id", "yca-main")
    const alert = tags.div().addClass("yca-alert")


    main.append(alert)

    const w = tags.div()
    const t = tags.div().html("Questo sito utilizza cookie di profilazione, anche di terze parti, per inviarti messaggi pubblicitari mirati e servizi in linea con le tue preferenze. Se vuoi saperne di più o negare il consenso a tutti o ad alcuni cookie clicca qui il pulsante “<a href=\"/utils/privacy\">Cookie policy</a>”. Proseguendo la navigazione acconsenti all’uso dei cookie. Il consenso può essere espresso anche cliccando sul tasto ok o sul tasto × oppure proseguendo la navigazione mediante scrolling.")
    const p = shards.modal.panel(w.append(t), setCookieBanner)
    const b = shards.anchor.full({
        text: "Ok", action: () => {
            p.close()
        }
    }).css({
        padding: "10px 40px",
        display: "block",
        width: "200px",
        textAlign: "center",
        margin: "10px auto"
    })

    w.append(b)
    main.append(p.el)

    body.append(header, main, blocks.footer.footer())

    const c = cookie.getCookie("acceptedCookie")
    if (!c)
        p.show()

    return { main, alert }
}


function setCookieBanner() {
    cookie.setCookie("acceptedCookie", "true", 30)
}