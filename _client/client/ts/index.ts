import * as pages from "./pages/_lib"
import * as templates from "./template/_lib"

const offline = true;
const test = true;


(window as any).home = pages.home.build;
(window as any).login = pages.login.build;
(window as any).contatti = pages.contatti.build;
(window as any).clienti = pages.clienti.build;
(window as any).territori = pages.territori.build;
(window as any).dettagli = pages.dettagli.build;
(window as any).faq = (args?: any) => {
    args ?
        pages.faq.build(args) :
        pages.faq.build()
};
(window as any).offerte = (args?: any) => {
    args ?
        pages.offerte.build(args) :
        pages.offerte.build()
};
(window as any).form = pages.form.build;
(window as any).thankyou = pages.thankyou.build;
(window as any).news = (args?: any) => {
    args ?
        pages.news.build(args) :
        pages.news.build()
};
(window as any).utils = (args?: any) => {
    args ?
        pages.utils.build(args) :
        pages.utils.build()
};


//____> amministrazione
(window as any).amministrazione = pages.amministrazione.amministrazione;
(window as any).migrazione = pages.migrazione.build;

(window as any).addRegione = pages.regioniAdmin.addRegione;
(window as any).changeRegione = pages.regioniAdmin.changeRegione;

(window as any).addFaq = pages.faqAdmin.addFaq;
(window as any).changeFaq = pages.faqAdmin.changeFaq;

(window as any).addProvincia = pages.provinceAdmin.addProvincia;
(window as any).changeProvincia = pages.provinceAdmin.changeProvincia;

(window as any).addUfficio = pages.ufficiAdmin.addUffici;
(window as any).changeUfficio = pages.ufficiAdmin.changeUfficio;

(window as any).addUtils = pages.utilsAdmin.addUtilsPage;
(window as any).changeUtils = pages.utilsAdmin.changeUtils;

(window as any).addLanding = pages.landingAdmin.addLandingPage;
(window as any).changeLanding = pages.landingAdmin.changeLanding;

(window as any).addNews = pages.newsAdmin.addNewsPage;
(window as any).changeNews = pages.newsAdmin.changeNews;

(window as any).addUsers = pages.usersAdmin.addUsers;
(window as any).changeUsers = pages.usersAdmin.changeUser;

(window as any).addListino = pages.listinoAdmin.addListino;
(window as any).changeListino = pages.listinoAdmin.changeListino;

(window as any).addComune = pages.comuni.addComune;
(window as any).changeComune = pages.comuni.changeComune;

(window as any).addCabina = pages.cabine.addCabina;
(window as any).changeCabina = pages.cabine.changeCabina;


(window as any).offertaluce = pages.offertaluce.build;
(window as any).offertagas = pages.offertagas.build;
(window as any).offertaluceazienda = pages.offertaluceazienda.build;
(window as any).offertagasazienda = pages.offertagasazienda.build;
(window as any).offertaplacetluce = pages.offertaplacetluce.build;
(window as any).offertaplacetgas = pages.offertaplacetgas.build;


(window as any).datiPersonali = pages.datiPersonaliAdmin.build;
(window as any).fatturazioneElettronica = pages.fatturazioneElettronicaAdmin.build;


(window as any).leggerebollettaluce = pages.leggerebollettaluce.build;
(window as any).leggerebollettagas = pages.leggerebollettagas.build;

(window as any).addOfferte = pages.offerteLuce.addAndChangeOfferte;
(window as any).changeOfferte = pages.offerteLuce.offerteChangeAll;


function alert(page: () => void) {
    if (!offline || (test && location.href.indexOf("localhost") > -1))
        return page
    else
        return pages.offline.build
}