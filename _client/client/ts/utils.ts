import * as tags from "./tags"
import * as api from "./api/_lib"


export function getParameterByName(name: string) {
	const url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
	if (!results) return "";
	if (!results[2]) return "";
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}

export function setCookie(name: string, value: string, days: number) {
    let expires = "";
    if (days) {
        let date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toISOString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

export function getCookie(name: string): string | undefined {
    let nameEQ = name + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return undefined;
}

export function deleteCookie(name: string) {
	let endDate = new Date()
	endDate.setFullYear(1200)
	document.cookie = `${name} = ${endDate.toISOString()}`
}



export function createTableBy2DArray(tableData: string[][]) {
	var tableBody = tags.tbody()

	tableData.forEach(function (rowData) {
		var row = tags.tr()

		rowData.forEach(function (cellData) {
			var cell = tags.td()
			cell.html(cellData)
			row.append(cell)
		});

		tableBody.append(row)
	});

	return tableBody
}
export function userManager() {

    function set(u: api.utente.Utente) {
        setCookie("user", JSON.stringify(u), 30)
    }

    function get() {
        try {
            return JSON.parse(decodeURIComponent(getCookie("user"))) as api.utente.Utente
        }
        catch (err) {
            console.log("unable to read cookie user", err)
            return undefined
        }
    }
    function getPromise(): Promise<api.utente.Utente | undefined> {
        const _id = get()._id
        if (!_id)
            return Promise.resolve(undefined)
        else {
            api.utente.getUtenti({_id}).then(u=>{
                set(u[0])
                console.log(u)
                return u
            })
        }
    }

    function isLogged(): Boolean {
        return !!getCookie("user")
    }

    return { set, get, getPromise, isLogged }
}

(window as any).refreshUser = userManager().getPromise
