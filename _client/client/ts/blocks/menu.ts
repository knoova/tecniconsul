import * as tags from "../tags"
import * as shards from "../shards/_lib"
import * as input from "../fields/input/_lib"
import * as cookies from "../cookies"
import * as api from "../api/_lib"
import * as utils from "../utils";
import * as forms from "../pages/form"
import * as pages from "../pages/_lib"

type Menu = MenuElement[]

interface MenuElement {
    text?: string
    image?: string
    path?: string
    submenu?: Menu
    class?: string
    callback?: () => void
}



export function menu(): JQuery {

    const loggedIn = cookies.getCookie("user")

    const myModal = forms.buildLoginModal()

    const menuEls: MenuElement[] = [
        { image: "/img/logo.jpg", path: "/", class: "hamburger" },
        loggedIn ? { text: "Clienti", path: "/clienti" } : { text: "Clienti", callback: myModal.show },//path: "/clienti" },,
        { text: "Informazioni", path: "/faq" },
        { text: "Contatti", path: "/contatti" },
        { image: "/img/numeroverde.png", path: "tel:800589579", class: "yca-menu-green-number" },

    ]
    const wrapper = tags.div().addClass("yca-menu-wrapper")
    const menuDiv = tags.div().addClass("yca-menu-wrapper-menu")
    for (let element of menuEls)
        if (element.text)
            menuDiv.append(
                tags.div().addClass("yca-menu-wrapper-menu-element").append(
                    tags.a().html(element.text).attr("href", element.path).addClass(element.class).click(element.callback)
                )
            )
        else if (element.image)
            menuDiv.append(
                tags.div().addClass("yca-menu-wrapper-menu-element").append(
                    tags.a().attr("href", element.path).append(
                        tags.img().attr("src", element.image).addClass(element.class)
                    )
                )
            )

    return wrapper.append(menuDiv, myModal.el)

}

export function mobile(): JQuery {
    const loggedIn = cookies.getCookie("user")
    const prefix = "os-header"
    const startPos: { x: number, y: number } = { x: 0, y: 0 }
    let closingMenu: boolean = false
    let openingMenu: boolean = false
    const prefix2 = prefix + "-navmobile"
    const wrapper = tags.div().addClass(prefix2)
    const container = tags.div().addClass(prefix2 + "-container")
    const hamburger = shards.anchor.full({ image: "/img/hamburger.png", type: "light" }).addClass(prefix2 + "-hamburger").on("click", openMenu)
    const logo = tags.div().addClass(prefix2 + "-logo").css({
        backgroundImage: "url('/img/logo.jpg')",
        backgroundSize: "contain",
        backgroundPosition: "center",
        position: "absolute",
        top: "0",
        left: "0",
        height: "100%",
        width: "calc(100% - 200px)",
        backgroundRepeat: "no-repeat",
        margin: "0 100px"
    })
    const header = tags.div().addClass(prefix2 + "-header")
    const body = tags.div().addClass(prefix2 + "-body")

    container[0].addEventListener("touchstart", (e: TouchEvent) => {
        startPos.x = e.touches[0].pageX
        startPos.y = e.touches[0].pageY
    })

    container[0].addEventListener("touchmove", (e: TouchEvent) => {
        const nowX = e.changedTouches[e.changedTouches.length - 1].pageX
        const nowY = e.changedTouches[e.changedTouches.length - 1].pageY

        if (startPos.x > nowX && startPos.x - nowX > 60 && startPos.x - nowX > (startPos.y - nowY) * 2)
            closeMenu()

    })


    container.click((event) => {
        if (!$(event.target).closest(body).length || !(container.children("div").length > 0))
            closeMenu()
    });

    function openMenu() {
        if (openingMenu || closingMenu)
            return
        openingMenu = true
        $(document.body).css({ overflow: "hidden" })
        container.fadeIn(500, () => {
            openingMenu = false
        })
        body.animate({
            left: "0"
        }, 500, () => {
            openingMenu = false
        })
    }

    function closeMenu() {
        if (openingMenu || closingMenu)
            return
        closingMenu = true
        body.animate(
            {
                left: "-300px"
            },
            500,
            () => {
                container.fadeOut(500, () => {
                    $(document.body).css({ overflow: "visible" })
                    closingMenu = false
                })
            })
    }

    wrapper.append(hamburger, logo, container.append(body.append(header)))

    const myModal = forms.buildLoginModal()
    wrapper.append(myModal.el)
    const menuEls: MenuElement[] = [
        { text: "Home", path: "/" },
        loggedIn ? { text: "Clienti", path: "/clienti" } : { text: "Clienti", callback: myModal.show },//path: "/clienti" },,
        { text: "Informazioni", path: "/faq" },
        { text: "Contatti", path: "/contatti" },
        { image: "/img/numeroverde.png", path: "tel:800589579", class: "yca-menu-green-number" },

    ]


    for (const element of menuEls) {
        // if (element.image && !element.submenu)
        //     continue;
        const db = element.callback ?
            shards.anchor.full({
                text: element.text,
                type: "light",
                action: () => {
                    closeMenu()
                    element.callback()
                }
            }) :
            shards.anchor.full({ text: element.text, type: "light", url: element.path })

        // if (element.active)
        //     db.addClass(prefix2 + "-nav-element-active")
        body.append(db.addClass("os-hq-menu-submenu-element"))
    }


    return wrapper
}





export function menuBackend(): JQuery {

    const user = utils.userManager().get()


    const menuAdmin: MenuElement[] = [
        { text: "Add Faq", path: "/amministrazione/addfaq" },
        { text: "Change Faq", path: "/amministrazione/changefaq" },
        { text: "Add Regione", path: "/amministrazione/addregione" },
        { text: "Change Regione", path: "/amministrazione/changeregione" },
        { text: "Add Provincia", path: "/amministrazione/addprovincia" },
        { text: "Change Provincia", path: "/amministrazione/changeprovincia" },
        { text: "Add Comune", path: "/amministrazione/addcomune" },
        { text: "Change Comune", path: "/amministrazione/changecomune" },
        { text: "Add Cabina", path: "/amministrazione/addcabina" },
        { text: "Change Cabina", path: "/amministrazione/changecabina" },
        { text: "Add Listino", path: "/amministrazione/addlistino" },
        //{ text: "Change Listino", path: "/amministrazione/changelistino" },
        { text: "Add Ufficio", path: "/amministrazione/addufficio" },
        { text: "Change Ufficio", path: "/amministrazione/changeufficio" },
        { text: "Add Utils", path: "/amministrazione/addutils" },
        { text: "Change Utils", path: "/amministrazione/changeutils" },
        { text: "Add Landing", path: "/amministrazione/addlanding" },
        { text: "Change Landing", path: "/amministrazione/changelanding" },
        { text: "Add News", path: "/amministrazione/addnews" },
        { text: "Change News", path: "/amministrazione/changenews" },
        { text: "Add User", path: "/amministrazione/addusers" },
        { text: "Change User", path: "/amministrazione/changeusers" },
        { text: "Modifica offerte", path: "/amministrazione/addOfferte" }
    ]

    const wrapper = tags.div().addClass("yca-menu-wrapper")
    if (user.isAdmin)
        for (let element of menuAdmin) {
            if (element.path)
                wrapper.append(shards.anchor.full({ text: element.text, url: element.path }).addClass("yca-menu-buttons"))
            if (element.callback)
                wrapper.append(shards.anchor.full({ text: element.text, action: element.callback }).addClass("yca-menu-buttons"))
        }

    return wrapper

}