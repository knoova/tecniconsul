import * as tags from "../tags"
import * as forms from "../pages/form"
import * as shards from "../shards/_lib"

export function footer() {
    const wrapper = tags.div().addClass("yca-footer").attr({ id: "yca-footer" })
    const f1 = tags.div()
    const f2 = tags.div()
    const f3 = tags.div()
    const f4 = tags.div()

    f1.append(
        tags.h1().html("azienda").addClass("yca-footer-title"),
        tags.img().attr({ src: "/img/logo.jpg" }).css({ maxWidth: "50%" }),
        tags.p().addClass("yca-footer-small").html(`Tecniconsul Energia srl<br/>Via Gandhi, 22<br/>42123 Reggio emilia<br/>Tel. 0522 322031<br/>Fax 0522 376213<br/>Registro Imprese RE<br/>p. IVA 02400570350<br />Società sottoposta a controllo e direzione di Canarbino SpA`),
        tags.p().addClass("yca-footer-big").html(`- <a href="/utils/chisiamo">Chi siamo</a>`),
        tags.p().addClass("yca-footer-big").html(`- <a href="/utils/privacy">Informativa sulla privacy</a>`),
        tags.p().addClass("yca-footer-big").html(`- <a target="_blank" href="/pdf/Codice-Etico-Tecniconsul-Energia.pdf">Codice etico</a>`),
        tags.p().addClass("yca-footer-big").html(`- <a href="/utils/livelliqualita">Livelli di qualità commerciale</a>`)
    )
    f2.append(
        tags.h1().html("le nostre offerte").addClass("yca-footer-title"),
        tags.p().addClass("yca-footer-big").html(`Casa`).append(
            tags.p().addClass("yca-footer-small").html(`<a href="/dettagli/luce">LUCE</a>`).css({ padding: "0 0 0 10px" }),
            tags.p().addClass("yca-footer-small").html(`<a href="/dettagli/gas">GAS</a>`).css({ padding: "0 0 0 10px" }),
            //tags.p().addClass("yca-footer-small").html(`LUCE + GAS`).css({ padding: "0 0 0 10px" })
        ),
        tags.p().addClass("yca-footer-big").html(`Azienda`).append(
            tags.p().addClass("yca-footer-small").html(`<a href="/dettagli/luceazienda">LUCE</a>`).css({ padding: "0 0 0 10px" }),
            tags.p().addClass("yca-footer-small").html(`<a href="/dettagli/gasazienda">GAS</a>`).css({ padding: "0 0 0 10px" }),
            //tags.p().addClass("yca-footer-small").html(`LUCE + GAS`).css({ padding: "0 0 0 10px" })
        ),
        tags.p().addClass("yca-footer-big").html(`<a href="/dettagli/placetluce">Tariffa placet luce</a>`),
        tags.p().addClass("yca-footer-big").html(`<a href="/dettagli/placetgas">Tariffa placet gas</a>`),
        tags.p().addClass("yca-footer-big").html(`<a href="/territori">Tariffa di tutela gas</a>`)
    )

    const vieniConNoi = forms.formVieniConNoi()
    const myModal = shards.modal.panel(vieniConNoi)

    f3.append(
        myModal.el,
        tags.h1().html("il mio contratto").addClass("yca-footer-title"),
        tags.p().append(
            tags.p().addClass("yca-footer-big").append(tags.a().html(`- Informazioni utili`).attr({ href: "/faq/infoutili" })),
            tags.p().addClass("yca-footer-big").append(tags.a().html(`- Dati necessari`).attr({ href: "/faq/datinecessari" })),
            tags.p().addClass("yca-footer-big").append(tags.a().html(`- Modalità di pagamento`).attr({ href: "/faq/modalitapagamento" })),
            tags.p().addClass("yca-footer-big").append(tags.a().html(`- Bonus gas`).attr({ href: "/faq/bonusgas" })),
            tags.p().addClass("yca-footer-big").append(tags.a().html(`- Fatti chiamare`).click(() => {
                myModal.show()
            }), tags.br(), tags.br(), tags.br(), tags.br()),
            // tags.p().append(tags.br()),
            // tags.p().append(tags.br()),
        )
    )

    f4.append(
        tags.h1().html("modulistica").addClass("yca-footer-title"),
        tags.p().append(
            tags.p().addClass("yca-footer-big").append(tags.a().html(`- Accesso dati Del.AEGG n.157`).attr({ href: "/pdf/Deliberadel15707_100218113906.pdf", target: "_blank" })),
            tags.p().addClass("yca-footer-big").append(tags.a().html(`- Normativa di riferimento gas`).attr({ href: "https://www.arera.it/it/operatori/gas_testintegrati.htm" })),
            tags.p().addClass("yca-footer-big").append(tags.a().html(`- Normativa di riferimento luce`).attr({ href: "https://www.arera.it/it/operatori/ele_%20testintegrati.htm" })),
            tags.p().addClass("yca-footer-big").append(tags.a().html(`- Assicurazioni clienti finali`).attr({ href: "/utils/assicurazioniclienti" })),
            tags.p().addClass("yca-footer-big").append(tags.a().html(`- Reclami`).attr({ href: "/pdf/ModelloReclamo_TE_ReggioEmilia_100222122723.pdf" , target: "_blank"}),
                tags.br(), tags.br(), tags.br(), tags.br(), tags.br(), tags.br(), tags.br()),
            tags.p().addClass("yca-footer-big").append(tags.a().html(`- Numero verde`).attr({ href: "tel:800589579" })),
            tags.p().addClass("yca-footer-big").append(tags.a().html(`- Contattaci`).attr({ href: "/contatti" }))
        )
    )




    return wrapper.append(f1, f2, f3, f4)
}