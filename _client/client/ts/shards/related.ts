import * as tags from "../tags"
import * as map from "./map"

export function related(image: string, title: string, description: string, pdf: { lang: string, url: string }[]): JQuery {
    const wrapper = tags.div().addClass("yca-related-wrapper")
    const img = tags.div().addClass("yca-related-image").append(tags.img().attr("src", image))
    const txt = tags.div().addClass("yca-related-text")
    const ti = tags.h1().addClass("yca-related-title").html(title)
    const desc = tags.p().html(description).addClass("yca-related-description")
    for (let file of pdf) {
        txt.append(tags.a().addClass("yca-pdf-wrapper").attr("href", file.url).append(tags.img().attr("src", "pdf.jpg"), tags.p().html(file.lang)))
    }
    return wrapper
}


export function uffici() {
    const wrapper = tags.div().css({ display: "flex", minHeight: "20vw" })

    const right = tags.div().css({ flex: "1", padding: "20px", backgroundColor: "#667a85" })
    const left = tags.div().css({ flex: "3", backgroundColor: "#ccc", minHeight: "100%" })

    const mappa = map.simple()

    left.append(mappa.el)

    function set(args: { city: string, address: string, email?: string, phone: string, hours: string }) {
        right.empty().append(
            tags.p().addClass("yca-contatti-whiteparagraph").html(args.address),
            tags.p().addClass("yca-contatti-whiteparagraph").html(args.phone),
            args.email ? tags.p().addClass("yca-contatti-whiteparagraph").append(tags.a().text(args.email).attr({ href: "mailto:" + args.email })) : null,
            tags.p().addClass("yca-contatti-whiteparagraph").html(args.hours),
        )

        mappa.set(args.address)
    }


    return { el: wrapper.append(left, right), set }
}