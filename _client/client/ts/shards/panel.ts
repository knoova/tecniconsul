

import * as tags from "../tags"
import * as anchor from "./anchor"
import * as modal from "./modal"

export function bluePanel(args: { logo?: string, text: string, url?: string | { name: string, url: string }[] }): JQuery {
    const wrapper = tags.div().addClass("yca-bluepanel-wrapper")
    const img = tags.img().addClass("yca-bluepanel-image").attr("src", args.logo)
    const txt = tags.p().addClass("yca-bluepanel-text").text(args.text)
    const link = tags.a().addClass("yca-bluepanel-link")
    if (args.url) {
        if (typeof args.url === "string") {
            link.attr({ href: args.url })
        }
        else {
            const mWrapper = tags.div().css({ padding: "40px 5px" })
            for (const su of args.url) {
                mWrapper.append(
                    tags.div().css({ padding: "15px 0" }).append(
                        anchor.full({ text: su.name, url: su.url })
                    )
                )
            }
            const mm = modal.panel(mWrapper)
            wrapper.append(mm.el)
            link.on("click touchend", mm.show)
        }
    }
    const imgWrapper = tags.div().addClass("yca-bluepanel-imagewrapper")
    link.append(imgWrapper.append(img), txt)
    return wrapper.append(link)
}

export function whitePanel(args: { logo?: string, text: string, url?: string }): JQuery {
    const wrapper = tags.div().addClass("yca-whitepanel-wrapper")
    const img = tags.img().addClass("yca-whitepanel-image").attr("src", args.logo)
    const txt = tags.p().addClass("yca-whitepanel-text").text(args.text)
    const link = tags.a().attr({ href: args.url ? args.url : "#" }).addClass("yca-whitepanel-link")
    const imgWrapper = tags.div().addClass("yca-whitepanel-imagewrapper")
    link.append(imgWrapper.append(img), txt)
    return wrapper.append(link)
}

export function stdPanel(args: { logo: string, text: string, url?: string }): JQuery {
    const wrapper = tags.div().addClass("yca-stdpanel-wrapper")
    const img = tags.img().addClass("yca-stdpanel-image").attr("src", args.logo)
    const txt = tags.p().addClass("yca-stdpanel-text").text(args.text)
    const link = tags.a().attr({ href: args.url ? args.url : "#" }).addClass("yca-stdpanel-link")
    const imgWrapper = tags.div().addClass("yca-stdpanel-imagewrapper")
    link.append(imgWrapper.append(img), txt)
    return wrapper.append(link)
}