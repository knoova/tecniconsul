import * as tag from "../tags"

export function badge(args: { prezzo: number, typeTariffa: string, fasciaOraria: string }) {
    const wrapper = tag.div().addClass("yca-pricebadge-badge")
    const prezzo = tag.div().addClass("yca-pricebadge-badge-prezzo").html(buildPrezzo(args.prezzo))
    const typeTariffa = tag.div().addClass("yca-pricebadge-badge-tariffa").html(args.typeTariffa)
    const fasciaOraria = tag.div().addClass("yca-pricebadge-badge-orario").html(args.fasciaOraria)

    function buildPrezzo(p: number) {
        return p.toString() + "€/kWh"
    }


    return wrapper.append(prezzo, typeTariffa, fasciaOraria)
}

export function description(args: { titolo: string, pre: string, testo: string }) {
    const wrapper = tag.div().addClass("yca-pricebadge-description")
    const titolo = tag.p().addClass("yca-pricebadge-description-titolo").html(args.titolo)
    const container = tag.p().addClass("yca-pricebadge-description-container")
    const pre = tag.strong().addClass("yca-pricebadge-description-container-pre").html(args.pre)
    const testo = tag.span().addClass("yca-pricebadge-description-container-testo").html(args.testo)
    container.append(pre, testo)


    return wrapper.append(titolo, container)
}