import * as tags from "../tags"
import * as modal from "./modal"
import * as anchor from "./anchor"

export function faq(args: {
    logo: string, title: string,
    subitle: string, text: string,
    hash: string, orientation: "left" | "right"
}): { el: JQuery, setShift: (shift: number) => number } {
    const myModal = modal.panel(tags.div().html(args.text))
    const wrapper = tags.div().addClass("yca-shard-faq").attr({ id: args.hash })
    const logo = tags.div().addClass("yca-shard-faq-logo").append(tags.img().attr({ src: args.logo }))
    const inner = tags.div().addClass("yca-shard-faq-inner").on("click touchend", () => {
        if (!myModal.shown)
            myModal.show()
        else
            myModal.close()
    })
    const left = tags.div().addClass("yca-shard-faq-left").append(
        tags.div().addClass("yca-shard-faq-left-inner").append(
            tags.h1().html(args.title).addClass("yca-shard-faq-left-title"),
            tags.p().html(args.subitle).addClass("yca-shard-faq-left-subtitle")
        )
    )
    function setShift(shift: number) {
        if (args.orientation == "left")
            inner.css({ left: `-${(5 * (shift ** 2.3))}px` })
        if (args.orientation == "right")
            inner.css({ right: `-${(5 * (shift ** 2.3))}px` })
        return 5 * (shift ** 2.3)
    }

    if (args.orientation == "left")
        wrapper.append(inner.append(logo.addClass("yca-to-left"), left.addClass("yca-to-left")).addClass("yca-to-left"), myModal.el)
    else
        wrapper.append(inner.append(left.addClass("yca-to-right"), logo.addClass("yca-to-right")).addClass("yca-to-right"), myModal.el)

    return { el: wrapper, setShift }
}

export function faqCategory(args: { title: string, logo: string, orientation: "left" | "right" }) {
    const el = tags.div().addClass("yca-shard-faqcategory")
    const title = tags.div().addClass("yca-shard-faqcategory-title").append(tags.h1().html(args.title))
    const container = tags.div().addClass("yca-shard-faqcategory-container")
    const logo = tags.div().addClass("yca-shard-faqcategory-container-logo").append(tags.img().attr({ src: args.logo }))
    const faqContainer = tags.div().addClass("yca-shard-faqcategory-container-faq")
    if (args.orientation == "left") {
        title.addClass("yca-to-left")
        container.append(logo.addClass("yca-to-left"), faqContainer.addClass("yca-to-left")).addClass("yca-to-left")
    }
    else {
        title.addClass("yca-to-right")
        container.append(faqContainer.addClass("yca-to-right"), logo.addClass("yca-to-right")).addClass("yca-to-right")
    }

    el.append(title, container)

    function set(faq: JQuery[]) {
        for (const f of faq) {
            faqContainer.append(f)
        }
    }
    return { el, set }
}