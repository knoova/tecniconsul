/*/// <reference path="../../typings/index.d.ts" />*/

import * as tags from "../tags"
import * as shards from "./_lib";

export interface ThumbBox {
	el: JQuery,
	add(els: JQuery[]): void,
	empty(): void
}

const time = 450
let defaultScrollTime = 0

export function thumbBox(args?: { els?: JQuery[], arrowHeight?: string, paginationWidth?: number, scrollTime?: number }): ThumbBox {
	const wrapper = tags.div().addClass("os-thumbbox");
	const internal = tags.div().addClass("os-thumbbox-internal");
	const points = tags.div().addClass("os-thumbbox-points");

	let started = false
	let timeout = 0

	let actualSelected = 0;

	let elements: JQuery[] = []
	let balls: JQuery[] = []

	if (args && args.scrollTime) {
		defaultScrollTime = args.scrollTime
		startTimer()
	}
	const bPrev = tags.div()
		.append(tags.img().attr({src: "/img/arrow_left.jpg"}))
		.addClass("os-thumbbox-previous")
		.click((e) => {
			stopTimer()
			previous()
		});
	const bNext = tags.div()
		.append(tags.img().attr({src: "/img/arrow_right.jpg"}))
		.addClass("os-thumbbox-next")
		.click((e) => {
			stopTimer()
			next()
		});

	if (args && args.arrowHeight) {
		bPrev.css("top", args.arrowHeight)
		bNext.css("top", args.arrowHeight)
	}

	wrapper.append(bPrev, bNext, internal, points);

	if (args && args.els)
		add(args.els);

	function startTimer() {
		if (!started) {
			started = true;
			timeout = window.setTimeout(goNext, defaultScrollTime)
		}
	}


	function stopTimer() {
		if (started) {
			started = false
			clearTimeout(timeout)
		}
	}

	let resizeTimer: any = 0
	wrapper
		.hover(() => checkPosition())
		.click(() => stopTimer())
		.on("touchend", () => {
			stopTimer()
			internal.scroll(() => {
				clearTimeout(resizeTimer);
				resizeTimer = setTimeout(checkPosition, 25);
			})
		})


	function scrollGap() {
		return (args && args.paginationWidth) ? args.paginationWidth : internal.width()
	}



	$(window).bind('resize', function () {
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(previous, 10);
	});

	function add(els: JQuery[]) {
		elements.push(...els)
		for (let e of elements) {
			const b = tags.span().addClass("os-thumbox-ball").addClass("os-thumbox-ball-" + els.indexOf(e).toString()).click(() => {
				goTo(els.indexOf(e))
			})
			balls.push(b)
			points.append(b)
			internal.append(e.addClass("os-thumbox-element-" + els.indexOf(e).toString()));
		}
		changeBall(0)
		checkPosition()
	}

	function empty() {
		internal.empty()
	}

	function goTo(go: number) {
		let scroll = getActualScroll().actual
		if (go > actualSelected) {
			for (let i = 0; i < go - actualSelected; i++) {
				scroll = getActualScroll(scroll).next
			}
		}
		else if (go < actualSelected) {
			for (let i = 0; i < actualSelected - go; i++) {
				scroll = getActualScroll(scroll).previous
			}
		}
		internal.stop().animate({ scrollLeft: scroll }, time, checkPosition)
		checkPosition()
	}

	function getActualScroll(now?: number) {
		const actual = now ? now : internal.scrollLeft()
		const scroll = { actual, previous: 0, next: 0, slideBegin: 0 }
		if (actual % scrollGap() != 0) {
			scroll.previous = Math.floor(actual / scrollGap()) * scrollGap()
			scroll.next = (Math.floor(actual / scrollGap()) + 1) * scrollGap()
			scroll.slideBegin = scroll.previous
		}
		else {
			scroll.slideBegin = actual
			scroll.previous = ((actual / scrollGap()) - 1) * scrollGap()
			scroll.next = ((actual / scrollGap()) + 1) * scrollGap()
		}
		return scroll
	}


	function next() {
		internal.stop().animate({ scrollLeft: getActualScroll().next }, time, checkPosition)
	}

	function previous() {
		internal.stop().animate({ scrollLeft: getActualScroll().previous }, time, checkPosition)
	}

	function goNext() {
		clearTimeout(timeout)
		internal.stop().animate({ scrollLeft: (internal.scrollLeft() >= (internal[0].scrollWidth - internal.width())) ? 0 : getActualScroll().next }, time, checkPosition)
		timeout = window.setTimeout(goNext, defaultScrollTime)
	}

	function changeBall(now: number) {
		for (let i = 0; i < balls.length; i++) {
			if (i == now)
				balls[i].addClass("os-thumbox-ball-selected")
			else
				balls[i].removeClass("os-thumbox-ball-selected")

		}
		actualSelected = now;
	}

	function checkPosition() {
		if (internal.scrollLeft() <= 0) {
			bPrev.addClass("os-thumbbox-keephidden");
		}
		else {
			bPrev.removeClass("os-thumbbox-keephidden")
		}
		if (internal.scrollLeft() >= (internal[0].scrollWidth - internal.width())) {
			bNext.addClass("os-thumbbox-keephidden")
		}
		else {
			bNext.removeClass("os-thumbbox-keephidden")
		}
		for (const e of elements) {
			if (checkInView(internal, e))
				changeBall(elements.indexOf(e))
		}
	}

	return { el: wrapper, add, empty };
}

export interface Hint<T> {
	slot: (value: T) => JQuery
}



function checkInView(container: JQuery, elem: JQuery) {
	const contCenter = container.width() / 2//container.scrollLeft() + ();

	const elemLeft = elem.offset().left - container.offset().left;
	const elemRight = elemLeft + elem.width();

	const isCentered = (elemLeft <= contCenter && contCenter < elemRight)
	// console.log("data", elem, contCenter, elemLeft, elemRight, isCentered)

	return isCentered;
}