import * as tags from "../tags"

export function build() {
    const n = Math.floor(Math.random() * 6)
    return tags.div().addClass("yca-wrapper").css({
        backgroundImage: `url(/img/random2/${n.toString()}.jpg)`,
        backgroundSize: "cover",
        width: "80vw",
        height: "36vw",
        position: "relative"
    })
}

export function lowerBanner() {
    return tags.a().addClass("yca-banner").css({
        display: "block",
        backgroundImage: `url(/img/banner.png)`,
        backgroundSize: "cover",
        backgroundPosition: "center",
        width: "80vw",
        height: "100px",
        margin: "auto",
        position: "relative",
        borderLeft: "2px solid #3260abff",
        borderRight: "2px solid #3260abff"
    }).attr({href: "https://www.arera.it/it/index.htm"})
}