import * as banner from "./banner"
import * as bannerone from "./bannerone"
import * as smallBanner from "./smallBanner"
import * as related from "./related"
import * as slider from "./slider"
import * as sliderElement from "./sliderElement"
import * as panel from "./panel"
import * as modal from "./modal"
import * as map from "./map"
import * as anchor from "./anchor"
import * as faq from "./faq"
import * as thumbbox from "./thumbBox"
import * as news from "./news"
import * as priceBadge from "./priceBadge"


export { banner, bannerone, smallBanner, related, slider, sliderElement, panel, modal, map, anchor, faq, thumbbox, news, priceBadge }