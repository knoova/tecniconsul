import * as tags from "../tags"

export function sliderElement(image: string, text?: string): JQuery {
    const wrapper = tags.div().addClass("yca-sliderelement-wrapper")
    const img = tags.div().addClass("yca-sliderelement-image").append(tags.img().attr("src", image));
    if (text)
        return wrapper.append(tags.div().addClass("yca-sliderelement-text").html(text), img);

    return wrapper.append(img)
}