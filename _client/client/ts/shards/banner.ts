import * as tags from "../tags"

export function banner(args: { language?: string, image: string, text: string, description: string, bgColor: string, textColor: string, category?: string, textPositionHigh?: boolean, isCompany?: boolean }): JQuery {
    
    const wrapper = tags.div().addClass("yca-banner-wrapper")
    const img = tags.div().addClass("yca-banner-image").append(tags.img().attr("src", args.image))
   
    const inner = tags.div().addClass("yca-banner-text-inner").css({ backgroundColor: args.bgColor, color: args.textColor }).html(args.text)
    const desc = tags.p().html(args.description)
    if (args.textPositionHigh) {
        inner.addClass("yca-banner-text-high")
    }

    return wrapper.append(img)
}