import * as tags from "../tags"

export function smallBanner(args: { language?: string, image: string, text: string, bgColor: string, textColor: string, category: string }): JQuery {
    const wrapper = tags.div().addClass("yca-smallbanner-wrapper")
    const img = tags.div().addClass("yca-smallbanner-image").append(tags.img().attr("src", args.image))
    const inner = tags.div().addClass("yca-smallbanner-text-inner").css({ backgroundColor: args.bgColor, color: args.textColor }).html(args.text)
    return wrapper.append(img, inner)
}