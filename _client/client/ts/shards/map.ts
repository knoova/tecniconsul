import * as tags from "../tags"

export function simple() {

    const wrapper = tags.div().css({ minHeight: "100%" })

    function set(address: string) {
        new google.maps.Geocoder().geocode({ address }, (coordinates: google.maps.GeocoderResult[], status: google.maps.GeocoderStatus) => {
            console.log("GoogleMapsResults: ", status, coordinates, coordinates[0].geometry.location.toJSON())
            if (status != google.maps.GeocoderStatus.OK)
                throw new Error(`Error in parsing api MAPS, status: ${status}`)
            const position = coordinates[0].geometry.location.toJSON()
            const gMap = new google.maps.Map(wrapper[0], {
                center: position,
                zoom: 14
            })

            const marker = new google.maps.Marker({
                position,
                map: gMap
            })
        })
    }

    return { el: wrapper, set }
}