import * as tags from "../tags"

export function panel(item: JQuery, onClose?: () => void) {
    const wrapper = tags.div().addClass("yca-modal").hide()
    const modal = tags.div().addClass("yca-modal-content")
    const cross = tags.button().addClass("yca-modal-cross").html("&times;").css({ backgroundColor: "#fff", border: "0px solid transparent" })
    let shown = false
    wrapper.append(
        modal.append(
            cross, item
        )
    )

    function show() {
        wrapper.show(450, () => {
            shown = true
        })
    }

    function close() {
        if (onClose)
            onClose()
        wrapper.hide(450, () => {
            shown = false
        })
        console.log("chiusura")
    }

    cross.on("click touchend", close)

    return { el: wrapper, show, close, shown }

}


