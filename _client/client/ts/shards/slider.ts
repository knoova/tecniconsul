import * as tags from "../tags"

export interface ThumbBox {
	el: JQuery,
	add(els: JQuery[]): void,
	empty(): void
	start(mTime?: number): void
}


export function build(args?: { els?: JQuery[], arrowHeight?: string, pageWidth?: number }): ThumbBox {
	let time = 2550
	const wrapper = tags.div().addClass("yca-slider");
	const internal = tags.div().addClass("yca-slider-internal");
	const elements: JQuery[] = []
	const numbers = { total: 0, actual: 0 }
	let started = false;
	let timeout = 0;


	const play = tags.div().addClass("yca-slider-internal-play").append(tags.img().attr("src", "img/play.png").addClass("yca-slider-internal-play-button"))

	wrapper.append(play, internal);

	if (args && args.els)
		add(args.els);

	function add(els: JQuery[]) {
		for (let e of els) {
			elements.push(e.css({ whiteSpace: "pre" }));
		}
		numbers.total = elements.length
		load(numbers.actual)
	}

	function empty() {
		internal.empty()
	}

	function manageTimer() {
		if (started) {
			play.show();
			started = false
			clearTimeout(timeout)
		}
		else {
			play.hide()
			started = true;
			timeout = window.setTimeout(goNext, time)
		}
	}

	internal.click(() => {
		manageTimer()
	})

	play.click(() => {
		manageTimer()
	})

	function start(mTime?: number) {
		time = mTime ? mTime : time
		manageTimer()
	}

	function goNext() {
		clearTimeout(timeout)
		numbers.actual = (numbers.actual + 1) % numbers.total
		internal.empty()
		load(numbers.actual)
		timeout = window.setTimeout(goNext, time)
	}

	function load(num: number) {
		internal.append(elements[num])
	}




	return { el: wrapper, add, empty, start };
}