
import * as tags from "../tags"
import { anchor, modal } from "./_lib";

export function news(args: { image: string, title: string, subtitle: string, date: string, body: string,  id: string }) {
    const wrapper = tags.div().addClass("yca-news-wrapper").attr({ id: args.id })
    const myModal = modal.panel(tags.p().html(args.body))
    const img = tags.div().addClass("yca-news-image").css({ backgroundImage: `url(${args.image})`, backgroundSize: "cover" })
    //const date = tags.div().addClass("yca-news-date").html(args.date)
    const ti = tags.h1().addClass("yca-news-title").html(args.title)
    const desc = tags.p().html(args.body).addClass("yca-news-description")
    const button = anchor.full({
        text: "Leggi di più",
        action: () => {
            myModal.show()
        }
    }).css({ padding: "15px 30px", display: "none" })

    return wrapper.append(ti, /*date,*/ img, desc, button, myModal.el)
}

export function thumbNews(args: { image: string, title: string, subtitle: string, date: string, body: string, id: string }) {
    const wrapper = tags.div().addClass("yca-thumbnews-wrapper")
    const myModal = modal.panel(tags.p().html(args.body))
    const img = tags.div().addClass("yca-thumbnews-image").css({ backgroundImage: `url(${args.image})`, backgroundSize: "cover" })
    const ti = tags.h1().addClass("yca-thumbnews-title").html(args.title)
    const desc = tags.p().html(args.subtitle).addClass("yca-thumbnews-description")
    const texts = tags.div().addClass("yca-thumbnews-textwrapper")
    const button = anchor.full({
        text: "Leggi di più",
        url: "/news/" + args.id
    }).css({ padding: "15px 30px" })
    return wrapper.append(img, texts.append(ti, desc, button, myModal.el.css({ color: "#000" })))
}