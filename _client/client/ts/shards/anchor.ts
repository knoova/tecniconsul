import * as tags from "../tags"


export interface Anchor {
    args: Partial<Argument>
    anchor: JQuery
    set(args: Partial<Argument>): void
}


export interface Argument {
    text: string,
    action: () => void | Promise<any>
    url: string
    image: string
    disabled: boolean
    type: "main" | "light"
}



/**crea un bottone standard esponendo i metodi per modificarlo */
export function dynamic(args: Partial<Argument>): Anchor {
    const anchor = tags.a().addClass('yca-anchor-main')

    let isEnabled = true

    let isCalling = false


    set(args)

    function disableAnchorForCalling() {
        isCalling = true
        anchor.addClass("yca-anchor-disabled")
    }
    function enableAnchorForCalling() {
        isCalling = false
        if (isEnabled)
            anchor.removeClass("yca-anchor-disabled")
    }


    anchor.click(e => {
        if (args.action && !isCalling && !args.url) {
            e.preventDefault()
            disableAnchorForCalling()
            const prom = args.action()
            if (!prom)
                enableAnchorForCalling()
            else
                prom.then(r => {
                    enableAnchorForCalling()
                }).catch(r => {
                    enableAnchorForCalling()
                })
        }
        else if (!isEnabled && !args.url) {
            e.preventDefault()
        }
    })

    function set(data: Partial<Argument>) {
        function setText(args: { message?: string, image?: string }) {
            anchor.empty()
            renderContent({ el: anchor, text: args.message, image: args.image })
        }


        function setAction(action?: () => void | Promise<any>) {
            if (action)
                args.action = action
            else
                args.action = undefined
        }

        function setUrl(url?: string) {
            if (url)
                anchor.attr({ href: url })
            else
                anchor.removeAttr("href")
        }

        function setStatus(disabled?: boolean): void {
            isEnabled = !disabled
            if (!isEnabled)
                anchor.addClass("yca-anchor-disabled")
            else
                anchor.removeClass("yca-anchor-disabled")
        }

        function setType(type: string) {
            anchor.removeClass("yca-anchor-main").removeClass("yca-anchor-light")
            anchor.addClass((!type || type == "yca-anchor-main") ? "yca-anchor-main" : "yca-anchor-light")
        }


        setText({ message: data.text, image: data.image })
        setUrl(data.url)
        setAction(data.action)
        setStatus(data.disabled)
        setType(data.type)
    }


    return { anchor, set, args }
}



export function full(args: Partial<Argument>) {
    const el = dynamic(args).anchor

    return el;
}


function renderContent(args: { el: JQuery, text?: string, image?: string }) {
    if (args.text)
        args.el.append(tags.span().html(args.text)).css('white-space', 'nowrap')
    else if (args.text && args.image)
        args.el.append(tags.img().attr({ src: args.image }), tags.span().html(args.text)).css('white-space', 'nowrap')
    else if (args.image)
        args.el.append(tags.img().attr({ src: args.image })).css('white-space', 'nowrap')
}