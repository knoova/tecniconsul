
export interface DataState<T> {
    state: "valid" | "invalid"
    value?: T
}

export interface Model<T> {
    write(data: T): void,
    read(): T,
    watch(Watcher: (data: T) => any): void
}

export function createModel<T>(starter: T): Model<T> {
    let value: T | undefined = undefined
    let watcher: undefined | ((data: T) => any) = undefined

    function write(data: T) {
        value = data;
        if (watcher)
            watcher(value)
    }

    function read(): T {
        return value
    }

    function watch(Watcher: (data: T) => any) {
        watcher = Watcher
    }

    if (starter) {
        write(starter)
    }

    return { write, read, watch }
}