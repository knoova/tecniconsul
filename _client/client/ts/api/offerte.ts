
interface Offerta { _id?: string, title: string; intro: string;  description: string; }

interface Offerte {
    casaAziendaPlacet: "casa" | "azienda" | "placet"; luceGas: "luce" | "gas";
    o1: Offerta, o2: Offerta, o3: Offerta, o4: Offerta
}

export function getOfferta(args: { casaAziendaPlacet: "casa" | "azienda" | "placet", luceGas: "luce" | "gas" }): Promise<Offerte> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            contentType: 'application/json',
            dataType: "json",
            url: "/api/v1/offerte/getofferta",
            data: JSON.stringify(args),
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function setOfferta(args: Offerte) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/api/v1/offerte/setofferta",
            data: JSON.stringify(args),
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function changeOfferta(args: Offerta) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/faq/changefaq",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function deleteOfferta(_id: string) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/faq/deletefaq",
            data: { _id },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}