import * as def from "../definitions/_lib"


export function putImage(image: Blob): Promise<{ _id: string, url: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/images/put/",
            data: {
                base64Image: image
            },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}