export interface LandingPages { slug: string, body: string, _id?: string }


export function getLanding(slug: string): Promise<LandingPages[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "GET",
            url: "/api/v1/landing/landing/" + slug,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}
export function getAllLanding(): Promise<LandingPages[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/landing/landing",
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function setLanding(args: LandingPages): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/landing/setlanding",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function changeLanding(args: LandingPages): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/landing/changelanding",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function deleteLanding(_id: string): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/landing/deletelanding",
            data: { _id },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}