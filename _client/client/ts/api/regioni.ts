

export function getRegioni(): Promise<{ name: string, _id: string }[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/regioni",
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function setRegione(args: {name: string, _id?: string}): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/setregione",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function changeRegione(args: { name: string, id: string }): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/changeregione",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function deleteRegione(id: string): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/deleteregione",
            data: { id },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}