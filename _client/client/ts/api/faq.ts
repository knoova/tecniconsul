
interface Faq { _id?: string, category: string, title: string, description: string, subtitle: string, logo: string, part: string }

export function getFAQ(args: { whichFAQ?: string, part?: string, category?: string }): Promise<Faq[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/faq/faqs",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function setFAQ(args: Faq) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/faq/setfaq",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function changeFAQ(args: Faq) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/faq/changefaq",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function deleteFAQ(_id: string) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/faq/deletefaq",
            data: { _id },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}