
export function putImage(args: { image: Blob, name: string }): Promise<{ _id: string, url: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/upload/put",
            data: {
                base64Image: args.image,
                name: args.name
            },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}


export function putCsv(args: { csv: string, cabina: string, type: string }): Promise<{ _id: string, url: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/upload/putcsv",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}


export function putXls(args: { xls: Blob, name: string }): Promise<{ ok: boolean }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/upload/putxls",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}