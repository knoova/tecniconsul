export interface News { title: string, subtitle: string, body: string, date: string, banner?: string, _id?: string }


export function getNews(): Promise<News[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/news/news",
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function setNews(args: News): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/news/setnews",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function changeNews(args: News): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/news/changenews",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function deleteNews(id: string): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/news/deletenews",
            data: { id },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}