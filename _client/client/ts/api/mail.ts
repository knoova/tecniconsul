
export function sendMail(data: { obj: string, body: string }): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/mail/sendcustomdata",
            data,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}
