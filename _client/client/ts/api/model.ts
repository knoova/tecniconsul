import * as def from "../definitions/_lib"

export function getAll(): Promise<def.User[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/modelle",
            //data: ts.user.read().value._id,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function getOne(_id: string): Promise<def.User> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/modelle/:" + _id,
            //data: ts.user.read().value._id,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}