
import * as generic from "./generic"
import * as model from "./model"
import * as images from "./images"
import * as regioni from "./regioni"
import * as utils from "./utils"
import * as faq from "./faq"
import * as uffici from "./uffici"
import * as news from "./news"
import * as upload from "./upload"
import * as utente from "./utente"
import * as contatto from "./contatto"
import * as mail from "./mail"
import * as comuni from "./comuni"
import * as landing from "./landing"
import * as offerte from "./offerte"

import * as migrazione from "./migrazione"

export { generic, model, images, regioni, utils, faq, uffici, news, upload, utente, contatto, mail, comuni, landing, offerte,  migrazione }
