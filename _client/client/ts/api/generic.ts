import * as cookies from "../cookies"

import * as md5 from "md5"

interface Contatto {
    _id?: string
    azienda?: boolean,
    privato?: boolean,
    nome: string,
    cognome?: string,
    age?: string,
    professione?: string,
    orario?: string,
    comune?: string,
    telefono?: string,
    commenti?: string
    acconsento: boolean,
    acconsentoNonObbl?: boolean,
    email?: string,
    documento?: string
    tipo: string
}

interface Utente {
    _id?: string,
    privato: boolean,
    azienda: boolean,
    nome?: string,
    cognome?: string,
    denominazione?: string,
    cf: string,
    piva?: string,
    provincia: string,
    indirizzo: string,
    telefono?: string,
    email: string,
    password: string,
    codcliente: string,
    isAdmin?: boolean
}

export function setProvincia(args: { regione: string, name: string, _id?: string }): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/setprovincia",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}


export function getProvince(regione?: string): Promise<{ name: string, _id: string }[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/province/" + (regione ? regione : ""),
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function getProvincia(id: string): Promise<{ _id: string, regione: string, name: string, tabella1?: string[][], tabella2?: string[][], tabella3?: string[][], tabella4?: string[][] }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/provincia/" + id,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}


export function deleteProvince(_id: string): Promise<{ name: string, _id: string }[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/deleteprovince",
            data: { _id },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}


export function changeProvince(args: { _id: string, name: string }): Promise<{ name: string, _id: string }[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/regioni/changeprovince/",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}


export function setContatto(args: Contatto): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/contatti/setcontatto",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}


export function getContatti(): Promise<Contatto[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/contatti/getcontatto",
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}


export function createUser(args: Utente): Promise<Utente> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/login/createuser",
            data: { ...args, password: md5(args.password) },
            success: data => {
                resolve(data)
            }
        })
    })
}

export function changeUser(args: Partial<Utente>): Promise<Utente> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/login/changeuser",
            data: args,
            success: data => {
                resolve(data)
            }
        })
    })
}