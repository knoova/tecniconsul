export interface Utils { title: string, slug: string, body: string, _id?: string }


export function getUtils(): Promise<Utils[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/utils/utils",
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function setUtils(args: Utils): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/utils/setutils",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function changeUtils(args: Utils): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/utils/changeutils",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function deleteUtils(_id: string): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/utils/deleteutils",
            data: { _id },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}