export interface Contatto { title: string, slug: string, body: string, _id?: string }


export function getContatto(): Promise<Contatto[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/contatto/contatto",
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function setContatto(args: Contatto): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/contatto/setcontatto",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function changeContatto(args: Contatto): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/contatto/changecontatto",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function deleteContatto(id: string): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/contatto/deletecontatto",
            data: { id },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}