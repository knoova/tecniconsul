interface Contatto { title: string, slug: string, body: string, _id?: string }
interface Faq { _id?: string, category: string, title: string, description: string, subtitle: string, logo: string, part: string }
interface LandingPages { slug: string, body: string, _id?: string }
interface News { title: string, subtitle: string, body: string, date: string, banner?: string, _id?: string }
interface Utente {
    _id?: string,
    privato: boolean,
    azienda: boolean,
    nome?: string,
    cognome?: string,
    denominazione?: string,
    cf: string,
    piva?: string,
    provincia: string,
    indirizzo: string,
    telefono?: string,
    acconsente?: boolean,
    email: string,
    password: string,
    codcliente: string,
    isAdmin?: boolean
}
interface Utils { title: string, slug: string, body: string, _id?: string }


export function putImage(url: string): Promise<{ ok: boolean, filename: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/upload/migrate",
            data: { url },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function getUtils(): Promise<Utils[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/utils/utils",
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function getUtenti(): Promise<Utente[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/utente/getutente",
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function getRegioni(): Promise<{ name: string, _id: string }[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/regioni/regioni",
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function getUffici(): Promise<{ _id: string, city: string, address: string, phone: string, hours: string }[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/office/getufficio",
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function getProvince(): Promise<{ regione: string, name: string, _id: string }[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/regioni/province/",
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}
export function getNews(): Promise<News[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/news/news",
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}
export function getAllLanding(): Promise<LandingPages[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/landing/landing",
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function getCabine(): Promise<{ regione: string, provincia: string, comune: string, name: string, _id: string }[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/cabine",
            data: {},
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function getComuni(): Promise<{ regione: string, provincia: string, name: string, _id: string }[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/comuni/comuni",
            data: {},
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}


export function getContatto(): Promise<Contatto[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/contatto/contatto",
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}


export function getFAQ(): Promise<Faq[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "http://tecniconsul.test.pinkpig.it/api/v1/faq/faqs",
            data: {},
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

