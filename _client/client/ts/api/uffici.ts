
export function getUffici(): Promise<{ _id: string, city: string, address: string, phone: string, hours: string }[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/office/getufficio",
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function setUfficio(data: { city: string, address: string, phone: string, hours: string }): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/office/setufficio",
            data,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function changeUfficio(data: { _id: string, city: string, address: string, phone: string, hours: string }): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/office/changeufficio",
            data,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function deleteUfficio(id: string): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/office/deleteufficio",
            data: { id },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}