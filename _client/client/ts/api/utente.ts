import * as md5 from "md5"

export interface Utente {
    _id?: string,
    privato: boolean,
    azienda: boolean,
    nome?: string,
    cognome?: string,
    denominazione?: string,
    cf: string,
    piva?: string,
    provincia: string,
    indirizzo: string,
    telefono?: string,
    acconsente?: boolean,
    email: string,
    password: string,
    codcliente: string,
    isAdmin?: boolean
}


export function login(args: { user: string, password: string }): Promise<Utente> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/login/login",
            data: { user: args.user, password: md5(args.password) },
            success: data => {
                resolve(data)
            }
        })
    })
}


export function logout(): Promise<Utente> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/login/logout",
            success: data => {
                resolve(data)
            }
        })
    })
}


(window as any).doLogout = logout

export function getUtenti(args?: Partial<Utente>): Promise<Utente[]> {
    return new Promise((resolve, reject) => {
        if (args)
            $.ajax({
                type: "POST",
                url: "/api/v1/utente/getutente",
                data: args,
                success: data => resolve(data),
                error: err => reject(err)
            })
        else
            $.ajax({
                type: "POST",
                url: "/api/v1/utente/getutente",
                success: data => resolve(data),
                error: err => reject(err)
            })
    })
}

export function setUtente(args: Utente): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/utente/setutente",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function changeUtente(args: Partial<Utente>): Promise<{ id: string }> {
    if (args.password)
        args.password = md5(args.password)
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/utente/changeutente",
            data: { ...args },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function deleteUtente(id: string): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/utente/deleteutente",
            data: { id },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}