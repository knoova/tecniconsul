
export function setComune(args: { regione: string, provincia: string, name: string }): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/setcomune",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}


export function getComuni(args: { regione?: string, provincia?: string }): Promise<{ name: string, _id: string }[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/comuni",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function getComune(id: string): Promise<{ _id: string, regione: string, provincia: string, name: string, cabine: { tabella1?: string[][], tabella2?: string[][], tabella3?: string[][], tabella4?: string[][] }[] }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/comune/" + id,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}


export function deleteComuni(_id: string): Promise<{ name: string, _id: string }[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/deletecomune",
            data: { _id },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}


export function changeComune(args: { _id: string, name: string, regione: string, provincia: string }): Promise<{ name: string, _id: string }[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/changecomuni/",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}


export function setCabina(args: { regione: string, provincia: string, comune: string, name: string }): Promise<{ id: string }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/setcabina",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}


export function deleteCabina(_id: string): Promise<{ name: string, _id: string }[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/deletecabina",
            data: { _id },
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}


export function changeCabina(args: { _id: string, name: string, regione: string, provincia: string, comune: string }): Promise<{ _id: string, name: string, regione: string, provincia: string, comune: string }[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/changecabina/",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}


export function getCabine(args: { regione?: string, provincia?: string, comune?: string }): Promise<{ name: string, _id: string }[]> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/cabine",
            data: args,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}

export function getCabina(id: string): Promise<{ _id: string, regione: string, provincia: string, comune: string, name: string, tabella1?: string[][], tabella2?: string[][], tabella3?: string[][], tabella4?: string[][] }> {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "/api/v1/comuni/cabina/" + id,
            success: data => resolve(data),
            error: err => reject(err)
        })
    })
}