import * as tags from "../../tags"
import * as models from "../../models/_lib"
import * as core from "./_core"

export function combo<T>(args: core.ComboInput, visualName: keyof T, valueName: keyof T, values: T[]): core.Input<string> {
    const wrapper = tags.div().append(tags.label().text(args.title))
    const comboBox = tags.select()
    const model = models.createModel<models.DataState<string>>(args.nullable ? { state: "valid", value: null } : { state: "invalid" })
    const trueCombo = <HTMLSelectElement>comboBox[0]
    for (const value of values) {
        comboBox.append(tags.option().attr({ value: value[valueName] }).html(value[visualName] as any as string))
    }

    comboBox.change((e) => {
        const v = trueCombo.value;
        model.write({ state: "valid", value: v as any as string })
    });

    function set(data: string) {
        model.write({ state: "valid", value: data as any as string })
        comboBox.val(data)
    }

    wrapper.append(comboBox)

    return { model, el: wrapper, set }
}