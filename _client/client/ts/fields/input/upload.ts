import * as tags from "../../tags"
import * as models from "../../models/_lib"
import * as core from "./_core"
import * as api from "../../api/_lib"

export function upload(args: core.UploadInput): core.Input<string> {
    const suffix = Math.floor(Math.random() * 1000000).toString()
    const label = tags.label().prop({ for: "buttonUpload" + suffix }).addClass("yca-custom-file-upload")
    const innerLabel = tags.span().text(args.title)
    const button = tags.ifile().attr({ id: "buttonUpload" + suffix })
    const model = models.createModel<models.DataState<string>>(args.nullable ? { state: "valid", value: null } : { state: "invalid" })

    if (args.files && args.files.length)
        button.attr({ accept: args.files.join(",") })
    const preview = tags.img()
    const wrapper = tags.div().append(label.append(innerLabel, preview), button)
    button.change(() => {
        var reader = new FileReader();
        const file = (<HTMLInputElement>button[0]).files[0]

        const name = file.name


        reader.onloadend = e => {
            innerLabel.text("Uploading")
        }

        function imageIsLoaded(e: any) {
            preview.attr({ src: e.target.result })
            api.upload.putImage({ image: e.target.result, name }).then(val => {
                //preview.attr({ src: val.file })
                innerLabel.text("Uploaded")
                model.write({ state: "valid", value: val.url })
            })
        };

        if (file) {
            reader.onload = imageIsLoaded
            reader.readAsDataURL(file)
            console.log(file)
        } else {
            preview.attr({ src: reader.result })
        }
    })

    function set(url: string) {
        model.write({ state: "valid", value: url })
    }

    model.watch(data => {
        if (data.state == "valid")
            preview.attr({ src: data.value })
    })

    return { model, el: wrapper, set }
}



export function uploadCSV(args: core.UploadInput, idCabina: string, type: string): core.Input<string> {
    const suffix = Math.floor(Math.random() * 1000000).toString()
    const label = tags.label().prop({ for: "buttonUpload" + suffix }).addClass("yca-custom-file-upload")
    const innerLabel = tags.span().text(args.title)
    const button = tags.ifile().attr({ id: "buttonUpload" + suffix })
    const model = models.createModel<models.DataState<string>>(args.nullable ? { state: "valid", value: null } : { state: "invalid" })

    if (args.files && args.files.length)
        button.attr({ accept: args.files.join(",") })
    const preview = tags.img()
    const wrapper = tags.div().append(label.append(innerLabel, preview), button)
    button.change(() => {
        var reader = new FileReader();
        const file = (<HTMLInputElement>button[0]).files[0]

        const name = file.name




        reader.onloadend = e => {
            innerLabel.text("Uploading")
        }

        function imageIsLoaded(e: any) {
            //preview.attr({ src: e.target.result })
            const csv: string = e.target.result
            api.upload.putCsv({ csv, cabina: idCabina, type: type }).then(val => {
                //preview.attr({ src: val.file })
                innerLabel.text("Uploaded")
                model.write({ state: "valid", value: csv })
            })
        };

        if (file) {
            reader.onload = imageIsLoaded
            reader.readAsText(file)
            console.log("file", file)
        }
    })

    function set(url: string) {
        model.write({ state: "valid", value: url })
    }

    model.watch(data => {
        if (data.state == "valid")
            console.log(type, data.value)
    })

    return { model, el: wrapper, set }
}




export function uploadXLS(args: core.UploadInput, idCabina: string): core.Input<Blob> {
    const suffix = Math.floor(Math.random() * 1000000).toString()
    const label = tags.label().prop({ for: "buttonUpload" + suffix }).addClass("yca-custom-file-upload")
    const innerLabel = tags.span().text(args.title)
    const button = tags.ifile().attr({ id: "buttonUpload" + suffix })
    const model = models.createModel<models.DataState<Blob>>(args.nullable ? { state: "valid", value: null } : { state: "invalid" })

    if (args.files && args.files.length)
        button.attr({ accept: args.files.join(",") })
    const preview = tags.img()
    const wrapper = tags.div().append(label.append(innerLabel, preview), button)
    button.change(() => {
        var reader = new FileReader();
        const file = (<HTMLInputElement>button[0]).files[0]

        const name = file.name




        reader.onloadend = e => {
            innerLabel.text("Uploading")
        }

        function imageIsLoaded(e: any) {
            //preview.attr({ src: e.target.result })
            const xls: Blob = e.target.result
            api.upload.putXls({ xls, name: idCabina }).then(val => {
                //preview.attr({ src: val.file })
                innerLabel.text("Uploaded")
                model.write({ state: "valid", value: xls })
            })
        };

        if (file) {
            reader.onload = imageIsLoaded
            reader.readAsText(file)
            console.log("file", file)
        }
    })

    function set(url: Blob) {
        model.write({ state: "valid", value: url })
    }

    model.watch(data => {
        if (data.state == "valid")
            console.log(data.value)
    })

    return { model, el: wrapper, set }
}