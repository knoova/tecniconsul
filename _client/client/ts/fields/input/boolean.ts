import * as tags from "../../tags"
import * as models from "../../models/_lib"
import * as core from "./_core"

export function booleanInput(args: core.InputArgs<boolean>): core.Input<boolean> {
    const title = tags.label().text(args.title)
    const input = tags.icheckbox()
    title.append(input, tags.span())

    const model = models.createModel<models.DataState<boolean>>({ state: "valid", value: false })

    input.on("click", () => {
        model.write({ state: "valid", value: !!<boolean>input.prop('checked') })
    })

    function set(value: boolean) {
        model.write({ state: "valid", value })
    }

    model.watch(data => {
        if (data.state == "valid") {
            if (data.value)
                input.prop("checked")
            else
                input.removeProp("checked")
        }
    })

    return { model, el: title, set }
}