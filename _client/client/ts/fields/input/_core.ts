import * as models from "../../models/_lib"

export type InputType = "string" | "password" | "mail" | "boolean" | "radio" | "upload" | "uploadImage" | "uploadPdf" | "number" | "combo"


export interface InputArgs<T> {
    type?: InputType
    title: string
    description?: string
    nullable?: boolean
}


export interface StringInput extends InputArgs<string> {
    multiline?: number
    password?: boolean
}

export interface NumberInput extends InputArgs<string> {
    max?: number
    min?: number
}


export interface DateInput extends InputArgs<string> {
    max?: number
    min?: number
}

export interface UploadInput extends InputArgs<string[]> {
    files?: string[]
}

export interface RadioInput extends InputArgs<boolean> {
    name: string
}

export interface ComboInput extends InputArgs<boolean> {
    
}

export interface Input<T> {
    model: models.Model<models.DataState<T>>,
    el: JQuery
    set: (value: T) => void
}

export type ObjInput<T extends Object> = {
    [K in keyof T]: InputArgs<T[K]>
}

