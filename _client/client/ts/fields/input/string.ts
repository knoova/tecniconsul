import * as tags from "../../tags"
import * as models from "../../models/_lib"
import * as core from "./_core"

export function stringInput(args: core.StringInput): core.Input<string> {
    const title = tags.label().text(args.title)
    const input = args.multiline ? tags.itextarea().attr({ rows: args.multiline }) : (args.password ? tags.ipassword() : tags.itext())
    title.append(input)

    const model = models.createModel<models.DataState<string>>(args.nullable ? { state: "valid", value: null } : { state: "invalid" })

    input.on("change input", () => {
        model.write({ state: "valid", value: <string>input.val() })
    })

    function set(val: string) {
        model.write({ state: "valid", value: val })
    }

    model.watch(val => {
        if (val.state == "valid")
            input.val(val.value)
    })

    return { model, el: title, set }
}

export function passwordInput(args: core.StringInput) {
    return stringInput({ ...args, password: true })
}

export function multilineInput(args: core.StringInput) {
    return stringInput({ ...args, multiline: 20 })
}
